package com.domosnap.engine.adapter.i2c;

import java.io.IOException;

/*
 * #%L
 * DomoSnap i2c Adapter
 * %%
 * Copyright (C) 2017 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.core.AbstractControllerAdapter;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.ConnectionListener;
import com.domosnap.engine.adapter.core.ConnectionStatusEnum;
import com.domosnap.engine.adapter.core.ScanListener;
import com.domosnap.engine.adapter.core.UnknownControllerListener;
import com.domosnap.engine.adapter.core.UnknownWhoException;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.humidity.HumiditySensor;
import com.domosnap.engine.controller.light.LightSensor;
import com.domosnap.engine.controller.nfc.Nfc;
import com.domosnap.engine.controller.pressure.PressureSensor;
import com.domosnap.engine.controller.temperature.TemperatureSensor;
import com.domosnap.engine.controller.where.URITools;
import com.domosnap.engine.controller.where.Where;
import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.processors.FlowableProcessor;
import io.reactivex.processors.PublishProcessor;

public class I2CControllerAdapter extends AbstractControllerAdapter {

	public static String SCHEMA = "i2c";
	
	protected final static String SCHEMA_BME280 = "bme280";
	protected final static int ADRESS_BME280 = 0x77;
	protected final static String SCHEMA_TSL2561 = "tsl2561";
	protected final static int ADRESS_TSL2561 = 0x39;
	protected final static String SCHEMA_PN532 = "pn532";
	protected final static int ADRESS_PN532 = 0x24;
	protected final static String SCHEMA_VEML6070 = "veml6070";
	protected final static int ADRESS_VEML6070 = 0x38;
	protected final static String SCHEMA_INA219 = "ina219";
	protected final static int ADRESS_INA219 = 0x40;
	
	private Log log = new Log(I2CControllerAdapter.class.getSimpleName());

	private I2CMonitorPublisher monitor;
	private I2CBus i2cBus;
	private List<ConnectionListener> connectionCommanderListenerList = Collections
			.synchronizedList(new ArrayList<ConnectionListener>());
	private List<ConnectionListener> connectionMonitorListenerList = Collections
			.synchronizedList(new ArrayList<ConnectionListener>());

	@Override
	protected Flowable<Command> createMonitorStream() {
		this.monitor = new I2CMonitorPublisher();
		return FlowableProcessor.create(this.monitor, BackpressureStrategy.DROP).share();
	}
	

	@Override
	protected PublishProcessor<Command> createCommandStream() {
		PublishProcessor<Command> commanderStream = PublishProcessor.create();
		commanderStream.subscribe(new I2CCommanderConsumer());	
		return commanderStream;
	}

	@Override
	protected void onCreatedController(Controller c) throws Exception {
		String path = c.getWhere().getPath();
		String adress;
		String type;
		
		if (path.indexOf("/") == -1) {
			try {
				type = findTypeByAdress(Integer.parseInt(path));
				adress = path;
			} catch (NumberFormatException e) {
				type = path;
				adress = String.valueOf(findAdressByType(type));
			}
			c.setWhere(new Where(formatURI(null) + "/" + type + "/" + c.getWhere().getPath()));
		} else {
			type = path.substring(0, path.indexOf("/"));
			adress = path.substring(path.indexOf("/") + 1);
		}
		
		switch (type) {
			case SCHEMA_BME280:
				monitor.addBME280(Integer.decode("0x" + adress));
				break;
			case SCHEMA_TSL2561:
				monitor.addTSL2561(Integer.decode("0x" + adress));
				break;
			case SCHEMA_VEML6070:
				monitor.addVEML6070(Integer.decode("0x" + adress));
				break;
			case SCHEMA_PN532:
				monitor.addPN532(Integer.decode("0x" + adress));
				break;
			case SCHEMA_INA219:
				monitor.addINA219(Integer.decode("0x" + adress));
				break;
			default:
				break;
		}
	}
	
	private String findTypeByAdress(int adress) throws UnknownWhoException {
		switch (adress) {
			case ADRESS_BME280:
				return SCHEMA_BME280;
			case ADRESS_TSL2561:
				return SCHEMA_TSL2561;
			case ADRESS_VEML6070: 
				return SCHEMA_VEML6070;
			case ADRESS_PN532:
				return SCHEMA_PN532;
			case ADRESS_INA219:
				return SCHEMA_INA219;
			default:
				throw new UnknownWhoException();
		}
	}
	
	private int findAdressByType(String type) throws UnknownWhoException {
		switch (type) {
			case SCHEMA_BME280:
				return ADRESS_BME280;
			case SCHEMA_TSL2561:
				return ADRESS_TSL2561;
			case SCHEMA_VEML6070:
				return ADRESS_VEML6070;
			case SCHEMA_PN532:
				return ADRESS_PN532;
			case SCHEMA_INA219:
				return ADRESS_INA219;
			default:
				throw new UnknownWhoException();
		}
	}
	
	@Override
	public void connect(String url) {
		connect();
	}
	
	@Override
	public void disconnect() {
		monitor.disconnect();
		
		try {
			i2cBus.close();
		} catch (IOException e1) {
			log.info(Session.Command,
					"Close bus raise an error [" + e1.getMessage() + "]", 1);
		}
		i2cBus = null;
		for (ConnectionListener connectionListener : connectionCommanderListenerList) {
			try {
				connectionListener.onClose();
			} catch (Exception e) {
				log.severe(Session.Command,
						"ConnectionListener raise an error [" + e.getMessage() + "]", 1);
			}
		}
		for (ConnectionListener connectionListener : connectionMonitorListenerList) {
			try {
				connectionListener.onClose();
			} catch (Exception e) {
				log.severe(Session.Command,
						"ConnectionListener raise an error [" + e.getMessage() + "]", 1);
			}
		}
	}

	@Override
	public boolean isConnected() {
		return i2cBus == null ? false : true;
	}

	@Override
	public void addCommanderConnectionListener(ConnectionListener listener) {
		if (!connectionCommanderListenerList.contains(listener)) {
			connectionCommanderListenerList.add(listener);
		}
	}

	@Override
	public void removeCommanderConnectionListener(ConnectionListener listener) {
		connectionCommanderListenerList.remove(listener);
	}
	
	@Override
	public void addMonitorConnectionListener(ConnectionListener listener) {
		if (!connectionMonitorListenerList.contains(listener)) {
			connectionMonitorListenerList.add(listener);
		}
	}

	@Override
	public void removeMonitorConnectionListener(ConnectionListener listener) {
		connectionMonitorListenerList.remove(listener);
	}

	@Override
	public void addUnknowControllerListener(UnknownControllerListener arg0) {
//		monitor.addUnknownControllerListener(arg0);
	}
	
	@Override
	public void removeUnknowControllerListener(UnknownControllerListener arg0) {
//		TODO this.getHueMonitor().removeUnknownControllerListener(arg0);
	}
	
	@Override
	public void connect() {
		if (!isConnected()) {
			try {
				monitor.connect();
				i2cBus = I2CFactory.getInstance(I2CBus.BUS_1); // TODO Depends onthe RasPI version
				for (ConnectionListener connectionListener : connectionCommanderListenerList) {
					connectionListener.onConnect(ConnectionStatusEnum.Connected);
				}

				for (ConnectionListener connectionListener : connectionMonitorListenerList) {
					connectionListener.onConnect(ConnectionStatusEnum.Connected);
				}
			
			} catch (Exception e) {
				log.severe(Session.Command, e.getMessage());
				for (ConnectionListener connectionListener : connectionCommanderListenerList) {
					connectionListener.onConnect(ConnectionStatusEnum.UnkownError);
				}
				for (ConnectionListener connectionListener : connectionMonitorListenerList) {
					connectionListener.onConnect(ConnectionStatusEnum.UnkownError);
				}
			} 
		}
	}

	@Override
	public void scan(ScanListener listener) {

		try {
			i2cBus = I2CFactory.getInstance(I2CBus.BUS_1);
		    log.finest(Session.Device, "Connected to bus OK!!!");
			
			listener.progess(15);
	
			
			// BME280
			try { 
			    i2cBus.getDevice(ADRESS_BME280).read();
				
				Where where = new Where(SCHEMA_BME280 + "/" + Integer.toHexString(ADRESS_BME280));
				HumiditySensor hs = createController(HumiditySensor.class, where);
				if (hs != null) {
					listener.foundController(hs);
				}
				PressureSensor ps = createController(PressureSensor.class, where);
				if (ps != null) {
					listener.foundController(ps);
				}
				TemperatureSensor ts = createController(TemperatureSensor.class, where);
				if (ts != null) {
					listener.foundController(ts);
				} 
			} catch (Exception e) {
				log.finest(Session.Device, "No BME found at " + Integer.toHexString(ADRESS_BME280) + ".");
			}
	
			try { 
				i2cBus.getDevice(0x76).read();
				Where where = new Where(SCHEMA_BME280 + "/76"); // sometime on this address...
		
				HumiditySensor hs = createController(HumiditySensor.class, where);
				if (hs != null) {
					listener.foundController(hs);
				}
				PressureSensor ps = createController(PressureSensor.class, where);
				if (ps != null) {
					listener.foundController(ps);
				}
				TemperatureSensor ts = createController(TemperatureSensor.class, where);
				if (ts != null) {
					listener.foundController(ts);
				}
			} catch (Exception e) {
				log.finest(Session.Device, "No BME found at 76.");
			}
			
			listener.progess(30); 		
			
			//  TSL2561
			try {
				i2cBus.getDevice(ADRESS_TSL2561).read();
				Where where = new Where(SCHEMA_TSL2561 + "/" + Integer.toHexString(ADRESS_TSL2561));
				LightSensor ls = createController(LightSensor.class, where);
				if (ls != null) {
					listener.foundController(ls);
				}
			} catch (Exception e) {
				log.finest(Session.Device, "No TSL2561 found at " + Integer.toHexString(ADRESS_TSL2561) + ".");
			} 
			listener.progess(60);
			
			// PN532
			try {
				I2CDevice dev = i2cBus.getDevice(ADRESS_PN532);
				Thread.sleep(500);
				dev.read();
				Where where = new Where(SCHEMA_PN532 + "/" + Integer.toHexString(ADRESS_PN532)); 
				Nfc nfs = createController(Nfc.class, where);
				if (nfs != null) {
					listener.foundController(nfs);
				}
			} catch (Exception e) {
				log.finest(Session.Device, "No PN532found at " + Integer.toHexString(ADRESS_PN532) + ".");
			}
		} catch (Exception e) {
			log.finest(Session.Device, "No Bus found.");
		}
		listener.progess(100);
		
		listener.scanFinished();
		
	}
	
	// TODO override => put in parent
	public List<Class<? extends Controller>> getSupportedDevice() {
		List<Class<? extends Controller>> result = new ArrayList<Class<? extends Controller>>();
		result.add(HumiditySensor.class);
		result.add(PressureSensor.class);
		result.add(TemperatureSensor.class);
		result.add(LightSensor.class);
		result.add(Nfc.class);
//		result.add(Counter TODO;
		return result;
	}

	@Override
	protected String formatURI(Where where) {
		// i2c://localhost/bme280/76
		String path = "";
		if (where != null) {
			path = where.getPath();
		}
		
		return URITools.generateURI(SCHEMA, null , "localhost", null, path);
	}

	@Override
	public String getSchema() {
		return SCHEMA;
	}
}
