package com.domosnap.engine.adapter.i2c.veml6070;

/*
 * #%L
 * DomoSnap i2c Adapter
 * %%
 * Copyright (C) 2017 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.IOException;

import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;

public class VEML6070 {

	// This next addresses is returned by "sudo i2cdetect -y 1", see above.
	public final static int VEML6070_I2CADDR_READ = 0x38;
	
	private static boolean verbose = true;

	private I2CBus bus;
	private I2CDevice veml6070;

	public VEML6070() throws I2CFactory.UnsupportedBusNumberException {
		this(VEML6070_I2CADDR_READ);
	}

	public VEML6070(int address) throws I2CFactory.UnsupportedBusNumberException {
		try {
			// Get i2c bus
			bus = I2CFactory.getInstance(I2CBus.BUS_1); // Depends onthe RasPI version
			if (verbose)
				System.out.println("Connected to bus. OK.");

			// Get device itself
			veml6070 = bus.getDevice(address);
			if (verbose)
				System.out.println("Connected to device. OK.");

			// Select command register
			// Integration time = 0.5T, shutdown mode disabled
			veml6070.write(((byte)0x02));
			Thread.sleep(500);
			
		} catch (IOException | InterruptedException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException(e);
		}
	}

	public int readUv() {
		try {
			// Read 2 bytes of data
			// uvlight msb, uvlight lsb
			byte[] data = new byte[2];
			data[0] = (byte)veml6070.read(0x73);
			data[1] = (byte)veml6070.read(0x71);
			// Convert the data
			int uvlight = (((int)(data[0] & 0xFF)) * 256) + (data[1] & 0xFF);
			return uvlight;
		}catch (Exception e) {
			System.err.println(e.getMessage());
			return 0;
		}
	}

}
