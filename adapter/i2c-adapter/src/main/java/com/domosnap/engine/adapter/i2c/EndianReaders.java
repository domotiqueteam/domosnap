package com.domosnap.engine.adapter.i2c;

import java.io.IOException;

/*
 * #%L
 * DomoSnap i2c Adapter
 * %%
 * Copyright (C) 2017 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.pi4j.io.i2c.I2CDevice;

public class EndianReaders {

	public enum Endianness {
		LITTLE_ENDIAN,
		BIG_ENDIAN
	}

	/**
	 * Read an unsigned byte from the I2C device
	 */
	public static int readU8(I2CDevice device, int i2caddr, int reg, boolean verbose) throws IOException {
		int result = 0;

		result = device.read(reg);
		if (verbose) {
			System.out.println("I2C: Device " + i2caddr + " (0x" + Integer.toHexString(i2caddr) +
					") returned " + result + " (0x" + Integer.toHexString(result) +
					") from reg " + reg + " (0x" + Integer.toHexString(reg) + ")");
		}

		return result; // & 0xFF;
	}

	/**
	 * Read a signed byte from the I2C device
	 */
	public static int readS8(I2CDevice device, int i2caddr, int reg, boolean verbose) throws IOException {
		int result = 0;

		result = device.read(reg); // & 0x7F;
		if (result > 127)
			result -= 256;
		if (verbose) {
			System.out.println("I2C: Device " + i2caddr + " returned " + result + " from reg " + reg);
		}
		return result; // & 0xFF;
	}

	public static int readU16LE(I2CDevice device, int i2caddr, int register, boolean verbose) throws IOException {
		return EndianReaders.readU16(device, i2caddr, register, Endianness.LITTLE_ENDIAN, verbose);
	}

	public static int readU16BE(I2CDevice device, int i2caddr, int register, boolean verbose) throws IOException {
		return EndianReaders.readU16(device, i2caddr, register, Endianness.BIG_ENDIAN, verbose);
	}

	public static int readU16(I2CDevice device, int i2caddr, int register, Endianness endianness, boolean verbose) throws IOException {
		int hi = EndianReaders.readU8(device, i2caddr, register, verbose);
		int lo = EndianReaders.readU8(device, i2caddr, register + 1, verbose);
		return ((endianness == Endianness.BIG_ENDIAN) ? (hi << 8) + lo : (lo << 8) + hi); // & 0xFFFF;
	}

	public static int readS16(I2CDevice device, int i2caddr, int register, Endianness endianness, boolean verbose) throws IOException {
		int hi = 0, lo = 0;
		if (endianness == Endianness.BIG_ENDIAN) {
			hi = EndianReaders.readS8(device, i2caddr, register, verbose);
			lo = EndianReaders.readU8(device, i2caddr, register + 1, verbose);
		} else {
			lo = EndianReaders.readU8(device, i2caddr, register, verbose);
			hi = EndianReaders.readS8(device, i2caddr, register + 1, verbose);
		}
		return ((hi << 8) + lo); // & 0xFFFF;
	}

	public static int readS16LE(I2CDevice device, int i2caddr, int register, boolean verbose) throws IOException {
		return EndianReaders.readS16(device, i2caddr, register, Endianness.LITTLE_ENDIAN, verbose);
	}

	public static int readS16BE(I2CDevice device, int i2caddr, int register, boolean verbose) throws IOException {
		return EndianReaders.readS16(device, i2caddr, register, Endianness.BIG_ENDIAN, verbose);
	}
}
