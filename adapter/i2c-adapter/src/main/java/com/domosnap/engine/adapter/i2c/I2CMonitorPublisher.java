package com.domosnap.engine.adapter.i2c;

import java.io.IOException;

/*
 * #%L
 * DomoSnap i2c Adapter
 * %%
 * Copyright (C) 2017 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.Command.Type;
import com.domosnap.engine.adapter.i2c.bme280.BME280;
import com.domosnap.engine.adapter.i2c.ina219.INA219;
import com.domosnap.engine.adapter.i2c.ina219.INA219.Adc;
import com.domosnap.engine.adapter.i2c.ina219.INA219.Address;
import com.domosnap.engine.adapter.i2c.ina219.INA219.Brng;
import com.domosnap.engine.adapter.i2c.ina219.INA219.Pga;
import com.domosnap.engine.adapter.i2c.pn532.IPN532Interface;
import com.domosnap.engine.adapter.i2c.pn532.PN532;
import com.domosnap.engine.adapter.i2c.pn532.PN532I2C;
import com.domosnap.engine.adapter.i2c.tsl2561.TSL2561;
import com.domosnap.engine.adapter.i2c.veml6070.VEML6070;
import com.domosnap.engine.controller.counter.CounterStateName;
import com.domosnap.engine.controller.counter.PowerManagement;
import com.domosnap.engine.controller.humidity.HumiditySensor;
import com.domosnap.engine.controller.humidity.HumiditySensorStateName;
import com.domosnap.engine.controller.light.LightSensor;
import com.domosnap.engine.controller.light.LightSensorStateName;
import com.domosnap.engine.controller.nfc.Nfc;
import com.domosnap.engine.controller.nfc.NfcStateName;
import com.domosnap.engine.controller.pressure.PressureSensor;
import com.domosnap.engine.controller.pressure.PressureSensorStateName;
import com.domosnap.engine.controller.temperature.TemperatureSensor;
import com.domosnap.engine.controller.temperature.TemperatureSensorStateName;
import com.domosnap.engine.controller.uv.UvSensor;
import com.domosnap.engine.controller.uv.UvSensorStateName;
import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.controller.what.impl.DoubleState;
import com.domosnap.engine.controller.what.impl.IntegerState;
import com.domosnap.engine.controller.what.impl.PercentageState;
import com.domosnap.engine.controller.what.impl.PressureUnitState;
import com.domosnap.engine.controller.what.impl.StringState;
import com.domosnap.engine.controller.what.impl.TemperatureUnitState;
import com.domosnap.engine.controller.where.URITools;
import com.domosnap.engine.controller.where.Where;
import com.pi4j.io.i2c.I2CFactory.UnsupportedBusNumberException;

import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;

public class I2CMonitorPublisher implements FlowableOnSubscribe<Command> {

	private List<FlowableEmitter<Command>> suscriberList = Collections
			.synchronizedList(new ArrayList<FlowableEmitter<Command>>());

	private Map<String, BME280> bmeList = new HashMap<>();
	private Map<String, TSL2561> tslList = new HashMap<>();
	private Map<String, VEML6070> vemlList = new HashMap<>();
	private Map<String, INA219> inaList = new HashMap<>();
	private Map<String, PN532> pnList = new HashMap<>();
	protected ExecutorService pool = null;
	protected boolean connected = true;
	protected final Log log = new Log(I2CMonitorPublisher.class.getSimpleName());
	
	protected int REFRESH_FREQUENCY = 60000;
	protected int PN532_REFRESH_FREQUENCY = 500;
	
	private class MonitorHandler implements Runnable {

		static final byte PN532_MIFARE_ISO14443A = 0x00;
		
		@Override
		public void run() {
			while (connected) {
				
				for (String adress : bmeList.keySet()) {
					BME280 bme = bmeList.get(adress);
					
					try {
						adress = I2CControllerAdapter.SCHEMA_BME280 + "/" + adress;
						for (FlowableEmitter<Command> suscriber : suscriberList) {
							
							// Temperature
							Command command = new Command(TemperatureSensor.class, new What(TemperatureSensorStateName.value.name(), new DoubleState(bme.readTemperature())),
										new Where(URITools.generateURI(I2CControllerAdapter.SCHEMA, null , "localhost", null, adress)), Type.COMMAND, null);
							suscriber.onNext(command);
							
							// Temperature unit
							command = new Command(TemperatureSensor.class, new What(TemperatureSensorStateName.unit.name(), TemperatureUnitState.Celsius()),
									new Where(URITools.generateURI(I2CControllerAdapter.SCHEMA, null , "localhost", null, adress)), Type.COMMAND, null);
							suscriber.onNext(command);
							
							// Humidity
							command = new Command(HumiditySensor.class, new What(HumiditySensorStateName.value.name(), new PercentageState((double)bme.readHumidity())),
									new Where(URITools.generateURI(I2CControllerAdapter.SCHEMA, null , "localhost", null, adress)), Type.COMMAND, null);
							suscriber.onNext(command);
							
							// Pressure unit
							command = new Command(PressureSensor.class, new What(PressureSensorStateName.unit.name(), PressureUnitState.Pascal()),
									new Where(URITools.generateURI(I2CControllerAdapter.SCHEMA, null , "localhost", null, adress)), Type.COMMAND, null);
							suscriber.onNext(command);
							
							// Pressure
							command = new Command(PressureSensor.class, new What(PressureSensorStateName.value.name(), new DoubleState(bme.readPressure())),
									new Where(URITools.generateURI(I2CControllerAdapter.SCHEMA, null , "localhost", null, adress)), Type.COMMAND, null);
							suscriber.onNext(command);
						
						}
						
					} catch (Exception e) {
						log.severe(Session.Monitor,
										"Exception occurs with message [" + e.getMessage() + "]. Message dropped. ", 1);
					}
				};
				for (String adress : tslList.keySet()) {
					TSL2561 tsl = tslList.get(adress);
					adress = I2CControllerAdapter.SCHEMA_TSL2561 + "/" + adress;
					try {
						for (FlowableEmitter<Command> suscriber : suscriberList) {
						
							// Lux
							Command command = new Command(LightSensor.class, new What(LightSensorStateName.lux.name(), new DoubleState(tsl.readLux())),
										new Where(URITools.generateURI(I2CControllerAdapter.SCHEMA, null , "localhost", null, adress)), Type.COMMAND, null);
							suscriber.onNext(command);						
						}
						
					} catch (Exception e) {
						log.severe(Session.Monitor,
										"Exception occurs with message [" + e.getMessage() + "]. Message dropped. ", 1);
					}
				};
				for (String adress : inaList.keySet()) {
					INA219 ina = inaList.get(adress);
					adress = I2CControllerAdapter.SCHEMA_INA219 + "/" + adress;
					try {
						for (FlowableEmitter<Command> suscriber : suscriberList) {
						
							// Power
							Command command = new Command(PowerManagement.class, new What(CounterStateName.ACTIVE_POWER.name(), new DoubleState(ina.getPower())),
										new Where(URITools.generateURI(I2CControllerAdapter.SCHEMA, null , "localhost", null, adress)), Type.COMMAND, null);
							suscriber.onNext(command);						
						}
						
					} catch (Exception e) {
						log.severe(Session.Monitor,
										"Exception occurs with message [" + e.getMessage() + "]. Message dropped.", 1);
					}
				};
				for (String adress : vemlList.keySet()) {
					VEML6070 veml= vemlList.get(adress);
					adress = I2CControllerAdapter.SCHEMA_VEML6070 + "/" + adress;
					try {
						for (FlowableEmitter<Command> suscriber : suscriberList) {
						
							// Uv
							Command command = new Command(UvSensor.class, new What(UvSensorStateName.uva.name(), new IntegerState(veml.readUv())),
										new Where(URITools.generateURI(I2CControllerAdapter.SCHEMA, null , "localhost", null, adress)), Type.COMMAND, null);
							suscriber.onNext(command);						
						}
						
					} catch (Exception e) {
						log.severe(Session.Monitor,
										"Exception occurs with message [" + e.getMessage() + "]. Message dropped. ", 1);
					}
				};
				int j = 0;
				while (connected && j < (REFRESH_FREQUENCY /PN532_REFRESH_FREQUENCY)) {
					j++;
					for (String adress : pnList.keySet()) {
						PN532 nfc = pnList.get(adress);
						adress = I2CControllerAdapter.SCHEMA_PN532 + "/" + adress;
						try {
							log.finest(Session.Monitor, "Waiting for an ISO14443A Card ...");

							byte[] buffer = new byte[8];
							int readLength = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A,
									buffer);

							if (readLength > 0) {
								log.finest(Session.Monitor, "Found an ISO14443A card");

								log.finest(Session.Monitor, "  UID Length: ");
								log.finest(Session.Monitor, "" + readLength);
								log.finest(Session.Monitor, " bytes");

								log.finest(Session.Monitor, "  UID Value: [");
								String value = "";
								for (int i = 0; i < readLength; i++) {
									value += Integer.toHexString(buffer[i]);
								}
								log.finest(Session.Monitor, value);
								log.finest(Session.Monitor, "]");
								
								for (FlowableEmitter<Command> suscriber : suscriberList) {
									
									// nfc reader value
									Command command = new Command(Nfc.class, new What(NfcStateName.passiveTargetId.name(), new StringState(value)),
												new Where(URITools.generateURI(I2CControllerAdapter.SCHEMA, null , "localhost", null, adress)), Type.COMMAND, null);
									suscriber.onNext(command);
								
								}
							}

						} catch (Exception e) {
							log.severe(Session.Monitor,
											"Exception occurs with message [" + e.getMessage() + "]. Message dropped. ", 1);
						}
					};
					try {
						Thread.sleep(PN532_REFRESH_FREQUENCY); // TODO make it parametrable
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

				}
				
			}
		}
	}
	
	@Override
	public void subscribe(FlowableEmitter<Command> e) throws Exception {
		suscriberList.add(e);
	}

	public boolean connect() {
		connected = true;
		getExecutorService().execute(new MonitorHandler());
		return true;
	}

	public void disconnect() {
		connected = false;
	}

	public void addBME280(int adress) throws UnsupportedBusNumberException {
		bmeList.put(Integer.toHexString(adress), new BME280(adress));
	}

	public void addTSL2561(int adress) throws UnsupportedBusNumberException {
		tslList.put(Integer.toHexString(adress), new TSL2561(adress));
	}
	
	public void addVEML6070(int adress) throws UnsupportedBusNumberException {
		vemlList.put(Integer.toHexString(adress), new VEML6070(adress));
	}
	
	public void addINA219(int adress) throws UnsupportedBusNumberException, IOException {
		inaList.put(Integer.toHexString(adress), new INA219(Address.getAddress(adress), 0, 10, Brng.V16, Pga.GAIN_1, Adc.BITS_10, Adc.SAMPLES_128));
	}
	
	public void addPN532(int adress) throws UnsupportedBusNumberException {
		IPN532Interface pn532Interface = new PN532I2C(adress);
		PN532 nfc = new PN532(pn532Interface);
		
		
		try {
			System.out.println("Starting up...");
			nfc.begin();
			Thread.sleep(1000);

			long versiondata = nfc.getFirmwareVersion();
			if (versiondata == 0) {
				System.out.println("Didn't find PN53x board");
			}
			// Got ok data, print it out!
			System.out.print("Found chip PN5");
			System.out.println(Long.toHexString((versiondata >> 24) & 0xFF));

			System.out.print("Firmware ver. ");
			System.out.print(Long.toHexString((versiondata >> 16) & 0xFF));
			System.out.print('.');
			System.out.println(Long.toHexString((versiondata >> 8) & 0xFF));

			// configure board to read RFID tags
			nfc.SAMConfig();
			pnList.put(Integer.toHexString(adress), nfc);
		} catch (InterruptedException e) {
			log.severe(Session.Monitor, e.getMessage());
		}
	}
	
	protected ExecutorService getExecutorService() {
		if (pool == null) {
			pool = Executors.newFixedThreadPool(2);
		}

		return pool;
	}
}
