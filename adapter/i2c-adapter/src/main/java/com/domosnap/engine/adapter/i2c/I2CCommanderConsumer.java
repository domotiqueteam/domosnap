package com.domosnap.engine.adapter.i2c;

/*
 * #%L
 * DomoSnap i2c Adapter
 * %%
 * Copyright (C) 2017 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.core.Command;

import io.reactivex.functions.Consumer;

public class I2CCommanderConsumer implements Consumer<Command> {

	protected final Log log = new Log(I2CCommanderConsumer.class.getSimpleName());
    
	@Override
	public void accept(Command command) {
		if (command == null) {
			log.severe(Session.Command, "Command unsupported (null).", 1);
			return;
		} else if (command.isWriteCommand()) {
			log.severe(Session.Command, "No action command supported [" +command.toString() + "].", 1);
			return;
		}
	}
}
