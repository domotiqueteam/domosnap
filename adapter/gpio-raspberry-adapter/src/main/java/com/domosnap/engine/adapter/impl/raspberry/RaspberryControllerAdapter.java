package com.domosnap.engine.adapter.impl.raspberry;

/*
 * #%L
 * DomoSnap gpio Raspberry adapter
 * %%
 * Copyright (C) 2016 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import com.domosnap.engine.Log;
import com.domosnap.engine.adapter.core.AbstractControllerAdapter;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.ConnectionListener;
import com.domosnap.engine.adapter.core.ScanListener;
import com.domosnap.engine.adapter.core.UnknownControllerListener;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.temperature.TemperatureSensor;
import com.domosnap.engine.controller.where.URITools;
import com.domosnap.engine.controller.where.Where;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.processors.FlowableProcessor;
import io.reactivex.processors.PublishProcessor;

public class RaspberryControllerAdapter extends AbstractControllerAdapter {

	private Log log = new Log(this.getClass().getSimpleName());

	public static String SCHEMA = "gpio-r";
	
	// gpio-r://localhost:10/1
	
	private RaspberryMonitorPublisher monitorPublisher;
	private RaspberryCommanderConsumer commanderConsumer;
	
	public RaspberryControllerAdapter() {
		super();
	}
				
	@Override
	public void connect() {
		if (!monitorPublisher.isConnected()) {
			monitorPublisher.connect();
		}
		if (!commanderConsumer.isConnected()) {
			commanderConsumer.connect();
		}
	}

	@Override
	public void connect(String url) {
		connect();
	}
	
	@Override
	public void disconnect() {
		monitorPublisher.disconnect();
		commanderConsumer.disconnect();
	}

	@Override
	public boolean isConnected() {
		return monitorPublisher.isConnected() && commanderConsumer.isConnected();
	}

	@Override
	public void addCommanderConnectionListener(ConnectionListener listener) {
		commanderConsumer.addConnectionListener(listener);
	}

	@Override
	public void removeCommanderConnectionListener(ConnectionListener listener) {
		commanderConsumer.removeConnectionListener(listener);
	}
	
	@Override
	public void addMonitorConnectionListener(ConnectionListener listener) {
		monitorPublisher.addConnectionListener(listener);
	}

	@Override
	public void removeMonitorConnectionListener(ConnectionListener listener) {
		monitorPublisher.removeConnectionListener(listener);
	}

	@Override
	public void addUnknowControllerListener(UnknownControllerListener arg0) {
//		monitorPublisher.addUnknownControllerListener(arg0);
	}
	
	@Override
	public void removeUnknowControllerListener(UnknownControllerListener arg0) {
//		monitorPublisher.removeUnknownControllerListener(arg0);
	}

	@Override
	public void scan(ScanListener listener) {

//		W1Master w1Master = new W1Master();
//
//		List<com.pi4j.component.temperature.TemperatureSensor> lis =  w1Master.getDevices(com.pi4j.component.temperature.TemperatureSensor.class);
//		int i = 1;
//		for (com.pi4j.component.temperature.TemperatureSensor device : lis) {
//			listener.progess(100/lis.size()*i); 
//			Where where = new Where(device.getName().trim());
//			TemperatureSensor ts = createController(TemperatureSensor.class, where);
//			if (ts != null) {
//				listener.foundController(Who.TEMPERATURE_SENSOR, where, ts);
//			}
//		}
		
		listener.progess(100);		
		listener.scanFinished();	
	}
	
	@Override
	public List<Class<? extends Controller>> getSupportedDevice() {
		List<Class<? extends Controller>> result = new ArrayList<Class<? extends Controller>>();
		result.add(TemperatureSensor.class);
		return result;
	}
	
	@Override
	protected Flowable<Command> createMonitorStream() {
		monitorPublisher = new RaspberryMonitorPublisher();
		return FlowableProcessor.create(monitorPublisher, BackpressureStrategy.DROP).share();
	}

	@Override
	protected PublishProcessor<Command> createCommandStream() {
		PublishProcessor<Command> commanderStream = PublishProcessor.create();
		commanderStream.subscribe(new RaspberryCommanderConsumer());
		return commanderStream;
	}

	@Override
	protected void onCreatedController(Controller c) {
	}

	@Override
	protected String formatURI(Where where) {
		String path;
		if (where == null) {
			path = "";
		} else {
			path = where.getPath();
		}
		return URITools.generateURI(SCHEMA, null , "localhost", null, path);
	}

	@Override
	public String getSchema() {
		return SCHEMA;
	}
}
