package com.domosnap.engine.adapter.impl.raspberry;

/*
 * #%L
 * DomoSnap gpio Raspberry adapter
 * %%
 * Copyright (C) 2016 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.Command.Type;
import com.domosnap.engine.adapter.core.ConnectionListener;
import com.domosnap.engine.controller.light.Light;
import com.domosnap.engine.controller.light.LightStateName;
import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.controller.what.impl.OnOffState;
import com.domosnap.engine.controller.where.Where;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;

public class RaspberryMonitorPublisher implements FlowableOnSubscribe<Command> {

	private List<FlowableEmitter<Command>> suscriberList = Collections.synchronizedList(new ArrayList<FlowableEmitter<Command>>());
	private GpioPinDigitalInput led;
    private GpioController gpio;
	protected final Log log = new Log(this.getClass().getSimpleName());
	protected final Object lock = new Object();
	protected boolean connected = true;

	public RaspberryMonitorPublisher() {
		connect();
	}

	void onMessageReceipt(GpioPinDigitalStateChangeEvent event) {
		try {
			for (FlowableEmitter<Command> suscriber : suscriberList) {
				Command command;
				if (event.getState().isHigh()) {
					command = new Command(Light.class, new What(LightStateName.status.name(), OnOffState.On()), new Where("GPIO_01"), Type.COMMAND, null);
				} else {
					command = new Command(Light.class, new What(LightStateName.status.name(), OnOffState.Off()), new Where("GPIO_01"), Type.COMMAND, null);
				}
				
				suscriber.onNext(command);
			}
		} catch (Exception e) {
			log.severe(Session.Command, "Exception occurs with message ["
					+ String.valueOf(event) + "]. Message dropped. " + e.getMessage());
		}
	}

	private class MonitorHandler implements Runnable {

		@Override
		public void run() {
			System.out.println("<--Pi4J--> GPIO Listen Example ... started.");

	        // create gpio controller = ici prendre les controllers que l'on a déclarer => pas de unknow supporté ici!

	        // provision gpio pin #02 as an input pin with its internal pull down resistor enabled
	        led = gpio.provisionDigitalInputPin(RaspiPin.GPIO_01, PinPullResistance.PULL_DOWN);

	        // set shutdown state for this input pin
	        led.setShutdownOptions(true);

	        // create and register gpio pin listener
	        led.addListener(new GpioPinListenerDigital() {
	            @Override
	            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
	                // display pin state on console
	                System.out.println(" --> GPIO PIN STATE CHANGE: " + event.getPin() + " = " + event.getState());
	                synchronized (lock) { // mutex on the main thread: only one connection or send message at the same time!
	                onMessageReceipt(event);
	                }
	            }

	        });

	        System.out.println(" ... complete the GPIO #02 circuit and see the listener feedback here in the console.");
			 
			// keep program running until user aborts (CTRL-C)
	        while(connected) {
	            try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }
		}
	}

	@Override
	public void subscribe(FlowableEmitter<Command> e) throws Exception {
		suscriberList.add(e);
	}
	
	private List<ConnectionListener> connectionListenerList = Collections
			.synchronizedList(new ArrayList<ConnectionListener>());

	
	public boolean connect() {
		connected = true;
		getExecutorService().execute(new MonitorHandler());
		return isConnected();
	}

	/**
	 * Close the client and stop the thread.<br/>
	 * No more connection will be try before method connect will be call again.
	 */
	public void disconnect() {
		connected = false;
		closeConnection();
	}

	private void closeConnection() {
		gpio.shutdown();
		gpio = null;
		led = null;
	}

	public boolean isConnected() {
		if (gpio == null || gpio.isShutdown()) {
			return false;
		} else {
			return true;
		}
	}

	public void addConnectionListener(ConnectionListener connectionListener) {
		if (!connectionListenerList.contains(connectionListener)) {
			connectionListenerList.add(connectionListener);
		}
	}

	public void removeConnectionListener(ConnectionListener connectionListener) {
		connectionListenerList.remove(connectionListener);
	}

	protected ExecutorService getExecutorService() {
		return Executors.newSingleThreadExecutor();
	}
}
