package com.domosnap.engine.adapter.onewire;

/*
 * #%L
 * DomoSnap 1wire Adapter
 * %%
 * Copyright (C) 2018 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.Command.Type;
import com.domosnap.engine.adapter.core.ConnectionListener;
import com.domosnap.engine.adapter.core.ConnectionStatusEnum;
import com.domosnap.engine.adapter.core.UnknownControllerListener;
import com.domosnap.engine.adapter.core.UnknownWhoException;
import com.domosnap.engine.controller.temperature.TemperatureSensor;
import com.domosnap.engine.controller.temperature.TemperatureSensorStateName;
import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.controller.what.impl.DoubleState;
import com.domosnap.engine.controller.what.impl.TemperatureUnitState;
import com.domosnap.engine.controller.where.URITools;
import com.domosnap.engine.controller.where.Where;
import com.pi4j.io.w1.W1Master;
import com.pi4j.temperature.TemperatureScale;

import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;

public class OneWireMonitorPublisher implements FlowableOnSubscribe<Command> {

	protected final Log log = new Log(OneWireMonitorPublisher.class.getSimpleName());

	private List<ConnectionListener> connectionListenerList = Collections
			.synchronizedList(new ArrayList<ConnectionListener>());
	private List<FlowableEmitter<Command>> suscriberList = Collections
			.synchronizedList(new ArrayList<FlowableEmitter<Command>>());
	private List<UnknownControllerListener> unknownControllerListenerList = Collections
			.synchronizedList(new ArrayList<UnknownControllerListener>());
	private Set<String> deviceIdList = new HashSet<String>();
	
	private W1Master w1Master;
	protected ExecutorService executor;
	protected boolean connected = false;
	
	private int poolingFrequency = 60000;
	
	private class MonitorHandler implements Runnable {
		@Override
		public void run() {
			while (connected) {
				
				for (com.pi4j.component.temperature.TemperatureSensor tempSensor : w1Master.getDevices(com.pi4j.component.temperature.TemperatureSensor.class)) {
					try {
							String id = tempSensor.getName().trim();
							Command commandTemp;
							
							// Temperature
							if (id.startsWith("28") || id.startsWith("10")) {
								commandTemp = new Command(TemperatureSensor.class, new What(TemperatureSensorStateName.value.name(), new DoubleState(tempSensor.getTemperature(TemperatureScale.CELSIUS))),
										new Where(URITools.generateURI(OneWireControllerAdapter.SCHEMA, null , "localhost", null, id)), Type.COMMAND, null);
							} else {
								throw new UnknownWhoException();
							}
							
							if (!deviceIdList.contains(id)) {
								// Unknown device
								log.finest(Session.Monitor, "Unknown controller detected [" + commandTemp.toString() + "].");
								synchronized (unknownControllerListenerList) {
									for (UnknownControllerListener listener : unknownControllerListenerList) {
										try {
											listener.foundUnknownController(commandTemp);
										} catch (Exception e) {
											log.warning(Session.Monitor, "Error with a listener when found an unknown controller on message [" + e.getMessage() +"]. Message dropped.");
										}
										for (FlowableEmitter<Command> suscriber : suscriberList) {
											suscriber.onNext(commandTemp);
										}
									}
								}
							}
							else { // Known device
								for (FlowableEmitter<Command> suscriber : suscriberList) {
									suscriber.onNext(commandTemp);
									// Temperature unit
									Command commandUnit = new Command(TemperatureSensor.class, new What(TemperatureSensorStateName.unit.name(), TemperatureUnitState.Celsius()),
											new Where(URITools.generateURI(OneWireControllerAdapter.SCHEMA, null , "localhost", null, id)), Type.COMMAND, null);
									suscriber.onNext(commandUnit);
								}
							}
						} catch (Exception e) {
							log.severe(Session.Monitor,
											"Exception occurs with message [" + e.getMessage() + "]. Message dropped. ", 1);
						}
					}
				try {
					Thread.sleep(poolingFrequency);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			};
		}
	}

	@Override
	public void subscribe(FlowableEmitter<Command> e) throws Exception {
		suscriberList.add(e);
	}

	public boolean connect() {
		if (isConnected())
			return true;
		
		try {
			w1Master = new W1Master();
			
			for (ConnectionListener connectionListener : connectionListenerList) {
				connectionListener.onConnect(ConnectionStatusEnum.Connected);
			}
			executor = Executors.newSingleThreadExecutor();
			executor.execute(new MonitorHandler());
			connected = true;
			return true;
		} catch (Exception e) {
			log.severe(Session.Monitor, e.getMessage());
			for (ConnectionListener connectionListener : connectionListenerList) {
				connectionListener.onConnect(ConnectionStatusEnum.UnkownError);
			}
			return false;
		} 
	}

	/**
	 * Close the client and stop the thread.<br/>
	 * No more connection will be try before method connect will be call again.
	 */
	public void disconnect() {
		if (executor != null) {
			executor.shutdown();
			executor = null;
		}
		connected = false;
		w1Master = null;

		synchronized (connectionListenerList) {
			for (ConnectionListener connectionListener : connectionListenerList) {
				try {
					connectionListener.onClose();
				} catch (Exception e) {
					log.severe(Session.Command,
							"ConnectionListener raise an error [" + e.getMessage() + "]", 1);
				}
			}
		}
	}

	public boolean isConnected() {
		return connected;
	}

	public void addConnectionListener(ConnectionListener connectionListener) {
		if (!connectionListenerList.contains(connectionListener)) {
			connectionListenerList.add(connectionListener);
		}
	}

	public void removeConnectionListener(ConnectionListener connectionListener) {
		connectionListenerList.remove(connectionListener);
	}
	
	public void addKnownDeviceId(String adress) {
		deviceIdList.add(adress);
	}

	public void addUnknownControllerListener(UnknownControllerListener arg0) {
		this.unknownControllerListenerList.add(arg0);
	}

	public void removeUnknownControllerListener(UnknownControllerListener arg0) {
		this.unknownControllerListenerList.remove(arg0);
	}
	
	public void setFrequency(int frequency) {
		this.poolingFrequency = frequency;
	}
	
	public int getFrequency() {
		return poolingFrequency;
	}
}
