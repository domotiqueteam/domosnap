package com.domosnap.engine.adapter.onewire;

/*
 * #%L
 * DomoSnap 1wire Adapter
 * %%
 * Copyright (C) 2018 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import com.domosnap.engine.adapter.core.AbstractControllerAdapter;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.ConnectionListener;
import com.domosnap.engine.adapter.core.ScanListener;
import com.domosnap.engine.adapter.core.UnknownControllerListener;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.temperature.TemperatureSensor;
import com.domosnap.engine.controller.where.URITools;
import com.domosnap.engine.controller.where.Where;
import com.pi4j.io.w1.W1Master;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.processors.FlowableProcessor;
import io.reactivex.processors.PublishProcessor;

public class OneWireControllerAdapter extends AbstractControllerAdapter {

	public static String SCHEMA = "ow";
	
//	private Log log = new Log(OneWireControllerAdapter.class.getSimpleName());

	private OneWireMonitorPublisher monitorPublisher;
	
	public OneWireControllerAdapter() {
		super();
	}
				
	@Override
	public void connect() {
		if (!monitorPublisher.isConnected()) {
			monitorPublisher.connect();
		}
	}

	@Override
	public void connect(String url) {
		connect();
	}
	
	@Override
	public void disconnect() {
		monitorPublisher.disconnect();
	}

	@Override
	public boolean isConnected() {
		return monitorPublisher.isConnected();
	}

	@Override
	public void addCommanderConnectionListener(ConnectionListener listener) {
	}

	@Override
	public void removeCommanderConnectionListener(ConnectionListener listener) {
	}
	
	@Override
	public void addMonitorConnectionListener(ConnectionListener listener) {
		monitorPublisher.addConnectionListener(listener);
	}

	@Override
	public void removeMonitorConnectionListener(ConnectionListener listener) {
		monitorPublisher.removeConnectionListener(listener);
	}

	@Override
	public void addUnknowControllerListener(UnknownControllerListener arg0) {
		monitorPublisher.addUnknownControllerListener(arg0);
	}
	
	@Override
	public void removeUnknowControllerListener(UnknownControllerListener arg0) {
		monitorPublisher.removeUnknownControllerListener(arg0);
	}

	@Override
	public void scan(ScanListener listener) {

		W1Master w1Master = new W1Master();

		List<com.pi4j.component.temperature.TemperatureSensor> lis =  w1Master.getDevices(com.pi4j.component.temperature.TemperatureSensor.class);
		int i = 1;
		for (com.pi4j.component.temperature.TemperatureSensor device : lis) {
			listener.progess(100/lis.size()*i); 
			Where where = new Where(device.getName().trim());
			TemperatureSensor ts = createController(TemperatureSensor.class, where);
			if (ts != null) {
				listener.foundController(ts);
			}
		}
		
		listener.progess(100);		
		listener.scanFinished();	
	}
	
	// TODO override => put in parent
	public List<Class<? extends Controller>> getSupportedDevice() {
		List<Class<? extends Controller>> result = new ArrayList<Class<? extends Controller>>();
		result.add(TemperatureSensor.class);
		
		return result;
	}
	
	public void setPoolingFrequency(int frequency) {
		monitorPublisher.setFrequency(frequency);
	}
	
	public int getPoolingFrequency() {
		return monitorPublisher.getFrequency();
	}

	@Override
	protected Flowable<Command> createMonitorStream() {
		monitorPublisher = new OneWireMonitorPublisher();
		return FlowableProcessor.create(monitorPublisher, BackpressureStrategy.DROP).share();
	}

	@Override
	protected PublishProcessor<Command> createCommandStream() {
		PublishProcessor<Command> commanderStream = PublishProcessor.create();
		commanderStream.subscribe(new OneWireCommanderConsumer());
		return commanderStream;
	}

	@Override
	protected void onCreatedController(Controller c) {
		monitorPublisher.addKnownDeviceId(c.getWhere().getPath());
	}

	@Override
	protected String formatURI(Where where) {
		String path;
		if (where == null) {
			path = "";
		} else {
			path = where.getPath();
		}
		return URITools.generateURI(SCHEMA, null , "localhost", null, path);
	}

	@Override
	public String getSchema() {
		return SCHEMA;
	}
}
