package com.domosnap.engine.connector.impl.onewire.test;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Assert;
import org.junit.Test;

import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.parser.ParseException;



public class UriOWTest {
	
	// uri ow
	private String  uriOneWire = "ow://localhost/28-122546564ff";

	@Test
	public void openwebnetTest() throws ParseException {
		
		print(uriOneWire);
		
		try {
			URI t = new URI(uriOneWire);
			Assert.assertEquals(uriOneWire, t.toString());
			Assert.assertEquals("/28-122546564ff", t.getPath());
			Assert.assertEquals("localhost", t.getHost());
			Assert.assertEquals("ow", t.getScheme());
			Assert.assertEquals(null, t.getRawFragment());
			Assert.assertEquals(null, t.getRawQuery());
		} catch (URISyntaxException e) {
			Assert.fail("Fail to parse ow adress.");
		}
	}
	
	private void print(String uri) {
		URI t = URI.create(uri);
		
		System.out.println("Authority: " + t.getAuthority());
		System.out.println("Raw Authority: " + t.getRawAuthority());
		System.out.println("Query:" + t.getQuery());
		System.out.println("RawQuery: " + t.getRawQuery());
		System.out.println("Scheme: " + t.getScheme());
		System.out.println("Fragment: " + t.getFragment());
		System.out.println("Raw Fragment: " + t.getRawFragment());
		System.out.println("Path: " + t.getPath());
		System.out.println("RawPath: " + t.getRawPath());
		System.out.println("Host: " + t.getHost());
		System.out.println("Port: " + t.getPort());
		System.out.println("Schema Specefic Part: " + t.getSchemeSpecificPart());
		System.out.println("Raw Scheme Specific Part: " + t.getRawSchemeSpecificPart());
		System.out.println("User Info: " + t.getUserInfo());
		System.out.println("Raw User Info: " + t.getRawUserInfo());
		System.out.println("is Absolute: " + t.isAbsolute());
		System.out.println("is Opaque: " + t.isOpaque());
		try {
			System.out.println("URL: " + t.toURL());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("ASCII: " + t.toASCIIString());
	}
}
