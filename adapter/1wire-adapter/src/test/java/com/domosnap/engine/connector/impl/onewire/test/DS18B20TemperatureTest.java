//package com.domosnap.engine.connector.impl.onewire.test;
//
//import org.junit.Test;
//
//import com.domosnap.engine.adapter.ControllerAdapter;
//import com.domosnap.engine.adapter.compliance.test.controller.temperaturesensor.TemperatureSensorTest;
//import com.domosnap.engine.adapter.compliance.test.controller.temperaturesensor.TemperatureSensorTest.ITemperatureSensorTest;
//import com.domosnap.engine.adapter.onewire.OneWireControllerAdapter;
//import com.domosnap.engine.controller.where.Where;
//
//public class DS18B20TemperatureTest implements ITemperatureSensorTest {
//
//	private static ControllerAdapter s = new OneWireControllerAdapter();
//
//	private  TemperatureSensorTest hst = new TemperatureSensorTest();
//	
//	public ControllerAdapter getControllerService() {
//		return s;
//	}
//	
//	public Where getWhere1() {
//		return new Where("28.F2FBE3467CC2");
//	}
//
//	@Test
//	@Override
//	public void statusTest() throws InterruptedException {
//		s.connect();
//		hst.statusTest(getControllerService(), getWhere1());
//		
//	}
//}
