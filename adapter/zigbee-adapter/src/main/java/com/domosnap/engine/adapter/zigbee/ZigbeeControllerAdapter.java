package com.domosnap.engine.adapter.zigbee;

import java.net.URI;

/*
 * #%L
 * DomoSnap zigbee adapter
 * %%
 * Copyright (C) 2016 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import com.domosnap.engine.Log;
import com.domosnap.engine.adapter.core.AbstractControllerAdapter;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.ConnectionListener;
import com.domosnap.engine.adapter.core.ScanListener;
import com.domosnap.engine.adapter.core.UnknownControllerListener;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.temperature.TemperatureSensor;
import com.domosnap.engine.controller.where.URITools;
import com.domosnap.engine.controller.where.Where;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.processors.FlowableProcessor;
import io.reactivex.processors.PublishProcessor;

public class ZigbeeControllerAdapter extends AbstractControllerAdapter {

	private Log log = new Log(this.getClass().getSimpleName());

	public static String SCHEMA = "zigbee";
	
	// zbee://dongle:10/1
	
	private ZigbeeMonitorPublisher monitorPublisher;
	private ZigbeeCommanderConsumer commanderConsumer;
	
	public ZigbeeControllerAdapter(String uri) {
		// TODO manage other dongle... = dongle as host...
		super(uri);
	}
				
	@Override
	public void connect() {
		if (!monitorPublisher.isConnected()) {
			monitorPublisher.connect(uri.getPath());
		}
//		if (!commanderConsumer.isConnected()) {
//			commanderConsumer.connect();
//		}
	}

	@Override
	public void connect(String url) {
		this.uri = URI.create(url);
		connect();
	}
	
	@Override
	public void disconnect() {
		monitorPublisher.disconnect();
		commanderConsumer.disconnect();
	}

	@Override
	public boolean isConnected() {
		return monitorPublisher.isConnected() && commanderConsumer.isConnected();
	}

	@Override
	public void addCommanderConnectionListener(ConnectionListener listener) {
		commanderConsumer.addConnectionListener(listener);
	}

	@Override
	public void removeCommanderConnectionListener(ConnectionListener listener) {
		commanderConsumer.removeConnectionListener(listener);
	}
	
	@Override
	public void addMonitorConnectionListener(ConnectionListener listener) {
		monitorPublisher.addConnectionListener(listener);
	}

	@Override
	public void removeMonitorConnectionListener(ConnectionListener listener) {
		monitorPublisher.removeConnectionListener(listener);
	}

	@Override
	public void addUnknowControllerListener(UnknownControllerListener arg0) {
//		monitorPublisher.addUnknownControllerListener(arg0);
	}
	
	@Override
	public void removeUnknowControllerListener(UnknownControllerListener arg0) {
//		monitorPublisher.removeUnknownControllerListener(arg0);
	}

	@Override
	public void scan(ScanListener listener) {
		
		listener.progess(100);		
		listener.scanFinished();	
	}
	
	@Override
	public List<Class<? extends Controller>> getSupportedDevice() {
		List<Class<? extends Controller>> result = new ArrayList<Class<? extends Controller>>();
		result.add(TemperatureSensor.class);
		
		return result;
	}
	
	@Override
	protected Flowable<Command> createMonitorStream() {
		monitorPublisher = new ZigbeeMonitorPublisher();
		return FlowableProcessor.create(monitorPublisher, BackpressureStrategy.DROP).share();
	}

	@Override
	protected PublishProcessor<Command> createCommandStream() {
		PublishProcessor<Command> commanderStream = PublishProcessor.create();
		commanderStream.subscribe(new ZigbeeCommanderConsumer());
		return commanderStream;
	}

	@Override
	protected void onCreatedController(Controller c) {
	}

	@Override
	protected String formatURI(Where where) {
		String path;
		if (where == null) {
			path = "";
		} else {
			path = where.getPath();
		}
		// TODO Ca ne va pas... = j'utilise cette fonction quand uri = null dans le constructor...
;
		return URITools.generateURI(SCHEMA, uri.getUserInfo() , uri.getHost(), uri.getPort(), uri.getPath() +"/" + path);
	}

	@Override
	public String getSchema() {
		return SCHEMA;
	}
}
