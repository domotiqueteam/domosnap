package com.domosnap;

import com.domosnap.engine.adapter.zigbee.ZigbeeControllerAdapter;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.ControllerChangeListener;
import com.domosnap.engine.controller.door.DoorSensor;
import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.controller.what.impl.BooleanState;
import com.domosnap.engine.controller.what.impl.OnOffState;
import com.domosnap.engine.controller.where.Where;

public class ZigbeeTest {

	public static void main(String[] args) {
		
		ZigbeeControllerAdapter cs = new ZigbeeControllerAdapter("zigbee:///dev/ttyACM0");
		final DoorSensor door = cs.createController(DoorSensor.class, new Where("39375/1"));

		cs.connect();
		// Listener will make us availabe to wait response from server
		door.addControllerChangeListener(new ControllerChangeListener() {

			@Override
			public void onStateChangeError(Controller controller,
					What oldStatus, What newStatus) {
				System.out.println("error...");
			}
			
			@Override
			public void onStateChange(Controller controller,
					What oldStatus, What newStatus) {
				if (((OnOffState) newStatus.getValue()).getValue()) {
					System.out.println("Porte Ouverte.");
				} else {
					System.out.println("Porte Fermée.");
				}
			
			}
		});
	}
}
