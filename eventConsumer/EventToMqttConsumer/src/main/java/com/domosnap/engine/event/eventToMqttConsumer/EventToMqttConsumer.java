package com.domosnap.engine.event.eventToMqttConsumer;

/*
 * #%L
 * DomoSnap Event to MQTT Consumer
 * %%
 * Copyright (C) 2018 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.function.Consumer;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.event.Event;

import io.netty.handler.codec.mqtt.MqttQoS;
import io.vertx.core.Context;
import io.vertx.core.Future;
import io.vertx.core.Verticle;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.mqtt.MqttClient;
import io.vertx.mqtt.MqttClientOptions;

public class EventToMqttConsumer implements Consumer<Event> {

	private static Log log = new Log(EventToMqttConsumer.class.getSimpleName());

	private MqttClient client;
	private String DEFAULT_CONFIG_MQTT_TOPIC_WRITE = "controller/"; //"domosnap-event-topic";

	private String hostname;
	private int port;
	private MqttClientOptions opts;
	private Vertx vertx;
	private boolean connected = false;
	
	public EventToMqttConsumer() {
	}
	
	// mosquitto_sub -h localhost -v -t test_channel -p 11112
	// mosquitto_pub -h localhost -t test_channel -m "Hello Raspberry Pi" -p 11112
	public EventToMqttConsumer(String host, int port, MqttClientOptions opt, Vertx vertx, String mainChannel, boolean autoConnect) {
		this.hostname = host;
		this.port = port;
		this.opts = opt;
		this.vertx = vertx;
		if (mainChannel != null) { this.DEFAULT_CONFIG_MQTT_TOPIC_WRITE = mainChannel; } 
		if (autoConnect) {connect();}
	}
	
	public void connect() {
		client = MqttClient.create(vertx,opts);

		client.connect(port, hostname, s -> {
			client.publishCompletionHandler(id -> {
				  log.finest(Session.Server, "Id of just received PUBACK or PUBCOMP packet is " + id);
				});
////				  // The line of code below will trigger publishCompletionHandler (QoS 2)
//				  .publish("sensor/temperature", Buffer.buffer("hello"), MqttQoS.EXACTLY_ONCE, false, false)
////				  // The line of code below will trigger publishCompletionHandler (QoS is 1)
//				  .publish("sensor/temperature", Buffer.buffer("hello"), MqttQoS.AT_LEAST_ONCE, false, false)
////				  // The line of code below does not trigger because QoS value is 0
//				  .publish("sensor/temperature", Buffer.buffer("hello"), MqttQoS.AT_LEAST_ONCE, false, false); 
			log.finest(Session.Server, "MqttClient connected.");
		});
		connected = true;
	}
	
	public void disconnect() {
		connected = false;
		client.disconnect();
	}
	
	@Override
	public void accept(final Event event) {

		try {
			if (connected) {
				String where = event.getCommand().getWhere().getPath();
				String who = event.getCommand().getWho().getSimpleName();
				String channel = DEFAULT_CONFIG_MQTT_TOPIC_WRITE.concat(who).concat("/").concat(where);
				
				for (What what : event.getCommand().getWhatList()) {
					log.finest(event.getOrigin(), "Channel [".concat(channel).concat("] - Value: ").concat(what.toString()));
					
					client.publish(channel,
							  Buffer.buffer(what.toString()),
							  MqttQoS.AT_LEAST_ONCE,
							  false,
							  false);	
				}
			} else {
				log.finest(event.getOrigin(), "MqttClient not connected. Event [".concat(event.toString()).concat("] will be not send."));
			}
		} catch (Exception e) {
			log.severe(event.getOrigin(), e.getMessage());
		}

	}
	
	public void setRootTopic(String rootTopic) {
		if (!rootTopic.endsWith("/")) {
			rootTopic = rootTopic + "/";
		}
		DEFAULT_CONFIG_MQTT_TOPIC_WRITE = rootTopic;
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		disconnect();
	}
	
	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();
		
		vertx.deployVerticle(new Verticle() {
			private EventToMqttConsumer queue;
			
			@Override
			public void stop(Future<Void> stopFuture) throws Exception {
				if (queue != null) {
					queue.disconnect();
				}
			}
			
			@Override
			public void start(Future<Void> startFuture) throws Exception {
				MqttClientOptions opt = new MqttClientOptions();
				opt.setMaxInflightQueue(100);
				queue = new EventToMqttConsumer("env-5291014.hidora.com", 11112, opt, vertx, null, true);
			}
			
			@Override
			public void init(Vertx vertx, Context context) {
			}
			
			@Override
			public Vertx getVertx() {
				return vertx;
			}
		});
	}

}
