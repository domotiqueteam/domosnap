package com.domosnap.engine.event.eventToElasticSearchConsumer;

/*
 * #%L
 * DomoSnap Event to Elastic Search Consumer
 * %%
 * Copyright (C) 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.function.Consumer;

import com.domosnap.engine.Log;
import com.domosnap.engine.event.Event;

public class EventToElasticSearchConsumer implements Consumer<Event> {

	private static Log log = new Log(EventToElasticSearchConsumer.class.getSimpleName());

	private int errorCount = 0;
	private int RETRY = 5;
	private String DEFAULT_CONFIG_MQTT_TOPIC_WRITE = "domosnap-event-topic";

	
	
	public void setRetry(int retry) {
		this.RETRY = retry;
	}
	
	public EventToElasticSearchConsumer() {
	}
	
	
	@Override
	public void accept(final Event event) {

		do {
			try {
//				client.publish(DEFAULT_CONFIG_MQTT_TOPIC_WRITE,
//						  Buffer.buffer(event.toString()),
//						  MqttQoS.AT_LEAST_ONCE,
//						  false,
//						  false);
			} catch (Exception e) {
				log.severe(event.getOrigin(), e.getMessage());
				errorCount++;
			}
		} while (errorCount > 0 && errorCount < RETRY);

	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
	}

}
