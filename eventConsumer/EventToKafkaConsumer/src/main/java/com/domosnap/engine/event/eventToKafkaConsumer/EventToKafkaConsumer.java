package com.domosnap.engine.event.eventToKafkaConsumer;

/*
 * #%L
 * DomoSnap Event to Kafka Consumer
 * %%
 * Copyright (C) 2018 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import com.domosnap.engine.Log;
import com.domosnap.engine.event.Event;

public class EventToKafkaConsumer implements Consumer<Event> {

	private static Log log = new Log(EventToKafkaConsumer.class.getSimpleName());

	private KafkaProducer<String, String> producer;
	private String DEFAULT_CONFIG_KAFKA_TOPIC_WRITE = "domosnap-event-topic";

	
	public EventToKafkaConsumer() {
		Map<String, Object> props = new HashMap<String, Object>();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "PLAINTEXT://localhost:9092");
		props.put(ProducerConfig.ACKS_CONFIG, "all"); // message passé à tous mes broker "0"= udp, "1"= tcp = récupérer mais pas stocké. Parametre min.insync.replica = 2
		props.put(ProducerConfig.RETRIES_CONFIG, "0");
		props.put(ProducerConfig.BATCH_SIZE_CONFIG, "100");
		props.put(ProducerConfig.LINGER_MS_CONFIG, "1");
		props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, "33554432");
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.StringSerializer");
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.StringSerializer");

		producer = new KafkaProducer<String, String>(props);
	}
	
	public EventToKafkaConsumer(Map<String, Object> props) {

		producer = new KafkaProducer<String, String>(props);
	}
	
	@Override
	public void accept(final Event event) {

		ProducerRecord<String, String> record = new ProducerRecord<String, String>(DEFAULT_CONFIG_KAFKA_TOPIC_WRITE, UUID.randomUUID().toString(), event.toString());
		producer.send(record, new Callback() {
			
			@Override
			public void onCompletion(RecordMetadata metadata, Exception exception) {
				if (exception != null) {
					log.severe(event.getOrigin(), exception.getMessage());
				} else {
					log.finest(event.getOrigin(), "Event send to Kafka with success [" + event + "]");
				}
				
			}
		});
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		producer.close();
	}

}
