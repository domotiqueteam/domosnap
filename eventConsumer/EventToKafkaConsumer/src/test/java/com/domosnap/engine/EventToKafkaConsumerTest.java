package com.domosnap.engine;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.junit.Test;

import com.domosnap.engine.adapter.ControllerAdapter;
import com.domosnap.engine.adapter.impl.openwebnet.OpenWebNetControllerAdapter;
import com.domosnap.engine.controller.light.Light;
import com.domosnap.engine.controller.what.impl.OnOffState;
import com.domosnap.engine.controller.where.Where;
import com.domosnap.engine.event.EventFactory;
import com.domosnap.engine.event.eventToKafkaConsumer.EventToKafkaConsumer;

public class EventToKafkaConsumerTest {

	
	ControllerAdapter ca = new OpenWebNetControllerAdapter("openwebnet://12345@localhost:1234");
	
	@Test
	public void testScan() {
		Map<String, Object> props = new HashMap<String, Object>();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "PLAINTEXT://192.168.1.186:9092");
		props.put(ProducerConfig.ACKS_CONFIG, "all");
		props.put(ProducerConfig.RETRIES_CONFIG, "0");
		props.put(ProducerConfig.BATCH_SIZE_CONFIG, "100");
		props.put(ProducerConfig.LINGER_MS_CONFIG, "1");
		props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, "33554432");
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.StringSerializer");
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.StringSerializer");

		
		EventToKafkaConsumer e = new EventToKafkaConsumer(props);
		EventFactory.addConsumer(e);
		
		Light l = ca.createController(Light.class, new Where("12"));
		int i = 0;
		while (i < 110) {
			l.setStatus(OnOffState.On());
			i++;
		}
		
		try {
			Thread.sleep(100000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
	
}
