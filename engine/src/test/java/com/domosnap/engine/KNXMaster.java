package com.domosnap.engine;

import java.util.ArrayList;
import java.util.List;

import com.domosnap.engine.adapter.core.ScanListener;
import com.domosnap.engine.adapter.impl.knx.KNXControllerAdapter;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.light.Light;
import com.domosnap.engine.controller.what.impl.OnOffState;

public class KNXMaster {

	
	private static List<Light> list = new ArrayList<Light>(); 
	
	public static void main(String[] args) {
		KNXControllerAdapter service = new KNXControllerAdapter();
		service.connect();
		service.scan(new ScanListener() {
			
			@Override
			public void scanFinished() {
				// TODO Auto-generated method stub
				System.out.println("Scan finish.");
			}
			
			@Override
			public void progess(int percent) {
				System.out.println("Avancement: " + percent);
			}
			
			@Override
			public void foundController(Controller controller) {
				// TODO Auto-generated method stub
				System.out.println("Controller found " + controller.getWhere().getPath());
				
				list.add((Light) controller);
			}
		});
		
		
		list.get(0).setStatus(OnOffState.On());
		service.disconnect();
	}
}
