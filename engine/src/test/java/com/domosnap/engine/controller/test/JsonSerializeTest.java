//package com.domosnap.engine.controller.test;
//
//import javax.xml.bind.UnmarshalException;
//
//import org.junit.Assert;
//import org.junit.Test;
//
//import com.domosnap.engine.connector.ControllerService;
//import com.domosnap.engine.connector.impl.openwebnet.OpenWebNetControllerService;
//import com.domosnap.engine.controller.automation.Automation;
//import com.domosnap.engine.controller.light.Light;
//import com.domosnap.engine.controller.what.impl.OnOffState;
//import com.domosnap.engine.controller.what.impl.UpDownState;
//import com.domosnap.engine.controller.where.Where;
//import com.domosnap.engine.services.PersistenceService;
//import com.domosnap.engine.services.impl.PersistenceServiceImpl;
//
//public class JsonSerializeTest {
//
//	private ControllerService controllerService = new OpenWebNetControllerService("localhost", 1234, 12345);
//	private PersistenceService persistence = new PersistenceServiceImpl(controllerService);
//	
//	@Test
//	public void lightJsonTest() throws UnmarshalException {
//		Light l = controllerService.createController(Light.class, new Where("11","11"));
//		Light lDest = controllerService.createController(Light.class, new Where("11","11"));
//		
//		l.setTitle("light title");
//		l.setDescription("light description");
//		l.setStatus(OnOffState.On());
//		try {
//			Thread.sleep(200);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//		System.out.println("Light: [" + l.toString() + "]");
//
//		Assert.assertEquals(l.getWho(), lDest.getWho());
//		Assert.assertEquals(l.getTitle(), lDest.getTitle());
//		Assert.assertEquals(l.getWhere(), lDest.getWhere());
//		Assert.assertEquals(l.getStatus().getValue(), lDest.getStatus().getValue());
//	}
//
//	@Test
//	public void automationJsonTest() {
//		
//		Automation l = controllerService.createController(Automation.class, new Where("16","16"));
//		Automation lDest = controllerService.createController(Automation.class, new Where("16","16"));
//		
//		l.setTitle("automation title");
//		l.setDescription("automation description");
//		l.setStatus(UpDownState.Up);
//		try {
//			Thread.sleep(200);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//		System.out.println("Automation: [" + l.toString() + "]");
//		Assert.assertEquals(l.getWho(), lDest.getWho());
//		Assert.assertEquals(l.getTitle(), lDest.getTitle());
//		Assert.assertEquals(l.getWhere(), lDest.getWhere());
//		Assert.assertEquals(l.getStatus(), lDest.getStatus());
//	}
//
//
//}
