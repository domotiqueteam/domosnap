package com.domosnap.engine.controller;

import org.junit.Assert;
import org.junit.Test;

import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.controller.what.WhatJsonCodec;
import com.domosnap.engine.controller.what.impl.BooleanState;

public class WhatJsonCodecTest {

	@Test
	public void jsonSerialization() {
		What w = new What("test", BooleanState.FALSE);
		
		
		String json = WhatJsonCodec.toJSon(w);
		System.out.println(json);
		
		What w2 = WhatJsonCodec.fromJson(json);
		
		Assert.assertEquals(w.getName(), w2.getName());
		Assert.assertEquals(w.getValue(), w2.getValue());
	}
}
