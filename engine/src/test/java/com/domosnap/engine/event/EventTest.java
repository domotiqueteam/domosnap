package com.domosnap.engine.event;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.Command.Type;
import com.domosnap.engine.controller.light.Light;
import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.controller.what.impl.BooleanState;
import com.domosnap.engine.controller.where.Where;

public class EventTest {

	
	@Test
	public void JsonTest() throws UnknownHostException {
		List<What> l = new ArrayList<>();
		
		l.add(new What("boolean",BooleanState.TRUE));
		Command c = new Command(Light.class, l, new Where("protocle://localhost/adress"), Type.COMMAND, null);
		Event e = new Event(Session.Monitor, c);
		String json = EventJsonCodec.toJSon(e);
		System.out.println(json);
		Event e2 = EventJsonCodec.fromJson(json);
		
		Assert.assertNotNull(e2.getCommand());
		Assert.assertEquals(Session.Monitor, e2.getOrigin());
	}
}
