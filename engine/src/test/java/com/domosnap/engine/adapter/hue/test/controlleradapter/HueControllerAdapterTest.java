package com.domosnap.engine.adapter.hue.test.controlleradapter;

import java.net.Socket;

import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import com.domosnap.engine.adapter.compliance.test.controlleradapter.ControllerAdapterTest;
import com.domosnap.engine.adapter.compliance.test.controlleradapter.ControllerAdapterTest.IControllerAdapterTest;
import com.domosnap.engine.adapter.impl.hue.HueControllerAdapter;

public class HueControllerAdapterTest implements IControllerAdapterTest {

	private ControllerAdapterTest caTest = new ControllerAdapterTest();
	
	@Test(timeout = 20000l)
	@Override
	public void testConnection() throws InterruptedException {
		
		//given
		final HueControllerAdapter controllerService = new HueControllerAdapter("hue://newdeveloper@127.0.0.1:8080");  
		
		//when
		caTest.testConnection(controllerService, "newdeveloper@127.0.0.1:8080");
	}
	
	@Test(timeout = 20000l)
	@Override
	public void testScan() throws InterruptedException {
		// when
		caTest.testScan( new HueControllerAdapter("newdeveloper@127.0.0.1:8080"));
	}
	
	@Before
	public void assume() {
		Assume.assumeTrue(is8080Open());
	}

	private boolean is8080Open() {
		try {
			Socket socket  = new Socket("127.0.0.1", 8080);
			boolean result = socket.isConnected();
			socket.close();
			return result;
		} catch (Exception e) {
			return false;
		}
	}
}
