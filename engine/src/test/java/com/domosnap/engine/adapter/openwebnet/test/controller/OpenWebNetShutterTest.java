package com.domosnap.engine.adapter.openwebnet.test.controller;

import org.junit.Before;
import org.junit.Test;

import com.domosnap.engine.adapter.ControllerAdapter;
import com.domosnap.engine.adapter.compliance.test.controller.shutter.ShutterTest;
import com.domosnap.engine.adapter.compliance.test.controller.shutter.ShutterTest.IAutomationTest;
import com.domosnap.engine.adapter.impl.openwebnet.OpenWebNetControllerAdapter;
import com.domosnap.engine.controller.where.Where;

public class OpenWebNetShutterTest implements IAutomationTest {

	private ControllerAdapter s;
	private ShutterTest at = new ShutterTest();
	
	@Before
	public void init() {
		s = new OpenWebNetControllerAdapter("openwebNet://12345@localhost:1234");
		s.connect();
	}
	
	@Test
	@Override
	public void statusUpDownTest() throws InterruptedException {
		at.statusUpDownTest(s, new Where("33"));
	}
}
