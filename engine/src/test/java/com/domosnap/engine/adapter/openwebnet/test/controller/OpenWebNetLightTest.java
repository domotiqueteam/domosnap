package com.domosnap.engine.adapter.openwebnet.test.controller;

import org.junit.Before;
import org.junit.Test;

import com.domosnap.engine.adapter.ControllerAdapter;
import com.domosnap.engine.adapter.compliance.test.controller.light.LightTest;
import com.domosnap.engine.adapter.compliance.test.controller.light.LightTest.ILightTest;
import com.domosnap.engine.adapter.impl.openwebnet.OpenWebNetControllerAdapter;
import com.domosnap.engine.controller.where.Where;

public class OpenWebNetLightTest implements ILightTest {

	private LightTest lt = new LightTest();
	
	private ControllerAdapter s;

	@Before
	public void init() {
		s = new OpenWebNetControllerAdapter("openwebNet://12345@localhost:1234");
		s.connect();
	}
	
	@Test
	@Override
	public void statusOnOffTest() throws InterruptedException {
		lt.statusOnOffTest(s, new Where("12"));
		
	}
	
}
