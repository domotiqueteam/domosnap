package com.domosnap.engine.adapter.core;

import org.junit.Assert;
import org.junit.Test;

import com.domosnap.engine.adapter.ControllerFactory;
import com.domosnap.engine.controller.light.Light;
import com.domosnap.engine.controller.where.Where;

public class ControllerFactoryTest {

	@Test
	public void testControllerCreation() throws InterruptedException{

		// Test that controller only reuse instance!
		Object instance1 = ControllerFactory.createController(Light.class, new Where("openwebnet://12345@localhost:1234/12"));
		Object instance2 = ControllerFactory.createController(Light.class, new Where("openwebnet://12345@localhost:1234/12"));
		
		Assert.assertNotNull(instance1);
		Assert.assertTrue(instance1 == instance2);
		Assert.assertTrue(instance1.equals(instance2));
	}

}