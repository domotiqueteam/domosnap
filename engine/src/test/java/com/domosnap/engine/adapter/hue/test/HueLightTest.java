package com.domosnap.engine.adapter.hue.test;

import java.net.Socket;

import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.domosnap.engine.adapter.ControllerAdapter;
import com.domosnap.engine.adapter.compliance.test.controller.light.LightTest;
import com.domosnap.engine.adapter.compliance.test.controller.light.LightTest.ILightTest;
import com.domosnap.engine.adapter.impl.hue.HueControllerAdapter;
import com.domosnap.engine.controller.where.Where;

@RunWith(JUnit4.class)
public class HueLightTest implements ILightTest {

	
	private ControllerAdapter controllerService = new HueControllerAdapter("hue://newdeveloper@localhost:8080");
	private LightTest lt = new LightTest();
	
	@Before
	public void assume() {
		Assume.assumeTrue(is8080Open());
	}

	@Test
	@Override
	public void statusOnOffTest() throws InterruptedException {
		lt.statusOnOffTest(controllerService, new Where("1"));
	}

	private boolean is8080Open() {
		try {
			Socket socket  = new Socket("127.0.0.1", 8080);
			boolean result = socket.isConnected();
			socket.close();
			return result;
		} catch (Exception e) {
			return false;
		}
	}

}
