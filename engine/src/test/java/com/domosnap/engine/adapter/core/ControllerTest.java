package com.domosnap.engine.adapter.core;

import org.junit.Assert;
import org.junit.Test;

import com.domosnap.engine.adapter.impl.openwebnet.OpenWebNetControllerAdapter;
import com.domosnap.engine.controller.ControllerJsonCodec;
import com.domosnap.engine.controller.light.Light;
import com.domosnap.engine.controller.where.Where;

public class ControllerTest {

	private final static String TITLE = "title";
	private final static String DESCRIPTION = "title";
	
	@Test
	public void JsonTest() {
		OpenWebNetControllerAdapter open = new OpenWebNetControllerAdapter();
		Light l = open.createController(Light.class, new Where("openwebnet://localhost:1234/12"));
		l.setTitle(TITLE);
		l.setDescription(DESCRIPTION);
		String where = l.getWhere().getUri();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		String json = ControllerJsonCodec.toJson(l);
		
		
		l.setTitle("rerewrwe");
		l.setTitle("rerewrwe");
		
		ControllerJsonCodec.fromJson(l, json);
		
		Assert.assertEquals(TITLE, l.getTitle());
		Assert.assertEquals(DESCRIPTION, l.getDescription());
		Assert.assertEquals(where, l.getWhere().getUri());
		
	}
}
