package com.domosnap.engine.adapter.openwebnet.test.controlleradapter;

import java.net.Socket;

import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import com.domosnap.engine.adapter.compliance.test.controlleradapter.ControllerAdapterTest;
import com.domosnap.engine.adapter.compliance.test.controlleradapter.ControllerAdapterTest.IControllerAdapterTest;
import com.domosnap.engine.adapter.impl.openwebnet.OpenWebNetControllerAdapter;

public class OpenWebNetControllerAdapterTest implements IControllerAdapterTest {

	private ControllerAdapterTest caTest = new ControllerAdapterTest();
	
	@Test(timeout = 20000l)
	@Override
	public void testConnection() throws InterruptedException {
		
		//given
		final OpenWebNetControllerAdapter controllerService = new OpenWebNetControllerAdapter("12345@127.0.0.1:1234");  
		
		//when
		caTest.testConnection(controllerService, "12345@127.0.0.1:1234");
	}
	
	@Test(timeout = 20000l)
	@Override
	public void testScan() throws InterruptedException {
		// given
		final OpenWebNetControllerAdapter controllerService = new OpenWebNetControllerAdapter("12345@127.0.0.1:1234");  
		// when
		caTest.testScan(controllerService);
	}
	
	
	@Before
	public void assume() {
		Assume.assumeTrue(is1234Open());
	}

	private boolean is1234Open() {
		try {
			Socket socket  = new Socket("127.0.0.1", 1234);
			boolean result = socket.isConnected();
			socket.close();
			return result;
		} catch (Exception e) {
			return false;
		}
	}
}
