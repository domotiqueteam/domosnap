package com.domosnap.engine.adapter.knx.test;

//public class KNXNetIPTest extends KNXServerTestCase {
//	
//	private final static Log log = new Log(KNXNetIPTest.class.getSimpleName());
//	
//	private KNXControllerService knxService = new KNXControllerService();
//	
//	@Test
//	public void connectionTest() {
//		
//		knxService.connect();
//		assertTrue(knxService.isConnected());
//		knxService.disconnect();
//		assertTrue(knxService.isConnected() == false);
//	}
//	
//	@Test
//	public void connectionAdvancedTest() {
//		
//		log.info(Session.Server, "Start advanced connection tests !");
//		KNXDiscoverer discoverer = KNXDiscoverer.getInstance();
//		KNXConnectionSettings connectionSettings = discoverer.getConnectionSettings(null);
//		
//		if (connectionSettings != null) {
//			
//			List<KNXNetworkLink> connections = null;
//			
//			log.info(Session.Server, "Test concurrent tunneling connections ...");
//			connections = createConcurrentConnections(connectionSettings, 10, false);
//			log.info(Session.Server, "Number of concurrent tunneling connections opened: "+ connections.size());
//			closeConcurrentConnections(connections);
//			
//			if (connectionSettings.getDeviceSettings().isRouter()) {
//				log.info(Session.Server, "Test concurrent routing connections ...");
//				connections = createConcurrentConnections(connectionSettings, 10, true);
//				log.info(Session.Server, "Number of concurrent routing connections opened: "+ connections.size());
//				closeConcurrentConnections(connections);
//			}
//		}
//	}
//	
//	@Test
//	public void scanDevicesTest() {
//		
//		knxService.connect();
//		// TODO Investigate to simulate with calimero-server
//		knxService.scan(new ScanListener() {
//			
//			@Override
//			public void scanFinished() {
//				// TODO Auto-generated method stub
//			}
//			
//			@Override
//			public void progess(int percent) {
//				// TODO Auto-generated method stub
//			}
//			
//			@Override
//			public void foundController(Who who, Where where, Controller controller) {
//				System.out.println(who.toString());
//			}
//		});
//		knxService.disconnect();
//	}
//	
//	private List<KNXNetworkLink> createConcurrentConnections(KNXConnectionSettings connectionSettings, int connectionLimit, boolean routingMode) {
//		
//		List<KNXNetworkLink> connections = new ArrayList<KNXNetworkLink>(connectionLimit);
//		boolean stopTest = false;
//		while (!stopTest && connections.size() < connectionLimit) {
//			try {
//				KNXNetworkLink connection = KNXConnection.createConnection(connectionSettings, routingMode);
//				connections.add(connection);
//			}
//			catch (Exception e) {
//				stopTest = true;
//			}
//		}
//		return connections;
//	}
//	
//	private void closeConcurrentConnections(List<KNXNetworkLink> connections) {
//		
//		if (connections != null) {
//			for (KNXNetworkLink connection : connections) {
//				try {
//					connection.close();
//				}
//				catch (Exception e) {
//				}
//			}
//		}
//		connections.clear();
//	}
//	
//	public static void main(String[] args) {
//		new KNXNetIPTest().connectionAdvancedTest();
//	}
//}
