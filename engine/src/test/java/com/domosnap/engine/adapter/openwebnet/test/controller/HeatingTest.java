package com.domosnap.engine.adapter.openwebnet.test.controller;

/*
 * #%L
 * MyDomoEngine
 * %%
 * Copyright (C) 2011 - 2013 A. de Giuli
 * %%
 * This file is part of MyDomo done by A. de Giuli (arnaud.degiuli(at)free.fr).
 * 
 *     MyDomo is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     MyDomo is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with MyDomo.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.domosnap.engine.adapter.ControllerAdapter;
import com.domosnap.engine.adapter.impl.openwebnet.OpenWebNetControllerAdapter;
import com.domosnap.engine.controller.heating.HeatingZone;
import com.domosnap.engine.controller.heating.stateValue.OffsetState;
import com.domosnap.engine.controller.heating.stateValue.OffsetState.Mode;
import com.domosnap.engine.controller.what.impl.DoubleState;
import com.domosnap.engine.controller.where.Where;

public class HeatingTest {

	// TODO revoir pour mettre des tests dans le core
	// TODO revoir si ce test ne peut pas etre remplacer par la sonde de temperature?
	
	private ControllerAdapter s;
	
	@Before
	public void init() {
		s = new OpenWebNetControllerAdapter("openwebNet://12345@localhost:1234");
		s.connect();
	}
	
	@Test
	public void getDesiredTemperatureTest() {
		HeatingZone hz = s.createController(HeatingZone.class, new Where("1"));
		System.out.println("getDesiredTemperatureSynchroneTest");
		// First we just wait 1 second to be sure the controller is initialize 

		
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
//		DesiredTemperature d = hz.getDesiredTemperature(HeatingZoneState.HEATING_MODE); TODO manage mode
		DoubleState d = hz.getDesiredTemperature();
		Assert.assertNotNull(d);
		Assert.assertEquals(19, d.getValue(), 0);	
	}

	
	// TODO Not supported actually...
//	@Test
//	public void setDesiredTemperatureTest() {
//		HeatingZone hz = s.createController(HeatingZone.class, new Where("11","11"));
//		System.out.println("setDesiredTemperatureTest");
//		
//		hz.addControllerChangeListener(new ControllerChangeListener() {
//
//			@Override
//			public void onStateChangeError(Controller controller,
//					What oldStatus, What newStatus) {
//				synchronized (lock) {
//					// When response from server is here we unlock the thread
//					System.out.println("Unlock...");
//					lock.notify();
//				}
//			}
//			
//			@Override
//			public void onStateChange(Controller controller,
//					What oldStatus, What newStatus) {
//				synchronized (lock) {
//					// When response from server is here we unlock the thread
//					System.out.println("Unlock...");
//					lock.notify();
//				}
//			}
//		});
//	
//		
//		hz.setDesiredTemperature(19, HeatingModeEnum.HEATING); TODO manage mode
//		hz.setDesiredTemperature(new DoubleState(19));
//		System.out.println("Wait desired temperature...");
//		try {
//			synchronized (lock) {
//				lock.wait();
//			}
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//		
//		DesiredTemperature r = hz.getDesiredTemperature(HeatingModeEnum.HEATING);
//		Assert.assertNotNull(r);
//		Assert.assertEquals(19, r.getDesiredTemperature(), 0);
//		
//		
//		hz.setDesiredTemperature(24, HeatingModeEnum.HEATING);
//		System.out.println("Wait desired temperature...");
//		try {
//			synchronized (lock) {
//				lock.wait();
//			}
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//		
//		r = hz.getDesiredTemperature(HeatingModeEnum.HEATING);
//		Assert.assertNotNull(r);
//		Assert.assertEquals(24, r.getDesiredTemperature(), 0);
//
//	}
	

	
	@Test
	public void getMeasureTemperatureTest() {
		HeatingZone hz = s.createController(HeatingZone.class, new Where("2"));
	
		System.out.println("getMeasureTemperatureTest");
		// First we just wait 1 second to be sure the controller is initialize 
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		DoubleState m = hz.getMeasureTemperature();
		Assert.assertNotNull(m);
		Assert.assertEquals(20, m.getDoubleValue(), 0);
	}

//	private ValvesStatus v = null;
//	
//	@Test
//	public void getValvesStatusTest() {
//		HeatingZone hz = s.createController(HeatingZone.class, "10");
//		getValvesStatus(hz);
//		s.onDestroy();
//	}
//	
//	public void getValvesStatus(HeatingZone hz) {
//		System.out.println("getValvesStatus");
//		DimensionStatusCallback<ValvesStatus> callback = new DimensionStatusCallback<ValvesStatus>() {
//			public void value(ValvesStatus value) {
//				v=value;
//				synchronized (lock) {
//					System.out.println("Unlock valve status...");
//					lock.notify();
//				}
//			}
//		};
//		
//		hz.getValvesStatus(callback);
//		System.out.println("Wait valve status...");
//		try {
//			synchronized (lock) {
//				lock.wait();
//			}
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//		
//		Assert.assertNotNull(v);
//		Assert.assertEquals(v.getHeatingValveStatus(), ValveStatusEnum.OFF);
//	}
	
	@Test
	public void getSetOffsetTest() {
		HeatingZone hz = s.createController(HeatingZone.class, new Where("3"));
		
		System.out.println("getSetOffsetTest");
		// First we just wait 1 second to be sure the controller is initialize 
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		OffsetState offset = hz.getOffset();		
		Assert.assertNotNull(offset);
		Assert.assertEquals(2, offset.getDegree());
		Assert.assertEquals(Mode.ON, offset.getMode());
	}
}
