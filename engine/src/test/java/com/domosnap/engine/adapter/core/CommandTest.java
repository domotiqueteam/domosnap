package com.domosnap.engine.adapter.core;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.junit.Assert;
import org.junit.Test;

import com.domosnap.engine.adapter.core.Command.Type;
import com.domosnap.engine.controller.light.Light;
import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.controller.what.impl.BooleanState;
import com.domosnap.engine.controller.what.impl.DateState;
import com.domosnap.engine.controller.what.impl.DoubleState;
import com.domosnap.engine.controller.what.impl.IntegerState;
import com.domosnap.engine.controller.what.impl.IpAddressState;
import com.domosnap.engine.controller.what.impl.MacAddressState;
import com.domosnap.engine.controller.what.impl.OnOffState;
import com.domosnap.engine.controller.what.impl.PercentageState;
import com.domosnap.engine.controller.what.impl.PressureUnitState;
import com.domosnap.engine.controller.what.impl.RGBState;
import com.domosnap.engine.controller.what.impl.StringState;
import com.domosnap.engine.controller.what.impl.TemperatureUnitState;
import com.domosnap.engine.controller.what.impl.TimeState;
import com.domosnap.engine.controller.what.impl.UpDownState;
import com.domosnap.engine.controller.what.impl.VersionState;
import com.domosnap.engine.controller.where.Where;

public class CommandTest {

	
	@Test
	public void JsonTest() {
		List<What> l = new ArrayList<>();
		
		DateState dt = new DateState(new Date());
		DoubleState ds = new DoubleState(10);
		IntegerState is = new IntegerState(10);
		IpAddressState ips = new IpAddressState("192.168.1.1");
		MacAddressState mcs = new MacAddressState("FA.19.19.19.19.19");
		PercentageState ps = new PercentageState(50d);
		PressureUnitState pus = PressureUnitState.Pascal();
		RGBState rgbs = new RGBState("FFFFFF");
		StringState ss = new StringState("String");
		TemperatureUnitState tus = TemperatureUnitState.Celsius();
		TimeState ts = new TimeState(12,12,12, TimeZone.getDefault());
		VersionState vs = new VersionState(1,1,1);
		
		l.add(new What("boolean",BooleanState.TRUE));
		l.add(new What("date",dt));
		l.add(new What("double",ds));
		l.add(new What("integer",is));
		l.add(new What("ipaddress",ips));
		l.add(new What("macaddress",mcs));
		l.add(new What("onoff",OnOffState.On()));
		l.add(new What("percent",ps));
		l.add(new What("pressureunit",pus));
		l.add(new What("rgb",rgbs));
		l.add(new What("string",ss));
		l.add(new What("temperatureunit",tus));
		l.add(new What("time",ts));
		l.add(new What("updown",UpDownState.Stop));
		l.add(new What("version",vs));
		
		Command c = new Command(Light.class, l, new Where("protocle://localhost/adress"), Type.COMMAND, null);
		String json = CommandJsonCodec.toJSon(c);
		System.out.println(json);
		Command c2 = CommandJsonCodec.fromJson(json);
		
		Assert.assertEquals(Light.class, c2.getWho());
		Assert.assertEquals(new Where("protocle://localhost/adress"), c2.getWhere());
		Assert.assertEquals(Type.COMMAND, c2.type);
		Assert.assertEquals(null, c2.getSource());
		Assert.assertEquals(BooleanState.TRUE, c2.getWhatList().get(0).getValue());
		System.out.println(dt.getValue().getTime());
		System.out.println(((DateState) c2.getWhatList().get(1).getValue()).getValue().getTime());
		Assert.assertEquals(dt, c2.getWhatList().get(1).getValue());
		Assert.assertEquals(ds, c2.getWhatList().get(2).getValue());
		Assert.assertEquals(is, c2.getWhatList().get(3).getValue());
		Assert.assertEquals(ips, c2.getWhatList().get(4).getValue());
		Assert.assertEquals(mcs, c2.getWhatList().get(5).getValue());
		Assert.assertEquals(OnOffState.On(), c2.getWhatList().get(6).getValue());
		Assert.assertEquals(ps, c2.getWhatList().get(7).getValue());
		Assert.assertEquals(pus, c2.getWhatList().get(8).getValue());
		Assert.assertEquals(rgbs, c2.getWhatList().get(9).getValue());
		Assert.assertEquals(ss, c2.getWhatList().get(10).getValue());
		Assert.assertEquals(tus, c2.getWhatList().get(11).getValue());
		Assert.assertEquals(ts, c2.getWhatList().get(12).getValue());
		Assert.assertEquals(UpDownState.Stop, c2.getWhatList().get(13).getValue());
		Assert.assertEquals(vs, c2.getWhatList().get(14).getValue());
	}
}
