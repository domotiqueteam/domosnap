package com.domosnap.engine.adapter.knx.test;

import java.net.URL;

import org.junit.After;
import org.junit.Before;

import tuwien.auto.calimero.exception.KNXException;
import tuwien.auto.calimero.server.Launcher;
import tuwien.auto.calimero.server.knxnetip.KNXnetIPServer;

public abstract class KNXServerTestCase {
	
	private Launcher knxServer;

	@Before
	public void startKNXServer() {
		
		URL configURL = getClass().getResource("/knxServer-config.xml");
		try {
			knxServer = new Launcher(configURL.toString());
			new Thread(knxServer).start();
		}
		catch (KNXException e) {
			e.printStackTrace();
		}
	}
	
	@After
	public void stopKNXServer() {
		
		if (knxServer != null) {
			KNXnetIPServer server = knxServer.getGateway().getServer();
			knxServer.getGateway().quit();
			server.shutdown();
		}
	}
}
