package com.domosnap.security.cert;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidParameterException;
import java.security.KeyPair;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.Date;

import sun.security.x509.AlgorithmId;
import sun.security.x509.CertificateAlgorithmId;
import sun.security.x509.CertificateIssuerName;
import sun.security.x509.CertificateSerialNumber;
import sun.security.x509.CertificateSubjectName;
import sun.security.x509.CertificateValidity;
import sun.security.x509.CertificateVersion;
import sun.security.x509.CertificateX509Key;
import sun.security.x509.X500Name;
import sun.security.x509.X509CertImpl;
import sun.security.x509.X509CertInfo;

/**
 * <code>
 * KeyPairGenerator keyGen = KeyPairGenerator.getInstance(KeyTypeName.RSA);
 * keyGen.initialize(2048);
 * 
 * KeyPair rootKeys = keyGen.generateKeyPair();
 * X500Name root = CertificateUtil.createX500Name("RootCA", "Google Inc.", "Alphabet", "Menlo Park", "California", "US");
 * X509Certificate rootCA = CertificateUtil.createSelfSignedCertificate(rootKeys, root, 365, SignatureTypeName.SHA256withRSA);
 * CertificateUtil.writeCertificate(rootCA, "/home/user/rootCA.cert");
 * 
 * KeyPair myKeys = keyGen.generateKeyPair();
 * X500Name myInfo = CertificateUtil.createX500Name("MyCertificate", null, "My Company", "Seattle", "Washington", "US");
 * X509Certificate myCert = SecurityUtil.createAndSignCertificate(myKeys, myInfo, 365, SignatureTypeName.SHA256withRSA, rootCA, rootKeys);
 * CertificateUtil.writeCertificate(myCert, "/home/user/myCert.cert");
 * </code>
 * 
 * @author Olivier
 */
public class CertificateUtil {
	
	private CertificateUtil() {
	}

	// CN=cName, OU=orgUnit, O=org, L=city, S=state, C=countryCode
	public static X500Name createX500Name(String commonName, String organizationUnit, String organizationName, String localityName, String stateName, String country) {
		
		StringBuilder dn = new StringBuilder();
		if (commonName != null) {
			dn.append("CN=").append(commonName);
		}
		if (organizationUnit != null) {
			dn.append(",OU=").append(organizationUnit);
		}
		if (organizationName != null) {
			dn.append(",O=").append(organizationName);
		}
		if (localityName != null) {
			dn.append(",L=").append(localityName);
		}
		if (stateName != null) {
			dn.append(",S=").append(stateName);
		}
		if (country != null) {
			dn.append(",C=").append(country);
		}
		
		String dnValue;
		if (dn.charAt(0) == ',') {
			dnValue = dn.substring(1);
		} else {
			dnValue = dn.toString();
		}
		
		X500Name result = null;
		try {
			result = new X500Name(dnValue);
		} catch (IOException e) {
			throw new InvalidParameterException("Invalid ");
		}
		return result;
	}
	
	/**
	 * Creates a self-signedcertificate.
	 * 
	 * @param keyPair The key-pair of the certificate owner
	 * @param owner The X500Name info of the owner
	 * @param days The certificate validity in days
	 * @param algorithm The algorithm name used to sign the certificate
	 * @return
	 * @throws CertificateException
	 */
	public static X509Certificate createSelfSignedCertificate(KeyPair keyPair, X500Name owner, int days, String algorithm) throws CertificateException  {

		X509CertImpl cert = null;
		try {
			X509CertInfo info = createCertInfo(keyPair, owner, owner, days, algorithm);
			
			// Create and sign the certificate
			cert = new X509CertImpl(info);
			cert.sign(keyPair.getPrivate(), algorithm);
		}
		catch (Exception e) {
			throw new CertificateException("Unable to create the self-certificate.", e);
		}
		return cert;
	}
	
	/**
	 * Creates a certificate and sign it with another one as issuer.
	 * 
	 * @param keyPair The key-pair of the certificate owner
	 * @param owner The X500Name info of the owner
	 * @param days The certificate validity in days
	 * @param algorithm The algorithm name used to sign the certificate
	 * @param issuer The X500Name info of the issuer
	 * @param issuerKeys The key-pair of the certificate issuer
	 * @return
	 * @throws CertificateException
	 */
	public static X509Certificate createAndSignCertificate(KeyPair keyPair, X500Name owner, int days, String algorithm, X509Certificate issuer, KeyPair issuerKeys) throws CertificateException {
		
		// Check that the public key of the issuer matches the public key of the certificate
		if (! issuer.getPublicKey().equals(issuerKeys.getPublic())) {
			throw new IllegalArgumentException("The issuer certificate does not match with the provided issuer keys");
		}
		
		X509CertImpl cert = null;
		try {
			X500Name issuerDN = new X500Name(issuer.getIssuerDN().toString());
			X509CertInfo info = createCertInfo(keyPair, owner, issuerDN, days, algorithm);
			
			// Create and sign the certificate
			cert = new X509CertImpl(info);
			cert.sign(issuerKeys.getPrivate(), algorithm);
		}
		catch (Exception e) {
			throw new CertificateException("Unable to create the certificate.", e);
		}
		return cert;
	}
	
	public static void writeCertificate(X509Certificate certificate, String file) {
		
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(file);
			fos.write("-----BEGIN CERTIFICATE-----\n".getBytes());
			fos.write(Base64.getMimeEncoder().encode(certificate.getEncoded()));
			fos.write("-----END CERTIFICATE-----\n".getBytes());
		}
		catch (Exception e) {
		}
		finally {
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
				}
			}
		}
	}
	
	private static X509CertInfo createCertInfo(KeyPair keyPair, X500Name owner, X500Name issuer, int days, String algorithm) throws CertificateException {
		
		// Create the certificate validity
		if (days < 1) {
			days = 365; // Default to one year
		}
		Date from = new Date();
		Date to = new Date(from.getTime() + days * 86400000l); // 1 day = 86 400 000 milliseconds
				
		X509CertInfo info = new X509CertInfo();
		try {
			info.set(X509CertInfo.VERSION, new CertificateVersion(CertificateVersion.V3));
			info.set(X509CertInfo.SERIAL_NUMBER, new CertificateSerialNumber(new BigInteger(64, new SecureRandom())));
			info.set(X509CertInfo.ALGORITHM_ID, new CertificateAlgorithmId(AlgorithmId.get(algorithm)));
			info.set(X509CertInfo.VALIDITY, new CertificateValidity(from, to));
			info.set(X509CertInfo.KEY, new CertificateX509Key(keyPair.getPublic()));
			
			try {
				info.set(X509CertInfo.ISSUER, new CertificateIssuerName(owner));
				info.set(X509CertInfo.SUBJECT, new CertificateSubjectName(issuer));
			}
			catch (CertificateException ce) { // Since JDK-1.8
				info.set(X509CertInfo.ISSUER, owner);
				info.set(X509CertInfo.SUBJECT, issuer);
			}
		}
		catch (Exception e) {
			throw new CertificateException("Unable to create certificate info with the specified parameters.", e);
		}
		return info;
	}
}
