package com.domosnap.security;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


public interface SignatureTypeName {

	public static final String DSA = "DSA";
	public static final String DSAWithSHA1 = "DSAWithSHA1";
	public static final String DSS = "DSS";
	
	public static final String MD2withRSA = "MD2withRSA";
	public static final String MD5withRSA = "MD5withRSA";
	public static final String MD5andSHA1withRSA = "MD5andSHA1withRSA";
	
	public static final String NONEwithDSA = "NONEwithDSA";
	public static final String NONEwithECDSA = "NONEwithECDSA";
	public static final String NONEwithRSA = "NONEwithRSA";
	
	public static final String SHA1withDSA = "SHA1withDSA";
	public static final String SHA1withECDSA = "SHA1withECDSA";
	public static final String SHA1withRSA = "SHA1withRSA";
	public static final String SHA224withDSA = "SHA224withDSA";
	public static final String SHA224withECDSA = "SHA224withECDSA";
	public static final String SHA224withRSA = "SHA224withRSA";
	public static final String SHA256withDSA = "SHA256withDSA";
	public static final String SHA256withECDSA = "SHA256withECDSA";
	public static final String SHA256withRSA = "SHA256withRSA";
	public static final String SHA384withECDSA = "SHA384withECDSA";
	public static final String SHA384withRSA = "SHA384withRSA";
	public static final String SHA512withECDSA = "SHA512withECDSA";
	public static final String SHA512withRSA = "SHA512withRSA";
	
	public static final String RawDSA = "RawDSA";
}
