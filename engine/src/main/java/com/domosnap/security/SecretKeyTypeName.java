package com.domosnap.security;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


public interface SecretKeyTypeName {

	public static final String DES = "DES";
	public static final String DESede = "DESede";
	
	public static final String PBKDF2WithHmacSHA1 = "PBKDF2WithHmacSHA1";
	public static final String PBKDF2WithHmacSHA224 = "PBKDF2WithHmacSHA224";
	public static final String PBKDF2WithHmacSHA256 = "PBKDF2WithHmacSHA256";
	public static final String PBKDF2WithHmacSHA384 = "PBKDF2WithHmacSHA384";
	public static final String PBKDF2WithHmacSHA512 = "PBKDF2WithHmacSHA512";
	
	public static final String PBE = "PBE";
	public static final String PBEWithMD5AndDES = "PBEWithMD5AndDES";
	public static final String PBEWithMD5AndTripleDES = "PBEWithMD5AndTripleDES";
	public static final String PBEWithHmacSHA1AndAES_128 = "PBEWithHmacSHA1AndAES_128";
	public static final String PBEWithHmacSHA224AndAES_128 = "PBEWithHmacSHA224AndAES_128";
	public static final String PBEWithHmacSHA256AndAES_128 = "PBEWithHmacSHA256AndAES_128";
	public static final String PBEWithHmacSHA384AndAES_128 = "PBEWithHmacSHA384AndAES_128";
	public static final String PBEWithHmacSHA512AndAES_128 = "PBEWithHmacSHA512AndAES_128";
	public static final String PBEWithHmacSHA1AndAES_256 = "PBEWithHmacSHA1AndAES_256";
	public static final String PBEWithHmacSHA224AndAES_256 = "PBEWithHmacSHA224AndAES_256";
	public static final String PBEWithHmacSHA256AndAES_256 = "PBEWithHmacSHA256AndAES_256";
	public static final String PBEWithHmacSHA384AndAES_256 = "PBEWithHmacSHA384AndAES_256";
	public static final String PBEWithHmacSHA512AndAES_256 = "PBEWithHmacSHA512AndAES_256";
	public static final String PBEWithSHA1AndDESede = "PBEWithSHA1AndDESede";
	public static final String PBEWithSHA1AndRC2_40 = "PBEWithSHA1AndRC2_40";
	public static final String PBEWithSHA1AndRC4_40 = "PBEWithSHA1AndRC4_40";
	public static final String PBEWithSHA1AndRC2_128 = "PBEWithSHA1AndRC2_128";
	public static final String PBEWithSHA1AndRC4_128 = "PBEWithSHA1AndRC4_128";
	
	public static final String TripleDES = "TripleDES";
}
