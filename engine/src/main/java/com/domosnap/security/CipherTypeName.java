package com.domosnap.security;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


public interface CipherTypeName {

	public static final String AES = "AES";
	public static final String AES_128_CBC_NoPadding = "AES_128/CBC/NoPadding";
	public static final String AES_128_CFB_NoPadding = "AES_128/CFB/NoPadding";
	public static final String AES_128_ECB_NoPadding = "AES_128/ECB/NoPadding";
	public static final String AES_128_GCM_NoPadding = "AES_128/GCM/NoPadding";
	public static final String AES_128_OFB_NoPadding = "AES_128/OFB/NoPadding";
	public static final String AES_192_CBC_NoPadding = "AES_192/CBC/NoPadding";
	public static final String AES_192_CFB_NoPadding = "AES_192/CFB/NoPadding";
	public static final String AES_192_ECB_NoPadding = "AES_192/ECB/NoPadding";
	public static final String AES_192_GCM_NoPadding = "AES_192/GCM/NoPadding";
	public static final String AES_192_OFB_NoPadding = "AES_192/OFB/NoPadding";
	public static final String AES_256_CBC_NoPadding = "AES_256/CBC/NoPadding";
	public static final String AES_256_CFB_NoPadding = "AES_256/CFB/NoPadding";
	public static final String AES_256_ECB_NoPadding = "AES_256/ECB/NoPadding";
	public static final String AES_256_GCM_NoPadding = "AES_256/GCM/NoPadding";
	public static final String AES_256_OFB_NoPadding = "AES_256/OFB/NoPadding";
	public static final String AESWrap = "AESWrap";
	public static final String AESWrap_128 = "AESWrap_128";
	public static final String AESWrap_192 = "AESWrap_192";
	public static final String AESWrap_256 = "AESWrap_256";
	
	public static final String ARCFOUR = "ARCFOUR";
	public static final String Blowfish = "Blowfish";
	
	public static final String DES = "DES";
	public static final String DESede = "DESede";
	public static final String DESedeWrap = "DESedeWrap";
	
	public static final String PBEWithMD5AndDES = "PBEWithMD5AndDES";
	public static final String PBEWithMD5AndTripleDES = "PBEWithMD5AndTripleDES";
	public static final String PBEWithHmacSHA1AndAES_128 = "PBEWithHmacSHA1AndAES_128";
	public static final String PBEWithHmacSHA224AndAES_128 = "PBEWithHmacSHA224AndAES_128";
	public static final String PBEWithHmacSHA256AndAES_128 = "PBEWithHmacSHA256AndAES_128";
	public static final String PBEWithHmacSHA384AndAES_128 = "PBEWithHmacSHA384AndAES_128";
	public static final String PBEWithHmacSHA512AndAES_128 = "PBEWithHmacSHA512AndAES_128";
	public static final String PBEWithHmacSHA1AndAES_256 = "PBEWithHmacSHA1AndAES_256";
	public static final String PBEWithHmacSHA224AndAES_256 = "PBEWithHmacSHA224AndAES_256";
	public static final String PBEWithHmacSHA256AndAES_256 = "PBEWithHmacSHA256AndAES_256";
	public static final String PBEWithHmacSHA384AndAES_256 = "PBEWithHmacSHA384AndAES_256";
	public static final String PBEWithHmacSHA512AndAES_256 = "PBEWithHmacSHA512AndAES_256";
	public static final String PBEWithSHA1AndDESede = "PBEWithSHA1AndDESede";
	public static final String PBEWithSHA1AndRC2_40 = "PBEWithSHA1AndRC2_40";
	public static final String PBEWithSHA1AndRC4_40 = "PBEWithSHA1AndRC4_40";
	public static final String PBEWithSHA1AndRC2_128 = "PBEWithSHA1AndRC2_128";
	public static final String PBEWithSHA1AndRC4_128 = "PBEWithSHA1AndRC4_128";
	
	public static final String RC2 = "RC2";
	public static final String RC4 = "RC4";
	public static final String RSA = "RSA";
	public static final String RSA_ECB_PKCS1Padding = "RSA/ECB/PKCS1Padding";
	
	public static final String TripleDES = "TripleDES";
}
