package com.domosnap.security.provider;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


public enum SecurityProviderTypeName {

	CIPHER("Cipher"),
	
	KEY_FACTORY("KeyFactory"),
	
	KEYPAIR_GENERATOR("KeyPairGenerator"),
	
	KEYSTORE("KeyStore"),
	
	MESSAGE_DIGEST("MessageDigest"),

	SECRETKEY_FACTORY("SecretKeyFactory"),
	
	SIGNATURE("Signature");
	
	private String type;
	
	private SecurityProviderTypeName(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
	
	public static SecurityProviderTypeName fromType(String type) {
		
		if (CIPHER.getType().equalsIgnoreCase(type)) {
			return CIPHER;
		}
		else if (KEY_FACTORY.getType().equalsIgnoreCase(type)) {
			return KEY_FACTORY;
		}
		else if (KEYPAIR_GENERATOR.getType().equalsIgnoreCase(type)) {
			return KEYPAIR_GENERATOR;
		}
		else if (KEYSTORE.getType().equalsIgnoreCase(type)) {
			return KEYSTORE;
		}
		else if (MESSAGE_DIGEST.getType().equalsIgnoreCase(type)) {
			return MESSAGE_DIGEST;
		}
		else if (SECRETKEY_FACTORY.getType().equalsIgnoreCase(type)) {
			return SECRETKEY_FACTORY;
		}
		else if (SIGNATURE.getType().equalsIgnoreCase(type)) {
			return SIGNATURE;
		}
		return null;
	}
}
