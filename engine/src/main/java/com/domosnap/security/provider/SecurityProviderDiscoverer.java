package com.domosnap.security.provider;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.security.Provider;
import java.security.Security;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * <code>
 * SecurityProviderDiscoverer discoverer = SecurityProviderDiscoverer.getInstance();
 * for (SecurityProviderType providerType : discoverer.searchProviders()) {
 * 
 * 		System.out.println(providerType.getName());
 * 			for (String algorithm : providerType.getAvailableAlgorithms()) {
 * 				System.out.println("\t"+ algorithm);
 * 			}
 * 		}
 * }
 * </code>
 * 
 * @author Olivier
 */
public class SecurityProviderDiscoverer {
	
	private static final SecurityProviderDiscoverer instance = new SecurityProviderDiscoverer();
	private Map<SecurityProviderTypeName, SecurityProviderType> providers;
	
	private SecurityProviderDiscoverer() {
		providers = new HashMap<SecurityProviderTypeName, SecurityProviderType>();
	}
	
	public static SecurityProviderDiscoverer getInstance() {
		return instance;
	}
	
	public SecurityProviderType getProvidersType(SecurityProviderTypeName name) {
		searchProviders();
		return providers.get(name);
	}
	
	public Collection<SecurityProviderType> searchProviders() {
		return searchProviders(false);
	}
	
	public Collection<SecurityProviderType> searchProviders(boolean forceSearch) {
		
		if (forceSearch) {
			providers.clear();
		}
		
		if (providers.isEmpty()) {
			for (Provider p : Security.getProviders()) {
				for (Object o : p.keySet()) {
					
					String providerName = o.toString();
					StringTokenizer st = new StringTokenizer(providerName, ".");
					while (st.hasMoreTokens()) {
						
						// Get the provider type
						String token = st.nextToken();
						if ("Alg".equals(token) && providerName.startsWith("Alg.Alias.")) {
							st.nextToken();
							token = st.nextToken();
						}
						
						SecurityProviderTypeName typeName = SecurityProviderTypeName.fromType(token);
						if (typeName == null) {
							continue;
						}
						
						SecurityProviderType provider = providers.get(typeName);
						if (provider == null) {
							provider = new SecurityProviderType(typeName.getType());
							providers.put(typeName, provider);
						}
						
						// Get the algorithm name
						StringBuilder algo = new StringBuilder();
						while (st.hasMoreTokens()) {
							
							token = st.nextToken();
							int spacePos = token.indexOf(" ");
							if (spacePos == -1) {
								algo.append(token);
								if (st.hasMoreTokens()) {
									algo.append(".");
								}
							} else {
								algo.append(token.substring(0, spacePos));
								break;
							}
						}
						provider.addAlgorithmName(algo.toString());
					}
				}
			}
		}
		return Collections.unmodifiableCollection(providers.values());
	}
}
