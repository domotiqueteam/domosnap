package com.domosnap.security.provider;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @author Olivier
 */
class SecurityProviderType {
	
	private String name;
	private Set<String> availableAlgorithms;
	
	SecurityProviderType(String name) {
		this.name = name;
		availableAlgorithms = new HashSet<String>();
	}
	
	public String getName() {
		return name;
	}
	
	public Set<String> getAvailableAlgorithms() {
		return Collections.unmodifiableSet(availableAlgorithms);
	}
	
	void addAlgorithmName(String name) {
		availableAlgorithms.add(name);
	}
}
