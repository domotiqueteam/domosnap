package com.domosnap.engine;

import java.io.IOException;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;

/**
 * Since we use the engine on android or other platform just use a little internal log system...
 * Sorry, it is faster than adapt log4j, slf4j, jdklogger,....
 * @author arnaud
 *
 */
public class Log {

	private static ArrayList<Appender> appenderList = new ArrayList<Appender>();
	
	private static Properties p = new Properties();
	static {
		try {
			p.load(Log.class.getResourceAsStream("/log.properties"));
		} catch (IOException e) {
			System.out.println("Impossible to load log configuration. Use default.");
		}
	}
	
	public boolean finest = false;
	public boolean fine = false;
	public boolean debug = false;
	public boolean error = false;
	public boolean info = false;
	public boolean warning = false;
	public boolean logMonitor = false;
	public boolean logCommand = false;
	private String tag;
	
	public Log(String tag) {
		this.tag = tag;
	
		String defaultt = p.getProperty("all", "true");
		String level = p.getProperty(tag, defaultt);
		switch (level) {
		case "true":
		case "finest":
			finest = true;
			fine = true;
			debug = true;
			error = true;
			info = true;
			warning = true;
			break;
		case "fine":
			finest = false;
			fine = true;
			debug = true;
			error = true;
			info = true;
			warning = true;
			break;
		case "debug":
			finest = false;
			fine = false;
			debug = true;
			error = true;
			info = true;
			warning = true;
			break;
		case "error":
			finest = false;
			fine = false;
			debug = false;
			error = true;
			info = true;
			warning = true;
			break;
		case "info":
			finest = false;
			fine = false;
			debug = false;
			error = false;
			info = true;
			warning = true;
			break;
		case "warning":
			finest = false;
			fine = false;
			debug = false;
			error = false;
			info = false;
			warning = true;
			break;
		default:
			finest = false;
			fine = false;
			debug = false;
			error = false;
			info = false;
			warning = false;
			break;
		}
		
		logMonitor = Boolean.valueOf(p.getProperty("logMonitor", "false"));
		logCommand = Boolean.valueOf(p.getProperty("logCommand", "false"));
	}
	
	public static void addAppender(Appender appender) {
		appenderList.add(appender);
	}
	
	public static void removeAppender(Appender appender) {
		appenderList.remove(appender);
	}
	
	/**
	 * Device means for log occuring in the device.
	 * Monitor means for log occuring in the monitor stream.
	 * Command means for log occuring in the command stream.
	 * Server means for log occruting in the server sig (gateway) log.
	 * Other means for other log.
	 *
	 */
	public enum Session {
		Device, Monitor, Command, Server, Other;
	}
	
	public void log(Level level, Session session, String log, int pad) {
		
		if (Log.Session.Monitor.name().equals(tag) && !logMonitor) {
			return; // not log when log monitor is false;
		}
		
		if (Log.Session.Command.name().equals(tag) && !logCommand) {
			return; // not log when log command is false;
		}
		
		if (debug && level.intValue()>Level.ALL.intValue() ||
				info && level.intValue()>Level.INFO.intValue() ||
				info && level.intValue()>Level.WARNING.intValue() ||
				error && level.intValue()>Level.SEVERE.intValue() ||
				finest && level.intValue()>Level.FINEST.intValue() ||
				warning && level.intValue()>Level.WARNING.intValue() ||
				fine && level.intValue()>Level.FINE.intValue())
		{

			StringBuilder sb = new StringBuilder("[");
					
			// [time] - [tag] - [session] - [Thread] - pad    log message
			sb.append(getTime()).append("] - ").append(pad(tag, 30)).append(" - ").append("[").append(pad(session.name(), 10)).append("]").append("-[").append(pad(Thread.currentThread().getName(), 17)).append("] : ");
			for (int i= 0; i<pad;i++) {
				sb.append("   ");
			}
			sb.append(log);
			
			String result = sb.toString();
			System.out.println(result);
			for (Appender appender : appenderList) {
				appender.log(tag, level, result);
			}
		}
	}
	
	public void finest(Session session, String log) {
		finest(session, log, 0);
	}

	public void finest(Session session, String log, int pad) {
		log(Level.FINEST, session, log, pad);
	}
	
	public void fine(Session session, String log) {
		fine(session, log, 0);
	}
	
	public void fine(Session session, String log, int pad) {
		log(Level.FINE, session, log, pad);
	}

	public void severe(Session session, String log) {
		severe(session, log,0);
	}

	public void severe(Session session, String log, int pad) {
		log(Level.SEVERE, session, log, pad);
	}
	
	public void info(Session session, String log) {
		info(session, log, 0);
	}
	
	public void info(Session session, String log, int pad) {
		log(Level.INFO, session, log, pad);
	}
	
	public void warning(Session session, String log) {
		warning(session, log, 0);
	}
	
	public void warning(Session session, String log, int pad) {
		log(Level.WARNING, session, log, pad);
	}

	public static Properties getConfig() {
		return p;
	}
	
	private String getTime() {
		return new SimpleDateFormat("hh:mm:ss:SSSS").format(new Date());
	}
	
	private String pad(String s, int pad) {
		for (;s.length() < pad;) {
			s = s.concat(" ");
		}
		return s;
	}
}
