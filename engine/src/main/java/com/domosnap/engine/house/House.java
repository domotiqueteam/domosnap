package com.domosnap.engine.house;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.domosnap.engine.controller.Controller;

/**
 * House is a contains all {@link Controller}.
 * 
 * It is subdivided in two categories:
 * 
 * <ul>
 * <li>{@link Group} that is a physical organization of {@link Controller}: it isn't possible to modify it and should be only use by technical layer. FOr user it is read only.</li>
 * <li>{@link Label} that is a logical organization of {@link Controller}: user can organized them as he want.</li>
 * </ul>
 * 
 * @see Group
 * @see Label
 */
public class House implements Serializable {

	public static final String JSON_TITLE = "title";
	public static final String JSON_ID = "id";
	public static final String JSON_LABELS = "labels";
	public static final String JSON_GROUPS = "groups";
	/** serial uid */
	private static final long serialVersionUID = 1L;
	private List<Group> groups; // Physical model
	private List<Label> labels; // Logical model
	

	public List<Group> getGroups() {
		if (groups == null) {
			groups = new ArrayList<Group>();
		}
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	public List<Label> getLabels() {
		if (labels == null) {
			labels = new LabelList();
		}
		return labels;
	}

	public void setLabels(List<Label> labels) {
		this.labels = new LabelList();
		this.labels.addAll(labels);
	}





	/**
	 * List of Label. Manage the link with house.
	 */
	private class LabelList extends ArrayList<Label> {

		/** serial UID*/
		private static final long serialVersionUID = 1L;

		
		protected LabelList() {
		}

		@Override
		public boolean add(Label e) {
			return super.add(e);
		}

		@Override
		public void add(int index, Label e) {
			super.add(index, e);
		}

		@Override
		public Label remove(int index) {
			Label e = super.remove(index);
			return e;
		}

		@Override
		public boolean remove(Object o) {
			boolean result = super.remove(o);
			return result;
		}

		@Override
		public void clear() {
			super.clear();
		}

		@Override
		public boolean addAll(Collection<? extends Label> c) {
			return super.addAll(c);
		}

		@Override
		public boolean addAll(int index, Collection<? extends Label> c) {
			return super.addAll(index, c);
		}

		@Override
		protected void removeRange(int fromIndex, int toIndex) {
			super.removeRange(fromIndex, toIndex);
		}
	}
}
