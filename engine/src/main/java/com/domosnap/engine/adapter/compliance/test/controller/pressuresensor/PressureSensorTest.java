package com.domosnap.engine.adapter.compliance.test.controller.pressuresensor;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import org.junit.Assert;

import com.domosnap.engine.adapter.ControllerAdapter;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.ControllerChangeListener;
import com.domosnap.engine.controller.pressure.PressureSensor;
import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.controller.where.Where;

public class PressureSensorTest {

	private Object lock = new Object();

	public interface IPressureSensorTest {
		public void statusPressureTest() throws InterruptedException;
	}
	
	public void statusPressureTest(ControllerAdapter cs, Where w) throws InterruptedException {
		
		final PressureSensor pressureSensor = cs.createController(PressureSensor.class, w);
		
		// Listener will make us availabe to wait response from server
		pressureSensor.addControllerChangeListener(new ControllerChangeListener() {

			@Override
			public void onStateChangeError(Controller controller,
					What oldStatus, What newStatus) {
				synchronized (lock) {
					// When response from server is here we unlock the thread
					System.out.println("Unlock...");
					lock.notify();
				}
			}
			
			@Override
			public void onStateChange(Controller controller,
					What oldStatus, What newStatus) {
				synchronized (lock) {
					// When response from server is here we unlock the thread
					System.out.println("Unlock...");
					lock.notify();
				}
			}
		});

		// First we just wait 1 second to be sure the controller is initialize 
		Thread.sleep(1000l);

		Assert.assertNotNull(pressureSensor.getPressure());
		Assert.assertTrue(0 < pressureSensor.getPressure().getValue());

		System.out.println("Finish...");
	}
}
