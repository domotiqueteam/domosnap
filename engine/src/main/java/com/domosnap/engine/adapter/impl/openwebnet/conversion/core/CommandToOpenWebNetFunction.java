package com.domosnap.engine.adapter.impl.openwebnet.conversion.core;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.UnSupportedStateException;
import com.domosnap.engine.adapter.core.UnknownStateException;
import com.domosnap.engine.adapter.core.UnknownStateValueException;
import com.domosnap.engine.adapter.core.UnknownWhoException;
import com.domosnap.engine.adapter.impl.openwebnet.connector.OpenWebNetConstant;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.dimension.DimensionStatus;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.dimension.DimensionValue;
import com.domosnap.engine.controller.gateway.Gateway;
import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.controller.what.impl.BooleanState;

import io.reactivex.functions.Function;

public class CommandToOpenWebNetFunction implements Function<Command, Publisher<String>> {

	private Log log = new Log(this.getClass().getSimpleName());
	
	@Override
	public Publisher<String> apply(final Command command) throws Exception {
		return new Publisher<String>() {
			@Override
			public void subscribe(Subscriber<? super String> arg0) {
				final List<String> converted = createMessage(command);
				if (converted == null) {
					if(log.finest) {
						log.fine(Session.Command, "No translation command for : " + command.toString(), 1);
					}
				} else {
				for (String string : converted) {
					if(log.finest) {
						log.fine(Session.Command, "Lunch command.", 1);
					}
					arg0.onNext(string);
				}
				}
			}
		};
	}

	/**
	 * Create the open message for action or status.
	 * 
	 * @return open web net message.
	 */
	protected final List<String> createMessage(Command command) {
		if (command.getWhere() == null || command.getWhere().getPath() == null) {
			if (!Gateway.class.equals(command.getWho())) {
				log.fine(Session.Command, "Command must contain a where.", 1);
				throw new IllegalArgumentException("Command must contain a where");
			}
		}
		List<String> commandList  = new ArrayList<String>();
		for (What what : command.getWhatList()) {
			try {
				Converter converter = OpenWebNetConverterRegistry.getConverter(command.getWho());
				String who =  converter.getOpenWebWho();
				String where = command.getWhere().getPath();
				
				if (command.isWriteCommand()) {
					if (converter.isDimension(what)) { // Dimension
						List<DimensionStatus<?>> dimensionStatus = converter.convertDimension(what, command.getSource().getStateMap());
						StringBuilder sb = new StringBuilder();
						
						for (DimensionStatus<?> dim : dimensionStatus) {
							for (DimensionValue dimension : dim.getValueList()) {
								sb.append(dimension.getValue());
								sb.append(OpenWebNetConstant.DIMENSION_SEPARATOR);
							}
							sb.setLength(sb.length() - 1);
							commandList.add(MessageFormat.format(
									OpenWebNetConstant.DIMENSION_COMMAND,
									new Object[] { who, where, dim.getCode(), sb.toString() }
							));
						}
					} else { 
						List<String> whatList = converter.convert(what, command.getSource().getStateMap());
						for (String stringWhat : whatList) {
							commandList.add(MessageFormat.format(
									OpenWebNetConstant.COMMAND,
									new Object[] {
											who,
											stringWhat,
											where 
									}));
						}
					}
				} else { // Statut request
					if (converter.isDimension(what)) { // Dimension
						List<DimensionStatus<?>> dimensionStatus = converter.convertDimension(what, command.getSource().getStateMap());
						for (DimensionStatus<?> dim : dimensionStatus) {
							commandList.add(MessageFormat.format(
									OpenWebNetConstant.DIMENSION_STATUS, new Object[] {
											who, where, dim.getCode() }));
						}
					} else {
						if ("status".equals(what.getName())) {
							commandList.add(MessageFormat.format(OpenWebNetConstant.STATUS,
								new Object[] { who, where }));
						} else if("reachable".equals(what.getName())) { // TODO ceci doit etre dans le light converter!!!!
							command.getSource().getStateMap().put("reachable", BooleanState.TRUE);
						}
					}
				}
			} catch (UnknownStateException e) {
				throw new IllegalArgumentException("Controller status unknown [" + what.getName() + "]");
			} catch (UnknownWhoException e) {
				throw new IllegalArgumentException("Controller Who unknown. [" + command.getWho() + "]");
			} catch (UnSupportedStateException e) {
				log.fine(Session.Command,"Controller status unsupported. [" + what.getName() + "]", 1);
				return null;
			} catch (UnknownStateValueException e) {
				throw new IllegalArgumentException("Controller status value unknown [" + what.getValue() != null ? what.getValue().toString() : "null" + "]");
			}
		}
		return commandList;
	}
}
