package com.domosnap.engine.adapter.impl.hue;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import com.domosnap.engine.adapter.core.AbstractControllerAdapter;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.ConnectionListener;
import com.domosnap.engine.adapter.core.ScanListener;
import com.domosnap.engine.adapter.core.UnknownControllerListener;
import com.domosnap.engine.adapter.impl.hue.conversion.CommandToHueRequestFunction;
import com.domosnap.engine.adapter.impl.hue.conversion.HueResponseToCommandFunction;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.light.Light;
import com.domosnap.engine.controller.where.URITools;
import com.domosnap.engine.controller.where.Where;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.processors.FlowableProcessor;
import io.reactivex.processors.PublishProcessor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class HueControllerAdapter extends AbstractControllerAdapter {

	public static String SCHEMA = "hue";
	
	private HueMonitorPublisher monitor;
	private HueResponseToCommandFunction hueResponseToCommandFunction;
	private HueCommanderConsumer commander;

	private OkHttpClient client = new OkHttpClient();

	public HueControllerAdapter() {
		super(SCHEMA + "://:unknown@localhost");
	}
	
	public HueControllerAdapter(String uri) {
		super(uri);
	}

	@Deprecated
	public HueControllerAdapter(String host, int port, String user) {
		super(SCHEMA + "://"+ user + "@" + host + ":" + port);
	}

	@Override
	public void connect() {
		if (!commander.isConnected()) {
			commander.connect();
		}
		if (!monitor.isConnected()) {
			monitor.connect();
		}
	}
	
	@Override
	public void connect(String uri) {
		// TODO factoriser avec le code qui set l'uri avec celui des constructeurs
		if (uri.indexOf("://") == -1) {
			uri = SCHEMA + "://" + uri;
		}
		
		this.uri = URI.create(uri);
	
		commander.setIp(getIp());
		commander.setPort(getPort());
		commander.setUser(getUser());
		
		monitor.setIp(getIp());
		monitor.setPort(getPort());
		monitor.setUser(getUser());
		
		hueResponseToCommandFunction.setIp(getIp());
		hueResponseToCommandFunction.setPort(getPort());
		hueResponseToCommandFunction.setUser(getUser());
		
		connect();
	}

	@Override
	public void disconnect() {
		monitor.disconnect();
		commander.disconnect();
	}

	/**
	 * Return true if all commander are connected (else false)
	 * 
	 * @return true if all commander are connected (else false)
	 */
	@Override
	public boolean isConnected() {
		return (commander == null ? false :  commander.isConnected())
				&& (monitor == null ? false : monitor.isConnected());
	}

	@Override
	public void addCommanderConnectionListener(ConnectionListener listener) {
		commander.addConnectionListener(listener);
	}

	@Override
	public void removeCommanderConnectionListener(ConnectionListener listener) {
		commander.removeConnectionListener(listener);
	}

	@Override
	public void addMonitorConnectionListener(ConnectionListener listener) {
		monitor.addConnectionListener(listener);
	}

	@Override
	public void removeMonitorConnectionListener(ConnectionListener listener) {
		monitor.removeConnectionListener(listener);
	}

	@Override
	public void addUnknowControllerListener(UnknownControllerListener arg0) {
		// TODO monitor.addUnknownControllerListener(arg0);
	}

	@Override
	public void removeUnknowControllerListener(UnknownControllerListener arg0) {
		// TODO this.getHueMonitor().removeUnknownControllerListener(arg0);
	}


	protected String getIp() {
		return uri.getHost();
	}
	
	protected int getPort() {
		return uri.getPort();
	}
	
	protected String getUser() {
		return uri.getUserInfo();
	}
	
	@Override
	public void scan(ScanListener listener) {

		Request request = new Request.Builder().url("http://" + getIp() + ":" + getPort() + "/api/" + getUser() + "/lights")
				.build();
		try {
			
			Response response = client.newCall(request).execute();
			
			String json = response.body().string();
			Gson gson = new Gson();
			JsonObject lights = gson.fromJson(json, JsonObject.class);
			int counter = 0;
			
			for (Entry<String, JsonElement> entryLight : lights.entrySet()) {

				String lightKey = entryLight.getKey();
				Where where = new Where(URITools.generateURI(SCHEMA, getUser(), getIp(), getPort(), lightKey));
				Light light = createController(Light.class, where);

				listener.foundController(light);
				// currentMapLight.get(lightKey).get("state").get("on")
				listener.progess(counter++ * 100 / lights.size());
			}
			listener.progess(100);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		listener.scanFinished();

	}

	@Override
	protected Flowable<Command> createMonitorStream() {
		monitor = new HueMonitorPublisher(getIp(), getPort(), getUser());
		
		hueResponseToCommandFunction = new HueResponseToCommandFunction(getIp(), getPort(), getUser());
		Flowable<Command> monitorStream = FlowableProcessor.create(monitor, BackpressureStrategy.DROP)
				.flatMap(hueResponseToCommandFunction).share();
		return monitorStream;
	}

	@Override
	protected PublishProcessor<Command> createCommandStream() {
		PublishProcessor<Command> commanderStream = PublishProcessor.create();

		this.commander = new HueCommanderConsumer(getIp(), getPort(), getUser());
		commanderStream.map(new CommandToHueRequestFunction(getIp(), getPort(), getUser())).subscribe(this.commander);

		return commanderStream;
	}
	
	@Override
	protected void onCreatedController(Controller c) {
		// Nothing to do
	}

	@Override
	protected String formatURI(Where where) {
		String path;
		if (where == null) {
			path="localhost";
		} else {
			path = where.getPath();
		}
		return URITools.generateURI(SCHEMA, getUser(), getIp(), getPort(), path);
	}

	@Override
	public String getSchema() {
		return SCHEMA;
	}

	@Override
	public List<Class<? extends Controller>> getSupportedDevice() {
		List<Class<? extends Controller>> result = new ArrayList<Class<? extends Controller>>();
		result.add(Light.class);
		return result;
	}
}
