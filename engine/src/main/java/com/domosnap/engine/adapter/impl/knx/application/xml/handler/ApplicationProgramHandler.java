package com.domosnap.engine.adapter.impl.knx.application.xml.handler;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.impl.knx.application.xml.ApplicationProgramElement;
import com.domosnap.engine.adapter.impl.knx.application.xml.ComObjectElement;
import com.domosnap.engine.adapter.impl.knx.application.xml.ComObjectRefElement;

public class ApplicationProgramHandler extends XMLHandler {
	
	private static Log log = new Log(ApplicationProgramHandler.class.getSimpleName());
	
	private static final String APPLICATION_PROGRAM = "ApplicationProgram";
	private static final String COM_OBJECT = "ComObject";
	private static final String COM_OBJECT_REF = "ComObjectRef";
	
	private Collection<ApplicationProgramElement> applicationPrograms;
	private Collection<ComObjectElement> comObjects;
	private Collection<ComObjectRefElement> comObjectRefs;
	private ApplicationProgramElement currentApplicationProgram;
	
	public ApplicationProgramHandler() {
		this.applicationPrograms = new ArrayList<ApplicationProgramElement>(1);
		this.comObjects = new ArrayList<ComObjectElement>();
		this.comObjectRefs = new ArrayList<ComObjectRefElement>();
	}
	
	public Collection<ApplicationProgramElement> getApplicationPrograms() {
		return Collections.unmodifiableCollection(applicationPrograms);
	}
	
	public Collection<ComObjectElement> getCommunicationObjects() {
		return Collections.unmodifiableCollection(comObjects);
	}

	public Collection<ComObjectRefElement> getComObjectRefs() {
		return Collections.unmodifiableCollection(comObjectRefs);
	}
	
	@Override
	public void dispose() {
		this.applicationPrograms.clear();
		this.comObjects.clear();
		this.comObjectRefs.clear();
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		
		if (APPLICATION_PROGRAM.equals(qName)) {
			
			currentApplicationProgram = new ApplicationProgramElement(
					getAttribute("Id", attributes),
					getAttribute("Name", attributes)
			);
			applicationPrograms.add(currentApplicationProgram);
			log.fine(Session.Server, currentApplicationProgram.toString());
		}
		else if (COM_OBJECT.equals(qName)) {
			
			comObjects.add(new ComObjectElement(
					currentApplicationProgram.getId(),
					getAttribute("Id", attributes),
					getAttributeAsInteger("Number", attributes),
					getAttribute("Name", attributes),
					getAttribute("Text", attributes),
					getAttribute("FunctionText", attributes),
					getAttributeDatapointType(attributes),
					getAttribute("ObjectSize", attributes),
					getAttributeAsBoolean("CommunicationFlag", attributes),
					getAttributeAsBoolean("ReadFlag", attributes),
					getAttributeAsBoolean("ReadOnInitFlag", attributes),
					getAttributeAsBoolean("TransmitFlag", attributes),
					getAttributeAsBoolean("UpdateFlag", attributes),
					getAttributeAsBoolean("WriteFlag", attributes)
//					ENABLED.equals(getAttribute("CommunicationFlag", attributes)),
//					ENABLED.equals(getAttribute("ReadFlag", attributes)),
//					ENABLED.equals(getAttribute("ReadOnInitFlag", attributes)),
//					ENABLED.equals(getAttribute("TransmitFlag", attributes)),
//					ENABLED.equals(getAttribute("UpdateFlag", attributes)),
//					ENABLED.equals(getAttribute("WriteFlag", attributes))
			));
		}
		else if (COM_OBJECT_REF.equals(qName)) {
			
			comObjectRefs.add(new ComObjectRefElement(
					getAttribute("Id", attributes),
					getAttribute("RefId", attributes),
					getAttributeDatapointType(attributes),
					getAttribute("FunctionText", attributes),
					getAttribute("Text", attributes)
			));
		}
		else {
			checkTranslationElement(uri, localName, qName, attributes);
		}
	}
}
