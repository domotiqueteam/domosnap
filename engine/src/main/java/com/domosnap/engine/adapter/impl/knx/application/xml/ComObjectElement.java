package com.domosnap.engine.adapter.impl.knx.application.xml;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

public class ComObjectElement extends XMLElement {
	
	private String applicationProgramId;
	private int number;
	private String text;
	private String functionText;
	private String datapointType;
	private String objectSize;
	private boolean communicationFlag;
	private boolean readFlag;
	private boolean readOnInitFlag;
	private boolean transmitFlag;
	private boolean updateFlag;
	private boolean writeFlag;
	
	public ComObjectElement(String applicationProgramId, String id, int number, String name, String text, String functionText, String datapointType, String objectSize,
			boolean communicationFlag, boolean readFlag, boolean readOnInitFlag, boolean transmitFlag, boolean updateFlag, boolean writeFlag) {
		
		super(id, name);
		this.applicationProgramId = applicationProgramId;
		this.number = number;
		this.text = text;
		this.functionText = functionText;
		this.datapointType = datapointType;
		this.objectSize = objectSize;
		this.communicationFlag = communicationFlag;
		this.readFlag = readFlag;
		this.readOnInitFlag = readOnInitFlag;
		this.transmitFlag = transmitFlag;
		this.updateFlag = updateFlag;
		this.writeFlag = writeFlag;
	}
	
	public String getApplicationProgramId() {
		return applicationProgramId;
	}

	public int getNumber() {
		return number;
	}
	
	public String getDatapointType() {
		return datapointType;
	}
	
	public String getObjectSize() {
		return objectSize;
	}
	
	public String getText() {
		return text;
	}
	
	public String getFunctionText() {
		return functionText;
	}
	
	public boolean isCommunicationFlag() {
		return communicationFlag;
	}

	public boolean isReadFlag() {
		return readFlag;
	}

	public boolean isReadOnInitFlag() {
		return readOnInitFlag;
	}

	public boolean isTransmitFlag() {
		return transmitFlag;
	}

	public boolean isUpdateFlag() {
		return updateFlag;
	}

	public boolean isWriteFlag() {
		return writeFlag;
	}

	@Override
	public String toString() {
		return new StringBuilder("<ComObject Id=\"").append(getId())
				.append("\" Name=\"").append(getName())
				.append("\" Text=\"").append(text)
				.append("\" FunctionText=\"").append(functionText)
				.append("\" DatapointType=\"").append(datapointType)
				.append("\" ObjectSize=\"").append(objectSize)
				.append("\" Flags=\"")
				.append(communicationFlag ? "C" : "-").append(" ")
				.append(readFlag ? "R" : "-").append(" ")
				.append(writeFlag ? "W" : "-").append(" ")
				.append(transmitFlag ? "T" : "-").append(" ")
				.append(updateFlag ? "U" : "-")
				.append("\" />").toString();
	}

	@Override
	public void translate(TranslationElement translation) {
		super.translate(translation);
		text = translation.getTranslation("Text", text);
		functionText = translation.getTranslation("FunctionText", functionText);
	}
}
