package com.domosnap.engine.adapter.impl.knx.application.xml;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import tuwien.auto.calimero.dptxlator.DPT;

public class DatapointTypeElement extends XMLElement {

	private Integer number;
	
	private DPT dpt;
	
	/**
	 * 
	 * @param id
	 * @param name
	 * @param dpt
	 */
	public DatapointTypeElement(String id, String name, Integer number, DPT dpt) {
		super(id, name);
		this.number = number;
		this.dpt = dpt;
	}

	public DPT getDPT() {
		return dpt;
	}
	
	public Integer getNumber() {
		return number;
	}

	@Override
	public String toString() {
		return new StringBuilder("<DatapointType Id=\"").append(getId())
				.append("\" Number=\"").append(getNumber())
				.append("\" Name=\"").append(getName())
				.append("\" />").toString();
	}
}
