package com.domosnap.engine.adapter.impl.knx;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.ControllerAdapter;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.ConnectionListener;
import com.domosnap.engine.adapter.core.ScanListener;
import com.domosnap.engine.adapter.core.UnknownControllerListener;
import com.domosnap.engine.adapter.impl.knx.controller.KNXLight;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.light.Light;
import com.domosnap.engine.controller.where.Where;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.processors.FlowableProcessor;
import io.reactivex.processors.PublishProcessor;

public class KNXControllerAdapter implements ControllerAdapter {
	
	private final static Log log = new Log(KNXControllerAdapter.class.getSimpleName());
	
	private String gatewayAddress;
	private KNXConnectionSettings connectionSettings;
	
	private PublishProcessor<Command> commanderStream;
	private KNXCommander commander;
	
	private Flowable<Command> monitorStream;
	private KNXMonitor monitor;
	
	private Map<Class<?>, Map<Where, Controller>> controllerList = new ConcurrentHashMap<Class<?>, Map<Where,Controller>>();

	/**
	 * 
	 */
	public KNXControllerAdapter() {
		connectionSettings = null;
	}

	@Override
	public <T extends Controller> List<T> createControllers(Class<T> clazz, Where... wheres) {
		List<T> list = new ArrayList<T>();
		for (Where where : wheres) {
			list.add(createController(clazz, where));
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T extends Controller> T createController(Class<T> clazz, Where where) {
		synchronized (controllerList) {
			
			if (clazz == Light.class) {
				// Hack to manage WHERE in KNX in waiting Where refactor
				clazz = (Class<T>) KNXLight.class;
			}
			
			Map<Where, Controller> map = controllerList.get(clazz);
			if (map == null) {
				map = new HashMap<Where, Controller>();
				controllerList.put(clazz,map);
			}

			T controller = (T) map.get(where);
			if (controller == null) {
				try {
					controller = (T) clazz.newInstance();
					controller.addMonitor(getMonitorStream());
					controller.addCommander(getCommanderStream());
					controller.setWhere(where);
					map.put(where, controller);
				} catch (InstantiationException e) {
					log.severe(Session.Device, "Error when instanciate the controller [" + where + "]. " + e.getMessage());
				} catch (IllegalAccessException e) {
					log.severe(Session.Device, "Error when instanciate the controller [" + where + "]. " + e.getMessage());
				}
			}
			return controller;
		}
	}
	
	@Override
	public void connect() {
		
		if (! isConnected()) {
			// Get the connection settings of the specified KNXnet/IP device if found or the first one available if any.
			KNXDiscoverer discoverer = KNXDiscoverer.getInstance();
			connectionSettings = discoverer.getConnectionSettings(gatewayAddress);
			if (connectionSettings == null) {
				return; // TODO Throw exception to the UI
			}
			// Get the KNXnet/IP device address if it was not specified
			if (gatewayAddress == null) {
				gatewayAddress = connectionSettings.getDeviceSettings().getInetAddress().toString();
			}
			
			// Connect both KNX commander and monitor
			commander = new KNXCommander(connectionSettings);
			commander.connect();
			
			monitor = new KNXMonitor(connectionSettings);
			monitor.start();
		}
	}
	
	@Override
	public void connect(String uri) {
		connect();
	}
	
	@Override
	public void disconnect() {
		if (commander != null) {
			commander.disconnect();
		}
		if (monitor != null) {
			monitor.disconnect();
		}
	}

	@Override
	public boolean isConnected() {
		return commander != null && monitor != null && commander.isConnected() && monitor.isStarted();
	}
	
	@Override
	public void addCommanderConnectionListener(ConnectionListener listener) {
		commander.addConnectionListener(listener);
	}

	@Override
	public void removeCommanderConnectionListener(ConnectionListener listener) {
		commander.removeConnectionListener(listener);
	}
	
	@Override
	public void addMonitorConnectionListener(ConnectionListener listener) {
		monitor.addConnectionListener(listener);
	}

	@Override
	public void removeMonitorConnectionListener(ConnectionListener listener) {
		monitor.removeConnectionListener(listener);
	}

	@Override
	public void addUnknowControllerListener(UnknownControllerListener arg0) {
		// TODO implement
	}
	
	@Override
	public void removeUnknowControllerListener(UnknownControllerListener arg0) {
		// TODO implement
	}
	
	@Override
	public void scan(ScanListener listener) {
		
		if (isConnected()) {
			try {
				// -- Demo --
				Light light1 = this.createController(Light.class, new Where("1/1/1#1/2/1"));
//				light1.addCommander(getCommanderStream());
//				light1.addMonitor(getMonitorStream());
				// 1/1/1 => On / Off - Light1
				// 1/2/1 => Etat (On / Off) - Light1
//				Where w1 = ;
//				light1.setWhere(w1); 
				listener.foundController(light1);
				listener.progess(25);
				
				KNXLight light2 = new KNXLight();
				light2.addCommander(getCommanderStream());
				light2.addMonitor(getMonitorStream());
				// 1/1/2 => On / Off - Light2
				// 1/2/2 => Etat (On / Off) - Light2
				Where w2 = new Where("1/1/2#1/2/2");
				light2.setWhere(w2); 
				listener.foundController(light2);
				listener.progess(50);
				
				KNXLight light3 = new KNXLight();
				light3.addCommander(getCommanderStream());
				light3.addMonitor(getMonitorStream());
				// 1/1/3 => On / Off - Light3
				// 1/2/3 => Etat (On / Off) - Light3
				Where w3 = new Where("1/1/3#1/2/3");
				light3.setWhere(w3); 
				listener.foundController(light3);
				listener.progess(75);
				
				KNXLight light4 = new KNXLight();
				light4.addCommander(getCommanderStream());
				light4.addMonitor(getMonitorStream());
				// 1/1/4 => On / Off - Light4
				// 1/2/4 => Etat (On / Off) - Light4
				Where w4 = new Where("1/1/4#1/2/4");
				light4.setWhere(w4); 
				listener.foundController(light4);
				listener.progess(100);
				
				listener.scanFinished();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		else {
			log.warning(Session.Server, "No connection is established with the KNX network, call connect() method first.");
		}
	}
	
	private PublishProcessor<Command> getCommanderStream() {
		if (commanderStream == null) {
			commanderStream = PublishProcessor.create();
			commanderStream.subscribe(commander);	
			log.finest(Session.Command, "Commander stream created.");
		}
		return commanderStream;
	}
	
	private Flowable<Command> getMonitorStream() {
		if (monitorStream == null) {
			monitorStream = FlowableProcessor.create(monitor, BackpressureStrategy.DROP);
			log.finest(Session.Monitor, "Monitor stream created.");
		}
		return monitorStream;
	}

	@Override
	public List<Class<? extends Controller>> getSupportedDevice() {
		// TODO Auto-generated method stub
		return null;
	}
}
