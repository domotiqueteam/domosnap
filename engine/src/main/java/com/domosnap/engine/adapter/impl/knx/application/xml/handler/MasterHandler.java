package com.domosnap.engine.adapter.impl.knx.application.xml.handler;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.impl.knx.application.xml.DatapointTypeElement;

import tuwien.auto.calimero.dptxlator.DPT;
import tuwien.auto.calimero.dptxlator.DPTXlator1BitControlled;
import tuwien.auto.calimero.dptxlator.DPTXlator2ByteFloat;
import tuwien.auto.calimero.dptxlator.DPTXlator2ByteUnsigned;
import tuwien.auto.calimero.dptxlator.DPTXlator3BitControlled;
import tuwien.auto.calimero.dptxlator.DPTXlator4ByteFloat;
import tuwien.auto.calimero.dptxlator.DPTXlator4ByteSigned;
import tuwien.auto.calimero.dptxlator.DPTXlator4ByteUnsigned;
import tuwien.auto.calimero.dptxlator.DPTXlator8BitUnsigned;
import tuwien.auto.calimero.dptxlator.DPTXlatorBoolean;
import tuwien.auto.calimero.dptxlator.DPTXlatorDate;
import tuwien.auto.calimero.dptxlator.DPTXlatorDateTime;
import tuwien.auto.calimero.dptxlator.DPTXlatorSceneControl;
import tuwien.auto.calimero.dptxlator.DPTXlatorSceneNumber;
import tuwien.auto.calimero.dptxlator.DPTXlatorString;
import tuwien.auto.calimero.dptxlator.DPTXlatorTime;

/**
 * 
 * @author DRIESBACH Olivier
 */
public class MasterHandler extends XMLHandler {
	
	private static Log log = new Log(MasterHandler.class.getSimpleName());
	
	private static final String DATAPOINT_TYPE = "DatapointType";
	private static final String DATAPOINT_SUBTYPE = "DatapointSubtype";

	private Collection<DatapointTypeElement> datapointTypes;
	private int currentDPTNumber;
	
	public MasterHandler() {
		this.datapointTypes = new ArrayList<DatapointTypeElement>();
	}
	
	public Collection<DatapointTypeElement> getDatapointTypes() {
		return Collections.unmodifiableCollection(datapointTypes);
	}
	
	@Override
	public void dispose() {
		this.datapointTypes.clear();
	}
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		
		if (DATAPOINT_TYPE.equals(qName)) {
			currentDPTNumber = getAttributeAsInteger("Number", attributes);
		}
		else if (DATAPOINT_SUBTYPE.equals(qName)) {
			parseDPSTElement(uri, localName, qName, attributes);
		}
		else {
			checkTranslationElement(uri, localName, qName, attributes);
		}
	}
	
	private void parseDPSTElement(String uri, String localName, String qName, Attributes attributes) {
		
		DPT dpt = getDPT(currentDPTNumber, getAttributeAsInteger("Number", attributes));
		DatapointTypeElement dte = new DatapointTypeElement(
				getAttribute("Id", attributes),
				getAttribute("Name", attributes),
				getAttributeAsInteger("Number", attributes),
				dpt
		);
		datapointTypes.add(dte);
		log.finest(Session.Server, dte.toString());
	}
	
	private DPT getDPT(int datapointType, int datapointSubtype) {
		
		switch (datapointType) {
			case 1: { // DPT 1.xxx
				switch (datapointSubtype) {
					case 1: return DPTXlatorBoolean.DPT_SWITCH;
					case 2: return DPTXlatorBoolean.DPT_BOOL;
					case 3: return DPTXlatorBoolean.DPT_ENABLE;
					case 4: return DPTXlatorBoolean.DPT_RAMP;
					case 5: return DPTXlatorBoolean.DPT_ALARM;
					case 6: return DPTXlatorBoolean.DPT_BINARYVALUE;
					case 7: return DPTXlatorBoolean.DPT_STEP;
					case 8: return DPTXlatorBoolean.DPT_UPDOWN;
					case 9: return DPTXlatorBoolean.DPT_OPENCLOSE;
					case 10: return DPTXlatorBoolean.DPT_START;
					case 11: return DPTXlatorBoolean.DPT_STATE;
					case 12: return DPTXlatorBoolean.DPT_INVERT;
					case 13: return DPTXlatorBoolean.DPT_DIMSENDSTYLE;
					case 14: return DPTXlatorBoolean.DPT_INPUTSOURCE;
					case 15: return DPTXlatorBoolean.DPT_RESET;
					case 16: return DPTXlatorBoolean.DPT_ACK;
					case 17: return DPTXlatorBoolean.DPT_TRIGGER;
					case 18: return DPTXlatorBoolean.DPT_OCCUPANCY;
					case 19: return DPTXlatorBoolean.DPT_WINDOW_DOOR;
					case 21: return DPTXlatorBoolean.DPT_LOGICAL_FUNCTION;
					case 22: return DPTXlatorBoolean.DPT_SCENE_AB;
					case 23: return DPTXlatorBoolean.DPT_SHUTTER_BLINDS_MODE;
					case 100: return DPTXlatorBoolean.DPT_HEAT_COOL;
					default: return DPTXlatorBoolean.DPT_SWITCH;
				}
			}
			
			case 2: { // DPT 2.xxx
				switch (datapointSubtype) {
					case 1: return DPTXlator1BitControlled.DPT_SWITCH_CONTROL;
					case 2: return DPTXlator1BitControlled.DPT_BOOL_CONTROL;
					case 3: return DPTXlator1BitControlled.DPT_ENABLE_CONTROL;
					case 4: return DPTXlator1BitControlled.DPT_RAMP_CONTROL;
					case 5: return DPTXlator1BitControlled.DPT_ALARM_CONTROL;
					case 6: return DPTXlator1BitControlled.DPT_BINARY_CONTROL;
					case 7: return DPTXlator1BitControlled.DPT_STEP_CONTROL;
					case 8: return DPTXlator1BitControlled.DPT_UPDOWN_CONTROL;
					case 9: return DPTXlator1BitControlled.DPT_OPENCLOSE_CONTROL;
					case 10: return DPTXlator1BitControlled.DPT_START_CONTROL;
					case 11: return DPTXlator1BitControlled.DPT_STATE_CONTROL;
					case 12: return DPTXlator1BitControlled.DPT_INVERT_CONTROL;
					default: return DPTXlator1BitControlled.DPT_SWITCH_CONTROL;
				}
			}
			
			case 3: { // DPT 3.xxx
				switch (datapointSubtype) {
					case 7: return DPTXlator3BitControlled.DPT_CONTROL_DIMMING;
					case 8: return DPTXlator3BitControlled.DPT_CONTROL_BLINDS;
					default: return DPTXlator3BitControlled.DPT_CONTROL_DIMMING;
				}
			}
			
//			case 4: { // DPT 4.xxx
//				switch (datapointSubtype) {
//					case 1: <DatapointSubtype Id="DPST-4-1" Number="1" Name="DPT_Char_ASCII" Text="character (ASCII)" />
//					case 2: <DatapointSubtype Id="DPST-4-2" Number="2" Name="DPT_Char_8859_1" Text="character (ISO 8859-1)" />
//				}
//			}
			
			case 5: { // DPT 5.xxx
				switch (datapointSubtype) {
					case 1: return DPTXlator8BitUnsigned.DPT_SCALING;
					case 3: return DPTXlator8BitUnsigned.DPT_ANGLE;
					case 4: return DPTXlator8BitUnsigned.DPT_PERCENT_U8;
					case 5: return DPTXlator8BitUnsigned.DPT_DECIMALFACTOR;
					case 6: return DPTXlator8BitUnsigned.DPT_TARIFF;
					case 10: return DPTXlator8BitUnsigned.DPT_VALUE_1_UCOUNT;
				}
			}
			
//			case 6: { // DPT 6.xxx
//				switch (datapointSubtype) {
//					case 1: <DatapointSubtype Id="DPST-6-1" Number="1" Name="DPT_Percent_V8" Text="percentage (-128..127%)" /> 
//					case 10: <DatapointSubtype Id="DPST-6-10" Number="10" Name="DPT_Value_1_Count" Text="counter pulses (-128..127)" />
//					case 20: <DatapointSubtype Id="DPST-6-20" Number="20" Name="DPT_Status_Mode3" Text="status with mode" />
//				}
//			}
			
			case 7: { // DPT 7.xxx
				switch (datapointSubtype) {
				
					case 1: return DPTXlator2ByteUnsigned.DPT_VALUE_2_UCOUNT;
					case 2: return DPTXlator2ByteUnsigned.DPT_TIMEPERIOD;
					case 3: return DPTXlator2ByteUnsigned.DPT_TIMEPERIOD_10;
					case 4: return DPTXlator2ByteUnsigned.DPT_TIMEPERIOD_100;
					case 5: return DPTXlator2ByteUnsigned.DPT_TIMEPERIOD_SEC;
					case 6: return DPTXlator2ByteUnsigned.DPT_TIMEPERIOD_MIN;
					case 7: return DPTXlator2ByteUnsigned.DPT_TIMEPERIOD_HOURS;
					case 10: return DPTXlator2ByteUnsigned.DPT_PROP_DATATYPE;
					case 11: return DPTXlator2ByteUnsigned.DPT_LENGTH;
					case 12: return DPTXlator2ByteUnsigned.DPT_ELECTRICAL_CURRENT;
//					case 13: <DatapointSubtype Id="DPST-7-13" Number="13" Name="DPT_Brightness" Text="brightness (lux)" />
//					case 600: <DatapointSubtype Id="DPST-7-600" Number="600" Name="DPT_Absolute_Colour_Temperature" Text="absolute colour temperature (K)" />
				}
			}
			
//			case 8: { // DPT 8.xxx
//				switch (datapointSubtype) {
//					case 1: <DatapointSubtype Id="DPST-8-1" Number="1" Name="DPT_Value_2_Count" Text="pulses difference" />
//					case 2: <DatapointSubtype Id="DPST-8-2" Number="2" Name="DPT_DeltaTimeMsec" Text="time lag (ms)" />
//					case 3: <DatapointSubtype Id="DPST-8-3" Number="3" Name="DPT_DeltaTime10Msec" Text="time lag(10 ms)" />
//					case 4: <DatapointSubtype Id="DPST-8-4" Number="4" Name="DPT_DeltaTime100Msec" Text="time lag (100 ms)" />
//					case 5:	<DatapointSubtype Id="DPST-8-5" Number="5" Name="DPT_DeltaTimeSec" Text="time lag (s)" />
//					case 6: <DatapointSubtype Id="DPST-8-6" Number="6" Name="DPT_DeltaTimeMin" Text="time lag (min)" />
//					case 7: <DatapointSubtype Id="DPST-8-7" Number="7" Name="DPT_DeltaTimeHrs" Text="time lag (h)" />
//					case 10: <DatapointSubtype Id="DPST-8-10" Number="10" Name="DPT_Percent_V16" Text="percentage difference (%)" />
//					case 11: <DatapointSubtype Id="DPST-8-11" Number="11" Name="DPT_Rotation_Angle" Text="rotation angle (°)" />
//			}
			
			case 9: { // DPT 9.xxx
				switch (datapointSubtype) {
					case 1: return DPTXlator2ByteFloat.DPT_TEMPERATURE;
					case 2: return DPTXlator2ByteFloat.DPT_TEMPERATURE_DIFFERENCE;
					case 3: return DPTXlator2ByteFloat.DPT_TEMPERATURE_GRADIENT;
					case 4: return DPTXlator2ByteFloat.DPT_INTENSITY_OF_LIGHT;
					case 5: return DPTXlator2ByteFloat.DPT_WIND_SPEED;
					case 6: return DPTXlator2ByteFloat.DPT_AIR_PRESSURE;
					case 7: return DPTXlator2ByteFloat.DPT_HUMIDITY;
					case 8: return DPTXlator2ByteFloat.DPT_AIRQUALITY;
//					case 9: <DatapointSubtype Id="DPST-9-9" Number="9" Name="DPT_Value_AirFlow" Text="air flow (m³/h)" />
					case 10: return DPTXlator2ByteFloat.DPT_TIME_DIFFERENCE1;
					case 11: return DPTXlator2ByteFloat.DPT_TIME_DIFFERENCE2;
					case 20: return DPTXlator2ByteFloat.DPT_VOLTAGE;
					case 21: return DPTXlator2ByteFloat.DPT_ELECTRICAL_CURRENT;
					case 22: return DPTXlator2ByteFloat.DPT_POWERDENSITY;
					case 23: return DPTXlator2ByteFloat.DPT_KELVIN_PER_PERCENT;
					case 24: return DPTXlator2ByteFloat.DPT_POWER;
					case 25: return DPTXlator2ByteFloat.DPT_VOLUME_FLOW;
					case 26: return DPTXlator2ByteFloat.DPT_VOLTAGE;
					case 27: return DPTXlator2ByteFloat.DPT_RAIN_AMOUNT;
					case 28: return DPTXlator2ByteFloat.DPT_TEMP_F;
				}
			}
			
			case 10: { // DPT 10.xxx
				switch (datapointSubtype) {
					case 1: return DPTXlatorTime.DPT_TIMEOFDAY;
				}
			}
			
			case 11: { // DPT 11.xxx
				switch (datapointSubtype) {
					case 1: return DPTXlatorDate.DPT_DATE;
				}
			}
			
			case 12: { // DPT 12.xxx
				switch (datapointSubtype) {
					case 1: return DPTXlator4ByteUnsigned.DPT_VALUE_4_UCOUNT;
				}
			}
			
			case 13: { // DPT 13.xxx
				switch (datapointSubtype) {
					case 1: return DPTXlator4ByteSigned.DPT_COUNT;
					case 2: return DPTXlator4ByteSigned.DPT_FLOWRATE;
					case 10: return DPTXlator4ByteSigned.DPT_ACTIVE_ENERGY;
					case 11: return DPTXlator4ByteSigned.DPT_APPARENT_ENERGY_KVAH;
					case 12: return DPTXlator4ByteSigned.DPT_REACTIVE_ENERGY;
					case 13: return DPTXlator4ByteSigned.DPT_ACTIVE_ENERGY_KWH;
					case 14: return DPTXlator4ByteSigned.DPT_APPARENT_ENERGY_KVAH;
					case 15: return DPTXlator4ByteSigned.DPT_REACTIVE_ENERGY_KVARH;
					case 100: return DPTXlator4ByteSigned.DPT_DELTA_TIME;
				}
			}
			
			case 14: { // DPT 14.xxx
				switch (datapointSubtype) {
					case 0: return DPTXlator4ByteFloat.DPT_ACCELERATION;
					case 1: return DPTXlator4ByteFloat.DPT_ACCELERATION_ANGULAR;
					case 2: return DPTXlator4ByteFloat.DPT_ACTIVATION_ENERGY;
					case 3: return DPTXlator4ByteFloat.DPT_ACTIVITY;
					case 4: return DPTXlator4ByteFloat.DPT_MOL;
					case 5: return DPTXlator4ByteFloat.DPT_AMPLITUDE;
					case 6: return DPTXlator4ByteFloat.DPT_ANGLE_RAD;
					case 7: return DPTXlator4ByteFloat.DPT_ANGLE_DEG;
					case 8: return DPTXlator4ByteFloat.DPT_ANGULAR_MOMENTUM;
					case 9: return DPTXlator4ByteFloat.DPT_ANGULAR_VELOCITY;
					case 10: return DPTXlator4ByteFloat.DPT_AREA;
					case 11: return DPTXlator4ByteFloat.DPT_CAPACITANCE;
					case 12: return DPTXlator4ByteFloat.DPT_CHARGE_DENSITY_SURFACE;
					case 13: return DPTXlator4ByteFloat.DPT_CHARGE_DENSITY_VOLUME;
					case 14: return DPTXlator4ByteFloat.DPT_COMPRESSIBILITY;
					case 15: return DPTXlator4ByteFloat.DPT_CONDUCTANCE;
					case 16: return DPTXlator4ByteFloat.DPT_ELECTRICAL_CONDUCTIVITY;
					case 17: return DPTXlator4ByteFloat.DPT_DENSITY;
					case 18: return DPTXlator4ByteFloat.DPT_ELECTRIC_CHARGE;
					case 19: return DPTXlator4ByteFloat.DPT_ELECTRIC_CURRENT;
					case 20: return DPTXlator4ByteFloat.DPT_ELECTRIC_CURRENTDENSITY;
					case 21: return DPTXlator4ByteFloat.DPT_ELECTRIC_DIPOLEMOMENT;
					case 22: return DPTXlator4ByteFloat.DPT_ELECTRIC_DISPLACEMENT;
					case 23: return DPTXlator4ByteFloat.DPT_ELECTRIC_FIELDSTRENGTH;
					case 24: return DPTXlator4ByteFloat.DPT_ELECTRIC_FLUX;
					case 25: return DPTXlator4ByteFloat.DPT_ELECTRIC_FLUX_DENSITY;
					case 26: return DPTXlator4ByteFloat.DPT_ELECTRIC_POLARIZATION;
					case 27: return DPTXlator4ByteFloat.DPT_ELECTRIC_POTENTIAL;
					case 28: return DPTXlator4ByteFloat.DPT_ELECTRIC_POTENTIAL_DIFFERENCE;
					case 29: return DPTXlator4ByteFloat.DPT_ELECTROMAGNETIC_MOMENT;
					case 30: return DPTXlator4ByteFloat.DPT_ELECTROMOTIVE_FORCE;
					case 31: return DPTXlator4ByteFloat.DPT_ENERGY;
					case 32: return DPTXlator4ByteFloat.DPT_FORCE;
					case 33: return DPTXlator4ByteFloat.DPT_FREQUENCY;
					case 34: return DPTXlator4ByteFloat.DPT_ANGULAR_FREQUENCY;
					case 35: return DPTXlator4ByteFloat.DPT_HEAT_CAPACITY;
					case 36: return DPTXlator4ByteFloat.DPT_HEAT_FLOWRATE;
					case 37: return DPTXlator4ByteFloat.DPT_HEAT_QUANTITY;
					case 38: return DPTXlator4ByteFloat.DPT_IMPEDANCE;
					case 39: return DPTXlator4ByteFloat.DPT_LENGTH;
					case 40: return DPTXlator4ByteFloat.DPT_LIGHT_QUANTITY;
					case 41: return DPTXlator4ByteFloat.DPT_LUMINANCE;
					case 42: return DPTXlator4ByteFloat.DPT_LUMINOUS_FLUX;
					case 43: return DPTXlator4ByteFloat.DPT_LUMINOUS_INTENSITY;
					case 44: return DPTXlator4ByteFloat.DPT_MAGNETIC_FIELDSTRENGTH;
					case 45: return DPTXlator4ByteFloat.DPT_MAGNETIC_FLUX;
					case 46: return DPTXlator4ByteFloat.DPT_MAGNETIC_FLUX_DENSITY;
					case 47: return DPTXlator4ByteFloat.DPT_MAGNETIC_MOMENT;
					case 48: return DPTXlator4ByteFloat.DPT_MAGNETIC_POLARIZATION;
					case 49: return DPTXlator4ByteFloat.DPT_MAGNETIZATION;
					case 50: return DPTXlator4ByteFloat.DPT_MAGNETOMOTIVE_FORCE;
					case 51: return DPTXlator4ByteFloat.DPT_MASS;
					case 52: return DPTXlator4ByteFloat.DPT_MASS_FLUX;
					case 53: return DPTXlator4ByteFloat.DPT_MOMENTUM;
					case 54: return DPTXlator4ByteFloat.DPT_PHASE_ANGLE_RAD;
					case 55: return DPTXlator4ByteFloat.DPT_PHASE_ANGLE_DEG;
					case 56: return DPTXlator4ByteFloat.DPT_POWER;
					case 57: return DPTXlator4ByteFloat.DPT_POWER_FACTOR;
					case 58: return DPTXlator4ByteFloat.DPT_PRESSURE;
					case 59: return DPTXlator4ByteFloat.DPT_REACTANCE;
					case 60: return DPTXlator4ByteFloat.DPT_RESISTANCE;
					case 61: return DPTXlator4ByteFloat.DPT_RESISTIVITY;
					case 62: return DPTXlator4ByteFloat.DPT_SELF_INDUCTANCE;
					case 63: return DPTXlator4ByteFloat.DPT_SOLID_ANGLE;
					case 64: return DPTXlator4ByteFloat.DPT_SOUND_INTENSITY;
					case 65: return DPTXlator4ByteFloat.DPT_SPEED;
					case 66: return DPTXlator4ByteFloat.DPT_STRESS;
					case 67: return DPTXlator4ByteFloat.DPT_SURFACE_TENSION;
					case 68: return DPTXlator4ByteFloat.DPT_COMMON_TEMPERATURE;
					case 69: return DPTXlator4ByteFloat.DPT_ABSOLUTE_TEMPERATURE;
					case 70: return DPTXlator4ByteFloat.DPT_TEMPERATURE_DIFFERENCE;
					case 71: return DPTXlator4ByteFloat.DPT_THERMAL_CAPACITY;
					case 72: return DPTXlator4ByteFloat.DPT_THERMAL_CONDUCTIVITY;
					case 73: return DPTXlator4ByteFloat.DPT_THERMOELECTRIC_POWER;
					case 74: return DPTXlator4ByteFloat.DPT_TIME;
					case 75: return DPTXlator4ByteFloat.DPT_TORQUE;
					case 76: return DPTXlator4ByteFloat.DPT_VOLUME;
					case 77: return DPTXlator4ByteFloat.DPT_VOLUME_FLUX;
					case 78: return DPTXlator4ByteFloat.DPT_WEIGHT;
					case 79: return DPTXlator4ByteFloat.DPT_WORK;
				}
			}
			
			case 16: { // DPT 16.xxx
				switch (datapointSubtype) {
					case 0: return DPTXlatorString.DPT_STRING_ASCII;
					case 1: return DPTXlatorString.DPT_STRING_8859_1;
				}
			}
			
			case 17: { // DPT 17.xxx
				switch (datapointSubtype) {
					case 1: return DPTXlatorSceneNumber.DPT_SCENE_NUMBER;
				}
			}
			
			case 18: { // DPT 18.xxx
				switch (datapointSubtype) {
					case 1: return DPTXlatorSceneControl.DPT_SCENE_CONTROL;
				}
			}
			
			case 19: { // DPT 19.xxx
				switch (datapointSubtype) {
					case 1: return DPTXlatorDateTime.DPT_DATE_TIME;
				}
			}
		}
		return null;
	}
}
