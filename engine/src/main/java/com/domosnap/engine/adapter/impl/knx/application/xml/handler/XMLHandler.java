package com.domosnap.engine.adapter.impl.knx.application.xml.handler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.domosnap.engine.adapter.impl.knx.application.xml.TranslationElement;

/**
 * 
 * @author DRIESBACH Olivier
 */
public abstract class XMLHandler extends DefaultHandler implements KNXConstants {
	
	private static final String LANGUAGE = "Language";
	private static final String TRANSLATION_ELEMENT = "TranslationElement";
	private static final String TRANSLATION = "Translation";
	
	private TranslationElement currentTranslation;
	private Collection<TranslationElement> translationList;
	private Map<String, Collection<TranslationElement>> translations;
	
	XMLHandler() {
		super();
		translations = new HashMap<String, Collection<TranslationElement>>();
	}

	/**
	 * 
	 * @return
	 */
	public Map<String, Collection<TranslationElement>> getTranslations() {
		return translations;
	}
	
	/**
	 * 
	 * @param name
	 * @param attributes
	 * @return
	 */
	protected String getAttribute(String name, Attributes attributes) {

		String result = null;
		if (attributes != null) {
			result = attributes.getValue(name);
		}
		return result;
	}
	
	/**
	 * 
	 * @param name
	 * @param attributes
	 * @return
	 */
	protected Integer getAttributeAsInteger(String name, Attributes attributes) {

		Integer result = null;
		try {
			result = Integer.parseInt(getAttribute(name, attributes));
		} catch (NumberFormatException e) {
		}
		return result;
	}
	
	/**
	 * 
	 * @param name
	 * @param attributes
	 * @return
	 */
	protected Boolean getAttributeAsBoolean(String name, Attributes attributes) {

		String attrValue = getAttribute(name, attributes);
		return attrValue == null ? null : ENABLED.equalsIgnoreCase(attrValue) ? Boolean.TRUE : "1".equals(attrValue) ? Boolean.TRUE : Boolean.FALSE;
	}
	
	/**
	 * 
	 * @param attributes
	 * @return
	 */
	protected String getAttributeDatapointType(Attributes attributes) {
		
		String result = getAttribute("DatapointType", attributes);
		if (result != null) {
			result = result.split(" ")[0];
		}
		return result;
	}
	
	protected void checkTranslationElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		
		if (LANGUAGE.equals(qName)) {
			translationList = new ArrayList<TranslationElement>(1);
			translations.put(getAttribute("Identifier", attributes), translationList);
		}
		else if (TRANSLATION_ELEMENT.equals(qName)) {
			currentTranslation = new TranslationElement(getAttribute("RefId", attributes));
			translationList.add(currentTranslation);
		}
		else if (TRANSLATION.equals(qName)) {
			currentTranslation.addTranslation(getAttribute("AttributeName", attributes), getAttribute("Text", attributes));
		}
	}
	
	public void dispose() {
		currentTranslation = null;
		translationList.clear();
		translations.clear();
	}
}
