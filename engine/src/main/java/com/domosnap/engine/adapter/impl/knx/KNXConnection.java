package com.domosnap.engine.adapter.impl.knx;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.net.InetSocketAddress;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;

import tuwien.auto.calimero.exception.KNXException;
import tuwien.auto.calimero.knxnetip.KNXnetIPConnection;
import tuwien.auto.calimero.knxnetip.KNXnetIPRouting;
import tuwien.auto.calimero.link.KNXNetworkLink;
import tuwien.auto.calimero.link.KNXNetworkLinkIP;

/**
 * Utility class to create connections to a KNXnet/IP device.
 * 
 * Use {@link KNXDiscoverer} to search for {@link KNXConnectionSettings}.
 * 
 * @see KNXDiscoverer
 * 
 * @author DRIESBACH Olivier
 */
public class KNXConnection {
	
	private final static Log log = new Log(KNXConnection.class.getSimpleName());
	
	/**
	 * 
	 * @param settings
	 * @param routingMode
	 * @return
	 * @throws KNXException
	 */
	public static KNXNetworkLink createConnection(KNXConnectionSettings settings, boolean routingMode) throws KNXException {
		
		if (settings == null) {
			throw new NullPointerException("Could not create KNX connection without medium settings.");
		}
		
		InetSocketAddress localSocket = null;
		InetSocketAddress remoteSocket = null;
		try {
			localSocket = new InetSocketAddress(settings.getLocalAddress(), 0); // 0 = random port
			if (routingMode) {
				log.info(Session.Server, "Connect to multicast IP address "+ KNXnetIPRouting.DEFAULT_MULTICAST);
				remoteSocket = new InetSocketAddress(KNXnetIPRouting.DEFAULT_MULTICAST, KNXnetIPConnection.DEFAULT_PORT);
			} else {
				log.info(Session.Server, "Connect to KNXnet/IP device address "+ settings.getDeviceSettings().getInetAddress().toString());
				remoteSocket = new InetSocketAddress(settings.getDeviceSettings().getInetAddress(), KNXnetIPConnection.DEFAULT_PORT);
			}
			
			KNXNetworkLink networkLink = new KNXNetworkLinkIP(routingMode ? KNXNetworkLinkIP.ROUTING : KNXNetworkLinkIP.TUNNELING, localSocket, remoteSocket, false, settings.getMediumSettings());
			logConnection(routingMode, localSocket, remoteSocket);
			return networkLink;
			
		} catch (Throwable t) {
			
			String msg = String.format("Could not establish %s connection from %s:%d to %s:%d",
					routingMode ? "routing" : "tunneling",
							localSocket.getAddress().toString(), localSocket.getPort(),
							remoteSocket.getAddress().toString(), remoteSocket.getPort()
			);
			throw new KNXException(msg, t);
		}
	}
	
	private static void logConnection(boolean routingMode, InetSocketAddress localSocket, InetSocketAddress remoteSocket) {
		log.info(Session.Server, String.format("New %s connection from %s:%d to %s:%d has been established.",
				routingMode ? "routing" : "tunneling",
						localSocket.getAddress().toString(), localSocket.getPort(),
						remoteSocket.getAddress().toString(), remoteSocket.getPort())
		);
	}
}
