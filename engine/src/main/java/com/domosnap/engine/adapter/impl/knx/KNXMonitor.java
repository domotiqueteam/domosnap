package com.domosnap.engine.adapter.impl.knx;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.Command.Type;
import com.domosnap.engine.adapter.core.ConnectionListener;
import com.domosnap.engine.adapter.core.ConnectionStatusEnum;
import com.domosnap.engine.controller.light.Light;
import com.domosnap.engine.controller.light.LightStateName;
import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.controller.what.impl.OnOffState;
import com.domosnap.engine.controller.where.Where;

import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import tuwien.auto.calimero.CloseEvent;
import tuwien.auto.calimero.DataUnitBuilder;
import tuwien.auto.calimero.FrameEvent;
import tuwien.auto.calimero.KNXAddress;
import tuwien.auto.calimero.cemi.CEMILData;
import tuwien.auto.calimero.dptxlator.DPTXlatorBoolean;
import tuwien.auto.calimero.exception.KNXException;
import tuwien.auto.calimero.link.KNXNetworkLink;
import tuwien.auto.calimero.link.NetworkLinkListener;

public class KNXMonitor implements FlowableOnSubscribe<Command> {
	
	private final static Log log = new Log(KNXMonitor.class.getSimpleName());
	
	/** */
	private KNXConnectionSettings connectionSettings;
	
	/** */
	private KNXNetworkLink networkLink;
	private NetworkLinkListener linkListener;
	
	/** */
	private Collection<FlowableEmitter<Command>> emitters;
	private List<ConnectionListener> listeners;
	
	/**
	 * 
	 * @param connectionSettings
	 */
	public KNXMonitor(KNXConnectionSettings connectionSettings) {
		
		super();
		this.connectionSettings = connectionSettings;
		this.listeners = new ArrayList<ConnectionListener>();
		this.emitters = new ArrayList<FlowableEmitter<Command>>(); // TODO Should be remomved
	}
	
	/**
	 * 
	 */
	public void connect() {
		
		if (! isConnected()) {
			try {
				// Monitor KNX events trought a routing connection with a connection listener
				networkLink = KNXConnection.createConnection(connectionSettings, connectionSettings.getDeviceSettings().isRouter());
				
				for (ConnectionListener listener : listeners) {
					listener.onConnect(ConnectionStatusEnum.Connected);
				}
			}
			catch (KNXException e) {
				log.severe(Session.Server, "Unable to connect the monitor to the KNX network.");
			}
		}
	}

	/**
	 * 
	 */
	public void disconnect() {
		
		if (isConnected()) {
			stop();
			networkLink.close();
		}
	}
	
	private boolean isConnected() {
		return networkLink != null && networkLink.isOpen();
	}
	
//	@Override
	public boolean isStarted() {
		return linkListener != null;
	}
	
	/**
	 * Start monitoring messages on the KNX bus
	 */
//	@Override
	public void start() {
		
		if (! isStarted()) {
			connect(); // Nothing happens if already connected
			
			if (isConnected()) {
				linkListener = new NetworkLinkListener() {
					
					@Override
					public void linkClosed(CloseEvent event) {
						log.warning(Session.Server, "The KNX connection has been closed, no more messages can be read on the bus.");
						for (ConnectionListener listener : listeners) {
							listener.onClose();
						}
					}
					
					@Override
					public void indication(FrameEvent event) {
						
//						try {
							CEMILData message = (CEMILData) event.getFrame();
							byte[] apdu = message.getPayload();
							int svc = DataUnitBuilder.getAPDUService(apdu);
							if (svc == 0x00) { // Group Read
								return;
							}
							
							// TODO Retrieve type from address
							KNXAddress targetAddress = message.getDestination();
							log.info(Session.Server, String.format("Message received from %s to %s", message.getSource().toString(), targetAddress));
							
							/*
							DPTXlatorBoolean translator = new DPTXlatorBoolean(DPTXlatorBoolean.DPT_SWITCH);
							translator.setData(DataUnitBuilder.extractASDU(apdu));
							String value = translator.getValue();
							
							onCommandReceived(new Command(Light.class,
									new What(LightStateName.status.name(), "on".equals(value) ? OnOffState.On() : OnOffState.Off()),
									new Where(message.getDestination().toString()),
									Type.QUERY, null));
							*/
//						}
//						catch (KNXException e) {
//							log.severe(Session.Server, "An error occured while reading a KNX message.");
//							e.printStackTrace();
//						}
					}
					
					@Override
					public void confirmation(FrameEvent event) {
					}
				};
				
				networkLink.addLinkListener(linkListener);
			}
		}
	}

	
	/**
	 * Stop monitoring messages on the KNX bus
	 */
//	@Override
	public void stop() {
		if (isStarted()) {
			networkLink.removeLinkListener(linkListener);
			linkListener = null;
		}
	}
	
	/**
	 * 
	 * @param listener
	 */
	public void addConnectionListener(ConnectionListener listener) {
		synchronized (listeners) {
			listeners.add(listener);
		}
	}

	/**
	 * 
	 * @param listener
	 */
	public void removeConnectionListener(ConnectionListener listener) {
		synchronized (listeners) {
			listeners.remove(listener);
		}
	}
	
	// TODO: Should be removed
	@Override
	public void subscribe(FlowableEmitter<Command> e) throws Exception {
		synchronized (emitters) {
			emitters.add(e);
		}
	}
	
	// TODO: Should be removed
	public void onCommandReceived(Command command) {
		synchronized (emitters) {
			for (FlowableEmitter<Command> emitter : emitters) {
				emitter.onNext(command);
			}
		}
	}
}
