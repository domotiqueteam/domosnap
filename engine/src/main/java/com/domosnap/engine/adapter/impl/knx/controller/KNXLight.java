package com.domosnap.engine.adapter.impl.knx.controller;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.Command.Type;
import com.domosnap.engine.controller.light.Light;
import com.domosnap.engine.controller.what.State;
import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.controller.where.Where;

import io.reactivex.Flowable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;

/**
 * 
 * @author DRIESBACH Olivier
 */
public class KNXLight extends Light {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Log log = new Log(this.getClass().getSimpleName());
	
	public void addMonitor(Flowable<Command> monitor) {
		this.monitor = monitor;
		this.monitor.subscribe(new Consumer<Command>() {
			@Override
			public void accept(Command command) throws Exception {
				// TODO Refactor the WHERE : When from and to is the same, it's a write address, should be the same as the current from.
				if (command.getWho() == KNXLight.class && (command.getWhere() == getWhere() || (command.getWhere() != null &&
						command.getWhere().getPath().equals(command.getWhere().getURI().getFragment()) && (
								command.getWhere().getPath().equals(getWhere().getPath()) ||
								command.getWhere().getPath().equals(getWhere().getURI().getFragment())
						)))) {
					
					notifyStateChange(command);
				}
			}
		}, new Consumer<Throwable>() {
			@Override
			public void accept(Throwable arg0) throws Exception {
				// TODO notifyStateChangeError(oldWhat, newWhat, result);
				System.out.println("Error");
				arg0.printStackTrace();
			}
		}, new Action() {
			@Override
			public void run() throws Exception {
				System.out.println("Finish");
			}
		});
	}
	
	@Override
	protected void set(String name, State<?> state) {
		
		if (state == null || name == null) {
			throw new NullPointerException("Could not set null state name.");
		}

		// For demo only: Override the where to put the same address in the from and the to which is the write address
		Where where = new Where(getWhere().getUri());
		Command command = new Command(getClass(), new What(name, state), where, Type.COMMAND, this);

		if (log.finest) {
			log.fine(Session.Command, "Event send [".concat(command.toString()).concat("]"));
		}

		// The command is sent to the gateway. Gateway transmits it to the actuator.
		// If everything is fine, Gateway provides through the monitor session
		// the new status => not need to set it here since it will be set by the
		// monitor way.
		server.offer(command);
	}
}
