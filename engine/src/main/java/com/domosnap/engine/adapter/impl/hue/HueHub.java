package com.domosnap.engine.adapter.impl.hue;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import okhttp3.Response;

public class HueHub {
	
	private final static HueHub hueHub = new HueHub();
	
	private Map<HueCommanderConsumer, String> commanderList = new HashMap<HueCommanderConsumer, String>();
	private Map<HueMonitorPublisher, String> monitorList = new HashMap<HueMonitorPublisher, String>();
	
	private HueHub() {
	}

	public static HueHub getInstance() {
		return hueHub;
	}
	
	public void pingMonitor(HueCommanderConsumer commander) {
		
		for (HueMonitorPublisher monitor : getMonitor(commander)) {
//			monitor.lock.notify(); // wake up monitor to get the response NOW and not wait next pull
			// TODO A REVOIR
			 Request request = new Request.Builder()
				      .url("http://" + monitor.getIp() + ":" + monitor.getPort() + "/api/" + monitor.getUser() + "/lights")
				      .build();
			 Response response = null;

			 try {
					// 1. Call Rest API
					response = monitor.client.newCall(request).execute();
					monitor.onMessageReceipt(response);
			 }catch (Exception e) {
				// TODO: handle exception
				 e.printStackTrace();
			}
		}
	}

	private List<HueMonitorPublisher> getMonitor(HueCommanderConsumer commander) {
		
		List<HueMonitorPublisher> result = new ArrayList<HueMonitorPublisher>();
		String host = commanderList.get(commander);
		
		for (HueMonitorPublisher monitor : monitorList.keySet()) {
			if (host.equals(monitorList.get(monitor))) {
				result.add(monitor);
			}
		}
		return result;
	}
	
	public void registerMonitor(HueMonitorPublisher monitor, String host) {
		monitorList.put(monitor, host);
	}
	
	public void removeMonitor(HueMonitorPublisher monitor) {
		monitorList.remove(monitor);
	}

	public void registerCommander(HueCommanderConsumer commander, String host) {
		commanderList.put(commander, host);
	}
	
	public void removeCommander(HueCommanderConsumer commander) {
		commanderList.remove(commander);
	}
}
