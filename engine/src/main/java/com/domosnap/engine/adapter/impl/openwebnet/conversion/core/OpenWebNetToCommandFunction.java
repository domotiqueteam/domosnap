package com.domosnap.engine.adapter.impl.openwebnet.conversion.core;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.UnSupportedStateException;
import com.domosnap.engine.adapter.core.UnknownControllerListener;
import com.domosnap.engine.adapter.core.UnknownStateException;
import com.domosnap.engine.adapter.core.UnknownStateValueException;
import com.domosnap.engine.adapter.core.UnknownWhoException;
import com.domosnap.engine.adapter.core.Command.Type;
import com.domosnap.engine.adapter.impl.openwebnet.OpenWebNetControllerAdapter;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.parser.ParseException;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.parser.TokenMgrError;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.controller.where.URITools;
import com.domosnap.engine.controller.where.Where;
import com.domosnap.engine.house.Label;

import io.reactivex.functions.Function;

public class OpenWebNetToCommandFunction implements Function<String, Publisher<Command>> {

	private Log log = new Log(OpenWebNetToCommandFunction.class.getSimpleName());
	
	private List<UnknownControllerListener> unknownControllerListenerList = Collections.synchronizedList(new ArrayList<UnknownControllerListener>());
	private List<Controller> controllerList = Collections.synchronizedList(new ArrayList<Controller>());
	
	private String ip;
	private int port;
	private Integer password;
	
	public OpenWebNetToCommandFunction(String ip, int port, Integer password) {
		super();
		this.ip = ip;
		this.port = port;
		this.password = password;
	}


	@Override
	public Publisher<Command> apply(final String message) throws Exception {

			return new Publisher<Command>() {
				@Override
				public void subscribe(Subscriber<? super Command> arg0) {
					try {
						OpenWebNetCommand command = new OpenWebNetCommand(message);

						if (!(command.isDimensionCommand() || command.isStandardCommand())) {
							log.severe(Session.Monitor, "Message received [" + message +"] don't match with standard command or dimension command.", 1);
							return;
						}

						boolean known = false;
						// TODO manage message on other bus
						if (command.isGeneralCommand()) {
							// We send command to all correct address
							for (int i = 11; i < 100; i++) {
								if (i % 10 != 0) { // group address (20, 30, ..) are not correct
									known |= updateController( ""+i, command, arg0);
								}
							}
						} else if (command.isGroupCommand()) {
							// We send command to group address
							synchronized (controllerList) {
								for (Controller controller : controllerList) {
									for (Label label : controller.getLabels()) {
										if (label.getId().equals(command.getGroup()) &&
												controller.getClass().equals(command.getWho())) {
											known = true;
											if (command.isStandardCommand()) {
												for(What what : command.getWhat(controller.getStateMap())) {
													Command c = new Command(command.getWho(), what, controller.getWhere(), Type.COMMAND, controller);
													arg0.onNext(c);
												}
											} else {
												for(What what : command.getDimension(controller.getStateMap())) {
													Command c = new Command(command.getWho(), what, controller.getWhere(), Type.COMMAND, controller);
													arg0.onNext(c);
												}
											}
											log.finest(Session.Monitor, "Message ["+ command.toString() + "] forwarded to controller.", 1);
										}
									}
								}
							}
						} else if (command.isEnvironmentCommand()) {
							String environment = command.getEnvironment();
							// We send ambiance command to address
							for (int i = 1; i < 10; i++) {
								known |= updateController(environment+i, command, arg0);
							}
						} else {
							// Command direct on a controller
							known = updateController(command.getWhere(), command, arg0);
						}

						if (!known) {
							// Detected unknown device
							log.finest(Session.Monitor, "Unknown controller detected [" +command.toString() + "].");
							synchronized (unknownControllerListenerList) {
								for (UnknownControllerListener listener : unknownControllerListenerList) {
									if (command.isStandardCommand()) {
										for (What what : command.getWhat(null)) {
											Command c = new Command(command.getWho(), what, new Where(URITools.generateURI(OpenWebNetControllerAdapter.SCHEMA, password == null ? null : String.valueOf(password), ip, port, command.getWhere())), Type.COMMAND, null);
											try {
												listener.foundUnknownController(c);
											} catch (Exception e) {
												log.warning(Session.Monitor, "Error with a listener when found an unknown controller on message [" + message +"]. Message dropped.");
											}
											arg0.onNext(c);
										}
									} else {
										for (What what : command.getDimension(null)) {
											Command c = new Command(command.getWho(), what, new Where(URITools.generateURI(OpenWebNetControllerAdapter.SCHEMA, password == null ? null : String.valueOf(password), ip, port, command.getWhere())), Type.COMMAND, null);
											try {
												listener.foundUnknownController(c);
											} catch (Exception e) {
												log.warning(Session.Monitor, "Error with a listener when found an unknown controller on message [" + message +"]. Message dropped.");
											}
											arg0.onNext(c);
										}
									}
								}
							}
						}
				} catch (ParseException e) {
					log.warning(Session.Monitor, "Unknown message received [" + message +"]. Message dropped.", 1);
				} catch (UnknownStateException e) {
					log.warning(Session.Monitor, "Unknown state received [" + message +"]. Message dropped.", 1);
				} catch (UnknownWhoException e) {
					log.warning(Session.Monitor, "Unknown who received [" + message +"]. Message dropped.", 1);
				} catch (UnSupportedStateException e) {
					log.warning(Session.Monitor, "UnSupportedState ["+ message + "]. Message dropped.", 1);
				} catch (UnknownStateValueException e) {
					log.warning(Session.Monitor, "UnknowStateValue ["+ message + "]. Message dropped.", 1);
				} catch (TokenMgrError e) {
					log.severe(Session.Monitor, "Received unexpected message ["+ message + "]. Message dropped but probably there is a serious problem with protocol implementation. ", 1);
					throw e;
				} catch (Exception e) {
					log.severe(Session.Monitor, "Exception occurs with message [" + message +"]. Message dropped. " +e.getMessage(), 1);
				}
			}
		};
	} 
	
	
	public void addUnknownControllerListener(UnknownControllerListener unknownControllerListener) {
		if (!unknownControllerListenerList.contains(unknownControllerListener)) {
			unknownControllerListenerList.add(unknownControllerListener);
		}
	}
	
	public void removeUnknownControllerListener(UnknownControllerListener unknownControllerListener) {
		unknownControllerListenerList.remove(unknownControllerListener);
	}

	
	public void addKnownController(Controller controller) {
		controllerList.add(controller);
	}
	
	private boolean updateController(String where, OpenWebNetCommand command, Subscriber<? super Command> arg0) throws UnknownStateException, UnknownWhoException, UnSupportedStateException, UnknownStateValueException {
		boolean known = false;

		// Manage what command
		synchronized (controllerList) {
			for (Controller controller : controllerList) {
				if (command.getWho().equals(controller.getClass()) && where.equals(controller.getWhere().getPath())) {
					known = true;
					if (command.isStandardCommand()) {
						for(What what : command.getWhat(controller.getStateMap())) {
							Command c = new Command(command.getWho(), what, controller.getWhere(), Type.COMMAND, controller);
							log.finest(Session.Monitor, "Message ["+ command.toString() + "] transformed to command [" + c + "].", 1);
							arg0.onNext(c);
						}
					} else {
						for(What what : command.getDimension(controller.getStateMap())) {
							Command c = new Command(command.getWho(), what, controller.getWhere(), Type.COMMAND, controller);
							log.finest(Session.Monitor, "Message ["+ command.toString() + "] transformed to command [" + c + "].", 1);
							arg0.onNext(c);
						}
					}

					break;
				}
			}
		}
		return known;
	}
	

	public void setIp(String ip) {
		this.ip = ip;
	}


	public void setPort(int port) {
		this.port = port;
	}


	public void setPassword(Integer password) {
		this.password = password;
	}
}
