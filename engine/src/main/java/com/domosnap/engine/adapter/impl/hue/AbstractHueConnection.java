package com.domosnap.engine.adapter.impl.hue;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.domosnap.engine.Log;
import com.domosnap.engine.adapter.core.ConnectionListener;
import com.domosnap.engine.adapter.core.ConnectionStatusEnum;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class AbstractHueConnection {

	final protected OkHttpClient client = new OkHttpClient();
	protected final Log log = new Log(this.getClass().getSimpleName());
	private boolean connected = false;

	
	final Object lock = new Object();
	protected final Object lockPool = new Object();
	protected ExecutorService pool = null;
	protected boolean forceConnection = true;
	private final int poolSize = 5;
	private String user = "w6uChF4XDX-WEN9mm1Wx5BatBOrZLTos3W-p2H75";
	private String ip;
	private int port;
	private int timeout = 5000;
	private List<ConnectionListener> connectionListenerList = Collections
			.synchronizedList(new ArrayList<ConnectionListener>());
	private Log.Session session;

	/**
	 * 
	 * @param ip
	 *            the ip or dns name of the open server
	 * @param port
	 *            the port number of the open server
	 * @param passwordOpen
	 *            not supported actually
	 */
	public AbstractHueConnection(String ip, int port, String user, Log.Session session) {
		this.ip = ip;
		this.port = port;
		this.user = user;
		this.session = session;

	}

	/**
	 * Return the ip of the open server.
	 * 
	 * @return the ip of the open server.
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * Define the ip of the open server.
	 * 
	 * @param ip
	 *            the ip of the open server.
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * Return the port of the open server.
	 * @return the port of the open server.
	 */
	public int getPort() {
		return port;
	}

	/**
	 * Define the port of the open server.
	 * @param port the port of the open server.
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * Define the gateway password.
	 * 
	 * @param passwordOpen
	 */
	public void setUser(String user) {
		this.user = user;
	}

	public String getUser() {
		return user;
	}

	/**
	 * Return the timeout of the connection to open server in millisecond.
	 * 
	 * @return the timeout of the connection to open server in millisecond.
	 */
	public int getTimeout() {
		return timeout;
	}

	/**
	 * Define the timeout of the connection to open server in millisecond.
	 * 
	 * @param timeout
	 *            the timeout of the connection to open server in millisecond.
	 */
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	/**
	 * Open the connection. If connection is not possible it will try again
	 * later:
	 * <li>
	 * <ul>
	 * after some time for monitor
	 * </ul>
	 * <ul>
	 * at the next command for commander
	 * </ul>
	 * </li>
	 * 
	 * @return
	 */
	public boolean connect() {
		forceConnection = true;
		// Make connection in thread to avoid blocking the user!
		getExecutorService().execute(new ConnectHandler()); 
		return true;
	}

	/**
	 * Close the client and stop the thread.<br/>
	 * No more connection will be try before method connect will be call again.
	 */
	public void disconnect() {
		if (connected) {
			connected = false;
			forceConnection = false;
			synchronized (connectionListenerList) {
				for (ConnectionListener connectionListener : connectionListenerList) {
					try {
						connectionListener.onClose();
					} catch (Exception e) {
						log.severe(session, getFormattedLog(client, 1,
								"ConnectionListener raise an error [" + e.getMessage() + "]"));
					}
				}
			}
			log.fine(session,
					getFormattedLog(client, 0, "End connection: " + session.name() + " session closed..."));

			synchronized (lockPool) {
				if (pool != null) {
					pool.shutdown();
					try {
						if (!pool.awaitTermination(500, TimeUnit.MILLISECONDS)) {
							List<Runnable> droppedTasks = pool.shutdownNow();
							log.warning(session,
									"Executor did not terminate in the specified time.");
							log.warning(session, "Executor was abruptly shut down. "
									+ droppedTasks.size() + " tasks will not be executed.");
						}
					} catch (InterruptedException e) {
						log.severe(session,
								"Executor error during abruptly shut down. " + e.getMessage());
					}
					pool = null;
				}
			}
		}
	}

	public boolean isConnected() {
		return connected;
	}

	public void addConnectionListener(ConnectionListener connectionListener) {
		if (!connectionListenerList.contains(connectionListener)) {
			connectionListenerList.add(connectionListener);
		}
	}

	public void removeConnectionListener(ConnectionListener connectionListener) {
		connectionListenerList.remove(connectionListener);
	}

	protected ExecutorService getExecutorService() {
		synchronized (lockPool) {
			if (pool == null) {
				pool = Executors.newFixedThreadPool(poolSize);
			}
		}

		return pool;
	}

	protected class ConnectHandler implements Runnable {
		public void run() {
			synchronized (lock) { // mutex on the main thread: only one
									// connection or send message at the same
									// time!
				if (!isConnected()) { // Test again since with the lock, maybe a
										// previous thread has opened the
										// connection!


					String ip = getIp();

					if (log.finest) {
						log.fine(session,
							getFormattedLog(client, 1, "HttpClient created [" + ip + "]"));
					}

					log.finest(session,
							getFormattedLog(client, 1, " ----- Step Connection ----- "));
					
					Request request = new Request.Builder()
						      .url("http://" + ip + ":" + port + "/api/" + user + "/lights")
						      .build();

					Response response;
					try {
						response = client.newCall(request).execute();
					} catch (IOException e) {
						callOpenWebConnectionListenerConnect(ConnectionStatusEnum.UnkownError);
						return;
					}

					if (!response.isSuccessful()) {
						// Bad return message
						log.severe(session,
								getFormattedLog(client, 1, "Bad message [" + response.body().toString() + "] received from [" + ip + "]"));
						callOpenWebConnectionListenerConnect(ConnectionStatusEnum.WrongAcknowledgement);
						return;
					}

					log.fine(session, getFormattedLog(client, 1, "Connection OK"));
					connected=true;
					callOpenWebConnectionListenerConnect(ConnectionStatusEnum.Connected);
					return;
				}
			}
		}

		private void callOpenWebConnectionListenerConnect(ConnectionStatusEnum connection) {
			synchronized (connectionListenerList) {
				for (ConnectionListener connectionListener : connectionListenerList) {
					try {
						connectionListener.onConnect(connection);
					} catch (Exception e) {
						log.severe(session, getFormattedLog(client, 1,
								"ConnectionListener raise an error [" + e.getMessage() + "]"));
					}
				}
			}
		}
	}

	private static int countConnection = 0;
	private static int countSession = 0;
	private static Map<Integer, String> mapSessionInstance = new ConcurrentHashMap<Integer, String>();
	private static Map<Integer, String> mapConnectionInstance = new ConcurrentHashMap<Integer, String>();

	public String getFormattedLog(Object object, int level, String msg) {

		String connectionNum = "Connection null";
		if (object != null) {
			if (mapConnectionInstance.containsKey(object.hashCode())) {
				connectionNum = mapConnectionInstance.get(object.hashCode());
			} else {
				connectionNum = "Connection " + pad(countConnection++, 4);
				mapConnectionInstance.put(object.hashCode(), connectionNum);
			}
		}

		String sessionNum;
		if (mapSessionInstance.containsKey(this.hashCode())) {
			sessionNum = mapSessionInstance.get(this.hashCode());
		} else {
			sessionNum = session.name() + pad(countSession++, 5);
			mapSessionInstance.put(this.hashCode(), sessionNum);
		}

		StringBuilder sb = new StringBuilder();
		sb.append("[").append(sessionNum).append("]-");
		sb.append("[").append(connectionNum).append("] : ");

		for (int i = 0; i < level; i++) {
			sb.append("   ");
		}

		sb.append(msg);
		return sb.toString();

	}

	private String pad(int i, int pad) {
		String s = "" + i;
		while (s.length() < pad) {
			s = " ".concat(s);
		}
		return s;
	}
}
