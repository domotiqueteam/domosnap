package com.domosnap.engine.adapter;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.core.ScanListener;
import com.domosnap.engine.adapter.impl.hue.HueControllerAdapter;
import com.domosnap.engine.adapter.impl.openwebnet.OpenWebNetControllerAdapter;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.where.Where;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


/**
 * {@link ControllerFactory} implements features to build or/and find {@link ControllerAdapter}. 
 */
public class ControllerFactory {
	
	private static Log log = new Log(ControllerFactory.class.getSimpleName());
	
	private static Map<String, Class<? extends ControllerAdapter>> adapterList = new HashMap<>();
	private static Map<String, ControllerAdapter> adapterInstancesList = new HashMap<>();
		
	static {
		registerProtocole("openwebnet", OpenWebNetControllerAdapter.class);
		registerProtocole("hue", HueControllerAdapter.class);
//		registerProtocole("knx", KNXControllerService.class);
	}
	
	/**
	 * Register a new protocol.
	 * @param schema Protocol name.
	 * @param controllerAdapter Class of the ControllerAdapter associated to the protocol
	 */
	public static void registerProtocole(String schema, Class<? extends ControllerAdapter> controllerAdapter) {
		ControllerFactory.adapterList.put(schema, controllerAdapter);
	}

	/**
	 * Unregister a protocol.
	 * @param schema protocol name.
	 */
	public static void unRegisterProtocole(String schema) {
		ControllerFactory.adapterList.remove(schema);
	}

	/**
	 * Return the list of supported protocol. Supported protocol are all register protocole.
	 * @return the list of supported protocol.
	 */
	public static Set<String> getSupportedProtocole() {
		return adapterList.keySet();
	}
	
	/**
	 * Return an instance of a {@link ControllerAdapter}.
	 * @param url uri of the procole gateway. As example: "openwebnet://192.168.1.35:20000/"
	 * @return an instance of {@link ControllerAdapter}
	 */
	public static ControllerAdapter getControllerAdapter(String url) {
		
		URI uri = URI.create(url);
		String schema = uri.getScheme();
		String server = uri.getHost();
		int port = uri.getPort();
		String username = uri.getUserInfo();
		
		String key = schema + server + port + username;
		
		ControllerAdapter  result = ControllerFactory.adapterInstancesList.get(key);
		if (result == null) {
			try {
				String protocole = uri.getScheme();
				Class<? extends ControllerAdapter> c = ControllerFactory.adapterList.get(protocole);
				result= c.newInstance();
				result.connect(url);
				ControllerFactory.adapterInstancesList.put(key, result);
			} catch (InstantiationException | IllegalAccessException e) {
				log.info(Session.Other, e.getMessage());
			}
		}
		
		return result;
	}
	
	/**
	 * Create a controller. Controller created is already connected
	 * to open web server (with a monitor session and a command session):
	 * this means that when you get the status ({@link Controllerstream#getStatus(String)})
	 * or change it, the order is sent to the open web server. 
	 * @param clazz the class of the controller you would like create.
	 * @param where the address of the device
	 * @return the new created and initialized controller
	 */
	public static <T extends Controller> T createController(
			Class<T> clazz, Where where) {
		return getControllerAdapter(where.getUri()).createController(clazz, where);
	}
	
	/**
	 * Create a controller. Controller created is already connected
	 * to open web server (with a monitor session and a command session):
	 * this means that when you get the status ({@link Controllerstream#getStatus(String)})
	 * or change it, the order is sent to the open web server. 
	 * @param clazz the class of the controller you would like create.
	 * @param where address of the device
	 * @return the new created and initialized controller
	 */
	public static <T extends Controller> List<T> createControllers(
			Class<T> clazz, Where... where) {
		
		Map<ControllerAdapter, List<Where>> whereByControllerAdapter = new HashMap<>();
		
		for (int i = 0; i < where.length; i++) {
			ControllerAdapter ca = getControllerAdapter(where[i].getUri());
			List<Where> whereList = whereByControllerAdapter.get(ca);
			if (whereList == null) {
				whereList = new ArrayList<>();
				whereByControllerAdapter.put(ca, whereList);
			}
			whereList.add(where[i]);
		}
		
		List<T> result = new ArrayList<>();
		for (ControllerAdapter ca : whereByControllerAdapter.keySet()) {
			List<Where> lw = whereByControllerAdapter.get(ca);
			result.addAll(ca.createControllers(clazz, lw.toArray(new Where[lw.size()])));
		}
		
		return result;
	}
	
	/**
	 * Lunch a scan on all protocol and gateway. This feature scan the physical device to find Controller. When a controller
	 * is find, {@link ScanListener#foundController(com.domosnap.engine.controller.who.Who, Where, Controller)} is called.
	 * @param listener callback when a controller is found.
	 */
	public static void scan(ScanListener listener) {
		adapterInstancesList.values().iterator().forEachRemaining(new Consumer<ControllerAdapter>() {
			@Override
			public void accept(ControllerAdapter t) {
				t.scan(listener);
			}
		});
	}
}
