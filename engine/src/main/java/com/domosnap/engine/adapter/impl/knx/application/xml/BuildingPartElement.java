package com.domosnap.engine.adapter.impl.knx.application.xml;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class BuildingPartElement extends XMLElement {
	
	private String type;
	private BuildingPartElement parent;
	private Collection<BuildingPartElement> parts;
	private Collection<String> deviceInstanceRefs;
	
	public BuildingPartElement(BuildingPartElement parent, String id, String name, String type) {
		super(id, name);
		this.parent = parent;
		this.type = type;
		parts = new ArrayList<BuildingPartElement>(1);
		deviceInstanceRefs = new ArrayList<String>();
	}
	
	public BuildingPartElement getParent() {
		return parent;
	}

	public String getType() {
		return type;
	}
	
	public void addDeviceInstance(String id) {
		deviceInstanceRefs.add(id);
	}
	
	public void addPart(BuildingPartElement part) {
		parts.add(part);
	}
	
	public Collection<BuildingPartElement> getParts() {
		return Collections.unmodifiableCollection(parts);
	}
	
	public Collection<String> getDeviceInstanceRefs() {
		return Collections.unmodifiableCollection(deviceInstanceRefs);
	}
	
	@Override
	public String toString() {
		return new StringBuilder()
				.append(getName()).append(" (")
				.append(getType()).append(")")
				.toString();
	}
}
