package com.domosnap.engine.adapter.impl.openwebnet.conversion.heating;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.domosnap.engine.adapter.core.UnSupportedStateException;
import com.domosnap.engine.adapter.core.UnknownStateException;
import com.domosnap.engine.adapter.core.UnknownStateValueException;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.Converter;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.dimension.DimensionStatus;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.dimension.DimensionValue;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.heating.dimension.ActuatorStatus;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.heating.dimension.DesiredTemperature;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.heating.dimension.MeasureTemperature;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.heating.dimension.ProbeTemperature;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.heating.dimension.SetOffset;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.heating.HeatingZone;
import com.domosnap.engine.controller.heating.HeatingZoneStateName;
import com.domosnap.engine.controller.heating.stateValue.HeatingZoneState;
import com.domosnap.engine.controller.heating.stateValue.OffsetState;
import com.domosnap.engine.controller.what.State;
import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.controller.what.impl.BooleanState;
import com.domosnap.engine.controller.what.impl.DoubleState;
import com.domosnap.engine.controller.what.impl.OnOffState;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


public class HeatingZoneConverter implements Converter {
	
	public static String OPEN_WEB_WHO = "4";
	
	public enum HeatingZoneStatus {
		HEATING_MODE("1"),
		HEATING_OFF("303"),
		THERMAL_PROTECTION("202");

		private String openWebNetCode;

		private HeatingZoneStatus(String code) {
			this.openWebNetCode = code;
		}

		public String getCode() {
			return openWebNetCode;
		}
		
		public static HeatingZoneStatus fromValue(String code) {
			for (HeatingZoneStatus zone: HeatingZoneStatus.values()) {
				if (zone.getCode().equals(code))
					return zone;
			}
			return null;
		}
	}
	
	public enum HeatingZoneDimension {
		MEASURE_TEMPERATURE("0"), 
		FAN_COIL_SPEED("11"),
		PROBE_STATUS("12"),
		LOCAL_OFFSET("13"),
		SET_TEMPERATURE("14"),
		VALVE_STATUS("19"),
		ACTUATOR_STATUS("20");
	
		private String openWebNetCode;
	
		private HeatingZoneDimension(String openWebNetCode) {
			this.openWebNetCode = openWebNetCode;
		}
	
		public String getCode() {
			return openWebNetCode;
		}
		
		public static HeatingZoneDimension fromValue(String code) {
			for (HeatingZoneDimension gd : HeatingZoneDimension.values()) {
				if (gd.getCode().equals(code))
					return gd;
			}
			
			return null;
		}
	}

	@Override
	public List<String> convert(What what, Map<String, State<?>> controllerStateList)
			throws UnknownStateException, UnSupportedStateException, UnknownStateValueException {
		if (what == null) {
			return null;
		}
		
		List<String> result = new ArrayList<String>();
		HeatingZoneStateName s = HeatingZoneStateName.valueOf(what.getName());
		
		switch (s) {
			case status:
				HeatingZoneState state = (HeatingZoneState) what.getValue();
				switch (state) {
					case HEATING_MODE:
						result.add(HeatingZoneStatus.HEATING_MODE.getCode());
						break;
					case OFF:
						result.add(HeatingZoneStatus.HEATING_OFF.getCode());
						break;
					case THERMAL_PROTECTION:
						result.add(HeatingZoneStatus.THERMAL_PROTECTION.getCode());
						break;
					default:
						new UnknownStateException();
						break;
				}
				break;
			default:
				throw new UnknownStateException();
		}		
		return result;
	}

	@Override
	public List<DimensionStatus<?>> convertDimension(What what, Map<String, State<?>> controllerStateList)
			throws UnknownStateException, UnSupportedStateException, UnknownStateValueException {

		HeatingZoneStateName s = HeatingZoneStateName.valueOf(what.getName());
		List<DimensionStatus<?>> result = new ArrayList<DimensionStatus<?>>();
		
		switch (s) {
			case local_offset:
				SetOffset so = new SetOffset();
				if (what.getValue() != null) {
					so.setStateValue((OffsetState) what.getValue());
				}
				result.add(so);
				break;
			case measure_temperature:
				MeasureTemperature mt = new MeasureTemperature();
				if (what.getValue() != null) {
					mt.setStateValue((DoubleState) what.getValue());
				}
				result.add(mt);
				break;
			case set_temperature:
				DesiredTemperature dt = new DesiredTemperature();
				if (what.getValue() != null) {
					dt.setStateValue((DoubleState) what.getValue());
					dt.setMode(1); // TODO Actually heating mode is hard coded...
				}
				result.add(dt);
				break;
			case actuator_status:
				ActuatorStatus as = new ActuatorStatus();
				if (what.getValue() != null) {
					as.setActuatorStatus(((BooleanState) what.getValue()).getValue());
				}
				result.add(as);
				break;
			case probe_temperature:
				throw new UnSupportedStateException(); // not writable
				
			default:
				throw new UnknownStateException();
		}
		
		return result;
	}

	@Override
	public List<What> convert(String code, Map<String, State<?>> controllerStateList)
			throws UnknownStateException, UnSupportedStateException, UnknownStateValueException {
		HeatingZoneStatus c = HeatingZoneStatus.fromValue(code);
		List<What> result = new ArrayList<What>();
		switch (c) {
		case HEATING_MODE:
			result.add(new What(HeatingZoneStateName.status.name(), HeatingZoneState.HEATING_MODE));
			break;
		case HEATING_OFF:
			result.add(new What(HeatingZoneStateName.status.name(), HeatingZoneState.OFF));
			break;
		case THERMAL_PROTECTION:
			result.add(new What(HeatingZoneStateName.status.name(), HeatingZoneState.THERMAL_PROTECTION));
			break;
		default:
			throw new UnknownStateException();
		}
		
		return result;
	}

	@Override
	public List<What> convertDimension(String dimension, List<DimensionValue> dimensionValueList,
			Map<String, State<?>> controllerStateList) throws UnknownStateException, UnSupportedStateException, UnknownStateValueException {
		HeatingZoneDimension gd = HeatingZoneDimension.fromValue(dimension);
		List<What> l = new ArrayList<What>();
		if (gd != null) {
			What what;
			switch (gd) {
			case ACTUATOR_STATUS:
				if ("1".equals(dimensionValueList.get(0).getValue())) {
					what = new What(HeatingZoneStateName.actuator_status.name(), OnOffState.On());	
				} else {
					what = new What(HeatingZoneStateName.actuator_status.name(), OnOffState.Off());
				}
				l.add(what);
				break;
				
			case FAN_COIL_SPEED:
				throw new UnSupportedStateException();
				
			case LOCAL_OFFSET:
				SetOffset so = new SetOffset();
				so.setValueList(dimensionValueList);
				what = new What(HeatingZoneStateName.local_offset.name(), so.getStateValue());
				l.add(what);
				break;
				
			case MEASURE_TEMPERATURE:
				MeasureTemperature mt = new MeasureTemperature();
				mt.setValueList(dimensionValueList);
				what = new What(HeatingZoneStateName.measure_temperature.name(), mt.getStateValue());
				l.add(what);
				break;
				
			case PROBE_STATUS:
				ProbeTemperature pt = new ProbeTemperature();
				pt.setValueList(dimensionValueList);
				what = new What(HeatingZoneStateName.probe_temperature.name(), pt.getStateValue());
				l.add(what);
				break;
				
			case SET_TEMPERATURE:
				DesiredTemperature dt = new DesiredTemperature();
				dt.setValueList(dimensionValueList);
				what = new What(HeatingZoneStateName.set_temperature.name(), dt.getStateValue());
				l.add(what);
				break;
				
			case VALVE_STATUS:
				throw new UnSupportedStateException();
				
			default:
				throw new UnknownStateException();
			}
			
			return l;
		
		} else {
			throw new UnknownStateException();
		}
	}

	@Override
	public boolean isDimension(What what) {
		
		if (HeatingZoneStateName.local_offset.name().equals(what.getName())) {
			return true;
		} else if (HeatingZoneStateName.measure_temperature.name().equals(what.getName())) {
			return true;
		} else if (HeatingZoneStateName.set_temperature.name().equals(what.getName())) {
			return true;
		} else if (HeatingZoneStateName.status.name().equals(what.getName())) {
			return false;
		} else if (HeatingZoneStateName.actuator_status.name().equals(what.getName())) {
			return false;
		} 	
		return false;
	}

	@Override
	public String getOpenWebWho() {
		return OPEN_WEB_WHO;
	}

	@Override
	public Class <? extends Controller> getHomeSnapWho() {
		return HeatingZone.class;
	}
}
