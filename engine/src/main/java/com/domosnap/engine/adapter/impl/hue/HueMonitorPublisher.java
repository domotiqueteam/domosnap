package com.domosnap.engine.adapter.impl.hue;

import java.io.IOException;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;

import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import okhttp3.Request;
import okhttp3.Response;

public class HueMonitorPublisher extends AbstractHueConnection implements FlowableOnSubscribe<Response> {

	private List<FlowableEmitter<Response>> suscriberList = Collections
			.synchronizedList(new ArrayList<FlowableEmitter<Response>>());

	protected final Log log = new Log(HueMonitorPublisher.class.getSimpleName());
	private int timerPooling = 10000;

	public HueMonitorPublisher(String host, int port, String user) {
		super(host, port, user, Session.Monitor);
		HueHub.getInstance().registerMonitor(this, host);
	}

	@Override
	public void setIp(String ip) {
		if (getIp() != null && !"".equals(getIp()) && getIp() != ip) {
			HueHub.getInstance().removeMonitor(this);
		}
		HueHub.getInstance().registerMonitor(this, ip);
		super.setIp(ip);
		;
	}

	@Override
	public boolean connect() {
		forceConnection = true;
		getExecutorService().execute(new MonitorHandler());
		return true;
	}

	void onMessageReceipt(Response command) {
		try {
			for (FlowableEmitter<Response> suscriber : suscriberList) {
				suscriber.onNext(command);
			}
		} catch (Exception e) {
			log.severe(Session.Monitor, getFormattedLog(command, 1,
					"Exception occurs with message [" + command.toString() + "]. Message dropped. " + e.getMessage()));
		}
	}

	private class MonitorHandler implements Runnable {

		@Override
		synchronized public void run() {
			long timer = 200;
			do {

				if (isConnected()) {
					timer = 1000;

					Request request = new Request.Builder()
							.url("http://" + getIp() + ":" + getPort() + "/api/" + getUser() + "/lights").build();
					Response response = null;

					try {
						// 1. Call Rest API
						response = client.newCall(request).execute();

						onMessageReceipt(response);

						Thread.sleep(timerPooling);
					} catch (IOException e) {
						HueMonitorPublisher.this.disconnect();
					} catch (InterruptedException e) {
						HueMonitorPublisher.this.disconnect();
					}
				} else {
					// If not connected try to connect
					new ConnectHandler().run(); // Make connection in thread to avoid blocking the user!
					try {
						Thread.sleep(timer);
						timer = timer * 2;
						if (timer > 10000) {
							timer = 10000;
						}
					} catch (InterruptedException e) {
						log.finest(Session.Monitor, getFormattedLog(client, 1, "Timeout interrupted."));
					}
				}

			} while (forceConnection);
		}
	}

	@Override
	public void subscribe(FlowableEmitter<Response> e) throws Exception {
		suscriberList.add(e);
	}
}
