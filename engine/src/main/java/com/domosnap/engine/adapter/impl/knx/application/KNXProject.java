package com.domosnap.engine.adapter.impl.knx.application;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.impl.knx.application.xml.BuildingPartElement;
import com.domosnap.engine.adapter.impl.knx.application.xml.ComObjectElement;
import com.domosnap.engine.adapter.impl.knx.application.xml.ComObjectInstanceRefElement;
import com.domosnap.engine.adapter.impl.knx.application.xml.ComObjectRefElement;
import com.domosnap.engine.adapter.impl.knx.application.xml.DatapointTypeElement;
import com.domosnap.engine.adapter.impl.knx.application.xml.DeviceInstanceElement;
import com.domosnap.engine.adapter.impl.knx.application.xml.GroupAddressElement;
import com.domosnap.engine.adapter.impl.knx.application.xml.ProductElement;
import com.domosnap.engine.adapter.impl.knx.application.xml.TranslationElement;
import com.domosnap.engine.adapter.impl.knx.application.xml.XMLElement;
import com.domosnap.engine.adapter.impl.knx.application.xml.XMLElementMap;
import com.domosnap.engine.adapter.impl.knx.application.xml.handler.ApplicationProgramHandler;
import com.domosnap.engine.adapter.impl.knx.application.xml.handler.HardwareHandler;
import com.domosnap.engine.adapter.impl.knx.application.xml.handler.MasterHandler;
import com.domosnap.engine.adapter.impl.knx.application.xml.handler.ProjectHandler;
import com.domosnap.engine.adapter.impl.knx.application.xml.handler.XMLHandler;

import tuwien.auto.calimero.GroupAddress;

public class KNXProject {
	
	private static Log log = new Log(KNXProject.class.getSimpleName());
	
	private static SAXParserFactory parserFactory;

	private String name;
	
	/**
	 * Contains all {@link XMLElement} found in the configuration file
	 */
	private XMLElementMap xmlElements;
	
	/**
	 * The KNX group addresses information with their datapoint types
	 */
	private Map<String, GroupAddressInfo> gaInfos;
	
	/**
	 * 
	 */
	public KNXProject() {
		xmlElements = new XMLElementMap();
		gaInfos = new HashMap<String, GroupAddressInfo>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, GroupAddressInfo> getGAInfos() {
		return gaInfos;
	}
	
	public static KNXProject importProject(String filePath, String language) throws FileNotFoundException {
		
		File knxProjectFile = new File(filePath);
		if (!knxProjectFile.exists()) {
			throw new FileNotFoundException("KNX project file not found : "+ filePath);
		}
		
		KNXProject knxProject = new KNXProject();
		ZipFile zip;
		try {
			zip = new ZipFile(knxProjectFile);
			
			Enumeration<? extends ZipEntry> zipEntries = zip.entries(); 
			while (zipEntries.hasMoreElements()) {
				
				ZipEntry zipEntry = zipEntries.nextElement();
				String entryName = zipEntry.getName();
				
				if ("knx_master.xml".equals(entryName)) {
					knxProject.importKNXMaster(zip.getInputStream(zipEntry), language);
				}
				else if (entryName.startsWith("M-")) {
					
					String[] path = entryName.split("/");
					if (path.length == 2) {
						if ("Hardware.xml".equals(path[1])) { 
							knxProject.importHardware(zip.getInputStream(zipEntry), language);
						}
						else if (path[1].startsWith("M-")) {
							knxProject.importApplicationProgram(zip.getInputStream(zipEntry), language);
						}
					}
				}
				else if (entryName.startsWith("P-")) {
					
					String[] path = entryName.split("/");
					if (path.length == 2 && "0.xml".equals(path[1])) {
						knxProject.importProject(zip.getInputStream(zipEntry), language);
					}
				}
				else {
					// Ignore
				}
				
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
		knxProject.compile();
		return knxProject;
	}
	
	private static SAXParserFactory getParserFactory() {
		if (parserFactory == null) {
			parserFactory = SAXParserFactory.newInstance();
		}
		return parserFactory;
	}
	
	private void compile() {
		
		// Put the device instances in the building parts
		for (BuildingPartElement buildingPart : xmlElements.getAll(BuildingPartElement.class)) {
			compileBuildingPart(buildingPart);
		}
		
		Map<String, Map<String, Set<GroupAddressInfo>>> deviceGAGroups = new HashMap<String, Map<String, Set<GroupAddressInfo>>>();
		
		// Create GroupAddressInfo objects based on all device instances which are associated to a KNX GroupAddress
		for (DeviceInstanceElement deviceInstance : xmlElements.getAll(DeviceInstanceElement.class)) {
			
			// Retrieve the product and application program of the device instance
			ProductElement product = xmlElements.get(ProductElement.class, deviceInstance.getProductRefId());
			Map<String, Set<GroupAddressInfo>> appGAGroups = null;
			Set<GroupAddressInfo> gaiGroup = null;
			if (product.isRailMounted()) { // Determine relationships between GA only for output modules (generally rail mounted)
				
				appGAGroups = deviceGAGroups.get(deviceInstance.getUID());
				if (appGAGroups == null) {
					appGAGroups = new HashMap<String, Set<GroupAddressInfo>>();
					deviceGAGroups.put(deviceInstance.getUID(), appGAGroups);
				}
			}
			
			// Determine the DPT type of the group address related to the comObject instance
			for (ComObjectInstanceRefElement comObjectInstance : deviceInstance.geComObjectInstanceRefElements()) {
			
				GroupAddressElement gaElement = xmlElements.get(GroupAddressElement.class, comObjectInstance.getGroupAddressRefId());
				GroupAddress ga = gaElement.getGroupAddress();
				GroupAddressInfo gaInfo = gaInfos.get(ga.toString());
				ComObjectRefElement comObjectRef = xmlElements.get(ComObjectRefElement.class, comObjectInstance.getRefId()); // Get the related ComObjectRef
				ComObjectElement comObject = xmlElements.get(ComObjectElement.class, comObjectRef.getRefId()); // Get the related ComObject
			
				if (gaInfo == null) { // Check if the comObjectInstance has DPT type ...
					String dptId = comObjectInstance.getDatapointType();
					if (dptId == null) { // ... if not, try to get it from the comObjectRef ...
						dptId = comObjectRef.getDatapointType();
						if (dptId == null) { // ... if not specified, use the one of the comObject if exists.
							dptId = comObject.getDatapointType();
						}
					}
					
					DatapointTypeElement datapointType = xmlElements.get(DatapointTypeElement.class, dptId);
					gaInfo = new GroupAddressInfo(gaElement.getName(), ga, datapointType == null ? null : datapointType.getDPT());
					gaInfos.put(ga.toString(), gaInfo);
				}
				gaInfo.addComObjectInfos(comObjectInstance, deviceInstance);
				
				if (product.isRailMounted()) {
					gaiGroup = appGAGroups.get(comObject.getName());
					if (gaiGroup == null) {
						gaiGroup = new HashSet<GroupAddressInfo>(2);
						appGAGroups.put(comObject.getName(), gaiGroup);
					}
					gaiGroup.add(gaInfo);
				}
			}
		}
		
		log.fine(Session.Server, "==================================================");
		log.fine(Session.Server, "== GROUP ADDRESS ASSOCIATIONS                   ==");
		log.fine(Session.Server, "==================================================");
		
		for (GroupAddressInfo gai : gaInfos.values()) {
			log.fine(Session.Server, String.format("%s - %s", gai.toString(), gai.getDpt().getDescription()));
			
			for (Entry<ComObjectInstanceRefElement, DeviceInstanceElement> entry : gai.getComObjectInfos().entrySet()) {
				
				ComObjectInstanceRefElement comObjectInstance = entry.getKey();
				ComObjectRefElement comObjectRef = xmlElements.get(ComObjectRefElement.class, comObjectInstance.getRefId()); // Get the related ComObjectRef
				ComObjectElement comObject = xmlElements.get(ComObjectElement.class, comObjectRef.getRefId()); // Get the related ComObject
				DeviceInstanceElement deviceInstance = entry.getValue();
				
				String functionText = comObjectRef.getFunctionText() == null ? comObject.getFunctionText() : comObjectRef.getFunctionText();
				log.fine(Session.Server, String.format("  => %s - %s - %s - %s", deviceInstance.getAddress(), deviceInstance.getName(), comObject.getText(), functionText));
			}
		}
	}
	
	private void compileBuildingPart(BuildingPartElement buildingPart) {
		
		log.fine(Session.Server, buildingPart.toString());
		for (String deviceInstanceRefId : buildingPart.getDeviceInstanceRefs()) {
			
			DeviceInstanceElement deviceInstance = xmlElements.get(DeviceInstanceElement.class, deviceInstanceRefId);
			if (deviceInstance.getName() == null) {
				
				ProductElement product = xmlElements.get(ProductElement.class, deviceInstance.getProductRefId());
				deviceInstance.setName(product.getText());
			}
			log.fine(Session.Server, deviceInstance.getName() +" (Device)");
		}
		
		for (BuildingPartElement partElement : buildingPart.getParts()) {
			compileBuildingPart(partElement);
		}
	}
	
	private void importHardware(InputStream inputStream, String language) {
		
		HardwareHandler handler = new HardwareHandler();
		parseXMLEntry(inputStream, handler);
		xmlElements.addAll(handler.getHardwares());
		xmlElements.addAll(handler.getProducts());
		xmlElements.addAll(handler.getPrograms());
		translateElements(handler, language);
		handler.dispose();
	}
	
	private void importApplicationProgram(InputStream inputStream, String language) {
		
		ApplicationProgramHandler handler = new ApplicationProgramHandler();
		parseXMLEntry(inputStream, handler);
		xmlElements.addAll(handler.getApplicationPrograms());
		xmlElements.addAll(handler.getCommunicationObjects());
		xmlElements.addAll(handler.getComObjectRefs());
		translateElements(handler, language);
		handler.dispose();
	}
	
	private void importProject(InputStream inputStream, String language) {
		
		ProjectHandler handler = new ProjectHandler();
		parseXMLEntry(inputStream, handler);
		xmlElements.addAll(handler.getBuildingParts());
		xmlElements.addAll(handler.getDeviceInstances());
		xmlElements.addAll(handler.getGroupAddresses());
		translateElements(handler, language);
		handler.dispose();
	}
	
	private void importKNXMaster(InputStream inputStream, String language) {
		
		MasterHandler handler = new MasterHandler();
		parseXMLEntry(inputStream, handler);
		xmlElements.addAll(handler.getDatapointTypes());
		translateElements(handler, language);
		handler.dispose();
	}
	
	private void parseXMLEntry(InputStream inputStream, DefaultHandler handler) {
	
		SAXParser parser = null;
		try {
			parser = getParserFactory().newSAXParser();
			parser.parse(inputStream, handler);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void translateElements(XMLHandler handler, String language) {
		
		Collection<TranslationElement> translations = handler.getTranslations().get(language);
		if (translations != null) {
			for (TranslationElement te : translations) {
				
				XMLElement element = xmlElements.findById(te.getRefId());
				if (element != null) {
					element.translate(te);
				}
			}
		}
	}
	
	public static void main(String[] args) {
		try {
			KNXProject project = KNXProject.importProject("C:\\Users\\Olivier\\Desktop\\KNX\\Maison.knxproj.zip", "fr-FR");
			project.getName();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
