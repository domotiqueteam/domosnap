package com.domosnap.engine.adapter.core;

import java.net.URI;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.ControllerAdapter;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.where.Where;
import com.domosnap.engine.event.EventFactory;

import io.reactivex.Flowable;
import io.reactivex.functions.Consumer;
import io.reactivex.processors.PublishProcessor;


public abstract class AbstractControllerAdapter implements ControllerAdapter {

	private Log log = new Log(AbstractControllerAdapter.class.getSimpleName());

	protected Map<Class<?>, Map<Where, Controller>> controllerList = new ConcurrentHashMap<Class<?>, Map<Where,Controller>>();
	protected URI uri;
	protected Flowable<Command> monitor;
	protected PublishProcessor<Command> commander;

	public AbstractControllerAdapter() {
		this(null);
	}

	public AbstractControllerAdapter(String uri) {
		
		if( uri == null) {
			uri =  getSchema() + formatURI(null);
		}
		
		if (uri.indexOf("://") == -1) {
			uri = getSchema() + "://" + uri;
		}
		
		this.uri = URI.create(uri);
		
		monitor = createMonitorStream();
		monitor.subscribe(new Consumer<Command>() {
			
			@Override
			public void accept(Command t) throws Exception {
				EventFactory.SendEvent(Session.Monitor, t);		
			}
		});
		log.finest(Session.Monitor, "Monitor created.");
		
		commander = createCommandStream();
		
		PublishProcessor<Command> commanderStream = PublishProcessor.create();
		commanderStream.subscribe(commander);
		
		commander.subscribe(new Consumer<Command>() {
			
			@Override
			public void accept(Command t) throws Exception {
				EventFactory.SendEvent(Session.Command, t);		
			}
		});
		log.finest(Session.Command, "Commander created.");

	}
	
	@Override
	public <T extends Controller> List<T> createControllers(
			Class<T> clazz, Where... wheres) {
		List<T> list = new ArrayList<T>();
		for (Where where : wheres) {
			list.add(createController(clazz, where));
		}
		return list;
	}

	/* (non-Javadoc)
	 * @see com.homesnap.engine.services.ControllerService#createController(com.homesnap.engine.Category, java.lang.String)
	 */
	@Override
	public <T extends Controller> T createController(
			Class<T> clazz, Where where) {
		synchronized (controllerList) {
			Map<Where, Controller> map = controllerList.get(clazz);
			if (map == null) {
				map = new HashMap<Where, Controller>();
				controllerList.put(clazz,map);
			}

			where = new Where(formatURI(where));
			
			@SuppressWarnings("unchecked")
			T controller = (T) map.get(where);
			
			if (controller == null) {
			
				try {			
					controller = (T) clazz.newInstance();
					controller.addMonitor(getMonitor());
					controller.addCommander(getCommander());
					controller.setWhere(where);
					onCreatedController(controller);
					map.put(where, controller);
				} catch (Exception e) {
					log.severe(Session.Device, "Error when instanciate the controller [" + where + "]. " + e.getMessage());
				}
			}
			return controller;
		}
	}
	
	protected Flowable<Command> getMonitor() {
		return monitor;
	}
	
	protected PublishProcessor<Command> getCommander() {
		return commander;
	}
	
	protected abstract Flowable<Command> createMonitorStream();
	
	protected abstract PublishProcessor<Command> createCommandStream();

	protected abstract void onCreatedController(Controller c) throws Exception;
	
	protected abstract String formatURI(Where where);
	
	public abstract String getSchema();
}
