package com.domosnap.engine.adapter;

import java.util.List;

import com.domosnap.engine.adapter.core.ConnectionListener;
import com.domosnap.engine.adapter.core.ScanListener;
import com.domosnap.engine.adapter.core.UnknownControllerListener;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.where.Where;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


/**
 * {@link ControllerAdapter} implements features to build or/and find {@link Controller}. 
 */
public interface ControllerAdapter {

	/**
	 * Create a controller. Controller created is already connected
	 * to physical gateway (with a monitor session and a command session):
	 * this means that when you get the status ({@link Controller#get(String)})
	 * or change it, the order is sent to the gateway. 
	 * @param clazz the class of the controller you would like create.
	 * @param where the address of the device
	 * @return the new created and initialized controller
	 */
	public <T extends Controller> T createController(
			Class<T> clazz, Where where);
	
	/**
	 * Create a {@link Controller}. {@link Controller} created is already connected
	 * to gateway (with a stream to send command and a stream to received command):
	 * this means that when you get the status ({@link Controller#get(String)})
	 * or change it, the order is sent to the gateway. 
	 * @param clazz the class of the controller you would like create.
	 * @param where address of the device
	 * @return the new created and initialized controller
	 */
	public <T extends Controller> List<T> createControllers(
			Class<T> clazz, Where... where);

	/**
	 * Return true if stream are connected to gateway.
	 * @return true if stream are connected to gateway.
	 */
	public boolean isConnected();
	
	/**
	 * Add a listener on new monitor stream connection
	 * @param listener
	 */
	public void addMonitorConnectionListener(ConnectionListener listener);

	/**
	 * Remove a listener on new monitor stream connection
	 * @param listener
	 */
	public void removeMonitorConnectionListener(ConnectionListener listener);

	/**
	 * Add a listener on new command stream connection
	 * @param listener
	 */
	public void addCommanderConnectionListener(ConnectionListener listener);

	/**
	 * Remove a listener on new command stream connection
	 * @param listener
	 */
	public void removeCommanderConnectionListener(ConnectionListener listener);
	
	/**
	 * Add a listener on unknown {@link Controller} instance detected. An unknown controller is detected
	 * when monitor stream get a message and doesn't know the where.
	 * @param listener
	 */
	public void addUnknowControllerListener(UnknownControllerListener listener);

	/**
	 * Remove a listener on unknown {@link Controller} detected. An unknown controller is detected
	 * when monitor stream get a message and doesn't know the where.
	 * @param listener
	 */
	public void removeUnknowControllerListener(UnknownControllerListener listener);
	
	/**
	 * Force connection for Commander and Monitor stream if there is no connection.
	 * @param url new parameter connection
	 */
	public void connect(String url);

	/**
	 * Force connection for Commander and Monitor stream if there is no connection.
	 */
	public void connect();
	
	/**
	 *  Disconnect Commander and Monitor stream and clean up all resources used.
	 */
	public void disconnect();
	
	/**
	 * Lunch a scan. This feature scan the physical device to find Controller. When a controller
	 * is find, {@link ScanListener#foundController(com.domosnap.engine.controller.who.Who, Where, Controller)} is called.
	 * @param listener callback when a controller is found.
	 */
	public void scan(ScanListener listener);
	
	/**
	 * Return the list of supported device.
	 * @return list of supported device.
	 */
	public List<Class<? extends Controller>> getSupportedDevice();
}
