package com.domosnap.engine.adapter.impl.knx.application.xml.handler;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.domosnap.engine.adapter.impl.knx.application.KNXProject;
import com.domosnap.engine.adapter.impl.knx.application.xml.BuildingPartElement;
import com.domosnap.engine.adapter.impl.knx.application.xml.ComObjectInstanceRefElement;
import com.domosnap.engine.adapter.impl.knx.application.xml.DeviceInstanceElement;
import com.domosnap.engine.adapter.impl.knx.application.xml.GroupAddressElement;

import tuwien.auto.calimero.GroupAddress;
import tuwien.auto.calimero.IndividualAddress;
import tuwien.auto.calimero.exception.KNXIllegalArgumentException;

/**
 * 
 * @author DRIESBACH Olivier
 */
public class ProjectHandler extends XMLHandler {
	
	private static final String AREA = "Area";
	private static final String LINE = "Line";
	private static final String BUILDING_PART = "BuildingPart";
	private static final String DEVICE_INSTANCE = "DeviceInstance";
	private static final String DEVICE_INSTANCE_REF = "DeviceInstanceRef";
	private static final String COM_OBJECT_INSTANCE_REF = "ComObjectInstanceRef";
	
	private static final String SEND = "Send";
	private static final String GROUP_RANGE = "GroupRange";
	private static final String GROUP_ADDRESS = "GroupAddress";
	
	private KNXProject project;
	private int currentArea;
	private int currentLine;
	private DeviceInstanceElement currentDevice;
	private Collection<DeviceInstanceElement> deviceInstances;
	private ComObjectInstanceRefElement currentComObject;
	
	private BuildingPartElement currentBuildingPart;
	private int groupRange = 0;
	
	/**
	 * Building parts
	 */
	private List<BuildingPartElement> buildingParts;
	
	/**
	 * Group addresses
	 */
	private Collection<GroupAddressElement> groupAddresses;
	
	/**
	 * 
	 */
	public ProjectHandler() {
		this.buildingParts = new ArrayList<BuildingPartElement>();
		this.deviceInstances = new ArrayList<DeviceInstanceElement>();
		this.groupAddresses = new ArrayList<GroupAddressElement>();
	}
	
	public Collection<BuildingPartElement> getBuildingParts() {
		return Collections.unmodifiableCollection(buildingParts);
	}
	
	public Collection<DeviceInstanceElement> getDeviceInstances() {
		return Collections.unmodifiableCollection(deviceInstances);
	}
	
	public Collection<GroupAddressElement> getGroupAddresses() {
		return Collections.unmodifiableCollection(groupAddresses);
	}
	
	@Override
	public void dispose() {
		this.buildingParts.clear();
		this.deviceInstances.clear();
		this.groupAddresses.clear();
	}

	@Override
	public void startDocument() throws SAXException {
		project = new KNXProject();
	}

	@Override
	public void endDocument() throws SAXException {
		project.toString();
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		
		if (AREA.equals(qName)) {
			currentArea = getAttributeAsInteger("Address", attributes);
		}
		else if (LINE.equals(qName)) {
			currentLine = getAttributeAsInteger("Address", attributes);
		}
		else if (BUILDING_PART.equals(qName)) {
			currentBuildingPart = parseBuildingPartElement(uri, localName, qName, attributes);
		}
		else if (DEVICE_INSTANCE.equals(qName)) {
			currentDevice = parseDeviceInstanceElement(uri, localName, qName, attributes);
		}
		else if (DEVICE_INSTANCE_REF.equals(qName)) {
			currentBuildingPart.addDeviceInstance(getAttribute("RefId", attributes));
		}
		else if (COM_OBJECT_INSTANCE_REF.equals(qName)) {
			currentComObject = new ComObjectInstanceRefElement(
					getAttribute("RefId", attributes),
					getAttributeDatapointType(attributes)
			);
		}
		else if (SEND.equals(qName)) {
			parseSendConnectorElement(uri, localName, qName, attributes);
		}
		else if (GROUP_RANGE.equals(qName)) {
			groupRange++;
		}
		else if (GROUP_ADDRESS.equals(qName)) {
			parseGroupAddressElement(uri, localName, qName, attributes);
		}
		else {
			checkTranslationElement(uri, localName, qName, attributes);
		}
	}
	
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		
		if (BUILDING_PART.equals(qName)) {
			BuildingPartElement parent = currentBuildingPart.getParent();
			if (parent == null) {
				buildingParts.add(currentBuildingPart);
			}
			else {
				currentBuildingPart = parent;
			}
		}
		else if (DEVICE_INSTANCE.equals(qName)) {
			currentDevice = null;
		}
		else if (GROUP_RANGE.equals(qName)) {
			groupRange--;
		}
	}
	
	private BuildingPartElement parseBuildingPartElement(String uri, String localName, String qName, Attributes attributes) {
		
		BuildingPartElement bpe = new BuildingPartElement(
				currentBuildingPart,
				getAttribute("Id", attributes),
				getAttribute("Name", attributes),
				getAttribute("Type", attributes)
		);
		
		if (currentBuildingPart != null) {
			currentBuildingPart.addPart(bpe);
		}
		return bpe;
	}
	
	private DeviceInstanceElement parseDeviceInstanceElement(String uri, String localName, String qName, Attributes attributes) {
		
		Integer address = getAttributeAsInteger("Address", attributes);
		IndividualAddress deviceAddress = address == null ? null : new IndividualAddress(currentArea, currentLine, address);
		
		DeviceInstanceElement device = new DeviceInstanceElement(
				getAttribute("Id", attributes),
				deviceAddress,
				getAttribute("Name", attributes),
				getAttribute("ProductRefId", attributes),
				getAttribute("Hardware2ProgramRefId", attributes)
		);
		deviceInstances.add(device);
		return device;
	}
	
	private void parseSendConnectorElement(String uri, String localName, String qName, Attributes attributes) {
		if (currentDevice != null && currentComObject != null) {
			currentComObject.setGroupAddressRefId(getAttribute("GroupAddressRefId", attributes));
			currentDevice.addComObjectInstanceRefElement(currentComObject);
		}
	}
	
	private void parseGroupAddressElement(String uri, String localName, String qName, Attributes attributes) {
		groupAddresses.add(new GroupAddressElement(
				getAttribute("Id", attributes),
				getAttribute("Name", attributes),
				convertGroupAddress(getAttributeAsInteger("Address", attributes))
			)
		);
	}

	/**
	 * 
	 * @param address
	 * @return
	 */
	private GroupAddress convertGroupAddress(Integer address) {
		
		if (address == null) {
			return null;
		}
		
		int addressValue = address.intValue();
		if (addressValue < 0 || addressValue > 0xffff) {
			throw new KNXIllegalArgumentException("Invalid address");
		}
		
		// Get the area number
		int area = addressValue >>> 11 & 0x1F;
		int line;
		if ((area & ~0x1F) != 0) {
			throw new KNXIllegalArgumentException("Invalid address");
		}
		
		if (groupRange == 2) {
			// Get the line number for level-3 group addresses
			line = addressValue >>> 8 & 0x07;
			if ((line & ~0x7) != 0) {
				throw new KNXIllegalArgumentException("Invalid address");
			}
			
			int device = addressValue & 0xFF;
			if ((device & ~0xFF) != 0) {
				throw new KNXIllegalArgumentException("Invalid address");
			}
			return new GroupAddress(area, line, device);
		}
		else if (groupRange == 1) {
			// Get the subgroup for level-2 group addresses
			line = addressValue & 0x07FF;
			if ((line & ~0x7FF) != 0) {
				throw new KNXIllegalArgumentException("Invalid address");
			}
			return new GroupAddress(area, line);
		}
		else {
			throw new KNXIllegalArgumentException("Invalid XML");
		}
	}
}
