package com.domosnap.engine.adapter.impl.openwebnet.connector;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.domosnap.engine.Log;
import com.domosnap.engine.adapter.core.ConnectionListener;
import com.domosnap.engine.adapter.core.ConnectionStatusEnum;

public abstract class AbstractOpenWebConnection {


	protected final Log log = new Log(this.getClass().getSimpleName());
	protected final Object lock = new Object();
//	protected final Object lockPool = new Object();
	protected Socket socket = null;
	protected ExecutorService pool = null;
	protected boolean forceConnection = true;
	private final int poolSize = 5;
	private Integer passwordOpen = null;
	private BufferedReader depuisClient = null;
	private PrintWriter versClient = null;
	private String ip;
	private int port = 0;
	private int timeout = 5000;
	private Password passwordEncoder = new Password();
	private List<ConnectionListener> connectionListenerList = Collections.synchronizedList(new ArrayList<ConnectionListener>());
	private Log.Session session;
	
	/**
	 * 
	 * @param ip the ip or dns name of the open server
	 * @param port the port number of the open server
	 * @param passwordOpen not supported actually
	 */
	public AbstractOpenWebConnection(String ip, int port, Integer passwordOpen, Log.Session session){ 
		this.ip = ip;
		this.port = port;
		this.passwordOpen = passwordOpen;
		this.session = session;
		
	}

	/**
	 * Return the ip of the open server.
	 * @return the ip of the open server.
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * Define the ip of the open server.
	 * @param ip the ip of the open server.
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * Return the port of the open server.
	 * @return the port of the open server.
	 */
	public int getPort() {
		return port;
	}

	/**
	 * Define the port of the open server.
	 * @param port the port of the open server.
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * Define the gateway password.
	 * @param passwordOpen
	 */
	public void setPasswordOpen(Integer passwordOpen) {
		this.passwordOpen = passwordOpen;
	}
	
	public Integer getPasswordOpen() {
		return passwordOpen;
	}
	
	/**
	 * Return the timeout of the connection to open server in millisecond.
	 * @return the timeout of the connection to open server in millisecond.
	 */
	public int getTimeout() {
		return timeout;
	}

	/**
	 * Define the timeout of the connection to open server in millisecond.
	 * @param timeout the timeout of the connection to open server in millisecond.
	 */
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	
	/**
	 * Open the connection. If connection is not possible it will try again later:
	 * <li>
	 * <ul> after some time for monitor </ul>
	 * <ul> at the next command for commander</ul>
	 * </li>
	 * @return
	 */
	public boolean connect() {
		forceConnection = true;
		// Make connection in thread to avoid blocking the user!
		getExecutorService().execute(new Runnable() {
			@Override
			public void run() {
				if (!isConnected()) { // Test again since with the lock, maybe a previous thread has opened the connection!
					connection();
				}
			}
		}); 
		return false;
	}
	
	/**
	 * Close the socket and stop the thread.<br/>
	 * No more connection will be try before method connect will be call again.
	 */
	public void disconnect(){
		forceConnection = false;
		closeConnection();
	}
	
	private void closeConnection() {
		if(socket != null){
			try {
				socket.close();
				socket = null;
			
				for (ConnectionListener connectionListener : connectionListenerList) {
					try {
						connectionListener.onClose();
					} catch (Exception e) {
						log.severe(session, getFormattedLog(1, "ConnectionListener raise an error [" + e.getMessage() +"]"));
					}
				}
				
				log.fine(session, getFormattedLog(0, "End connection: " + session.name() + " session closed..."));
			} catch (IOException e) {
				log.severe(session, getFormattedLog(0, "End connection:  Error during closing socket [" + e.getMessage() + "]"));
			}
			
			if(pool!=null){
				pool.shutdown();
				try {
					if (!pool.awaitTermination(500, TimeUnit.MILLISECONDS)) {
						List<Runnable> droppedTasks = pool.shutdownNow();
						log.warning(session, "Executor was shut down. "
								+ droppedTasks.size() + " tasks will not be executed.");

					}
				} catch (InterruptedException e) {
					log.severe(session,
							"Executor error during abruptly shut down. " + e.getMessage());
				}
				pool = null;
			}
		}
		
	}

	public boolean isConnected() {
		if(socket != null && socket.isConnected()){
			return true;		
		} else {
			return false;
		}
	}

	public void addConnectionListener(
			ConnectionListener connectionListener) {
		if (!connectionListenerList.contains(connectionListener)) {
			connectionListenerList.add(connectionListener);
		}
	}
	
	public void removeConnectionListener(ConnectionListener connectionListener) {
		connectionListenerList.remove(connectionListener);
	}
	
	protected synchronized ExecutorService getExecutorService() {
		if (pool == null) {
			pool = Executors.newFixedThreadPool(poolSize);
		}

		return pool;
	}

	
	protected void writeMessage(String msg) {
		if (versClient != null) {
			versClient.print(msg);
			versClient.flush();
			log.fine(session, getFormattedLog(1, "Send to ".concat(session.name().toUpperCase()).concat( " server: ").concat(msg)));
		}
	}
	
	protected String readMessage(){
		int indice = 0;
		boolean exit = false;
		char respond[] = new char[1024];
		char c = ' ';
		int ci = 0;
		String responseString = null;

		try{
			do { 
				if(socket != null && !socket.isInputShutdown()) {
					ci = depuisClient.read();		    		
					if (ci == -1) {
						log.finest(session, getFormattedLog(1, "End of read from monitor socket."));
						closeConnection();
						break;
					} else { 
						c = (char) ci;			        				        
						if (c == '#' && indice > 1 && '#' == respond[indice-1]) {
							respond[indice] = c;
							exit = true;
							break;
						} else {
							respond[indice] = c;
							indice = indice + 1;
						}
					}
				} else {
					closeConnection();
					break;
				}
			} while(true);
		}catch(IOException e){
			log.severe(session, getFormattedLog(0, "Socket not available"));
		}

		if (exit == true){
			responseString = new String(respond,0,indice+1);
		}

		log.fine(session, getFormattedLog(1, "Read from " + session.name().toUpperCase() + " server: " + responseString));

		return responseString;
	}
	
	protected synchronized void connection() {
		
		try {
			
			String ip = getIp();
			int port = getPort();
			
			if (ip == null || port == 0) {
				log.fine(session, getFormattedLog(1, "Not enough information to initiate connection to socket ["+ ip +":"+ port+"]"));
				return;
			}

			log.fine(session, getFormattedLog(1, "Begin connection to socket ["+ ip +":"+ port+"]"));
			
			socket = new Socket();
			InetSocketAddress address = new InetSocketAddress(ip, port);
			socket.connect(address, getTimeout());
			depuisClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			versClient = new PrintWriter(socket.getOutputStream(),true);
		} catch (Exception e){
			try {
		
				log.severe(session, getFormattedLog(1, "Impossible to connect to the server ["+ip+":"+port+"]:" + e.getMessage()));
				if (socket!=null && socket.isConnected()) {
					closeConnection();
				}
				throw(e);
			}
			catch (SocketTimeoutException b) {
				callOpenWebConnectionListenerConnect(ConnectionStatusEnum.Timeout);
				return;
			} catch (IllegalArgumentException b){
				callOpenWebConnectionListenerConnect(ConnectionStatusEnum.InvalidAddress);
				return;
			} catch (Exception b){
				callOpenWebConnectionListenerConnect(ConnectionStatusEnum.UnkownError);
				return;
			} 
		}

		if(socket != null) {
				
			log.finest(session, getFormattedLog(1, " ----- Step Connection ----- "));
			String msg = readMessage();
			if (!OpenWebNetConstant.ACK.equals(msg)) {
				// Bad return message
				log.severe(session, getFormattedLog(1, "Bad message [" + msg + "] received from [" + ip + "]"));
				callOpenWebConnectionListenerConnect(ConnectionStatusEnum.WrongAcknowledgement);
				closeConnection();
				return;
			}

			log.finest(session, getFormattedLog(1, " ----- Step Identification -----"));
			if (Log.Session.Monitor.equals(session)) {
				writeMessage(OpenWebNetConstant.MONITOR_SESSION);
			} else {
				writeMessage(OpenWebNetConstant.COMMAND_SESSION);
			}

			if(passwordOpen != null){
				log.finest(session, getFormattedLog(1, " ----- Step authentification -----"));
				msg = readMessage().substring(2); // Remove *# 
				msg = msg.substring(0, msg.length()-2); // Remove last ##
				writeMessage("*#".concat(passwordEncoder.calcPass(getPasswordOpen(), msg).concat("##"))); // Send encoded password		    	
			}
			
			log.finest(session, getFormattedLog(1, " ----- Step Final -----"));
			msg = readMessage();

			if (!OpenWebNetConstant.ACK.equals(msg)) {		       	
				log.severe(session, getFormattedLog(1, "Problem during connection to [" + getIp() + "] with message [" + msg + "]"));
				callOpenWebConnectionListenerConnect(ConnectionStatusEnum.WrongAcknowledgement);
				closeConnection();
				return;
			}

			log.fine(session, getFormattedLog(1, "Connection OK"));
			callOpenWebConnectionListenerConnect(ConnectionStatusEnum.Connected);
			return;
		} else {
			log.severe(session, getFormattedLog(0, "End connection: No socket... Impossible to connect"));
			callOpenWebConnectionListenerConnect(ConnectionStatusEnum.NoSocket);
			return;
		}
	}
	
	private void callOpenWebConnectionListenerConnect(ConnectionStatusEnum connection) {
		for (ConnectionListener connectionListener : connectionListenerList) {
			try {
				connectionListener.onConnect(connection);
			} catch (Exception e) {
				log.severe(session, getFormattedLog(1, "ConnectionListener raise an error [" + e.getMessage() +"]"));
			}
		}
	}
	
	private static int countSocket = 0;
	private static int countSession = 0;
	private static Map<Integer, String> mapSessionInstance = new ConcurrentHashMap<Integer, String>();
	private static Map<Integer, String> mapSocketInstance = new ConcurrentHashMap<Integer, String>();
	
	public String getFormattedLog(int level, String msg) {
		
		
		String socketNum = "Socket null";
		if(socket != null) {
			if (mapSocketInstance.containsKey(socket.hashCode())) {
				socketNum = mapSocketInstance.get(socket.hashCode());
			} else {
				socketNum = "Socket " + pad(countSocket++, 4);
				mapSocketInstance.put(socket.hashCode(),socketNum);
			}
		}

		String sessionNum;
		if (mapSessionInstance.containsKey(this.hashCode())) {
			sessionNum = mapSessionInstance.get(this.hashCode());
		} else {
			sessionNum = session.name() + pad(countSession++, 5);
			mapSessionInstance.put(this.hashCode(),sessionNum);
		}
		
		StringBuilder sb = new StringBuilder();
		sb.append("[").append(sessionNum).append("]-");
		sb.append("[").append(socketNum).append("] : ");
		
		for (int i= 0; i<level;i++) {
			sb.append("   ");
		}
		
		sb.append(msg);
		return sb.toString();
		
	}
	
	private String pad(int i, int pad) {
		String s = "" + i;
		while (s.length() < pad) {
			s = " ".concat(s);
		}
		return s;
	}
}
