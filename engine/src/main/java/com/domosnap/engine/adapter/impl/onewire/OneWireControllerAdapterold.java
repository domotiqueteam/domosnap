package com.domosnap.engine.adapter.impl.onewire;

import java.util.ArrayList;
import java.util.List;

import com.dalsemi.onewire.application.monitor.DeviceMonitorEvent;
import com.dalsemi.onewire.application.monitor.DeviceMonitorEventListener;
import com.dalsemi.onewire.application.monitor.DeviceMonitorException;
import com.dalsemi.onewire.container.OneWireContainer;
import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.ControllerAdapter;
import com.domosnap.engine.adapter.core.ConnectionListener;
import com.domosnap.engine.adapter.core.ScanListener;
import com.domosnap.engine.adapter.core.UnknownControllerListener;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.where.Where;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

public class OneWireControllerAdapterold  implements ControllerAdapter, DeviceMonitorEventListener {
	
	private final static Log log = new Log(OneWireControllerAdapterold.class.getSimpleName());
	
	private boolean connected = false;

	@Override
	public <T extends Controller> T createController(Class<T> clazz, Where where) {
		return null;
	}

	@Override
	public <T extends Controller> List<T> createControllers(Class<T> clazz, Where... wheres) {
		List<T> list = new ArrayList<T>();
		for (Where where : wheres) {
			list.add(createController(clazz, where));
		}
		return list;
	}

	@Override
	public boolean isConnected() {
		return connected;
	}

	@Override
	public void addMonitorConnectionListener(ConnectionListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeMonitorConnectionListener(ConnectionListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addCommanderConnectionListener(ConnectionListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeCommanderConnectionListener(ConnectionListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addUnknowControllerListener(UnknownControllerListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeUnknowControllerListener(UnknownControllerListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void connect() {
		
		OneWireDiscoverer discoverer = OneWireDiscoverer.getInstance();
		for (OneWireAdapter adapter : discoverer.getAdapters()) {
			connected = connected || adapter.connect();
		}
	}
	
	@Override
	public void connect(String uri) {
		connect();
	}

	@Override
	public void disconnect() {
		
		OneWireDiscoverer discoverer = OneWireDiscoverer.getInstance();
		for (OneWireAdapter adapter : discoverer.getAdapters()) {
			adapter.disconnect();
		}
		connected = false;
	}

	@Override
	public void scan(ScanListener listener) {
		
		OneWireDiscoverer discoverer = OneWireDiscoverer.getInstance();
		for (OneWireAdapter adapter : discoverer.getAdapters()) {
			adapter.addDeviceMonitorListener(this);
			adapter.startDeviceMonitor();
		}
	}
	
	@Override
	public void deviceArrival(DeviceMonitorEvent event) {
		
		for (int i=0; i<event.getDeviceCount(); i++) {
			OneWireContainer container = event.getContainerAt(i);
			log.fine(Session.Server, "1-wire device detected : "+ container.getName());
//			factory.createDeviceControllers(container, container.getAddressAsString(), this);
		}
	}
	
	@Override
	public void deviceDeparture(DeviceMonitorEvent event) {
		
		for (int i=0; i<event.getDeviceCount(); i++) {
			OneWireContainer container = event.getContainerAt(i);
			log.fine(Session.Server, "1-wire device lost : "+ container.getName());
			// TODO Find a way to notify that the controller address is no longer reachable
		}
	}

	@Override
	public void networkException(DeviceMonitorException e) {
		// TODO Auto-generated method stub
		log.severe(Session.Server, "1-wire connection exception : "+ e.getMessage());
	}

	@Override
	public List<Class<? extends Controller>> getSupportedDevice() {
		// TODO Auto-generated method stub
		return null;
	}
}
