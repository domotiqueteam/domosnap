package com.domosnap.engine.adapter.impl.openwebnet.conversion.shutter;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.domosnap.engine.adapter.core.UnSupportedStateException;
import com.domosnap.engine.adapter.core.UnknownStateException;
import com.domosnap.engine.adapter.core.UnknownStateValueException;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.Converter;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.dimension.DimensionStatus;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.dimension.DimensionValue;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.shutter.Shutter;
import com.domosnap.engine.controller.shutter.ShutterStateName;
import com.domosnap.engine.controller.what.State;
import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.controller.what.impl.UpDownState;
import com.domosnap.engine.controller.what.impl.UpDownState.UpDownValue;

public class ShutterStatusConverter implements Converter {
	
	public enum AutomationStatus {
		AUTOMATION_UP("1", UpDownValue.UP),
		AUTOMATION_DOWN("2", UpDownValue.DOWN),
		AUTOMATION_STOP("0", UpDownValue.STOP);
		
		private String code;
		private UpDownValue value;
		private AutomationStatus(String code, UpDownValue value) {
			this.code = code;
			this.value = value;
		}
	
		public String getCode() {
			return code;
		}
		
		public UpDownValue getValue() {
			return value;
		}
	
		public static AutomationStatus fromValue(String code) {
			for (AutomationStatus automation: AutomationStatus.values()) {
				if (automation.getCode().equals(code))
					return automation;
			}
			return null;
		}
	
		public static String fromValue(What name) {
			if (name == null) {
				return null;
			}
			
			for (AutomationStatus automation: AutomationStatus.values()) {
				if (automation.value.equals(name.getValue()))
					return automation.code;
			}
			return null;
		}
	}

	@Override
	public List<String> convert(What what, Map<String, State<?>> controllerStateList)
			throws UnknownStateException, UnSupportedStateException, UnknownStateValueException {
		
		if (what == null) {
			return null;
		}
		
		List<String> result = new ArrayList<String>();
		ShutterStateName s = ShutterStateName.valueOf(what.getName());
		
		UpDownState status = (UpDownState) controllerStateList.get(ShutterStateName.status.name());
		switch (s) {
		case status:
			status = (UpDownState) what.getValue();
			if (UpDownState.Down == status) {
				result.add(AutomationStatus.AUTOMATION_DOWN.getCode());
				return result;
			} else if (UpDownState.Up == status) {
				result.add(AutomationStatus.AUTOMATION_UP.getCode());
				return result;
			} else {
				result.add(AutomationStatus.AUTOMATION_STOP.getCode());
				return result;
			}
		}
				
		return result;
	}

	@Override
	public List<DimensionStatus<?>> convertDimension(What what, Map<String, State<?>> controllerStateList)
			throws UnknownStateException, UnSupportedStateException, UnknownStateValueException {
		throw new UnSupportedStateException();
	}

	@Override
	public List<What> convert(String code, Map<String, State<?>> controllerStateList)
			throws UnknownStateException, UnSupportedStateException, UnknownStateValueException {
		AutomationStatus c = AutomationStatus.fromValue(code);
		List<What> result = new ArrayList<What>();
		switch (c) {
		case AUTOMATION_DOWN:
			result.add(new What(ShutterStateName.status.name(), UpDownState.Down));
			break;
		case AUTOMATION_STOP:
			result.add(new What(ShutterStateName.status.name(), UpDownState.Stop));
			break;
		case AUTOMATION_UP:
			result.add(new What(ShutterStateName.status.name(), UpDownState.Up));
			break;
		default:
			break;
		}
		
		return result;
	}

	@Override
	public List<What> convertDimension(String dimension, List<DimensionValue> dimensionValueList,
			Map<String, State<?>> controllerStateList) throws UnknownStateException, UnSupportedStateException, UnknownStateValueException {
		throw new UnSupportedStateException();
	}

	@Override
	public boolean isDimension(What what) {
		return false;
	}

	@Override
	public String getOpenWebWho() {
		return "2";
	}

	@Override
	public Class<? extends Controller> getHomeSnapWho() {
		return Shutter.class;
	}
}
