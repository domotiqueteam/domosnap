package com.domosnap.engine.adapter.impl.knx;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.network.NetworkConfig;

import tuwien.auto.calimero.exception.KNXException;
import tuwien.auto.calimero.knxnetip.Discoverer;
import tuwien.auto.calimero.knxnetip.servicetype.SearchResponse;
import tuwien.auto.calimero.knxnetip.util.DeviceDIB;
import tuwien.auto.calimero.knxnetip.util.HPAI;
import tuwien.auto.calimero.link.medium.KNXMediumSettings;

/**
 * 
 * @author DRIESBACH Olivier
 */
public class KNXDiscoverer {
	
	/** */
	private static Log log = new Log(KNXDiscoverer.class.getSimpleName());
	
	/** */
	private static KNXDiscoverer instance = new KNXDiscoverer();
	
	/** */
	private Map<InetAddress, KNXConnectionSettings> connectionSettings;
	
	/** */
	private long lastSearch;
	
	/**
	 * 
	 */
	private KNXDiscoverer() {
		lastSearch = -1;
		connectionSettings = new HashMap<InetAddress, KNXConnectionSettings>(1);
	}
	
	/**
	 * 
	 * @return
	 */
	public static KNXDiscoverer getInstance() {
		return instance;
	}
	
	/**
	 * Returns all bindable inet adresses found by the discoverer since the last device search.
	 * @return an immutable collection of inet adresses
	 */
	public Collection<InetAddress> getBindableAddresses() {
		return Collections.unmodifiableCollection(connectionSettings.keySet());
	}
	
	/**
	 * Returns the connection settings of a KNXnet/IP device.
	 * @param ipAddress The IPv4 address of the KNXnet/IP device or <code>null</code> to get the first one discovered on IP network
	 * @return the connection settings of a KNXnet/IP device or <code>null</code> if no device was found
	 */
	public KNXConnectionSettings getConnectionSettings(String ipAddress) {
		
		if (connectionSettings.isEmpty() && lastSearch == -1) {
			searchKNXConnectionSettings(); // Search for the first time on the network
		}
		if (connectionSettings.isEmpty()) { 
			log.severe(Session.Server, "No KNXnet/IP device found.");
			return null;
		}
		
		KNXConnectionSettings result = null;
		if (ipAddress != null) { // Try to get the settings of the specified device
			try {
				result = connectionSettings.get(InetAddress.getByName(ipAddress));
			} catch (UnknownHostException e) {
			}
			if (result == null) {
				log.info(Session.Server, "No KNXnet/IP device found at "+ ipAddress +", the settings of the first one found will be use instead.");
			}
		}
		
		if (result == null) {
			result = connectionSettings.values().iterator().next();
		}
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public void searchKNXConnectionSettings() {
		searchKNXConnectionSettings(false);
	}
	
	/**
	 * 
	 * @param forceSearch
	 */
	public void searchKNXConnectionSettings(boolean forceSearch) {
		
		synchronized (connectionSettings) {
			
			if (lastSearch == -1 || forceSearch) {
				
				lastSearch = System.currentTimeMillis();
				connectionSettings.clear(); // Clear previous results
				
				// Get all IPv4 only bindable inet addresses of this computer to improve Discoverer search
				List<InetAddress> localAddresses = new ArrayList<InetAddress>();
				for (InetAddress inetAddress : NetworkConfig.getInstance().getInetAdresses()) {
					if (!inetAddress.isLoopbackAddress() && inetAddress.getAddress().length == 4) {
						localAddresses.add(inetAddress);
					}
				}

				// Start discovery on each bindable INetAddress
				List<KNXDiscoveryWorker> workers = new ArrayList<KNXDiscoveryWorker>();
				for (InetAddress localAddress : localAddresses) {
					
					KNXDiscoveryWorker worker = new KNXDiscoveryWorker(localAddress);
					workers.add(worker);
					worker.start();
				}
				
				// Wait the end of search of each worker in order to get all KNX NetIP devices found by IP address
				do { 
					Iterator<KNXDiscoveryWorker> iter = workers.iterator();
					while (iter.hasNext()) {
						
						KNXDiscoveryWorker worker = iter.next();
						if (worker.isAlive()) {
							continue;
						} else {
							for (KNXConnectionSettings settings : worker.getConnectionSettings()) {
								connectionSettings.put(settings.getDeviceSettings().getInetAddress(), settings);
							}
							iter.remove();
						}
					}
					if (!workers.isEmpty()) {
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
						}
					}
				} while (!workers.isEmpty());
				
				if (connectionSettings.isEmpty()) {
					log.info(Session.Server, "No KNXnet/IP device found on the network.");
				}
			}
		}
	}
	
	/**
	 * 
	 * @author DRIESBACH Olivier
	 * @version 1.0
	 * @since 1.0
	 */
	private class KNXDiscoveryWorker extends Thread {
		
		private InetAddress localAddress;
		private List<KNXConnectionSettings> connectionSettings;
		
		KNXDiscoveryWorker(InetAddress localAddress) {
			super("KNX discoverer on "+ localAddress.toString());
			this.localAddress = localAddress;
		}
		
		public List<KNXConnectionSettings> getConnectionSettings() {
			return connectionSettings;
		}
		
		@Override
		public void run() {
			
			Discoverer discoverer = null;
			NetworkInterface networkInterface = null;
			try {
				discoverer = new Discoverer(localAddress, 0, false, false);
				networkInterface = NetworkConfig.getInstance().getInterface(localAddress);
				
				log.info(Session.Server, new StringBuilder("Start searching KNXnet/IP devices on ")
						.append(localAddress.getHostAddress()).append(" (")
						.append(networkInterface.getDisplayName()).append(")")
						.toString());
				
				discoverer.startSearch(networkInterface, 3, true);
			}
			catch (KNXException e) {
				// TODO Log
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			connectionSettings = new ArrayList<KNXConnectionSettings>();
			
			log.info(Session.Server, new StringBuilder()
					.append("Found ").append(discoverer.getSearchResponses().length).append(" devices")
					.toString());
			
			for (SearchResponse response : discoverer.getSearchResponses()) {
				
				HPAI endPoint = response.getControlEndpoint();
				DeviceDIB netIPDevice = response.getDevice();
				KNXDeviceSettings deviceSettings = new KNXDeviceSettings(endPoint.getAddress(), endPoint.getPort(), netIPDevice.getAddress(), netIPDevice.getMulticastAddress());
				KNXMediumSettings mediumSettings = KNXMediumSettings.create(netIPDevice.getKNXMedium(), null);
				
				log.info(Session.Server, new StringBuilder()
						.append("Found device ").append(netIPDevice.getName())
						.append(" (IP Address: ").append(endPoint.getAddress())
						.append(", KNX Address: ").append(netIPDevice.getAddress())
						.append(", MAC Address: ").append(netIPDevice.getMACAddressString())
						.append(", Router: ").append(deviceSettings.isRouter())
						.append(")")
						.toString());
				
				connectionSettings.add(new KNXConnectionSettings(localAddress, deviceSettings, mediumSettings));
			}
			
			if (networkInterface != null) {
				log.info(Session.Server, new StringBuilder("End of search on ")
						.append(networkInterface.getDisplayName()).toString());
			}
		}
	}
}
