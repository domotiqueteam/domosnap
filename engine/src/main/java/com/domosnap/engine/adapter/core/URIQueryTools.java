package com.domosnap.engine.adapter.core;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class URIQueryTools {

//	public Map<String, List<String>> splitQuery(URI url) {
//	    if (String.isNullOrEmpty(url.getQuery())) {
//	        return Collections.emptyMap();
//	    }
//	    return Arrays.stream(url.getQuery().split("&"))
//	            .map(this::splitQueryParameter)
//	            .collect(Collectors.groupingBy(SimpleImmutableEntry::getKey, LinkedHashMap::new, mapping(Map.Entry::getValue, toList())));
//	}
//
//	public SimpleImmutableEntry<String, String> splitQueryParameter(String it) {
//	    final int idx = it.indexOf("=");
//	    final String key = idx > 0 ? it.substring(0, idx) : it;
//	    final String value = idx > 0 && it.length() > idx + 1 ? it.substring(idx + 1) : null;
//	    return new SimpleImmutableEntry<>(key, value);
//	}

	public static Map<String, List<String>> splitQuery(URI url) throws UnsupportedEncodingException {
		final Map<String, List<String>> query_pairs = new LinkedHashMap<String, List<String>>();
		final String[] pairs = url.getQuery().split("&");
		for (String pair : pairs) {
			final int idx = pair.indexOf("=");
			final String key = idx > 0 ? URLDecoder.decode(pair.substring(0, idx), "UTF-8") : pair;
			if (!query_pairs.containsKey(key)) {
				query_pairs.put(key, new LinkedList<String>());
			}
			final String value = idx > 0 && pair.length() > idx + 1
					? URLDecoder.decode(pair.substring(idx + 1), "UTF-8")
					: null;
			query_pairs.get(key).add(value);
		}
		return query_pairs;
	}
}
