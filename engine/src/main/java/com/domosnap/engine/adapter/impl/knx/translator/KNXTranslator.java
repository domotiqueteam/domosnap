package com.domosnap.engine.adapter.impl.knx.translator;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;

import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.light.Light;
import com.domosnap.engine.controller.shutter.Shutter;
import com.domosnap.engine.controller.what.What;

import tuwien.auto.calimero.dptxlator.DPT;

/**
 * 
 * @author Olivier Driesbach
 * @version 1.0
 * @since 1.0
 */
public class KNXTranslator {
	
	private static final Map<Class<? extends Controller>, Translator> types = new HashMap<Class<? extends Controller>, Translator>();
	static {
		types.put(Light.class, new KNXLightTranslator());
		types.put(Shutter.class, new KNXShutterTranslator());
	}
	
	/**
	 * 
	 * @param who
	 * @param value
	 * @return
	 */
	public static DPT getDataPointType(Class<? extends Controller> who, What what) {
		return types.get(who).getDataPointType(what.getName());
	}
	
	public static String translateToKNX(Class<? extends Controller> who, What what) {
		return types.get(who).translateToKNX(what);
	}
}
