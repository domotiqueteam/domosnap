package com.domosnap.engine.adapter.impl.hue.conversion;

import java.util.ArrayList;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.List;
import java.util.Map.Entry;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.Command.Type;
import com.domosnap.engine.adapter.core.UnSupportedStateException;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.light.Light;
import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.controller.what.impl.BooleanState;
import com.domosnap.engine.controller.what.impl.OnOffState;
import com.domosnap.engine.controller.where.URITools;
import com.domosnap.engine.controller.where.Where;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import io.reactivex.functions.Function;
import okhttp3.Response;

public class HueResponseToCommandFunction implements Function<Response, Publisher<Command>> {

	private Log log = new Log(HueResponseToCommandFunction.class.getSimpleName());
	private String ip;
	private int port;
	private String user;
	
	public HueResponseToCommandFunction(String ip, int port, String user) {
		super();
		this.ip = ip;
		this.port = port;
		this.user = user;
	}

	@Override
	public Publisher<Command> apply(final Response message) throws Exception {

		return new Publisher<Command>() {
			@Override
			public void subscribe(Subscriber<? super Command> subscriber) {
				try {
					if (message.body() == null) {
						return;
					}
					if (200 != message.code()) {
						return;
					}

					Class<? extends Controller> who;
					What whatOnOff = null;
					What whatReachable = null;
					Where where = null;
					Type type = "GET".equals(message.request().method()) ? Type.QUERY : Type.COMMAND;
					Controller controller = null;

					List<String> pathList = message.request().url().pathSegments();

					// Who
					if ("lights".equals(pathList.get(2))) {
						who = Light.class;
					} else {
						// TODO unsupported
						who = null;
					}

					// Where

					// What
					if (Type.COMMAND.equals(type)) { // Action
						where = new Where(URITools.generateURI("hue", user, ip, port, pathList.get(3)));
						String property = pathList.get(4);
						if (!"state".equals(property)) {
							// UnkownState
							return;
						}
						String bodyJson = message.body().string();

						JsonParser parser = new JsonParser();
						JsonArray ajs = parser.parse(bodyJson).getAsJsonArray();
						JsonObject ojs = ajs.get(0).getAsJsonObject();
						JsonObject state = ojs.getAsJsonObject("success");
						// for (String string : ojs.keySet()) {
						whatOnOff = createWhat("on", String.valueOf(state.get("on")));
					
						subscriber.onNext(new Command(who, whatOnOff, where, type, controller));
					
				

					} else { // status
						if (pathList.size() == 3) { // monitor is reading status from ALL device
							String bodyJson = message.body().string();
							log.finest(Session.Monitor, bodyJson);
							JsonParser parser = new JsonParser();
							JsonObject lights = parser.parse(bodyJson).getAsJsonObject(); // Each property name is a
																							// key, and property value
																							// the light entity

							for (Entry<String, JsonElement> key : lights.entrySet()) {
								where = new Where(URITools.generateURI("hue", user, ip, port, key.getKey()));
								JsonObject light = lights.getAsJsonObject(key.getKey());
								JsonObject state = light.getAsJsonObject("state");
								whatOnOff = createWhat("on", state.get("on").getAsString());
								whatReachable = createWhat("reachable", String.valueOf(state.get("reachable")));
								ArrayList<What> whatList = new ArrayList<What>();
								whatList.add(whatOnOff);
								whatList.add(whatReachable);
								subscriber.onNext(new Command(who, whatList, where, type, controller));
							}
							// {"1":
							// {"modelid":"LCT001","name":"Hue Lamp
							// 1","swversion":"65003148","state":{"xy":[0,0],"ct":0,"alert":"none","sat":254,"effect":"none","bri":254,"hue":4444,"colormode":"hs","reachable":true,"on":true},"type":"Extended
							// color
							// light","pointsymbol":{"1":"none","2":"none","3":"none","4":"none","5":"none","6":"none","7":"none","8":"none"},"uniqueid":"00:17:88:01:00:d4:12:08-0a"},
							// "2":
							// {"modelid":"LCT001","name":"Hue Lamp
							// 2","swversion":"65003148","state":{"xy":[0.346,0.3568],"ct":201,"alert":"none","sat":144,"effect":"none","bri":254,"hue":23536,"colormode":"hs","reachable":true,"on":true},"type":"Extended
							// color
							// light","pointsymbol":{"1":"none","2":"none","3":"none","4":"none","5":"none","6":"none","7":"none","8":"none"},"uniqueid":"00:17:88:01:00:d4:12:08-0b"},
							// "3":
							// {"modelid":"LCT001","name":"Hue Lamp
							// 3","swversion":"65003148","state":{"xy":[0.346,0.3568],"ct":201,"alert":"none","sat":254,"effect":"none","bri":254,"hue":65136,"colormode":"hs","reachable":true,"on":true},"type":"Extended
							// color
							// light","pointsymbol":{"1":"none","2":"none","3":"none","4":"none","5":"none","6":"none","7":"none","8":"none"},"uniqueid":"00:17:88:01:00:d4:12:08-0c"}
							//
							// }

						} else { // Command is read the status of ONE device
							where = new Where(URITools.generateURI("hue", user, ip, port, pathList.get(3)));
							String bodyJson = message.body().string();
							JsonParser parser = new JsonParser();
							JsonObject ligth = parser.parse(bodyJson).getAsJsonObject();

							JsonObject state = ligth.getAsJsonObject("state");
							whatOnOff = createWhat("on", state.get("on").getAsString());
							whatReachable = createWhat("reachable", String.valueOf(state.get("reachable")));			
							ArrayList<What> whatList = new ArrayList<What>();
							whatList.add(whatOnOff);
							whatList.add(whatReachable);
							subscriber.onNext(new Command(who, whatList, where, type, controller));
						}
					}
					// 4. End
					message.body().close();
					message.close();
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		};
	}

	private What createWhat(String name, String value) throws UnSupportedStateException {
		
		if(name.equals("on")) {
			
			if ("true".equals(value)) {
				return new What("status", OnOffState.On());
			} else if ("false".equals(value)) {
				return new What("status", OnOffState.Off());
			} else {
				throw new UnSupportedStateException(); 
			}
		}
		if(name.equals("reachable")) {
			if ("true".equals(value)) {
				return new What(name, BooleanState.TRUE);
			} else if ("false".equals(value)) {
				return new What(name, BooleanState.FALSE);
			} else {
				throw new UnSupportedStateException(); 
			}
			
		}
		
		throw new UnSupportedStateException();
		
		
	}
	
	public void setIp(String ip) {
		this.ip = ip;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public void setUser(String user) {
		this.user = user;
	}
}
