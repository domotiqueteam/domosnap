package com.domosnap.engine.adapter.impl.knx.application.xml;

import java.util.HashMap;
import java.util.Map;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

public class TranslationElement extends XMLRefElement {

	private Map<String, String> translations;
	
	/**
	 * 
	 * @param refId
	 */
	public TranslationElement(String refId) {
		super(null, refId);
		translations = new HashMap<String, String>();
	}
	
	/**
	 * 
	 * @param key
	 * @param value
	 */
	public void addTranslation(String key, String value) {
		translations.put(key, value);
	}
	
	/**
	 * 
	 * @return
	 */
	public String getTranslation(String attributeName, String defaultValue) {
		
		String value = translations.get(attributeName);
		return value == null ? defaultValue : value;
	}
}
