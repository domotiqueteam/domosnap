package com.domosnap.engine.adapter.impl.knx;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.net.InetAddress;

import tuwien.auto.calimero.link.medium.KNXMediumSettings;

/**
 * 
 * @author DRIESBACH Olivier
 * @version 1.0
 * @since 1.0
 */
public class KNXConnectionSettings {
	
	/** The local address used to communicate with the remote device */
	private InetAddress localAddress;
	
	/** The KNXnet/IP settings of the remote device */
	private KNXDeviceSettings deviceSettings;
	
	/** The KNX settings of local device */
	private KNXMediumSettings mediumSettings;
	
	/**
	 * 
	 * @param locaAddress
	 * @param deviceSettings
	 * @param mediumSettings
	 */
	KNXConnectionSettings(InetAddress locaAddress, KNXDeviceSettings deviceSettings, KNXMediumSettings mediumSettings) {
		this.localAddress = locaAddress;
		this.deviceSettings = deviceSettings;
		this.mediumSettings = mediumSettings;
	}
	
	/**
	 * 
	 * @return
	 */
	public InetAddress getLocalAddress() {
		return localAddress;
	}

	/**
	 * 
	 * @return
	 */
	public KNXDeviceSettings getDeviceSettings() {
		return deviceSettings;
	}

	/**
	 * 
	 * @return
	 */
	public KNXMediumSettings getMediumSettings() {
		return mediumSettings;
	}
}
