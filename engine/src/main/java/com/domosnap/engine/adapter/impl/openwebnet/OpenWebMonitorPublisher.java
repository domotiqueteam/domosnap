package com.domosnap.engine.adapter.impl.openwebnet;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.impl.openwebnet.connector.AbstractOpenWebConnection;
import com.domosnap.engine.adapter.impl.openwebnet.connector.OpenWebNetConstant;

import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;

public class OpenWebMonitorPublisher extends AbstractOpenWebConnection implements FlowableOnSubscribe<String>{

	private List<FlowableEmitter<String>> suscriberList = Collections.synchronizedList(new ArrayList<FlowableEmitter<String>>());

	/**
	 * 
	 * @param ip the ip or dns name of the open server
	 * @param port the port number of the open server
	 * @param passwordOpen not supported actually
	 * @param connect connect automatically
	 */
	public OpenWebMonitorPublisher(String ip, int port, Integer passwordOpen) { 
		super(ip, port, passwordOpen, Log.Session.Monitor);
	}

	@Override
	public boolean connect() {
		forceConnection = true;
		getExecutorService().execute(new MonitorHandler());
		return false;
	}

	private class MonitorHandler implements Runnable {

		@Override
		public void run() {
			long timer = 200;
			do {
				synchronized(lock) { // mutex on the main thread: only one connection or send message at the same time!
					if (isConnected()) {
						timer = 1000;
						// If connected read data
						String msg = readMessage();
						if (msg != null && !OpenWebNetConstant.ACK.equals(msg)) {
							for (FlowableEmitter<String> suscriber : suscriberList) {
								suscriber.onNext(msg);
							}
						}
					} else {
						// If not connected try to connect
						connection();
						try {
							Thread.sleep(timer);
							timer = timer > 10000 ? 10000 : timer * 2; // max we wait 10 seconds to reconnect
						} catch (InterruptedException e) {
							log.finest(Session.Monitor, getFormattedLog(1, "Timeout interrupted."));
						}
					}
				}
			} while (forceConnection);
		}
	}

	@Override
	public void subscribe(FlowableEmitter<String> e) throws Exception {
		suscriberList.add(e);
	}
}
