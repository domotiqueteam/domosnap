package com.domosnap.engine.adapter.impl.knx.application.xml.handler;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.domosnap.engine.adapter.impl.knx.application.xml.HardwareElement;
import com.domosnap.engine.adapter.impl.knx.application.xml.HardwareProgramElement;
import com.domosnap.engine.adapter.impl.knx.application.xml.ProductElement;

/**
 * 
 * @author DRIESBACH Olivier
 */
public class HardwareHandler extends XMLHandler {
	
	private static final String HARDWARE = "Hardware";
	private static final String PRODUCT = "Product";
	private static final String PROGRAM = "Hardware2Program";
	private static final String PROGRAM_REF = "ApplicationProgramRef";
	
	private Collection<HardwareElement> hardwares;
	private Collection<ProductElement> products;
	private Collection<HardwareProgramElement> programs;
	private HardwareProgramElement currentProgram;
	
	public HardwareHandler() {
		this.hardwares = new ArrayList<HardwareElement>();
		this.products = new ArrayList<ProductElement>();
		this.programs = new ArrayList<HardwareProgramElement>();
	}
	
	public Collection<HardwareElement> getHardwares() {
		return Collections.unmodifiableCollection(hardwares);
	}
	
	public Collection<ProductElement> getProducts() {
		return Collections.unmodifiableCollection(products);
	}
	
	public Collection<HardwareProgramElement> getPrograms() {
		return Collections.unmodifiableCollection(programs);
	}
	
	@Override
	public void dispose() {
		this.hardwares.clear();
		this.products.clear();
		this.programs.clear();
	}
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		
		if (HARDWARE.equals(qName) && attributes.getLength() != 0) {
			hardwares.add(new HardwareElement(
					getAttribute("Id", attributes),
					getAttribute("Name", attributes),
					getAttributeAsBoolean("IsCoupler", attributes),
					getAttributeAsBoolean("IsPowerSupply", attributes),
					getAttributeAsBoolean("IsPowerLineRepeater", attributes),
					getAttributeAsBoolean("IsPowerLineSignalFilter", attributes)
			));
		}
		else if (PRODUCT.equals(qName)) {
			products.add(new ProductElement(
					getAttribute("Id", attributes),
					getAttribute("Text", attributes),
					getAttributeAsBoolean("IsRailMounted", attributes)
			));
		}
		else if (PROGRAM.equals(qName)) {
			currentProgram = new HardwareProgramElement(
					getAttribute("Id", attributes)
			);
			programs.add(currentProgram);
		}
		else if (PROGRAM_REF.equals(qName)) {
			currentProgram.setApplicationProgramRefId(getAttribute("RefId", attributes));
		}
		else {
			checkTranslationElement(uri, localName, qName, attributes);
		}
	}
}
