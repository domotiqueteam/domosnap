package com.domosnap.engine.adapter.impl.openwebnet.conversion.gateway;

import java.util.ArrayList;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.List;
import java.util.Map;

import com.domosnap.engine.adapter.core.UnSupportedStateException;
import com.domosnap.engine.adapter.core.UnknownStateException;
import com.domosnap.engine.adapter.core.UnknownStateValueException;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.Converter;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.dimension.DimensionStatus;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.dimension.DimensionValue;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.gateway.dimension.Date;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.gateway.dimension.DateTime;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.gateway.dimension.DistributionVersion;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.gateway.dimension.FirmwareVersion;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.gateway.dimension.IpAddress;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.gateway.dimension.KernelVersion;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.gateway.dimension.MacAddress;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.gateway.dimension.Model;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.gateway.dimension.NetMask;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.gateway.dimension.Time;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.gateway.dimension.UpTime;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.gateway.Gateway;
import com.domosnap.engine.controller.gateway.GatewayStateName;
import com.domosnap.engine.controller.what.State;
import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.controller.what.impl.DateState;
import com.domosnap.engine.controller.what.impl.IpAddressState;
import com.domosnap.engine.controller.what.impl.MacAddressState;
import com.domosnap.engine.controller.what.impl.StringState;
import com.domosnap.engine.controller.what.impl.TimeState;
import com.domosnap.engine.controller.what.impl.VersionState;

public class GatewayStatusConverter implements Converter {
	
	public static String OPEN_WEB_WHO = "13";
	
	public enum GatewayDimension {
		DATE("1"),
		DATETIME("22"),
		DISTRIBUTION_VERSION("24"),
		FIRMWARE_VERSION("16"),
		IP_ADDRESS("10"),
		KERNEL_VERSION("23"),
		MAC_ADDRESS("12"),
		MODEL("15"),
		NETMASK("11"),
		TIME("0"),
		UPTIME("19");
		
		private String openWebNetCode;
	
		private GatewayDimension(String code) {
			this.openWebNetCode = code;
		}
	
		public String getCode() {
			return openWebNetCode;
		}
		
		public static GatewayDimension fromValue(String value) {
			for (GatewayDimension c : GatewayDimension.values()) {
				if (c.getCode().equals(value)) {
					return c;
				}
			}
			return null;
		}
	
	}

	@Override
	public List<String> convert(What what, Map<String, State<?>> controllerStateList)
			throws UnknownStateException, UnSupportedStateException, UnknownStateValueException {
		// Gateway has no state... only dimension.
		throw new UnSupportedStateException();
	}

	@Override
	public List<DimensionStatus<?>> convertDimension(What what, Map<String, State<?>> controllerStateList)
			throws UnknownStateException, UnSupportedStateException, UnknownStateValueException {
		GatewayStateName c = GatewayStateName.valueOf(what.getName());
		
		
		List<DimensionStatus<?>> l = new ArrayList<DimensionStatus<?>>();
		switch (c) {
		case date:
			Date dd = new Date();
			if (what.getValue() != null) dd.setStateValue((DateState) what.getValue());
			l.add(dd);
			break;

		case datetime:
			DateTime dt = new DateTime();
			if (what.getValue() != null) dt.setStateValue((DateState) what.getValue());
			l.add(dt);
			break;
			
		case distribution_version:
			DistributionVersion dv = new DistributionVersion();
			if (what.getValue() != null) dv.setStateValue((VersionState) what.getValue());
			l.add(dv);
			break;
			
		case firmware_version:
			FirmwareVersion fv = new FirmwareVersion();
			if (what.getValue() != null) fv.setStateValue((VersionState) what.getValue());
			l.add(fv);
			break;
			
		case ip_address:
			IpAddress ai = new IpAddress();
			if (what.getValue() != null) ai.setStateValue((IpAddressState) what.getValue());
			l.add(ai);
			break;
			
		case kernel_version:
			KernelVersion kv = new KernelVersion();
			if (what.getValue() != null) kv.setStateValue((VersionState) what.getValue());
			l.add(kv);
			break;
			
		case mac_address:
			MacAddress ma = new MacAddress();
			if (what.getValue() != null) ma.setStateValue((MacAddressState) what.getValue());
			l.add(ma);
			break;
			
		case model:
			Model m = new Model();
			if (what.getValue() != null) m.setStateValue((StringState) what.getValue());
			l.add(m);
			break;
			
		case netmask:
			NetMask nm = new NetMask();
			if (what.getValue() != null) nm.setStateValue((IpAddressState) what.getValue());
			l.add(nm);
			break;
			
		case time:
			Time t = new Time();
			if (what.getValue() != null) t.setStateValue((TimeState) what.getValue());
			l.add(t);
			break;
			
		case uptime:
			UpTime ut = new UpTime();
			if (what.getValue() != null) ut.setStateValue((DateState) what.getValue());
			l.add(ut);
			break;
			
		default:
			break;
		}
		
		return l;
	}

	@Override
	public List<What> convert(String code, Map<String, State<?>> controllerStateList)
			throws UnknownStateException, UnSupportedStateException, UnknownStateValueException {
		throw new UnSupportedStateException();
	}

	@Override
	public List<What> convertDimension(String dimension, List<DimensionValue> dimensionValueList,
			Map<String, State<?>> controllerStateList) throws UnknownStateException, UnSupportedStateException, UnknownStateValueException {

			GatewayDimension gd = GatewayDimension.fromValue(dimension);
			List<What> l = new ArrayList<What>();
			if (gd != null) {
				What what;
				switch (gd) {
				case DATE:
					Date dd = new Date();
					dd.setValueList(dimensionValueList);
					what = new What(GatewayStateName.date.name(), dd.getStateValue());
					l.add(what);
					break;

				case DATETIME:
					DateTime dt = new DateTime();
					dt.setValueList(dimensionValueList);
					what = new What(GatewayStateName.datetime.name(), dt.getStateValue());
					l.add(what);
					break;
					
				case DISTRIBUTION_VERSION:
					DistributionVersion dv = new DistributionVersion();
					dv.setValueList(dimensionValueList);
					what = new What(GatewayStateName.distribution_version.name(), dv.getStateValue());
					l.add(what);
					break;
					
				case FIRMWARE_VERSION:
					FirmwareVersion fv = new FirmwareVersion();
					fv.setValueList(dimensionValueList);
					what = new What(GatewayStateName.firmware_version.name(), fv.getStateValue());
					l.add(what);
					break;
					
				case IP_ADDRESS:
					IpAddress ia = new IpAddress();
					ia.setValueList(dimensionValueList);
					what = new What(GatewayStateName.ip_address.name(), ia.getStateValue());
					l.add(what);
					break;
					
				case KERNEL_VERSION:
					KernelVersion kv = new KernelVersion();
					kv.setValueList(dimensionValueList);
					what = new What(GatewayStateName.kernel_version.name(), kv.getStateValue());
					l.add(what);
					break;
					
				case MAC_ADDRESS:
					MacAddress ma = new MacAddress();
					ma.setValueList(dimensionValueList);
					what = new What(GatewayStateName.mac_address.name(), ma.getStateValue());
					l.add(what);
					break;
					
				case MODEL:
					Model m = new Model();
					m.setValueList(dimensionValueList);
					what = new What(GatewayStateName.model.name(), m.getStateValue());
					l.add(what);
					break;
					
				case NETMASK:
					NetMask nm = new NetMask();
					nm.setValueList(dimensionValueList);
					what = new What(GatewayStateName.netmask.name(), nm.getStateValue());
					l.add(what);
					break;
					
					
				case TIME:
					Time t = new Time();
					t.setValueList(dimensionValueList);
					what = new What(GatewayStateName.time.name(), t.getStateValue());
					l.add(what);
					break;
					
				case UPTIME:
					UpTime ut = new UpTime();
					ut.setValueList(dimensionValueList);
					what = new What(GatewayStateName.uptime.name(), ut.getStateValue());
					l.add(what);
					break;
					
				default:
					break;
				}
				
				return l;
			
			} else {
				return null;
			}
	}

	@Override
	public boolean isDimension(What what) {
		return true;
	}

	@Override
	public String getOpenWebWho() {
		return OPEN_WEB_WHO;
	}

	@Override
	public Class<? extends Controller> getHomeSnapWho() {
		return Gateway.class;
	}
}
