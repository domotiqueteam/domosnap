package com.domosnap.engine.adapter.impl.openwebnet.conversion.heating.dimension;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.dimension.DimensionStatusImpl;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.dimension.DimensionValue;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.dimension.DimensionValueImpl;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.heating.HeatingZoneConverter.HeatingZoneDimension;
import com.domosnap.engine.controller.heating.stateValue.OffsetState;
import com.domosnap.engine.controller.heating.stateValue.OffsetState.Mode;

public class SetOffset extends DimensionStatusImpl<OffsetState> {

	private int LOCAL_OFFSET_POS = 0;
	
	public SetOffset() {
		super(new DimensionValue[] { 
				new DimensionValueImpl()  // OL: Local Offset
				},
				HeatingZoneDimension.LOCAL_OFFSET.getCode()
		);
	}

	@Override
	public OffsetState getStateValue() {
		String val = getStringValue(LOCAL_OFFSET_POS);
		if ("00".equals(val)) {
			return new OffsetState(OffsetState.Mode.ON, 0);
		} else if ("01".equals(val)) {
			return new OffsetState(OffsetState.Mode.ON, 1);
		} else if ("11".equals(val)) {
			return new OffsetState(OffsetState.Mode.ON, -1);
		} else if ("02".equals(val)) {
			return new OffsetState(OffsetState.Mode.ON, 2);
		} else if ("12".equals(val)) {
			return new OffsetState(OffsetState.Mode.ON, -2);
		} else if ("03".equals(val)) {
			return new OffsetState(OffsetState.Mode.ON, 3);
		} else if ("13".equals(val)) {
			return new OffsetState(OffsetState.Mode.ON, -3);
		} else if ("4".equals(val)) {
			return new OffsetState(OffsetState.Mode.OFF, 0);
		} else if ("5".equals(val)) {
			return new OffsetState(OffsetState.Mode.PROTECTION, 0);
		} else {
			return null;
		}
	}

	@Override
	public void setStateValue(OffsetState value) {
		String val;
		Mode mode = value.getMode();
		int degree = value.getDegree();
		
		if (OffsetState.Mode.ON.equals(mode) && 3 == degree) {
			val = "00";
		} else if (OffsetState.Mode.ON.equals(mode) && 1 == degree) {
			val = "01";
		} else if (OffsetState.Mode.ON.equals(mode) && -1 == degree) {
			val = "11";
		} else if (OffsetState.Mode.ON.equals(mode) &&  2 == degree) {
			val = "02";
		} else if (OffsetState.Mode.ON.equals(mode) &&  -2 == degree) {
			val = "12";
		} else if (OffsetState.Mode.ON.equals(mode) &&  3 == degree) {
			val = "03";
		} else if (OffsetState.Mode.ON.equals(mode) &&  -3 == degree) {
			val = "13";
		} else if (OffsetState.Mode.OFF.equals(mode) &&  0 == degree) {
			val = "4";
		} else if (OffsetState.Mode.PROTECTION.equals(mode) && 0 == degree) {
			val = "5";
		} else {
			throw new RuntimeException("Offset ["+ mode + ";" + degree + "] unSupported!");
		}
		
		setStringValue(val, LOCAL_OFFSET_POS);
	}
}
