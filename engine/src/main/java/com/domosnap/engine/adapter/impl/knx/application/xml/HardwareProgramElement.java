package com.domosnap.engine.adapter.impl.knx.application.xml;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


public class HardwareProgramElement extends XMLElement {

	private String applicationProgramRefId;
	
	public HardwareProgramElement(String id) {
		super(id, null);
	}
	
	public String getApplicationProgramRefId() {
		return applicationProgramRefId;
	}
	
	public void setApplicationProgramRefId(String applicationProgramRefId) {
		this.applicationProgramRefId = applicationProgramRefId;
	}

	@Override
	public String toString() {
		return new StringBuilder("<Hardware2Program Id=\"").append(getId())
				.append("\" ApplicationProgramRefId=\"").append(applicationProgramRefId)
				.append("\" />").toString();
	}
}
