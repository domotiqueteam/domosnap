package com.domosnap.engine.adapter.impl.knx.application.xml;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import tuwien.auto.calimero.IndividualAddress;

public class DeviceInstanceElement extends XMLElement {
	
	private IndividualAddress address;
	private String productRefId;
	private String programRefId;
	private Collection<ComObjectInstanceRefElement> comObjectInstanceRefElements;

	public DeviceInstanceElement(String id, IndividualAddress address, String name, String productRefId, String programRefId) {
		super(id, name);
		this.address = address;
		this.productRefId = productRefId;
		this.programRefId = programRefId;
		comObjectInstanceRefElements = new ArrayList<ComObjectInstanceRefElement>();
	}

	public IndividualAddress getAddress() {
		return address;
	}
	
	public String getProductRefId() {
		return productRefId;
	}
	
	public String getProgramRefId() {
		return programRefId;
	}
	
	public Collection<ComObjectInstanceRefElement> geComObjectInstanceRefElements() {
		return Collections.unmodifiableCollection(comObjectInstanceRefElements);
	}
	
	public void addComObjectInstanceRefElement(ComObjectInstanceRefElement comObjectInstanceRefElement) {
		comObjectInstanceRefElements.add(comObjectInstanceRefElement);
	}
	
	@Override
	public String toString() {
		return new StringBuilder("<DeviceInstance Id=\"").append(getId())
				.append("\" Address=\"").append(getAddress())
				.append("\" Name=\"").append(getName())
				.append("\" ProductRefId=\"").append(productRefId)
				.append("\" ProgramRefId=\"").append(programRefId)
				.append("\" />").toString();
	}
}
