package com.domosnap.engine.adapter.impl.knx.application.xml;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class XMLElementMap {
	
	private Map<String, XMLElement> identifiers;
	private Map<Class<? extends XMLElement>, Map<String, ? extends XMLElement>> elements;
	
	public XMLElementMap() {
		identifiers = new HashMap<String, XMLElement>();
		elements = new HashMap<Class<? extends XMLElement>, Map<String, ? extends XMLElement>>();
	}
	
	public <T extends XMLElement> T get(Class<T> clazz, String uid) {
		
		T result = null;
		Map<String, T> map = getElements(clazz);
		if (map != null) {
			result = map.get(uid);
		}
		return result;
	}
	
	public <T extends XMLElement> Collection<T> getAll(Class<T> clazz) {
		
		Collection<T> result = null;
		Map<String, T> map = getElements(clazz);
		if (map != null) {
			result = map.values();
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public <T extends XMLElement> void put(T element) {
		
		if (element != null) {
			synchronized (elements) {
				
				Map<String, T> map = (Map<String, T>) elements.get(element.getClass());
				if (map == null) {
					map = new HashMap<String, T>();
					elements.put(element.getClass(), map);
				}
				map.put(element.getUID(), element);
				identifiers.put(element.getUID(), element);
			}
		}
	}
	
	public <T extends XMLElement> void addAll(Collection<T> elements) {
		if (elements != null) {
			for (T element : elements) {
				put(element);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private <T extends XMLElement> Map<String, T> getElements(Class<T> clazz) {
		
		Map<String, T> result = null;
		if (clazz != null) {
			synchronized (elements) {
				result = (Map<String, T>) elements.get(clazz);
			}
		}
		return result;
	}

	public XMLElement findById(String id) {
		return identifiers.get(id);
	}
}