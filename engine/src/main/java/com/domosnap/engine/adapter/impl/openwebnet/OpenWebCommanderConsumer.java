package com.domosnap.engine.adapter.impl.openwebnet;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.impl.openwebnet.connector.AbstractOpenWebConnection;
import com.domosnap.engine.adapter.impl.openwebnet.connector.OpenWebNetConstant;

import io.reactivex.functions.Consumer;

public class OpenWebCommanderConsumer extends AbstractOpenWebConnection implements Consumer<String> {

	/**
	 * 
	 * @param ip the ip or dns name of the open server
	 * @param port the port number of the open server
	 * @param passwordOpen 
	 */
	public OpenWebCommanderConsumer(String ip, int port, Integer passwordOpen) {
		super(ip, port, passwordOpen, Log.Session.Command);
	}
	
	@Override
	public void accept(String command) throws Exception {
		if (command == null) {
			log.severe(Session.Command, getFormattedLog(1, "Command unsupported (null)."));
			return;
		}

		if (log.finest) {
			log.fine(Session.Command, getFormattedLog(0, "Sending command [".concat(command).concat("]")));
		}
		// Send asynchronously the command!
		getExecutorService().execute(new CommandHandler(command));
	}

	private class CommandHandler implements Runnable {

		private final String command;
		
		public CommandHandler(String command) {
			this.command = command;
		}

		@Override
		public void run() {
			synchronized (lock) { // mutex on the main thread: only one connection or send message at the same time!

				if (!isConnected() && forceConnection) { // If socket close? => init connection.
					connection();
				}

				if (isConnected()) { // Test again since with the lock, maybe a previous thread has closed the connection!
					writeMessage(command);
					String msg = readMessage();

					if(msg == null){
						log.severe(Session.Command, getFormattedLog(0, "Command [" + command +  "] failed."));
						return;
					}

					if (OpenWebNetConstant.ACK.equals(msg)){
						if (log.finest) {
							log.finest(Session.Command, getFormattedLog(0, "Command [" + command +  "] sent."));
						}
						return;
					} else if (OpenWebNetConstant.NACK.equals(msg)){
						log.severe(Session.Command, getFormattedLog(0, "Command [" + command +  "] failed."));
						return;
					} else { // First return was information. The next should be result (1 or n) and the acknowledgment
							boolean continu = true;
							String actionReturn = msg;
							while (continu) {
								msg = readMessage();
								if(OpenWebNetConstant.ACK.equals(msg)){
									// Case where command is ok
									if (log.finest) {
										log.finest(Session.Command, getFormattedLog(1, "Result of [" + command +  "] received [" + actionReturn + "]"));
										log.finest(Session.Command, getFormattedLog(0, "Command [" + command +  "] sent."));
									}
									continu = false;
									
								} else if (OpenWebNetConstant.NACK.equals(msg)) {
									// Case where command is ko
									log.severe(Session.Command, getFormattedLog(0, "Command [" + command +  "] failed."));
									continu = false;
								} else {
									// case where command is partial: we get more than once return. This can occurs for exemple when we send a command to a group: for each controller we get a return and the acknoledge at the end.
									if (log.finest) {
										log.finest(Session.Command, getFormattedLog(1, "Result of [" + command +  "] received [" + actionReturn + "]"));
									}
									actionReturn = msg;
								}
							}
							return;
					}
				} else { // connection closed...
					log.severe(Session.Command, getFormattedLog(0, "Command [" + command +  "] failed (Connection closed)."));
					return;
				}
			}
		}
	}
}
