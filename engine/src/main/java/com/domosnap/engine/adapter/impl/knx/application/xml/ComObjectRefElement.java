package com.domosnap.engine.adapter.impl.knx.application.xml;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


/**
 * 
 * @author DRIESBACH Olivier
 */
public class ComObjectRefElement extends XMLRefElement {
	
	private String datapointType;
	private String functionText;
	private String text;
	
	/**
	 * 
	 * @param id
	 * @param refId
	 * @param datapointType
	 * @param functionText
	 * @param text
	 */
	public ComObjectRefElement(String id, String refId, String datapointType, String functionText, String text) {
		super(id, refId);
		this.datapointType = datapointType;
		this.functionText = functionText;
		this.text = text;
	}
	
	public String getDatapointType() {
		return datapointType;
	}

	public String getFunctionText() {
		return functionText;
	}

	public String getText() {
		return text;
	}
	
	@Override
	public String toString() {
		return new StringBuilder("<ComObjectRef Id=\"").append(getRefId())
				.append("\" RefId=\"").append(getRefId())
				.append("\" DatapointType=\"").append(datapointType)
				.append("\" Text=\"").append(text)
				.append("\" FunctionText=\"").append(functionText)
				.append("\" />").toString();
	}
	
	@Override
	public void translate(TranslationElement translation) {
		super.translate(translation);
		text = translation.getTranslation("Text", text);
		functionText = translation.getTranslation("FunctionText", functionText);
	}
}
