package com.domosnap.engine.adapter.impl.openwebnet.conversion.shutter;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.adapter.core.Command;

import io.reactivex.functions.Function;

public class ShutterOpenWebNetConvertToCommandFunction implements Function<String, Command> {

	@Override
	public Command apply(String arg0) throws Exception {
//		final List<What> converted = new AutomationStatusConverter().convert(arg0, null);
////		converted.add("LIGTH EVENT");
//		
//		return new Publisher<Command>() {
//			@Override
//			public void subscribe(Subscriber<? super Command> arg0) {
//				for (Command string : converted) {
//					arg0.onNext(string);
//				}
//			}
//		};
		return null;
	}

}
