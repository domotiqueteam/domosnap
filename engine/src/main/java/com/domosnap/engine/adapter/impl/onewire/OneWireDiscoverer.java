package com.domosnap.engine.adapter.impl.onewire;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dalsemi.onewire.OneWireAccessProvider;
import com.dalsemi.onewire.adapter.DSPortAdapter;
import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;

public class OneWireDiscoverer {

	/** */
	private static Log log = new Log(OneWireDiscoverer.class.getSimpleName());
	
	/** */
	private static OneWireDiscoverer instance = new OneWireDiscoverer();
	
	/** */
	private Map<String, OneWireAdapter> adapters;
	
	/** */
	private long lastSearch;
	
	/**
	 * 
	 */
	public OneWireDiscoverer() {
		adapters = new HashMap<String, OneWireAdapter>(1);
	}
	
	/**
	 * 
	 * @return
	 */
	public static OneWireDiscoverer getInstance() {
		return instance;
	}
	
	/**
	 * Returns all the 1-wire adapter found.
	 * @return
	 */
	public Collection<OneWireAdapter> getAdapters() {
		
		if (adapters.isEmpty() && lastSearch == -1) {
			searchAdapters(); // Search for the first time on the network
		}
		return Collections.unmodifiableCollection(adapters.values());
	}
	
	/**
	 * 
	 * @return
	 */
	public void searchAdapters() {
		searchAdapters(false);
	}
	
	/**
	 * 
	 * @param forceSearch
	 */
	public void searchAdapters(boolean forceSearch) {
		
		synchronized (adapters) {
			
			if (lastSearch == -1 || forceSearch) {
				
				lastSearch = System.currentTimeMillis();
				adapters.clear(); // Clear previous results
		
				@SuppressWarnings("unchecked")
				List<DSPortAdapter> allAdapters = Collections.list(OneWireAccessProvider.enumerateAllAdapters());
				
				// Scan one time all ports before looping
				for (DSPortAdapter adapter : allAdapters) {
					
					@SuppressWarnings("unchecked")
					Enumeration<String> portNames = adapter.getPortNames();
					while (portNames.hasMoreElements()) {
						
						String portName = portNames.nextElement();
						try {
							if (adapter.selectPort(portName)) { // Connects the adapter to the port to detect its presence or not
								
								OneWireAdapter owAdapter = new OneWireAdapter(adapter, portName);
								log.info(Session.Server, String.format("1-Wire adapter %s found on port %s [Address : %s, Version : %s]",
										adapter.getAdapterName(), portName, adapter.getAdapterAddress(), adapter.getAdapterVersion()));
								owAdapter.disconnect();
								
								synchronized (adapters) {
									adapters.put(createAdapterKey(owAdapter.getName(), portName), owAdapter);
								}
//								notifyAdapterConnected(owAdapter);
							}
						}
						catch (Exception e) {
							// Adapter has not been detected
						}
						portNames.nextElement();
					}
				}
			}
		}
	}
	
	private String createAdapterKey(String adapterName, String portName) {
		return String.format("%s#%s", adapterName, portName);
	}
}
