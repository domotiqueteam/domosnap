package com.domosnap.engine.adapter.impl.knx.translator;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.HashMap;
import java.util.Map;

import com.domosnap.engine.controller.light.LightStateName;
import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.controller.what.impl.IntegerState;
import com.domosnap.engine.controller.what.impl.OnOffState;

import tuwien.auto.calimero.dptxlator.DPT;
import tuwien.auto.calimero.dptxlator.DPTXlator8BitUnsigned;
import tuwien.auto.calimero.dptxlator.DPTXlatorBoolean;

public class KNXLightTranslator implements Translator {
	
	private static final Map<LightStateName, DPT> types = new HashMap<LightStateName, DPT>();
	static {
		types.put(LightStateName.status, DPTXlatorBoolean.DPT_SWITCH);
		types.put(LightStateName.level, DPTXlator8BitUnsigned.DPT_SCALING);
	}
	
	@Override
	public DPT getDataPointType(String stateName) {
		return types.get(LightStateName.valueOf(stateName));
	}
	
	@Override
	public What translateFromKNX(String stateName, String stateValue) {
		
		switch (LightStateName.valueOf(stateName)) {
			case status:
				return new What(stateName, DPTXlatorBoolean.DPT_SWITCH.getLowerValue().equals(stateValue) ? OnOffState.Off() : OnOffState.On());
				
			case level: {
				
				Integer value = null;
				try {
					value = new Integer(stateValue);
				} catch (Exception e) {
				}
				
				return new What(stateName, new IntegerState(value.intValue()));
			}
				
			default:
				return null;
		}
	}

	@Override
	public String translateToKNX(What what) {
		
		switch (LightStateName.valueOf(what.getName())) {
			case status: {
				OnOffState state = (OnOffState) what.getValue();
				return OnOffState.Off().getValue().equals(state.getValue()) ? DPTXlatorBoolean.DPT_SWITCH.getLowerValue() : DPTXlatorBoolean.DPT_SWITCH.getUpperValue();
			}
			
			case level: {
				return what.getValue().toString();
			}
			
			default:
				return null;
		}
	}
}
