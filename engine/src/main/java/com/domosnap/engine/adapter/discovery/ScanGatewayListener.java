package com.domosnap.engine.adapter.discovery;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * {@link ScanGatewayListener} must be provided to {@link AdapterDiscovery#scan(ScanGatewayListener)}.
 * It act as a callback when an unknown controller is found.
 */
public interface ScanGatewayListener {

	/**
	 * Called each time a new gateway is found.
	 * @param uri uri of the gateway
	 */
	public void foundGateway(String uri);

	/**
	 * Called by {@link AdapterDiscovery#scan(ScanGatewayListener)} by providing percent progress.
	 * When percent = 100, scan SHOULD be finished but without any warranty.
	 * @param percent
	 */
	public void progess(int percent);
	
	/**
	 *  Called by {@link AdapterDiscovery#scan(ScanGatewayListener)} when percent = 100.
	 *  Scan SHOULD be finished but without any warranty.
	 */
	public void scanFinished();
}
