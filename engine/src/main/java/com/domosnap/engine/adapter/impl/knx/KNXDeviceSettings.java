package com.domosnap.engine.adapter.impl.knx;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.net.InetAddress;
import java.net.UnknownHostException;

import tuwien.auto.calimero.IndividualAddress;

/**
 * 
 * @author DRIESBACH Olivier
 */
public class KNXDeviceSettings {
	
	/** The inetAddress of the KNXnet/IP device */
	private InetAddress inetAddress;
	
	/** The multicast IP address of the KNXnet/IP device if it is a router */
	private InetAddress multicastAddress;
	
	/** The KNX address of the device */
	private IndividualAddress address;
	
	/** The port number of the socket used for communicating with the KNXnet/IP device */
	private int port;
	
	/** Indicates if the device accepts connections on a multicast IP address */
	
	/**
	 * 
	 * @param inetAddress
	 * @param port
	 * @param address
	 * @param multicastAddress
	 */
	public KNXDeviceSettings(InetAddress inetAddress, int port, IndividualAddress address, byte[] multicastAddress) {
		this.inetAddress = inetAddress;
		this.port = port;
		this.address = address;
		
		try {
			this.multicastAddress = InetAddress.getByAddress(multicastAddress);
		}
		catch (UnknownHostException e) {
			this.multicastAddress = null;
		}
	}

	/**
	 * 
	 * @return
	 */
	public InetAddress getInetAddress() {
		return inetAddress;
	}

	/**
	 * 
	 * @return
	 */
	public IndividualAddress getAddress() {
		return address;
	}

	/**
	 * 
	 * @return
	 */
	public int getPort() {
		return port;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isRouter() {
		return (multicastAddress != null);
	}
}
