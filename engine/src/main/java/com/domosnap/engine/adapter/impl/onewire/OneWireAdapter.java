package com.domosnap.engine.adapter.impl.onewire;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;

import com.dalsemi.onewire.OneWireException;
import com.dalsemi.onewire.adapter.DSPortAdapter;
import com.dalsemi.onewire.application.monitor.DeviceMonitor;
import com.dalsemi.onewire.application.monitor.DeviceMonitorEventListener;
import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;

public class OneWireAdapter {
	
	/** */
	private static Log log = new Log(OneWireAdapter.class.getSimpleName());
	
	/** The port adapter device */
	private DSPortAdapter adapter;
	
	/** The port name of the adapter */
	private String portName;
	
	/** The physical address of the adapter to prevent from reading the 1-Wire bus */
	private String address;
	
	/** The version number of the adapter to prevent from reading the 1-Wire bus */
	private String version;
	
	/** */
	private DeviceMonitor deviceMonitor;
	
	/** */
	private Collection<DeviceMonitorEventListener> deviceListeners;
	
	/** */
	private boolean connected;
	
	/**
	 * Creates a 1-Wire port adapter linked to the port name on which one the device has been detected.
	 * If the adapter is currently connected, the <code>portName</code> attribute can be ignored.
	 * The port name of the adapter will be used each time the {@link #connect()} method is called.
	 * 
	 * If the adapter is not yet connected, the {@link #connect()} method could fail because there is
	 * no guarantee that the device is currently plugged into the physical port designated by the
	 * <code>portName</code> attribute.
	 * 
	 * @param adapter The {@link DSPortAdapter} instance of the device
	 * @param portName The port name of the adapter
	 * 
	 * @see OneWirePortScanner#searchAdapters()
	 */
	public OneWireAdapter(DSPortAdapter adapter, String portName) {
		
		this.adapter = adapter;
		try {
			this.portName = adapter.getPortName();
			connected = true;
		} catch (OneWireException e) {
			this.portName = portName;
			connected = false;
		}
		deviceListeners = new ArrayList<DeviceMonitorEventListener>(1);
	}
	
	/**
	 * 
	 * @return
	 */
	public String getAddress() {
		return address;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getName() {
		return adapter.getAdapterName();
	}
	
	/**
	 * 
	 * @return
	 */
	public String getPortName() {
		return portName;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * 
	 * @return
	 */
	public boolean connect() {
		
		if (isConnected()) {
			return true;
		}
		
		boolean result = false;
		try {
			result = adapter.selectPort(portName);
			
			// Read all adapter static information once
			try {
				address = adapter.getAdapterAddress();
				version = adapter.getAdapterVersion();
			} catch (OneWireException e) {
			}
			log.info(Session.Server, "Connection established to 1-wire adapter "+ getName() +" on port "+ getPortName());
			
		} catch (Exception e) {
			log.severe(Session.Server, "Unable to connect the 1-wire adapter "+ getName() +" on port "+ getPortName());
		}
		return result;
	}
	
	/**
	 * 
	 */
	public void disconnect() {
		
		if (isConnected()) {
			
			if (deviceMonitor != null) {
				stopDeviceMonitor();
			}
			
			try {
				adapter.freePort();
			} catch (OneWireException e) {
				log.severe(Session.Server, e.getMessage());
			}
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isConnected() {
		return connected;
	}
	
	/**
	 * 
	 * @param listener
	 */
	public void addDeviceMonitorListener(DeviceMonitorEventListener listener) {
		synchronized (deviceListeners) {
			deviceListeners.add(listener);
		}
	}
	
	/**
	 * 	
	 */
	public void startDeviceMonitor() {
		
		try {
			deviceMonitor = new DeviceMonitor(adapter);
			for (DeviceMonitorEventListener listener : deviceListeners) {
				deviceMonitor.addDeviceMonitorEventListener(listener);
			}
			new Thread(deviceMonitor).start();
		}
		catch (Throwable t) {
		}
	}
	
	/**
	 * 
	 */
	public void stopDeviceMonitor() {
		
		try {
			deviceMonitor.killMonitor();
		}
		catch (Throwable t) {
		}
		finally {
			deviceMonitor = null;
		}
	}
	
//	public void sendCommand(final Command command, CommandListener resultListener) {
//		if (isConnected()) {
//			OneWireContainer owc = monitor.getContainer(command.getWhere().getTo());
//			if (owc == null) {
//			} else {
//				executeCommand(owc, command);
//				resultListener.onCommand(new CommandResult() {
//					
//					@Override
//					public CommandResultStatus getStatus() {
//						return CommandResultStatus.ok;
//					}
//
//					@Override
//					public State getWhat(StateName name) {
//						return null;
//					}
//
//					@Override
//					public Who getWho() {
//						return null;
//					}
//
//					@Override
//					public Where getWhere() {
//						return null;
//					}
//				});
//			}
//		} else {
//		}
//	}

//	private void executeCommand(OneWireContainer container, Command command) {
//		
//		// OneWireSensor
//		if (container instanceof ADContainer) {
//		}
//		if (container instanceof ClockContainer) {
//			sendClockCommand(command, (ClockContainer) container);
//		}
//		if (container instanceof HumidityContainer) {
//		}
//		if (container instanceof MissionContainer) {
//		}
//		if (container instanceof PotentiometerContainer) {
//		}
//		if (container instanceof TemperatureContainer) {
//			sendTemperatureCommand(command, (TemperatureContainer) container);
//		}
//		if (container instanceof SwitchContainer) {
//		}
//	}
	
//	private static final String DEFAULT_DATE_PATTERN = "dd/MM/yyyy";
//	private static final String DEFAULT_DATE_TIME_PATTERN = "dd/MM/yyyy HH:mm:ss";
//	private static final String DEFAULT_TIME_PATTERN = "HH:mm:ss";
	
//	private void sendClockCommand(Command command, ClockContainer container) {
		
//		new OneWireSensorCommand<ClockContainer>(adapter, command, container) {
//
//			@Override
//			public StateValue read(State state, ClockContainer sensor, byte[] deviceState) throws OneWireException {
//				
//				StateValue result = null;
//				if (state.getName() instanceof ClockSensorStateName) {
//					
//					ClockSensorStateName stateName = (ClockSensorStateName) state.getName();
//					switch (stateName) {
//						case ALARM: {
//							// TODO Faire un DateUtil
//							long time = sensor.getClockAlarm(deviceState);
//							result = new DateState(new Date(time));
//							break;
//						}
//						
//						case DATE:
//						case TIME:
//						case DATE_TIME: {
//							String pattern;
//							if (StateNameEnum.DATE.equals(state.getName())) {
//								pattern = DEFAULT_DATE_PATTERN;
//							}
//							if (StateNameEnum.TIME.equals(state.getName())) {
//								pattern = DEFAULT_TIME_PATTERN;
//							}
//							if (StateNameEnum.DATE_TIME.equals(state.getName())) {
//								pattern = DEFAULT_DATE_TIME_PATTERN;
//							}
//							else {
//								throw new IllegalArgumentException("StateName is not supported : "+ state.getName());
//							}
//							
//							long time = sensor.getClock(deviceState);
//							result = new DateState(new Date(time));
//							result = new StringValue(new SimpleDateFormat(pattern).format(new Date(time)));
//							break;
//						}
//						default: {
//							result = null;
//						}
//					}
//				}
//				return result;
//			}
//
//			@Override
//			public void write(State state, ClockContainer sensor, byte[] deviceState) throws OneWireException {
//				
//				if (state.getName() instanceof ClockSensorStateName) {
//					
//					ClockSensorStateName stateName = (ClockSensorStateName) state.getName();
//					switch (stateName) {
//						case ALARM: {
//							Date newDate;
//							try {
//								newDate = new SimpleDateFormat(DEFAULT_TIME_PATTERN).parse(state.getValue().getValue());
//								sensor.setClockAlarm(newDate.getTime(), deviceState);
//							} catch (ParseException e) {
//								// TODO
//								e.printStackTrace();
//							}
//							break;
//						}
//						
//						case DATE:
//						case TIME:
//						case DATE_TIME: {
//							Date newDate;
//							try {
//								newDate = new SimpleDateFormat(DEFAULT_DATE_TIME_PATTERN).parse(state.getValue().getValue());
//								sensor.setClock(newDate.getTime(), deviceState);
//							} catch (ParseException e) {
//								// TODO
//								e.printStackTrace();
//							}
//							break;
//						}
//						default: {
//						}
//					}
//				}
//			}
//			
//		}.send();
//	}

//	private void sendTemperatureCommand(Command command, TemperatureContainer container) {
		
//		new OneWireSensorCommand<TemperatureContainer>(adapter, command, container) {
//			
//			@Override
//			public StateValue read(State state, TemperatureContainer sensor, byte[] deviceState) throws OneWireException {
//				
//				StateValue result = null;
//				if (state.getName() instanceof TemperatureSensorStateName) {
//					
//					TemperatureSensorStateName stateName = (TemperatureSensorStateName) state.getName();
//					switch (stateName) {
//						case ALARM_HIGH: {
//							result = new DoubleState(sensor.getTemperatureAlarm(TemperatureContainer.ALARM_HIGH, deviceState));
//							break;
//						}
//						case ALARM_LOW: {
//							result = new DoubleState(sensor.getTemperatureAlarm(TemperatureContainer.ALARM_LOW, deviceState));
//							break;
//						}
//						case HIGHEST_TEMP: {
//							result = new DoubleState(sensor.getMaxTemperature());
//							break;
//						}
//						case LOWEST_TEMP: {
//							result = new DoubleState(sensor.getMinTemperature());
//							break;
//						}
//						default: {
//							result = null;
//						}
//					}
//				}
//				return result;
//			}
//			
//			@Override
//			public void write(State state, TemperatureContainer sensor, byte[] deviceState) throws OneWireException {
//				
//				if (state.getName() instanceof TemperatureSensorStateName) {
//					
//					TemperatureSensorStateName stateName = (TemperatureSensorStateName) state.getName();
//					switch (stateName) {
//						case ALARM_HIGH: {
//							sensor.setTemperatureAlarm(TemperatureContainer.ALARM_HIGH,
//									((DoubleState) state.getValue()).getDoubleValue(), deviceState);
//							break;
//						}
//						case ALARM_LOW: {
//							sensor.setTemperatureAlarm(TemperatureContainer.ALARM_LOW,
//									((DoubleState) state.getValue()).getDoubleValue(), deviceState);
//							break;
//						}
//						default: {
//							// TODO log.debug()
//						}
//					}
//				}
//			}
//		}.send();
//	}
}
