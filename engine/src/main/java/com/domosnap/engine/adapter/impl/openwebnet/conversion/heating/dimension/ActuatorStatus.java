package com.domosnap.engine.adapter.impl.openwebnet.conversion.heating.dimension;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.dimension.DimensionStatusImpl;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.dimension.DimensionValue;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.dimension.DimensionValueImpl;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.heating.HeatingZoneConverter.HeatingZoneDimension;
import com.domosnap.engine.controller.what.impl.OnOffState;

public class ActuatorStatus extends DimensionStatusImpl<OnOffState> {

	public ActuatorStatus() {
				super(new DimensionValue[] { 
						new DimensionValueImpl()  // on or off
						},
						HeatingZoneDimension.ACTUATOR_STATUS.getCode()
				);
	}

	public boolean getActuatorStatus() {
		return "1".equals(getStringValue(0));
	}

	public void setActuatorStatus(boolean value) {
		setStringValue(value ? "1" : "0", 0);
	}

	@Override
	public OnOffState getStateValue() {
		if (getActuatorStatus()) {
			return OnOffState.On();
		} else {
			return OnOffState.Off();
		}
	}

	@Override
	public void setStateValue(OnOffState value) {
		setActuatorStatus(value.getValue());
	}

}
