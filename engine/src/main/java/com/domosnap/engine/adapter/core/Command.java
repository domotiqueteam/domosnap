package com.domosnap.engine.adapter.core;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.domosnap.engine.controller.Controller;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.controller.where.Where;

/**
 * {@link Command} represent an IoT event. {@link Controller} raise a command to change a state.
 * This command is send to Command stream which send it to the physical gateway.
 * When gateway has changed the status, it raise the command to monitor stream which send it to
 * Controller which change the state.
 */
public class Command {

	public enum Type {
		QUERY, 		// Query command
		COMMAND;	// Command command
	}

	protected Long timestamp = new Date().getTime(); // timeline of the command
	private Class<? extends Controller> who; 							   	// Controller type
	private List<What> whatList;				   	// list of event = new status
	private Where where;							// Controller address
	protected Type type;								// Command Type
	private Controller source;						// Controller source
	
	/**
	 * Constructor for an event.
	 * @param who Controller type
	 * @param what an event: new state value
	 * @param where Controller address
	 * @param type  Command type
	 * @param controller Source Controller
	 */
	public Command(Class<? extends Controller> who, What what, Where where, Type type, Controller controller) {
		this.whatList = new ArrayList<What>();
		this.whatList.add(what);
		this.who = who;
		this.where = where;
		this.type = type;
		this.source = controller;
	}
	
	/**
	 * Contructor for some events.
	 * @param who Controller type
	 * @param what list of event: new state value
	 * @param where Controller address
	 * @param type  Command type
	 * @param controller Source Controller
	 */
	public Command(Class<? extends Controller> who, List<What> what, Where where, Type type, Controller controller) {
		this.whatList = what;
		this.who = who;
		this.where = where;
		this.type = type;
		this.source = controller;
	}
	
	public Class<? extends Controller> getWho() {
		return who;
	}

	public List<What> getWhatList() {
		return whatList;
	}

	public Where getWhere() {
		return where;
	}
	
	public Controller getSource() {
		return source;
	}

	public boolean isQueryCommand() {
		return Type.QUERY.equals(type);
	}

	public boolean isWriteCommand() {
		return Type.COMMAND.equals(type);
	}
	
	public String toString() {
		return CommandJsonCodec.toJSon(this);
	}
}
