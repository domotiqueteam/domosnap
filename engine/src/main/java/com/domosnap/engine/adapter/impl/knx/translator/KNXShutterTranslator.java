package com.domosnap.engine.adapter.impl.knx.translator;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.HashMap;
import java.util.Map;

import com.domosnap.engine.controller.shutter.ShutterStateName;
import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.controller.what.impl.UpDownState;
import com.domosnap.engine.controller.what.impl.UpDownState.UpDownValue;

import tuwien.auto.calimero.dptxlator.DPT;
import tuwien.auto.calimero.dptxlator.DPTXlatorBoolean;

public class KNXShutterTranslator implements Translator {

	private static final Map<ShutterStateName, DPT> types = new HashMap<ShutterStateName, DPT>();
	static {
		types.put(ShutterStateName.status, DPTXlatorBoolean.DPT_UPDOWN);
	}
	
	@Override
	public DPT getDataPointType(String stateName) {
		return types.get(ShutterStateName.valueOf(stateName));
	}

	@Override
	public What translateFromKNX(String stateName, String stateValue) {
		
		switch (ShutterStateName.valueOf(stateName)) {
			case status:
				return new What(stateName, DPTXlatorBoolean.DPT_UPDOWN.getLowerValue().equals(stateValue) ? UpDownState.Down : UpDownState.Up);
				
			default:
				return null;
		}
	}

	@Override
	public String translateToKNX(What what) {
		
		switch (ShutterStateName.valueOf(what.getName())) {
			case status: {
				UpDownState state = (UpDownState) what.getValue();
				return UpDownValue.DOWN.equals(state.getValue()) ? DPTXlatorBoolean.DPT_UPDOWN.getLowerValue() : DPTXlatorBoolean.DPT_SWITCH.getUpperValue();
			}
		
			default:
				return null;
		}
	}
}
