package com.domosnap.engine.adapter.compliance.test.controlleradapter;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import com.domosnap.engine.adapter.ControllerAdapter;
import com.domosnap.engine.adapter.core.ConnectionListener;
import com.domosnap.engine.adapter.core.ConnectionStatusEnum;
import com.domosnap.engine.adapter.core.ScanListener;
import com.domosnap.engine.controller.Controller;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

public class ControllerAdapterTest {

	public interface IControllerAdapterTest {
		public void testScan() throws InterruptedException;
		public void testConnection() throws InterruptedException;
	}
	
	
	private boolean connectionOK ;
	private boolean scanFinish ; 
	private boolean foundAtLeastOnController;
	private boolean scanReallyFinish;

	public void testScan(ControllerAdapter controllerService) throws InterruptedException{
		this.connectionOK = false;
		this.scanFinish = false; 
		this.foundAtLeastOnController = false;
		this.scanReallyFinish = false;

		controllerService.addCommanderConnectionListener(new ConnectionListener() {
			
			@Override
			public void onConnect(ConnectionStatusEnum connectionStatus) {
				if(connectionStatus== ConnectionStatusEnum.Connected){
					connectionOK = true;
					
				}else{
					fail("connection fail"+connectionStatus);
				}
				
			}
			
			@Override
			public void onClose() {
				
			}
		});
//		controllerService.connect();
		
		//Then
		controllerService.scan(new ScanListener() {
			
			@Override
			public void progess(int percent) {
				if(percent==100){
					scanFinish = true;
				}
				
			}
			
			@Override
			public void foundController(Controller controller) {
				foundAtLeastOnController = true;
			}

			@Override
			public void scanFinished() {
				scanReallyFinish = true;
				
			}
		});
		
		
		Thread.sleep(10000);

		assertTrue("La connexion est un echecs",connectionOK);
		assertTrue("Le scan est un echecs",scanFinish);
		assertTrue("Scan pas fini...",scanReallyFinish);
		assertTrue("Aucun controler trouvé suite au scan",foundAtLeastOnController);

	}

	
	private boolean connectionOKMonitor = false;
	private boolean connectionOKCommander = false;
	
	public void testConnection(ControllerAdapter controllerService, String uri) throws InterruptedException{
		
		controllerService.addMonitorConnectionListener(new ConnectionListener() {
			
			@Override
			public void onConnect(ConnectionStatusEnum connectionStatus) {
				connectionOKMonitor = true;
			}
			
			@Override
			public void onClose() {
				if(!connectionOKMonitor){
					fail("receive on close but never call close on the connection and never start !!!!");
				}
				connectionOKMonitor = false;
			}
		});
		
		controllerService.addCommanderConnectionListener(new ConnectionListener() {
			
			@Override
			public void onConnect(ConnectionStatusEnum connectionStatus) {
				connectionOKCommander = true;
			}
			
			@Override
			public void onClose() {
				if(!connectionOKCommander){
					fail("receive on close but never call close on the connection and never start !!!!");
				}
				connectionOKCommander = false;
			}
		});
		
		//When
		controllerService.connect();
		//Then
		Thread.sleep(4000);
		assertTrue("La connexion est un echecs",connectionOKMonitor);
		assertTrue("La connexion est un echecs",connectionOKCommander);

		//When
			controllerService.disconnect();
			// Then
			Thread.sleep(4000);
			assertTrue("La déconnexion est un echecs",!connectionOKMonitor);
			assertTrue("La déconnexion est un echecs",!connectionOKCommander);
			assertTrue("isConnected ne marche pas",!controllerService.isConnected());
					
			//When
			controllerService.connect(uri);
			//Then
			Thread.sleep(4000);
			assertTrue("La connexion est un echecs",connectionOKMonitor);
			assertTrue("La connexion est un echecs",connectionOKCommander);
		}
}
