package com.domosnap.engine.adapter.impl.hue.conversion;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.controller.light.Light;
import com.domosnap.engine.controller.light.LightStateName;
import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.controller.what.impl.OnOffState;

import io.reactivex.functions.Function;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;

public class CommandToHueRequestFunction implements Function<Command, Request> {

	private Log log = new Log(CommandToHueRequestFunction.class.getSimpleName());
	
	private String host;
	private int port;
	private String user;
	
	public CommandToHueRequestFunction(String host, int port, String user) {
		this.host = host;
		this.port = port;
		this.user = user;
	}
	
	
	@Override
	public Request apply(final Command command) throws Exception {
		log.fine(Session.Command, "Prepare to send command [" + command + "].");
		final Request converted = createMessage(command);
		if (converted == null) {
			if(log.finest) {
				log.fine(Session.Command, "No translation command for : " + command.toString(), 1);
			}
		} else {
			if(log.finest) {
				log.fine(Session.Command, "Command translated.", 1);
			}
		}
		return converted;
	}
		
	/**
	 * Create hue requests for action or status.
	 */
	protected final Request createMessage(Command command) {
		if (command.getWhere() == null || command.getWhere().getPath() == null) {
			log.fine(Session.Command, "Command must contain a where.");
			throw new IllegalArgumentException("Command must contain a where");
		}

		// Actually only LIGHT is supported by hue => check that Domosnap doesn't ask another device to Hue protocole
		if (!Light.class.equals(command.getWho())) {
			throw new IllegalArgumentException("Controller Who unknown. [" + command.getWho() + "]");
		}

		String who = "lights";
		String where = command.getWhere().getPath();
		String whatName;
		String whatValue;
		Request request = null;

		if (command.isWriteCommand()) { // Write/Action request
			
			String body = "{";
			
			for (What what : command.getWhatList()) {
				String stateName = what.getName();
				if (LightStateName.blink.name().equals(stateName)) {
					log.fine(Session.Command,"Controller status unsupported. [" + what.getName() + "]");
					break;
				} else if (LightStateName.blink_time.name().equals(stateName)) {
					log.fine(Session.Command,"Controller status unsupported. [" + what.getName() + "]");
					break;
				} else if (LightStateName.color.name().equals(stateName)) {
					log.fine(Session.Command,"Controller status unsupported. [" + what.getName() + "]");
					break;
				} else if (LightStateName.level.name().equals(stateName)) {
					log.fine(Session.Command,"Controller status unsupported. [" + what.getName() + "]");
					break;
				} else if (LightStateName.status.name().equals(stateName)) {
					whatName="on";
					if (((OnOffState)what.getValue()).getValue()) {
						whatValue = "true";
					} else {
						whatValue = "false";
					}
				} else  {
					throw new IllegalArgumentException("Controller status unknown [" + what.getName() + "]");
				}
				
				body = body.concat("\"").concat(whatName).concat("\":").concat(whatValue).concat(",");
				
			}
			body = body.substring(0, body.length()-1);
			body = body.concat("}");

			RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), body);
			request = new Request.Builder()
					.url("http://" + host + ":" + port + "/api/" + user + "/"+who+"/"+where + "/state")
					.put(requestBody)
					.build();
			
		} else { // Read/Status request
			request = new Request.Builder()
					.url("http://" + host + ":" + port + "/api/" + user + "/lights/"+where)
					.build();
		}

		return request;

	}
}
