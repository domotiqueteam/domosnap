package com.domosnap.engine.adapter.impl.knx;
/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.ConnectionListener;
import com.domosnap.engine.adapter.core.ConnectionStatusEnum;
import com.domosnap.engine.adapter.impl.knx.translator.KNXTranslator;
import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.controller.where.Where;

import io.reactivex.functions.Consumer;
import tuwien.auto.calimero.CloseEvent;
import tuwien.auto.calimero.FrameEvent;
import tuwien.auto.calimero.GroupAddress;
import tuwien.auto.calimero.datapoint.CommandDP;
import tuwien.auto.calimero.datapoint.Datapoint;
import tuwien.auto.calimero.datapoint.StateDP;
import tuwien.auto.calimero.dptxlator.DPT;
import tuwien.auto.calimero.exception.KNXException;
import tuwien.auto.calimero.exception.KNXFormatException;
import tuwien.auto.calimero.link.KNXNetworkLink;
import tuwien.auto.calimero.link.NetworkLinkListener;
import tuwien.auto.calimero.process.ProcessCommunicator;
import tuwien.auto.calimero.process.ProcessCommunicatorImpl;

public class KNXCommander implements Consumer<Command> {
	
	private static final Log log = new Log(KNXCommander.class.getSimpleName());

	/** */
	private KNXConnectionSettings connectionSettings;
	
	/** */
	private KNXNetworkLink networkLink;
	private Thread autoConnect;
	private NetworkLinkListener autoConnectListener;
	
	/** */
	private ProcessCommunicator communicator;
	
	/** */
	private List<ConnectionListener> listeners;

	/**
	 * 
	 */
	public KNXCommander(KNXConnectionSettings connectionSettings) {
		this.connectionSettings = connectionSettings;
		this.listeners = new ArrayList<ConnectionListener>();
	}

	/**
	 * 
	 */
	public void connect() {
		
		if (! isConnected()) {
			try {
				networkLink = KNXConnection.createConnection(connectionSettings, false);
				networkLink.addLinkListener(createListenerAutoConnect());
				networkLink.addLinkListener(createListenerOnClose());
				
				communicator = new ProcessCommunicatorImpl(networkLink);
				
				for (ConnectionListener listener : listeners) {
					listener.onConnect(ConnectionStatusEnum.Connected);
				}
			}
			catch (KNXException e) {
				log.severe(Session.Server, "Unable to connect the commander to the KNX network.");
				e.printStackTrace();
			}
		}
	}
	
	private NetworkLinkListener createListenerAutoConnect() {
		
		final KNXCommander commander = this;
		final Thread autoConnect = new Thread("KNXnet/IP auto-connect thread") {
			
			@Override
			public void run() {
				
				while (commander.isConnected() == false) {
					
					try {
						sleep(30 * 1000); // 30 seconds
						commander.connect();
					}
					catch (InterruptedException e) {
					}
				}
			}
		};
		this.autoConnect = autoConnect;
		
		autoConnectListener = new NetworkLinkListener() {
			
			@Override
			public void linkClosed(CloseEvent e) {
				e.toString();
				autoConnect.start();
			}
			
			@Override
			public void indication(FrameEvent e) {
			}
			
			@Override
			public void confirmation(FrameEvent e) {
			}
		};
		
		return autoConnectListener;
	}
	
	private NetworkLinkListener createListenerOnClose() {
		return new NetworkLinkListener() {
			
			@Override
			public void linkClosed(CloseEvent e) {
				for (ConnectionListener listener : listeners) {
					listener.onClose();
				}
			}
			
			@Override
			public void indication(FrameEvent e) {
			}
			
			@Override
			public void confirmation(FrameEvent e) {
			}
		};
	}

	/**
	 * 
	 */
	public void disconnect() {
		
		if (autoConnect.isAlive()) { // To stop trying to reconnect if the connection has been lost
			autoConnect.interrupt();
		}
		networkLink.removeLinkListener(autoConnectListener);
		
		if (communicator != null) {
			communicator.detach();
		}
		networkLink.close();
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isConnected() {
		return networkLink != null && networkLink.isOpen();
	}
	
	@Override
	public void accept(Command command) {
		
		if (isConnected()) {
			
			for (What what : command.getWhatList()) {
				try {
					
					DPT datapointType = KNXTranslator.getDataPointType(command.getWho(), what);
					if (datapointType == null) {
						continue; // What is not supported by this command
					}
					
					GroupAddress targetAddress = getGroupAddress(command.getWhere(), what);
					if (targetAddress == null) {
						continue; // What is not available in the controller
					}
					
					if (command.isQueryCommand()) {
						Datapoint datapoint = new StateDP(targetAddress, "", 0, datapointType.getID());
						communicator.read(datapoint);
					}
					else if (command.isWriteCommand()) {
						Datapoint datapoint = new CommandDP(targetAddress, "", 0, datapointType.getID());
						String stateValue = KNXTranslator.translateToKNX(command.getWho(), what);
						communicator.write(datapoint, stateValue);
					}
					else {
						// TODO Invalid command
					}
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					log.warning(Session.Command, "Unable to read/write command "+ command.toString());
					e.printStackTrace();
				}
			}
		}
		else {
			log.warning(Session.Command, "KNX commander is not connected, unable to send command "+ command.toString());
		}
	}
	
	/**
	 * 
	 * @param listener
	 */
	public void addConnectionListener(ConnectionListener listener) {
		synchronized (listeners) {
			listeners.add(listener);
		}
	}

	/**
	 * 
	 * @param listener
	 */
	public void removeConnectionListener(ConnectionListener listener) {
		synchronized (listeners) {
			listeners.remove(listener);
		}
	}
	
	/**
	 * 
	 * @param command
	 * @param what
	 * @return
	 */
	private GroupAddress getGroupAddress(Where where, What what) {
		
		StringTokenizer st = new StringTokenizer(where.getURI().getQuery(), "&");
		while (st.hasMoreElements()) {
			
			String token = st.nextToken();
			String[] values = token.split("=");
			if (what.getName().equals(values[0])) {
				try {
					return new GroupAddress(values[1].replace('-', '/'));
				}
				catch (KNXFormatException e) {
					// TODO Remonter l'erreur sur l'adresse
					log.warning(Session.Command, "Invalid command address "+ where.toString());
					e.printStackTrace();
				}
			}
		}
		return null;
	}
}
