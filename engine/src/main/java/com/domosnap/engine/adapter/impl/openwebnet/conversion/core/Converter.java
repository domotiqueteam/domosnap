package com.domosnap.engine.adapter.impl.openwebnet.conversion.core;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.List;
import java.util.Map;

import com.domosnap.engine.adapter.core.UnSupportedStateException;
import com.domosnap.engine.adapter.core.UnknownStateException;
import com.domosnap.engine.adapter.core.UnknownStateValueException;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.dimension.DimensionStatus;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.dimension.DimensionValue;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.what.State;
import com.domosnap.engine.controller.what.What;

public interface Converter {

	public List<String> convert(What what, Map<String, State<?>> controllerStateList) throws UnknownStateException, UnSupportedStateException, UnknownStateValueException;
	public List<DimensionStatus<?>> convertDimension(What what, Map<String, State<?>> controllerStateList) throws UnknownStateException, UnSupportedStateException, UnknownStateValueException;
	public List<What> convert(String code, Map<String, State<?>> controllerStateList) throws UnknownStateException, UnSupportedStateException, UnknownStateValueException;
	public List<What>  convertDimension(String dimension, List<DimensionValue> dimensionValueList,  Map<String, State<?>> controllerStateList) throws UnknownStateException, UnSupportedStateException, UnknownStateValueException;
	public boolean isDimension(What what);
	public String getOpenWebWho();
	public Class<? extends Controller> getHomeSnapWho();
}
