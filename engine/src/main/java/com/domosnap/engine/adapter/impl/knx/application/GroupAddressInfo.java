package com.domosnap.engine.adapter.impl.knx.application;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.domosnap.engine.adapter.impl.knx.application.xml.ComObjectInstanceRefElement;
import com.domosnap.engine.adapter.impl.knx.application.xml.DeviceInstanceElement;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import tuwien.auto.calimero.GroupAddress;
import tuwien.auto.calimero.dptxlator.DPT;

public class GroupAddressInfo {

	private String name;
	
	private GroupAddress groupAddress;
	
	private DPT dpt;
	
	private Map<ComObjectInstanceRefElement, DeviceInstanceElement> comObjectInfos;
	
	protected GroupAddressInfo(String name, GroupAddress groupAddress, DPT dpt) {
		this.name = name;
		this.groupAddress = groupAddress;
		this.dpt = dpt;
		this.comObjectInfos = new HashMap<ComObjectInstanceRefElement, DeviceInstanceElement>();
	}

	public String getName() {
		return name;
	}

	public GroupAddress getGroupAddress() {
		return groupAddress;
	}

	public DPT getDpt() {
		return dpt;
	}
	
	public Map<ComObjectInstanceRefElement, DeviceInstanceElement> getComObjectInfos() {
		return Collections.unmodifiableMap(comObjectInfos);
	}

	void addComObjectInfos(ComObjectInstanceRefElement comObjectRef, DeviceInstanceElement deviceInstance) {
		if (comObjectInfos.containsKey(comObjectRef)) {
			System.out.println("ERROR");
		}
		comObjectInfos.put(comObjectRef, deviceInstance);
	}

	@Override
	public String toString() {
		return new StringBuilder()
				.append(groupAddress.toString()).append(" - ")
				.append(name)
				.toString();
	}
}
