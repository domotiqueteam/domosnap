package com.domosnap.engine.adapter.impl.knx;

import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.light.Light;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.controller.light.LightStateName;
import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.controller.what.impl.OnOffState;

import tuwien.auto.calimero.dptxlator.DPT;
import tuwien.auto.calimero.dptxlator.DPTXlatorBoolean;

/**
 * 
 * @author DRIESBACH Olivier
 * @version 1.0
 * @since 1.0
 */
public class KNXTranslate {

	/**
	 * 
	 * @param who
	 * @param value
	 * @return
	 */
	public static DPT getStateType(Class<? extends Controller> who) {
		
		DPT result = null;
		if (who.getName().equals(Light.class.getName())) {
			result = DPTXlatorBoolean.DPT_SWITCH;
		}
		return result;
	}

	/**
	 * 
	 * @param who
	 * @param stateValue
	 * @return
	 */
	public static What convertKNXValue(Class<? extends Controller> who, What what, String stateValue) {
		
		What result = null;
		if (Light.class.equals(who)) {
			if (LightStateName.status.name().equals(what.getName())) {
				result = new What(what.getName(), DPTXlatorBoolean.DPT_SWITCH.getLowerValue().equals(stateValue) ? OnOffState.Off() : OnOffState.On());
			}				
		}
		return result;
	}
	
	/**
	 * 
	 * @param who
	 * @param stateValue
	 * @return
	 */
	public static String convertDomoSnapValue(Class<? extends Controller> who, What what) {
		
		String result = null;
		if (Light.class.getName().equals(who.getName())) {
			if (LightStateName.status.name().equals(what.getName())) {
				OnOffState state = (OnOffState) what.getValue();
				result = state.getValue() ? DPTXlatorBoolean.DPT_SWITCH.getUpperValue() : DPTXlatorBoolean.DPT_SWITCH.getLowerValue();
			}	
		}
		return result;
	}
}
