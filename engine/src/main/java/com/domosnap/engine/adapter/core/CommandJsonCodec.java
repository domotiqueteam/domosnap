package com.domosnap.engine.adapter.core;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import com.domosnap.engine.adapter.core.Command.Type;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.controller.what.WhatJsonCodec;
import com.domosnap.engine.controller.where.Where;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class CommandJsonCodec {

	public static String toJSon(Command command) {
		// TODO optimise
		StringBuilder result = new StringBuilder("{")
				// Add Timestamp
				.append("\"timestamp\":\"").append(String.valueOf(command.timestamp))
				
				// Add List of what
				.append("\",\"what\":[");
				for (What what : command.getWhatList()) {
					result = result.append(WhatJsonCodec.toJSon(what)).append(",");
				}		
				if (command.getWhatList().size() > 0) {
					result.setLength(result.length()-1);
				}
				
				// Add Where
				return result.append("],\"where\":\"").append(command.getWhere().getUri())
				
				// Add Who
						.append("\",\"who\":\"").append(command.getWho().getName())
				// Add Type
						.append("\",\"type\":\"").append(command.type.name())
				// Add Source
				//		.concat(",\"source\":").concat(this.source == null ? "" : String.valueOf(this.source))
						.append("\"}")
				.toString();
	}
	
	@SuppressWarnings("unchecked")
	public static Command fromJson(String json) {
		JsonParser jp = new JsonParser();
		JsonObject je = jp.parse(json).getAsJsonObject();
		
		Class<? extends Controller> who = null;
		List<What> whats = new ArrayList<>();
		Where where;
		Type type;
		
		// Add List of what
		JsonArray ja = je.getAsJsonArray("what");
		for (JsonElement jsonElement : ja) {
			whats.add(WhatJsonCodec.fromJson(jsonElement.toString()));
		}
		
		// Add Where
		where = new Where(je.get("where").getAsString());
	
		// Add Who
		try {
			who = (Class<? extends Controller>) Class.forName(je.get("who").getAsString());
		} catch (ClassNotFoundException e) {
			// TODO LOG!!! => unsupported device!!!
			e.printStackTrace();
		}

		// Add Type
		type = Type.valueOf(je.get("type").getAsString());
		
		Command c = new Command(who, whats, where, type, null);
		c.timestamp = je.get("timestamp").getAsLong();
		
		return c;
	}
}
