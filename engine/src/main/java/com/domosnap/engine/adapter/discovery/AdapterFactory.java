package com.domosnap.engine.adapter.discovery;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import com.domosnap.engine.adapter.ControllerAdapter;
import com.domosnap.engine.adapter.impl.openwebnet.OpenWebAdapterDiscovery;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


/**
 * {@link AdapterFactory} implements features to build or/and find {@link ControllerAdapter}. 
 */
public class AdapterFactory {
	
	private static Map<String, AdapterDiscovery> discoverList = new HashMap<>();
		
	static {
		registerDiscover("openwebnet", new OpenWebAdapterDiscovery());
//		registerDiscover("hue", HueControllerAdapter.class);
//		registerProtocole("knx", KNXControllerService.class);
	}
	
	/**
	 * Register a new protocol.
	 * @param schema Protocol name.
	 * @param controllerAdapter Class of the ControllerAdapter associated to the protocol
	 */
	public static void registerDiscover(String schema, AdapterDiscovery controllerAdapterDiscovery) {
		AdapterFactory.discoverList.put(schema, controllerAdapterDiscovery);
	}

	/**
	 * Unregister a protocol.
	 * @param schema protocol name.
	 */
	public static void unRegisterDiscover(String schema) {
		AdapterFactory.discoverList.remove(schema);
	}

	/**
	 * Return the list of supported protocol. Supported protocol are all register protocole.
	 * @return the list of supported protocol.
	 */
	public static Set<String> getSupportedProtocole() {
		return discoverList.keySet();
	}
	
	
	public static void scanGateway(ScanGatewayListener listener) {
		discoverList.values().iterator().forEachRemaining(new Consumer<AdapterDiscovery>() {

			@Override
			public void accept(AdapterDiscovery t) {
				t.scanGateway(listener);
			}
			
		});
	}
}
