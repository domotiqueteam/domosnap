package com.domosnap.engine.adapter.impl.openwebnet.conversion.gateway.dimension;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.dimension.DimensionStatusImpl;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.dimension.DimensionValue;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.dimension.DimensionValueImpl;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.gateway.GatewayStatusConverter.GatewayDimension;
import com.domosnap.engine.controller.what.impl.VersionState;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


public class KernelVersion extends DimensionStatusImpl<VersionState> {
	
	private int VERSION_POS = 0;
	private int RELEASE_POS = 1;
	private int BUILD_POS = 2;
	
	public KernelVersion() {
		super(new DimensionValue[] { 
				new DimensionValueImpl(), // Version
				new DimensionValueImpl(), // Release
				new DimensionValueImpl()  // Build
				},
			GatewayDimension.KERNEL_VERSION.getCode()
		);
	}

	@Override
	public VersionState getStateValue() {
		int version = getIntValue(VERSION_POS);
		int release = getIntValue(RELEASE_POS);
		int build = getIntValue(BUILD_POS);

		VersionState ver = new VersionState(version, release, build);

		return ver;
	}

	@Override
	public void setStateValue(VersionState value) {
		new Log(KernelVersion.class.getSimpleName()).severe(Session.Other, "Try to modify KernelVersion state: but it is read-only...");
	}

	public void setDistributionVersion(int version, int release, int build) {
		setIntValue(version, VERSION_POS, 2);
		setIntValue(release, RELEASE_POS, 2);
		setIntValue(build, BUILD_POS, 2);
	}
}
