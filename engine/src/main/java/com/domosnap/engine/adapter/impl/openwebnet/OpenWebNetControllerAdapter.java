package com.domosnap.engine.adapter.impl.openwebnet;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.reactivestreams.Subscription;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.core.AbstractControllerAdapter;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.ConnectionListener;
import com.domosnap.engine.adapter.core.ScanListener;
import com.domosnap.engine.adapter.core.UnknownControllerListener;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.CommandToOpenWebNetFunction;
import com.domosnap.engine.adapter.impl.openwebnet.conversion.core.OpenWebNetToCommandFunction;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.gateway.Gateway;
import com.domosnap.engine.controller.heating.HeatingZone;
import com.domosnap.engine.controller.light.Light;
import com.domosnap.engine.controller.shutter.Shutter;
import com.domosnap.engine.controller.temperature.TemperatureSensor;
import com.domosnap.engine.controller.where.URITools;
import com.domosnap.engine.controller.where.Where;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableSubscriber;
import io.reactivex.flowables.ConnectableFlowable;
import io.reactivex.processors.FlowableProcessor;
import io.reactivex.processors.PublishProcessor;


public class OpenWebNetControllerAdapter extends AbstractControllerAdapter {

	public static String SCHEMA = "openwebnet";
	
	private static Log log = new Log(OpenWebNetControllerAdapter.class.getSimpleName());

	private OpenWebMonitorPublisher monitor;
	private OpenWebNetToCommandFunction openWebNetConvertToCommandFunction;
	private OpenWebCommanderConsumer commander;

	public OpenWebNetControllerAdapter() {
		super(SCHEMA + "://:12345@localhost:20000");
	}

	public OpenWebNetControllerAdapter(String uri) {
		super(uri);
	}
	
	/**
	 * Constructor.
	 * @param host the hostname or ip of My Open Server
	 * @param port the port number of My Open Server
	 * @param password the passwod of My Open Server (null if not)
	 */
	@Deprecated
	public OpenWebNetControllerAdapter(String host, int port, Integer passwordOpen) {
		super(passwordOpen == null ? SCHEMA + "://"+ host + ":" + port : SCHEMA + "://"+ passwordOpen + "@" + host + ":" + port);
	}

	@Override
	protected Flowable<Command> createMonitorStream() {
		
		this.monitor = new OpenWebMonitorPublisher(getIp(), getPort(), getPassword());
		openWebNetConvertToCommandFunction = new OpenWebNetToCommandFunction(getIp(), getPort(), getPassword());
		ConnectableFlowable<Command> monitorStream = PublishProcessor.create(this.monitor, BackpressureStrategy.DROP).flatMap(openWebNetConvertToCommandFunction, true, Integer.MAX_VALUE, Integer.MAX_VALUE).share().publish();
		monitorStream.connect();
	
		return monitorStream;
	}
	
	protected PublishProcessor<Command> createCommandStream() {
		
		PublishProcessor<Command> commanderStream = PublishProcessor.create();
			commanderStream.subscribe(new FlowableSubscriber<Command>() {

				@Override
				public void onNext(Command arg0) {
	//					if (log.finest) {
	//						log.fine(arg0.getWho().toString().toLowerCase(), log.getFormattedLog(Session.Command, Thread.currentThread().getName(), 0, "Event send [".concat(arg0.toJSon()).concat("]")));
	//					}
				}
	
				@Override
				public void onError(Throwable arg0) {
	//					System.out.println("Error");
	//					arg0.printStackTrace();
				}
	
				@Override
				public void onComplete() {
	//						System.out.println("Finish");
				}
				@Override
				public void onSubscribe(Subscription s) {
					s.request(Long.MAX_VALUE); // To avoid backpressure...
				}
		});

		Flowable<String> openwebcommander = commanderStream.flatMap(new CommandToOpenWebNetFunction(), true, Integer.MAX_VALUE, Integer.MAX_VALUE);
		this.commander = getCommanderConsumer();
		openwebcommander.subscribe(this.commander);	
	
		return commanderStream;
	}
	
	private OpenWebCommanderConsumer getCommanderConsumer() {
		if (commander == null) {
			commander = new OpenWebCommanderConsumer(getIp(), getPort(), getPassword());
		}
		return commander;
	}


	@Override
	public void disconnect() {
		if (monitor != null) {
			monitor.disconnect();
		}
		commander.disconnect();
	}

	/**
	 * Return true if all commander and monitor are connected (else false)
	 * @return true if all commander and monitor are connected (else false)
	 */
	@Override
	public boolean isConnected() {
		return monitor.isConnected() && commander.isConnected();
	}

	@Override
	public void addCommanderConnectionListener(ConnectionListener listener) {
		getCommanderConsumer().addConnectionListener(listener);
	}

	@Override
	public void removeCommanderConnectionListener(ConnectionListener listener) {
		if (commander != null) {
			commander.removeConnectionListener(listener);
		}
	}
	
	@Override
	public void addMonitorConnectionListener(ConnectionListener listener) {
		monitor.addConnectionListener(listener);
	}

	@Override
	public void removeMonitorConnectionListener(ConnectionListener listener) {
		if (monitor != null) {
			monitor.removeConnectionListener(listener);
		}
	}

	@Override
	public void addUnknowControllerListener(UnknownControllerListener unknownControllerListener) {
		openWebNetConvertToCommandFunction.addUnknownControllerListener(unknownControllerListener);
	}
	
	@Override
	public void removeUnknowControllerListener(UnknownControllerListener unknownControllerListener) {
		if (openWebNetConvertToCommandFunction != null) {
			openWebNetConvertToCommandFunction.removeUnknownControllerListener(unknownControllerListener);
		}
	}

	@Override
	public void connect() {
		if (!commander.isConnected()) {
			commander.connect();
		}
		if (!monitor.isConnected()) {
			monitor.connect();
		}
	}

	@Override
	public void connect(String uri) {
		// TODO factoriser avec le code qui set l'uri avec celui des constructeurs
		if (uri.indexOf("://") == -1) {
			uri = SCHEMA + "://" + uri;
		}
		this.uri = URI.create(uri);
		
		String ip = getIp();
		int port = getPort();
		Integer password = getPassword();
		
		if (commander != null) {
			commander.setIp(ip);
			commander.setPort(port);
			commander.setPasswordOpen(password);
		}
		if (monitor != null) {
			monitor.setIp(ip);
			monitor.setPort(port);
			monitor.setPasswordOpen(password);
			openWebNetConvertToCommandFunction.setIp(ip);
			openWebNetConvertToCommandFunction.setPort(port);
			openWebNetConvertToCommandFunction.setPassword(port);
		}
		
		connect();
	}
	
	@Override
	protected void onCreatedController(Controller c) {
		openWebNetConvertToCommandFunction.addKnownController(c);
	}

	@Override
	protected String formatURI(Where where) {
		String path;
		if (where == null)
			path = "";
		else 
			path = where.getPath();
			
		return URITools.generateURI(SCHEMA, getPassword() == null ? null : String.valueOf(getPassword()), getIp() == null ? "localhost" : getIp(), getPort(), path);
	}

	@Override
	public String getSchema() {
		return SCHEMA;
	}
	
	protected String getIp() {
		return uri.getHost();
	}
	
	protected int getPort() {
		return uri.getPort();
	}
	
	protected Integer getPassword() {
		String passwordStr = uri.getUserInfo();
		if (passwordStr == null) {
			passwordStr = "0";
		}
		if (passwordStr.contains(":")) {
			passwordStr = passwordStr.substring(passwordStr.indexOf(":")+1);
		}
		return Integer.parseInt(passwordStr);
	}
	
	
	private OpenWebCommanderConsumer commanderScan;
	@SuppressWarnings("unused")
	private Flowable<Command> flowMonitorScan;

	private Flowable<Command> getOpenWebMonitorScan(final ScanListener listener) {
	
		// Start a new monitor since the old one has all controller to monitor registered.
		final OpenWebMonitorPublisher monitorScan = new OpenWebMonitorPublisher(getIp(), getPort(), getPassword());
		monitorScan.connect();

		// Convert openwebnet string to a command.
		final OpenWebNetToCommandFunction convertFunctionScan = new OpenWebNetToCommandFunction(getIp(), getPort(), getPassword());;

		// During convertion detect unknowControllers and create them
		convertFunctionScan.addUnknownControllerListener(
				new UnknownControllerListener() {
					
					@Override
					public void foundUnknownController(Command command) {
						Controller c = null;
						if (Light.class.equals(command.getWho())) {
							log.finest(Session.Monitor, monitorScan.getFormattedLog(0, "Instanciate controller." + command.getWhere().getPath()));
							c = createController(Light.class, command.getWhere()); // TODO ici on pourrait optimiser: g la command qui a déclencher le unknown => j'ai les status pas la peine de les redemander...
							convertFunctionScan.addKnownController(c);
						}
						if (Shutter.class.equals(command.getWho())) {
							c = createController(Shutter.class, command.getWhere());
							convertFunctionScan.addKnownController(c);
						}
						
						if (HeatingZone.class.equals(command.getWho())) {
							c = createController(HeatingZone.class, command.getWhere());
							convertFunctionScan.addKnownController(c);
						}
						if (c != null) {
							listener.foundController(c);
						}
					}
				}
		);

		ConnectableFlowable<Command> result = FlowableProcessor.create(monitorScan, BackpressureStrategy.DROP).flatMap(convertFunctionScan, true, Integer.MAX_VALUE, Integer.MAX_VALUE).publish();

		result.connect();
		
		log.finest(Session.Monitor, monitorScan.getFormattedLog(0, "MonitorScan created."));
		return result;
	}

	private OpenWebCommanderConsumer getOpenWebCommanderScan() {

		// Consume string and send to gateway
		commanderScan = new OpenWebCommanderConsumer(getIp(), getPort(), getPassword());
		commanderScan.connect();
		
		log.finest(Session.Command, commanderScan.getFormattedLog(0, "CommanderScan created."));
		
		return commander;
	}

	@Override
	public void scan(ScanListener listener) {
		flowMonitorScan = getOpenWebMonitorScan(listener); // To be sure that monitor scan is instanciate...
		final OpenWebCommanderConsumer commander = getOpenWebCommanderScan();


		
		for (int i= 1; i < 10; i++) {
			synchronized (listener) {
					listener.progess(i*10);
				try {
					listener.wait(1000);
				} catch (InterruptedException e) {
					log.finest(Session.Command, commanderScan.getFormattedLog(0, "Error during waiting scanner."));
				}
			}

			// command i
			String commandLight = "*#1*"+i+"##";
			try {
				commander.accept(commandLight);
			} catch (Exception e) {
				log.severe(Session.Command, e.getMessage());
			}
			
			// command i
			String commandAutomation = "*#2*"+i+"##";
			try {
				commander.accept(commandAutomation);
			} catch (Exception e) {
				log.severe(Session.Command, e.getMessage());
			}
			
			for (int j = 0; j < 10; j++) {
				// command i
				String commandHeating = "*#4*"+(((i-1)*10) + j) +"*0##";
				try {
					commander.accept(commandHeating);
				} catch (Exception e) {
					log.severe(Session.Command, e.getMessage());
				}
			}
		}

		synchronized (listener) {
			listener.progess(100);
			listener.scanFinished();
		}
	}

	@Override
	public List<Class<? extends Controller>> getSupportedDevice() {
		List<Class<? extends Controller>> result = new ArrayList<Class<? extends Controller>>();
		result.add(TemperatureSensor.class);
		result.add(Light.class);
		result.add(Shutter.class);
		result.add(Gateway.class);
		return result;
	}
}
