package com.domosnap.engine.adapter.impl.knx.application.xml;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


public class HardwareElement extends XMLElement {

	private boolean coupler;
	private boolean powerLineRepeater;
	private boolean powerLineSignalFilter;
	private boolean powerSupply;
	
	private ProductElement product;
	private HardwareProgramElement program;
	
	/**
	 * 
	 * @param id
	 * @param name
	 */
	public HardwareElement(String id, String name, boolean coupler, boolean powerLineRepeater, boolean powerLineSignalFilter, boolean powerSupply) {
		super(id, name);
		this.coupler = coupler;
		this.powerSupply = powerSupply;
		this.powerLineRepeater = powerLineRepeater;
		this.powerLineSignalFilter = powerLineSignalFilter;
	}
	
	public boolean isCoupler() {
		return coupler;
	}

	public boolean isPowerLineRepeater() {
		return powerLineRepeater;
	}

	public boolean isPowerLineSignalFilter() {
		return powerLineSignalFilter;
	}
	
	public boolean isPowerSupply() {
		return powerSupply;
	}
	
	public ProductElement getProduct() {
		return product;
	}
	
	public HardwareProgramElement getProgram() {
		return program;
	}
	
	@Override
	public String toString() {
		return new StringBuilder("<Hardware Id=\"").append(getId())
				.append("\" Name=\"").append(getName())
				.append("\" IsCoupler=\"").append(coupler)
				.append("\" IsPowerLineRepeater=\"").append(powerLineRepeater)
				.append("\" IsPowerLineSignalFilter=\"").append(powerLineSignalFilter)
				.append("\" IsPowerSupply=\"").append(powerSupply)
				.append("\" />").toString();
	}
}
