package com.domosnap.engine.controller.what.impl;


/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

public class PercentageState extends MinMaxState<Double> {
	
	public static final double MIN = 0;
	public static final double MAX = 100;
	
	private double value;

	public PercentageState(String percent) {
		super(MIN, MAX);
		fromString(percent);
	}

	
	public PercentageState(Double percent) {
		super(MIN, MAX);
		setValue(percent);
	}
	
	public Double getValue() {
		return this.value;
	}

	public void setValue(Double value) {
		try {
			if (MIN <= value && value <= MAX) {
				this.value = value;
			} else {
				throwInvalidValueException(String.valueOf(value));
			}
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Value "+ value +" is not a valid number.");
		}
	}

	@Override
	public String toString() {
		return String.valueOf(value) +" %";
	}

	@Override
	public void fromString(String value) {
		this.value = Double.parseDouble(value.substring(0, value.length()-2)); // remove the " %"
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		long temp;
		temp = Double.doubleToLongBits(value);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PercentageState other = (PercentageState) obj;
		if (Double.doubleToLongBits(value) != Double.doubleToLongBits(other.value))
			return false;
		return true;
	}
}
