package com.domosnap.engine.controller.what.impl;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.controller.what.State;



public class TemperatureUnitState implements State<TemperatureUnitState.TemperatureUnit> {

	public enum TemperatureUnit {Celsius, Fahreinheit, Kelvin, Reaumur, Planck};	
	
	private TemperatureUnit value;
	
	public TemperatureUnitState(String value) {
		fromString(value);
	}
	
	private TemperatureUnitState(TemperatureUnit value) {
		this.value = value;
	}

	public TemperatureUnit getValue() {
		return value;
	}

	@Override
	public void setValue(TemperatureUnit value) {
		this.value = value;
	}

	@Override
	public void fromString(String value) {
		this.value = TemperatureUnit.valueOf(value);
	}
	
	@Override
	public String toString() {
		return value.name(); 
	}
	
	public static TemperatureUnitState Celsius() {
		// I don't use a static singleton since serialization/deserialization
		// toString/fromString override the value
		return new TemperatureUnitState(TemperatureUnit.Celsius); 
	}
	
	public static TemperatureUnitState Fahreinheit() {
		// I don't use a static singleton since serialization/deserialization
		// toString/fromString override the value
		return new TemperatureUnitState(TemperatureUnit.Fahreinheit);
	}

	public static TemperatureUnitState Kelvin() {
		// I don't use a static singleton since serialization/deserialization
		// toString/fromString override the value
		return new TemperatureUnitState(TemperatureUnit.Kelvin);
	}
	
	public static TemperatureUnitState Reaumur() {
		// I don't use a static singleton since serialization/deserialization
		// toString/fromString override the value
		return new TemperatureUnitState(TemperatureUnit.Reaumur);
	}
	
	public static TemperatureUnitState Planck() {
		// I don't use a static singleton since serialization/deserialization
		// toString/fromString override the value
		return new TemperatureUnitState(TemperatureUnit.Planck);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TemperatureUnitState other = (TemperatureUnitState) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

}
