package com.domosnap.engine.controller.heating;


/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import com.domosnap.engine.controller.heating.stateValue.HeatingZoneState;
import com.domosnap.engine.controller.heating.stateValue.OffsetState;
import com.domosnap.engine.controller.temperature.TemperatureSensor;
import com.domosnap.engine.controller.what.impl.DoubleState;
import com.domosnap.engine.controller.what.impl.TemperatureUnitState;

public class HeatingZone extends TemperatureSensor { // TODO manage the hierarchy!!! 

	/** uuid */
	private static final long serialVersionUID = 1L;

	public HeatingZoneState getStatus() {
		return (HeatingZoneState) get(HeatingZoneStateName.status.name());
	}

	@Override
	public DoubleState getTemperature() {
		return getMeasureTemperature();
	}

	@Override
	public TemperatureUnitState getUnit() {
		return TemperatureUnitState.Celsius();
	}

	@Override
	public void setUnit(TemperatureUnitState value) {
		new UnsupportedOperationException();
	}

	public void setStatus(HeatingZoneState value) {
		set(HeatingZoneStateName.status.name(), value);
	}

	public DoubleState getDesiredTemperature() {
		return (DoubleState) get(HeatingZoneStateName.set_temperature.name());
	}

	public void setOffset(OffsetState offset) {
		set(HeatingZoneStateName.local_offset.name(), offset);
	}

	public OffsetState getOffset() {
		return (OffsetState) get(HeatingZoneStateName.local_offset.name());
	}

	public DoubleState getMeasureTemperature() {
		return (DoubleState) get(HeatingZoneStateName.measure_temperature.name());
	}

	@Override
	public List<String> getStateList() {
		List<String> result = new ArrayList<String>();
		HeatingZoneStateName[] list = HeatingZoneStateName.values();
		for (int i = 0; i < list.length; i++) {
			result.add(list[i].name());
		};
		return result;
	}
}
