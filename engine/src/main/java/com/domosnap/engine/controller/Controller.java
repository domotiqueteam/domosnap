package com.domosnap.engine.controller;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.Command.Type;
import com.domosnap.engine.controller.what.State;
import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.controller.where.Where;
import com.domosnap.engine.house.Group;
import com.domosnap.engine.house.Label;

import io.reactivex.Flowable;
import io.reactivex.functions.Consumer;
import io.reactivex.processors.PublishProcessor;

/**
 * Controller is the abstract class for all device supporter by SnapHome.
 * Each sub class represent a device. Controller contains the main logic to manage status.
 *
 */
public abstract class Controller implements Serializable {

	/** serial uid */
	private static final long serialVersionUID = 1L;
	
	private Log log = new Log(this.getClass().getSimpleName());
	
	// Model
	private String title; // Title of the controller
	private String description; // Description of the controller
	private LabelList labelList = new LabelList(this); // List of label where the controller appears
	private Group group; // Group containing this controller
	protected Where where; // Address of the controller
	protected Map<String, State<?>> whatList = new HashMap<String, State<?>>(); //List of all state names with their current values of the controller. This map represents the complete status of the controller.

	// Stream
	protected transient PublishProcessor<Command> server; // Stream to send command
	protected transient Flowable<Command> monitor; // Stream to received command
	
	// Listener
	private List<ControllerChangeListener> controllerChangeListenerList = Collections.synchronizedList(new ArrayList<ControllerChangeListener>()); // list of listener
	private List<Consumer<Command>> commandListenerList = Collections.synchronizedList(new ArrayList<>());
	
	public void addCommander(PublishProcessor<Command> commander) {
		this.server = commander;
	}
	
	private final Class<? extends Controller> myclass = this.getClass();
	
	public void addMonitor(Flowable<Command> monitor) {
		this.monitor = monitor;
		this.monitor.subscribe(new Consumer<Command>() {
			@Override
			public void accept(Command command) throws Exception {
				final Where w = command.getWhere();
				if (myclass.equals(command.getWho()) && (w !=null && w.getPath() != null && w.getPath().equals(getWhere().getPath()))) {
					notifyStateChange(command);
				}
			}
		});
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String name) {
		this.title = name;
	}

	/**
	 * Return the description of the controller
	 * @return description of the controller
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Define the description of the controller.
	 * @param description of the controller
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Return the unique id of the controller
	 * @return unique id of the controller
	 */
	public String getId() {
		return getWhere().getUri();
	}

	/**
	 * Return the list of label associated to this component.
	 * @return list of label.
	 */
	public LabelList getLabels() {
		return labelList;
	}

	/**
	 * Return the group associated to this component.
	 * @return the group associated to this component.
	 */
	public Group getGroup() {
		return group;
	}

	/**
	 * Defines the group associated to this component.
	 * <br/><b>Warning</b> Set null here is like to delete physically 
	 * the controller = we remove it from label
	 * @param group the group associated to this component.
	 */
	public void setGroup(Group group) {
		if (group == null){
			// Set group to null is as delete it: remove from label.
			for (Label label : getLabels()) {
				label.remove(this);
			}
		}
		this.group = group;
	}

	/**
	 * Returns the address of the targeted device
	 * @return the address of the targeted device
	 */
	public Where getWhere() {
		return where;
	}

	/**
	 * Defines the address of the targeted device to control
	 * @param newValue the address of the targeted device to control
	 */
	public void setWhere(Where newValue) {
		if ((where == null && newValue != null) 
				|| (where != null && newValue == null) 
				|| ((where != null && newValue != null) && (where.getUri() != newValue.getUri())))
		{
			// Optimization: when Where is the same = don't need to recall all status....
			this.where = newValue;
			requestAllStatus();
		}
	}
	
	/**
	 * Clear the cache list of status and request them all.
	 * <b>Carreful:</b> request are asynchronously done so even
	 * if the method return, during some millisecond all
	 * status will be not available. 
	 */
	public void requestAllStatus() {
		this.whatList.clear();
		// If we not set a null value (setWhere(null) => re-init the controller), we request all status
		if (where != null) {
			if (server == null) {
				log.info(Session.Command, "Impossible to requestAllStatus since commander stream is null. Maybe your build your onw Controller and forget to provide a Commander stream before set where? Else there is a probleme with the ControllerService builder.");
				return;
			}
			
			List<What> whatList = new ArrayList<What>();
			for (String state : getStateList()) {
				whatList.add(new What(state, null));
			}
			
			server.offer(new Command(this.getClass(), whatList, where, Type.QUERY, this));	
		}
	}

	public abstract List<String> getStateList();

	public Map<String, State<?>> getStateMap() {
		return whatList;
	}

	/**
	 * If current value of a state name is known, provide it directly 
	 * without request the server. 
	 * 
	 * @param name The state name
	 * @return The current value of a state name or null if a request to the server
	 * is done (when current value is null).
	 */
	protected State<?> get(final String name) {
		if (name == null) {
			throw new NullPointerException("Could not get null state name.");
		}

		State<?> value = whatList.get(name);
		return value;
	}

	/**
	 * Update a value of the status.
	 * @param name The state name to update
	 * @param state The new value of the state name
	 */
	protected void set(String name, State<?> state) {

		if (state == null || name == null) {
			throw new NullPointerException("Could not set state name [" + name + "," + state.toString()+ "]");
		}

		Command command = new Command(this.getClass(), new What(name, state), where, Type.COMMAND, this);
		pushCommand(command);
	}

	protected void pushCommand(Command command) {
		if (log.finest) {
			log.fine(Session.Command, "Command send to adapter [".concat(command.toString()).concat("]"));
		}
		
		// The command is sent to the gateway. Gateway transmits it to the actuator.
		// If everything is fine, Gateway provides through the monitor session
		// the new status => not need to set it here since it will be set by the
		// monitor way.
		server.offer(command);
	}

	protected void addCommandProvider(Flowable<Command> hook) {
		hook.subscribe(new Consumer<Command>() {
			@Override
			public void accept(Command t) throws Exception {
				pushCommand(t);
			}
		});
	}

	protected void addCommandConsumer(Consumer<Command> hook) {
		this.commandListenerList.add(hook);
	}

	protected void removeCommandConsumer(Consumer<Command> hook) {
		this.commandListenerList.remove(hook);
	}
	
	/**
	 * @param listener
	 *            the new change listener.
	 */
	public void addControllerChangeListener(ControllerChangeListener listener) {
		controllerChangeListenerList.add(listener);
	}

	/**
	 * @param listener
	 *            the change listener to remove.
	 */
	public void removeControllerChangeListener(ControllerChangeListener listener) {
		controllerChangeListenerList.remove(listener);
	}
	
	protected void notifyStateChange(Command command) {
		for (What what : command.getWhatList()) {
			State<?> oldState = whatList.get(what.getName());
			whatList.put(what.getName(), what.getValue());
			if (log.finest) {
				log.fine(Session.Monitor, "Controller [" + getWhere().toString() + "] updated with command [" + command.toString() + "]");
			}
			for (ControllerChangeListener listener : controllerChangeListenerList) {
				listener.onStateChange(this,new What(what.getName(), oldState) , what);
			}
			for (Consumer<Command> listener: commandListenerList) {
				try {
					listener.accept(command);
				} catch (Exception e) {
					if (log.error) {
						log.severe(Session.Monitor, "Error in Consumer<Command> with error: " + e.getMessage());
					}
				}
			}
		}
	}
	
	@Override
	public String toString() {
		return ControllerJsonCodec.toJson(this);
	}
}
