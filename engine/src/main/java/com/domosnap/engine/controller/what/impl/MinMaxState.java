package com.domosnap.engine.controller.what.impl;

import com.domosnap.engine.controller.what.State;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */



/**
 * Class representing an interval of numeric values defined by a minimum and a maximum value.
 * 
 */
public abstract class MinMaxState<T extends Number> implements State<T> {
	
	private T minValue;
	private T maxValue;
	
	/**
	 * 
	 * @param minValue
	 * @param maxValue
	 */
	MinMaxState(T minValue, T maxValue) {
		this.minValue = minValue;
		this.maxValue = maxValue;
	}
	
	/**
	 * 
	 * @return
	 */
	public T getMinValue() {
		return minValue;
	}
	
	/**
	 * 
	 * @return
	 */
	public T getMaxValue() {
		return maxValue;
	}
	
	/**
	 * Throws an {@link IllegalArgumentException} explaning that the value must be between values defined by {@link #getMinValue()} and {@link #getMaxValue()} methods.
	 * @throws IllegalArgumentException if value is lower than the min value or greater than the max value of this type
	 */
	protected void throwInvalidValueException(String value) {
		throw new IllegalArgumentException("Invalid value "+ value +", must be between "+ getMinValue().toString() +" and "+ getMaxValue().toString() +".");
	}

	@Override
	public String toString() {
		return "["+ getMinValue().toString() +".."+ getMaxValue().toString() +"]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((maxValue == null) ? 0 : maxValue.hashCode());
		result = prime * result + ((minValue == null) ? 0 : minValue.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MinMaxState<?> other = (MinMaxState<?>) obj;
		if (maxValue == null) {
			if (other.maxValue != null)
				return false;
		} else if (!maxValue.equals(other.maxValue))
			return false;
		if (minValue == null) {
			if (other.minValue != null)
				return false;
		} else if (!minValue.equals(other.minValue))
			return false;
		return true;
	}
}
