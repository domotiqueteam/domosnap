package com.domosnap.engine.controller.what.impl;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.controller.what.State;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


public class IpAddressState implements State<InetAddress> {

	private InetAddress ipAdress;
	private static Log log = new Log(IpAddressState.class.getName());
	
	public IpAddressState(String address) {
		fromString(address);
	}

	public IpAddressState(InetAddress address) {
		setValue(address);
	}

	@Override
	public InetAddress getValue() {
		return ipAdress;
	}

	@Override
	public void setValue(InetAddress value) {
		this.ipAdress = value;
	}

	@Override
	public void fromString(String value) {
		try {
			ipAdress = Inet4Address.getByName(value);
		} catch (UnknownHostException e) {
			log.severe(Session.Monitor, e.getMessage());
		}
	}
	
	@Override
	public String toString() {
		return ipAdress.getHostAddress();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ipAdress == null) ? 0 : ipAdress.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IpAddressState other = (IpAddressState) obj;
		if (ipAdress == null) {
			if (other.ipAdress != null)
				return false;
		} else if (!ipAdress.equals(other.ipAdress))
			return false;
		return true;
	}
}
