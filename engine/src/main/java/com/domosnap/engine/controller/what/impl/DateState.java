package com.domosnap.engine.controller.what.impl;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.controller.what.State;

public class DateState implements State<Date> {

	private Log log = new Log(DateState.class.getSimpleName());
	public static String DATE_PATTERN = "dd-MM-yyyy";
	private Date value;
	
	public DateState(String value) {
		fromString(value);		
	}
	
	public DateState(Date value) {
		setValue(value);		
	}

	@Override
	public void setValue(Date value) {
		Calendar gc = GregorianCalendar.getInstance();
		gc.setTime(value);
		gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		gc.set(Calendar.MILLISECOND, 0);
		this.value = gc.getTime();
	}

	@Override
	public Date getValue() {
		return value;
	}

	@Override
	public void fromString(String value) {
		try {
			this.value = new SimpleDateFormat(DATE_PATTERN).parse(value);
		} catch (ParseException e) {
			log.fine(Session.Device, e.getMessage());
		}
	}

	@Override
	public String toString() {
		return new SimpleDateFormat(DATE_PATTERN).format(value);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DateState other = (DateState) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

}
