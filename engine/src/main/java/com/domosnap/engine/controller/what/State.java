package com.domosnap.engine.controller.what;

import java.lang.reflect.InvocationTargetException;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * State represents an instance of a status which is define by a key/value pair.
 *
 */
public interface State<T> {
	
	final static Log log = new Log(State.class.getName());
	
	public T getValue();
	
	public void setValue(T value);

	/**
	 * Define the value of the status.
	 * Override it to manage different type.
	 */
	public void fromString(String value);
	
	/**
	 * The value of the status
	 * @return the value of the status
	 */
	public String toString();
	
	public static String toJson(State<?> state) {
		return state.toString();
	}
	
	public static <R extends State<?>> R fromString(Class<R> clazz, String state) {
		try {
			return clazz.getConstructor(String.class).newInstance(state);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException | InstantiationException e) {
			log.severe(Session.Other, e.getMessage());
			return null;
		}
	}
}
