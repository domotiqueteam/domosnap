package com.domosnap.engine.controller.where;

import java.net.URI;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * {@link Where} represents a physical adress of a device. A physical adress is composed of two main thing:
 * - a gateway depending the procole = transform ip world in electric physical world.
 * - a physical device = a light for example.
 * 
 * 
 * So uri is a good standard to described:
 * uri = protocol://user:password@serverip/devicepath[#fragment][?query]
 * 
 */
public class Where {

	private String uri;
	private URI uriObject;
	
	public Where(String uri) {
		this.uri = uri;
		uriObject = URI.create(uri);
	}

	/**
	 * Return the controller address
	 * @return the controller address
	 */
	public String getPath() {
		String path = uriObject.getPath();
		if (path.startsWith("/")) {
			path = path.substring(1);
		}
		return path;
	}
	
	/**
	 * Return the controller full address
	 * @return the controller full address
	 */
	public URI getURI() {
		return uriObject;
	}

	/**
	 * Return the controller protocol used.
	 * @return the device protocol
	 */
	public String getProtocol() {
		return uriObject.getScheme();
	}
		
	/**
	 * Return the global device address (with gateway to use).
	 * @return the full device address
	 */
	public String getUri() {
		return uri;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uri == null) ? 0 : uri.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Where other = (Where) obj;
		if (uri == null) {
			if (other.uri != null)
				return false;
		} else if (!uri.equals(other.uri))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return uri;
	}
}
