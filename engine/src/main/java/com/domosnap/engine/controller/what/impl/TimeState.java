package com.domosnap.engine.controller.what.impl;

import java.text.ParseException;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import com.domosnap.engine.controller.what.State;

public class TimeState implements State<Date>{

	public static final String TIME_PATTERN = "HH:mm:ss";

	private int hours, minutes, seconds;
	private TimeZone tz;

	public TimeState(String value) {
		fromString(value);
	}
	
	public TimeState(int hours, int minutes, int seconds, TimeZone tz) {
		this.hours = hours;
		this.minutes = minutes;
		this.seconds = seconds;
		if (tz == null) {
			this.tz = TimeZone.getDefault();
		} else {
			this.tz = tz;
		}
	}

	public Date getTime() {
		Calendar c = new GregorianCalendar();
		c.set(Calendar.HOUR_OF_DAY, hours);
		c.set(Calendar.MINUTE, minutes);
		c.set(Calendar.SECOND, seconds);
		c.set(Calendar.DAY_OF_MONTH, 0);
		c.set(Calendar.MONTH, 0);
		c.set(Calendar.YEAR, 0);
		c.get(Calendar.HOUR_OF_DAY); // fix bug https://bugs.java.com/bugdatabase/view_bug.do?bug_id=4827490
		c.setTimeZone(tz);
		return c.getTime();
	}
	
	public void setTime(Date date) {
		Calendar c = new GregorianCalendar();
		c.setTime(date);
		hours = c.get(Calendar.HOUR_OF_DAY);
		minutes = c.get(Calendar.MINUTE);
		seconds = c.get(Calendar.SECOND);
		tz = c.getTimeZone();
	}

	@Override
	public Date getValue() {
		return getTime();
	}

	@Override
	public void setValue(Date value) {
		setTime(value);
	}

	@Override
	public void fromString(String value) {
		try {
			setValue(new SimpleDateFormat(TIME_PATTERN).parse(value));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public String toString() {
		Date d = getValue();
		if (d == null) {
			return "null";
		}
		return new SimpleDateFormat(TIME_PATTERN).format(d);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + hours;
		result = prime * result + minutes;
		result = prime * result + seconds;
		result = prime * result + ((tz == null) ? 0 : tz.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TimeState other = (TimeState) obj;
		if (hours != other.hours)
			return false;
		if (minutes != other.minutes)
			return false;
		if (seconds != other.seconds)
			return false;
		if (tz == null) {
			if (other.tz != null)
				return false;
		} else if (!tz.equals(other.tz))
			return false;
		return true;
	}
	
	
	public static void main(String[] args) {
		System.out.println(new TimeState(23, 10, 00, getTimeZoneValue()).toString());
	}
	
	protected static TimeZone getTimeZoneValue() {
		String value = "001";
		String id;
		if ("0".equals(value.charAt(0))) {
			id = "GMT+" + value.substring(1)+":00";
		} else { 
			id = "GMT-"  + value.substring(1)+":00";
		}
		System.out.println(id);
		System.out.println(TimeZone.getTimeZone(id).getID());
		return TimeZone.getTimeZone(id);
	}
}
