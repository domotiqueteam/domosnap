package com.domosnap.engine.controller.what;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.lang.reflect.InvocationTargetException;
import java.util.Map.Entry;
import java.util.Set;

import com.domosnap.engine.controller.what.impl.BooleanState;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class WhatJsonCodec {

	// TODO improve... for exemple: {key=value} more than {name=key, value=value}
	public static String toJSon(What what) {
		return new StringBuilder("{\"").append(what.getName()).append("\":\"").append(String.valueOf(what.getValue())).append("\",\"type\":\"").append(what.getValue() == null ? "" : what.getValue().getClass().getSimpleName()).append("\"}").toString();
	}
	
	public static What fromJson(String json) {
		JsonParser jp = new JsonParser();
		JsonObject je = jp.parse(json).getAsJsonObject();
		
		Set<Entry<String, JsonElement>> properties = je.entrySet();

		String name = null; // = je.get("name").getAsString();
		String value = null; // = je.get("value").getAsString();
		String classs = null; // = je.get("type").getAsString();

		if( 2 != properties.size()) {
//			log.severe("what not goof...");
		}
		
		for (Entry<String, JsonElement> entry : properties) {
			String key = entry.getKey();
			if ("type".equals(key)) {
				classs = entry.getValue().getAsString();
			} else {
				name = key;
				value = entry.getValue().getAsString();
			}
		}
		
		
		if ("null".equals(value)) {
			return new What (name, null);
		}
		
		State<?> state;
		try {
			Class<?> claname =  Class.forName(BooleanState.class.getPackage().getName() + "." + classs);
			
			if (claname.equals(BooleanState.class)) {
				state = BooleanState.TRUE.toString().equals(value) ?  BooleanState.TRUE : BooleanState.FALSE;
			} else {
				state =(State<?>) claname.getConstructor(String.class).newInstance(value);
			}
			
			return new What(name, state);

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
