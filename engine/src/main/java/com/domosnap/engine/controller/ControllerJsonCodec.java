package com.domosnap.engine.controller;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.Map.Entry;

import com.domosnap.engine.controller.what.State;
import com.domosnap.engine.controller.where.Where;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ControllerJsonCodec {

	public static final String JSON_TITLE = "title";
	public static final String JSON_STATES = "states";
	public static final String JSON_WHERE = "where";
	public static final String JSON_WHO = "who";
	public static final String JSON_DESCRIPTION = "description";
	
	public static String toJson(Controller controller) {

		StringBuffer sb = new StringBuffer("{");
		
		sb.append("\"").append(JSON_WHO).append("\":\"").append(controller.getClass().getName()).append("\"");
		sb.append(",\"").append(JSON_TITLE).append("\":\"").append(controller.getTitle()).append("\"");
		sb.append(",\"").append(JSON_DESCRIPTION).append("\":\"").append(controller.getDescription()).append("\"");

		if (controller.getWhere() != null) {
			sb.append(",\"").append(JSON_WHERE).append("\":\"").append(controller.getId()).append("\"");
		}

		sb.append(",\"").append(JSON_STATES).append("\":{");
		if (!controller.whatList.isEmpty()) {
			int i = 0;
			for (Entry<String, State<?>> entry : controller.whatList.entrySet()) {
				State<?> state = entry.getValue();
				String name = entry.getKey();
				if (i++ != 0) {
					sb.append(",");
				}
				sb.append("\"").append(name).append("\":")
						.append(state == null ? "null" : "\"" + state.toString() + "\"");
			}
		}

		sb.append("}}");
		return sb.toString();
	}

	public static Controller fromJson(Controller controller, String json) {
		
		JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
		
		if (jsonObject.has(JSON_TITLE))
			controller.setTitle(jsonObject.get(JSON_TITLE).getAsString());
	
		if (jsonObject.has(JSON_DESCRIPTION))
			controller.setDescription(jsonObject.get(JSON_DESCRIPTION).getAsString());
			
	
		if (jsonObject.has(JSON_WHERE)) {
			String to = jsonObject.get(JSON_WHERE).getAsString();
			if (!"null".equals(String.valueOf(to))) {
				controller.setWhere(new Where(to));
			}
		}
	
//		if (jsonObject.has(JSON_STATES)) {
//			JsonObject states = jsonObject.get(JSON_STATES).getAsJsonObject();
//			for (final Entry<String, JsonElement> entry: states.entrySet()) {
//				String value = entry.getValue().getAsString();
//				State<?> state = controller.get(entry.getKey());
//				state.fromString(value);
//				
//			}
//		}
		
		return controller;
	}
}
