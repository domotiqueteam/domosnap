package com.domosnap.engine.controller;

import com.domosnap.engine.adapter.core.Command;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.controller.what.State;

import io.reactivex.Flowable;
import io.reactivex.functions.Consumer;

public class ControllerHelper {

	private Controller c;
	
	public ControllerHelper(Controller c) {
		this.c=c;
	}
	
	public void set(String name, State<?> status) {
		c.set(name, status);
	}

	@Deprecated
	public void setState(String name, String value) {
		
		// FIXME: pb here => we update the instance of status 
		// and update with the same status => if there is a changelistener
		// the old and new status will be the same even if value is different....cheatttsafsadhafl!
		State<?> state = c.get(name);
		state.fromString(value); // TODO pb with BooleanState...
		c.set(name, state);
		 
	}
	
	public void addCommandProvider(Flowable<Command> commandFlowable) {
		c.addCommandProvider(commandFlowable);
	}
	
	public void addCommandConsumer(Consumer<Command> commandListener) {
		c.addCommandConsumer(commandListener);
	}
	
	public void removeCommandConsumer(Consumer<Command> commandListener) {
		c.removeCommandConsumer(commandListener);
	}
	
	public void pushCommand(Command command) {
		
		c.pushCommand(command);
	}	
}
