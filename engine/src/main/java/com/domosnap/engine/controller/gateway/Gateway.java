package com.domosnap.engine.controller.gateway;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.what.impl.DateState;
import com.domosnap.engine.controller.what.impl.IpAddressState;
import com.domosnap.engine.controller.what.impl.StringState;
import com.domosnap.engine.controller.what.impl.VersionState;


public class Gateway extends Controller {

	/** uuid */
	private static final long serialVersionUID = 1L;

	public Date getDate() {
		return ((DateState) get(GatewayStateName.date.name())).getValue();
	}

	public void setDate(Date newDate) {
		set(GatewayStateName.date.name(), new DateState(newDate));
	}
	
	public InetAddress getIpAddress() {
		return ((IpAddressState) get(GatewayStateName.ip_address.name())).getValue();
	}

	public InetAddress getNetMask() {
		return ((IpAddressState) get(GatewayStateName.netmask.name())).getValue();
	}

	public String getDeviceType() {
		// TODO Protect if get return null...
		return ((StringState) get(GatewayStateName.model.name())).getValue();
	}

	public VersionState getFirmwareVersion() {
		return (VersionState) get(GatewayStateName.firmware_version.name());
	}

	public DateState getUpTime() {
		return (DateState) get(GatewayStateName.uptime.name());
	}

	public VersionState getKernelVersion() {
		return (VersionState) get(GatewayStateName.kernel_version.name());
	}

	public VersionState getDistributionVersion() {
		return (VersionState) get(GatewayStateName.distribution_version.name());
	}

	@Override
	public List<String> getStateList() {
		List<String> result = new ArrayList<String>();
		GatewayStateName[] list = GatewayStateName.values();
		for (int i = 0; i < list.length; i++) {
			result.add(list[i].name());
		};
		return result;
	}
}
