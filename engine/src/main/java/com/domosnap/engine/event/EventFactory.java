package com.domosnap.engine.event;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.Log;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.Command.Type;
import com.domosnap.engine.controller.light.LightStateName;
import com.domosnap.engine.controller.shutter.Shutter;
import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.controller.what.impl.OnOffState;
import com.domosnap.engine.controller.where.Where;


public class EventFactory {

	private static Log log = new Log(EventFactory.class.getSimpleName());
	private static List<Consumer<Event>> cloudServices = new ArrayList<>(); 
	private static String channel = "channel-" + UUID.randomUUID().toString();
	private static ExecutorService e = Executors.newFixedThreadPool(10);

	public static void addConsumer(Consumer<Event> consumer) {
		EventFactory.cloudServices.add(consumer);
	}

	public static void removeConsumer(Consumer<Event> consumer) {
		EventFactory.cloudServices.remove(consumer);
	}
	
	public static void setChannel(String channel) {
		EventFactory.channel = channel; // TODO move in consumer??? or keep here as main channel?
	}

	public static String getChannel() {
		return EventFactory.channel;
	}
	
	/**
	 * Call the web service to harvest all event. 
	 * @param origin 
	 * @param command
	 * @return The response from the server => check API. If cloud services are disactivated, return is null.
	 */
	public static Future<Boolean> SendEvent(final Log.Session origin, final Command command) {
		if (cloudServices.size() == 0) {
			log.finest(origin, "No Consumer<Event> activated: Event not send.");
			return null;	
		}
		
		// Check
		if (command == null) {
			log.finest(origin, "Null command to send. Abort.");
			return null;
		}

		// Callable
//		FutureTask<Boolean> f = new FutureTask<Boolean>(new Callable<Boolean>() {
//
//			@Override
//			public Boolean call() throws Exception {
				
				Event event = new Event(origin, command);
		
				for (Consumer<Event> consumer : cloudServices) {
					try {
						consumer.accept(event);
						log.finest(origin, "Event processed [" + event + "]");
					} catch (Exception e) {
						log.severe(origin, "Event not send [" + event + "] - " + e.getMessage());
					}
				}
//				return true;
//			}
//		});
//		e.execute(f);
//		return f;
			return null;
	}
	

	public static void main(String[] args) {
		channel = "Test";
		EventToHttpConsumer cons = new EventToHttpConsumer();
		cons.setServerURL("http://52.166.149.166:80");
		cons.setUsername("domosnap");
		cons.setPassword("#h7EPj-zEm^Mu5eEAp6wpXc-_$ey6Y!m");
		addConsumer(cons);
		Future<Boolean> f = SendEvent(Log.Session.Monitor, new Command(Shutter.class,
				new What(LightStateName.status.name(), OnOffState.On()), new Where("openwebnet://13245@localhost:20000/12"), Type.COMMAND, null));
		try {
			f.get(1000, TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
		e.shutdown();
	}
}
