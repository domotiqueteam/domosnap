package com.domosnap.engine.event;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.IOException;
import java.util.function.Consumer;

import com.domosnap.engine.Log;

import okhttp3.Authenticator;
import okhttp3.Credentials;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.Route;

public class EventToHttpConsumer implements Consumer<Event> {

	private static Log log = new Log(EventToHttpConsumer.class.getSimpleName());
	private OkHttpClient client;
	private String serverURL = null;// "http://52.166.149.166:80";
	private String username = null; // "domosnap";
	private String password = null; // "#h7EPj-zEm^Mu5eEAp6wpXc-_$ey6Y!m";

	private int errorCount = 0;
	private int RETRY = 5;
	
	public void setRetry(int retry) {
		this.RETRY = retry;
	}
	
	public void setServerURL(String serverURL) {
		this.serverURL = serverURL;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public void accept(Event event) {

		do {
			try {
				RequestBody body = RequestBody.create(MediaType.parse("json/application"), event.toString());
				Request request = new Request.Builder().url(serverURL).post(body).build();
				Response response = getClient().newCall(request).execute();

				log.finest(event.getOrigin(), "Event send [" + event + "]");

				if (response.isSuccessful()) {
					log.finest(event.getOrigin(), "Response [" + response.body().string() + "]");
				} else {
					log.severe(event.getOrigin(),
							"Reponse unexpected (code: " + response.code() + ") [" + response.body().string() + "]"
									+ (errorCount == 0 ? ". Retry will be done." : ". Retry[" + errorCount + "]"));
					errorCount++;
				}

			} catch (Exception e) {
				log.severe(event.getOrigin(), e.getMessage());
				errorCount++;
			}
		} while (errorCount > 0 && errorCount < RETRY);
	}

	private OkHttpClient getClient() {
		if (client == null) {
			if (username != null) {
				client = new OkHttpClient().newBuilder().authenticator(new Authenticator() {

					@Override
					public Request authenticate(Route route, Response response) throws IOException {
						String credential = Credentials.basic(username, password);
						return response.request().newBuilder().header("Authorization", credential).build();
					}
				}).build();
			}
		} else {
			// No security
			client = new OkHttpClient().newBuilder().build();
		}

		return client;
	}

}
