package com.domosnap.engine.event;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.text.MessageFormat;

import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.CommandJsonCodec;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class EventJsonCodec {

	public final static String toJSon(Event event) {

		return MessageFormat.format(
				"'{'\"origin\":\"{0}\",\"command\":{1}'}'",
				event.getOrigin() == null ? Session.Monitor.name() : event.getOrigin().name(),
				event.getCommand() == null ? "" : event.getCommand().toString());

	}
	
	public static Event fromJson(String json) {
		JsonParser jp = new JsonParser();
		JsonObject je = jp.parse(json).getAsJsonObject();
		
		Session origin = Session.valueOf(je.get("origin").getAsString());
		Command command = CommandJsonCodec.fromJson(je.get("command").getAsJsonObject().toString());

		return new Event(origin, command);
	}
}
