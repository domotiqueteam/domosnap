package com.domosnap.engine.event;

/*
 * #%L
 * DomoSnapEngine
 * %%
 * Copyright (C) 2011 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;

import com.domosnap.engine.Log;
import com.domosnap.engine.adapter.core.Command;

public class Event implements Serializable {

	private static final long serialVersionUID = 1L;
	private Log.Session origin;
	private Command command;

	public Event(Log.Session origin, Command command) {
		this.origin = origin;
		this.command = command;
	}

	@Override
	public String toString() {
		return EventJsonCodec.toJSon(this);
	}
	
	public Log.Session getOrigin() {
		return origin;
	}

	public void setOrigin(Log.Session origin) {
		this.origin = origin;
	}

	public Command getCommand() {
		return command;
	}

	public void setCommand(Command command) {
		this.command = command;
	}
}
