package com.domosnap.crypt.home;

/*
 * #%L
 * DomoSnap Crypt Home project
 * %%
 * Copyright (C) 2018 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.CommandJsonCodec;
import com.domosnap.engine.adapter.core.ScanListener;
import com.domosnap.engine.adapter.impl.openwebnet.OpenWebNetControllerAdapter;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.ControllerHelper;

import io.reactivex.functions.Consumer;
import io.reactivex.processors.PublishProcessor;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;

public class KafkaHomeServer extends AbstractVerticle {

	private Log log = new Log(KafkaHomeServer.class.getName());

	protected static transient PublishProcessor<Command> toHome; // Stream to receive command / In domosnap
	protected static transient Consumer<Command> toServer; // Stream to send command / Out domosnap
	
	public void updateController(Controller controller, String channelIn, String channelOut) {
		if (controller != null) {
			// Init controller and add decorator to send/received command to 
			new ControllerHelper(controller).addCommandProvider(getToHome(channelIn));
			new ControllerHelper(controller).addCommandConsumer(getToServer(channelOut));
		}
	}

	KafkaProducer<String, String> producer;
	KafkaConsumer<String, String> consumer;
	
	
	private PublishProcessor<Command> getToHome(String channelIn){
		if (toHome == null) {
			toHome = PublishProcessor.create();
			String host = "localhost"; // "localhost:9092"
			int port = 9092;
			List<String> topicsFromServer = Arrays.asList(channelIn);
	        Properties props2 = new Properties();
	        props2.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "PLAINTEXT://" + host + ":" + port);
	        props2.put(ConsumerConfig.GROUP_ID_CONFIG, "test"); // partie d'un groupe de lecteur
	        props2.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");
	        props2.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
	        props2.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
	        props2.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
	        consumer = new KafkaConsumer<>(props2); // connect to server to get message from monitor and push to Home server
	        consumer.subscribe(topicsFromServer);

	        vertx.setTimer(1000, new Handler<Long>() {
				@Override
				public void handle(Long event) {
					ConsumerRecords<String, String> records = consumer.poll(100);
		            for (ConsumerRecord<String, String> record : records) {
		                System.out.printf("offset = %d, key = %s, value = %s%n", record.offset(), record.key(), record.value());
		        		toHome.offer(
		        			CommandJsonCodec.fromJson(record.value()) //deserialize command
		        		);
		        	}	
				}
			});
		}
		return toHome;
	}
	
	private Consumer<Command> getToServer(String channelOut) {
		if (toServer == null) {
			String host = "localhost"; // "localhost:9092"
			int port = 9092;
			List<String> topicsToServer = Arrays.asList(channelOut);
			Map<String, Object> props = new HashMap<String, Object>();
			props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "PLAINTEXT://" + host + ":" + port);
			props.put(ProducerConfig.ACKS_CONFIG, "all");
			props.put(ProducerConfig.RETRIES_CONFIG, "0");
			props.put(ProducerConfig.BATCH_SIZE_CONFIG, "100");
			props.put(ProducerConfig.LINGER_MS_CONFIG, "1");
			props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, "33554432");
			props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
					"org.apache.kafka.common.serialization.StringSerializer");
			props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
					"org.apache.kafka.common.serialization.StringSerializer");
		
			producer = new KafkaProducer<String, String>(props); // Connect to home monitor to get message and send to server

			toServer = new Consumer<Command>() {
				@Override
				public void accept(Command command) {
					// send vers le serveur	
					for (String topic : topicsToServer) {
						ProducerRecord<String, String> record = new ProducerRecord<String, String>(topic, UUID.randomUUID().toString(), CommandJsonCodec.toJSon(command));
						producer.send(record, new Callback() {
							
							@Override
							public void onCompletion(RecordMetadata metadata, Exception exception) {
								if (exception != null) {
									exception.printStackTrace();
								} else {
									log.finest(Session.Command, "Command send to Kafka with success [" + command + "]");
								}
								
							}
						});
					}	
				}
			};
		}
		return toServer;
	}
	
	
	@Override
	public void start() {
		
		// init flux avec le getController
//		=> il faut récupérer le flux monitor et Command.Type.. = pour cela c'est l'ControllerAdapter 	
		
		
		OpenWebNetControllerAdapter own = new OpenWebNetControllerAdapter("s");
		own.scan(new ScanListener() {
			
			@Override
			public void scanFinished() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void progess(int percent) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void foundController(Controller controller) {
				updateController(controller, "channelIn", "channelOut");
			}
		});
	}
	
	@Override
	public void stop() throws Exception {
		super.stop();
		if (producer != null)
			producer.close();
		if (consumer != null) 
			consumer.close();
		System.out.println("Home Server Service Stopped");
	}
	
	public static void main(String[] args) {
		io.vertx.core.Vertx vertx = io.vertx.core.Vertx.vertx();
		vertx.deployVerticle(KafkaHomeServer.class.getName());
	}

}
//    =======> consumer ======> 
//  C                           gateway
//    <======= monitor  <======
//    
//    
//    =======> webconsummer ======>         <=========        =======>      ======> consumer  ======>
//  C                                server            maison            C?                            gateway
//    <======= webmonitor   <======         <=========        <=======      <====== monitor   <======


//    =======> kafkaConsummer ======>         <=========        =======>      ======> consumer  ======>
//  C                                  Kafka            KafkaHome         C?                            gateway
//    <======= kafkaMonitor   <======         <=========        <=======      <====== monitor   <======
