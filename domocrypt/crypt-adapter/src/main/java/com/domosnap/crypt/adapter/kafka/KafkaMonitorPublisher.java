package com.domosnap.crypt.adapter.kafka;

import java.io.UnsupportedEncodingException;
import java.net.URI;

/*
 * #%L
 * DomoSnap Crypt Adapter project
 * %%
 * Copyright (C) 2018 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.CommandJsonCodec;
import com.domosnap.engine.adapter.core.ConnectionListener;
import com.domosnap.engine.adapter.core.ConnectionStatusEnum;
import com.domosnap.engine.adapter.core.URIQueryTools;
import com.domosnap.engine.adapter.core.UnknownControllerListener;

import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;

public class KafkaMonitorPublisher implements FlowableOnSubscribe<Command> {

	private List<FlowableEmitter<Command>> suscriberList = Collections.synchronizedList(new ArrayList<FlowableEmitter<Command>>());
	protected final Log log = new Log(this.getClass().getSimpleName());
	private boolean connected = false;
	private Properties props = null;
	private Collection<String> topics;
	private List<ConnectionListener> connectionListenerList = Collections.synchronizedList(new ArrayList<ConnectionListener>());
	
	@Override
	public void subscribe(FlowableEmitter<Command> e) throws Exception {
		suscriberList.add(e);
	}

	protected ExecutorService getExecutorService() {
		return Executors.newSingleThreadExecutor();
	}
	
	public void connect(URI uri) {
		
		String host = uri.getHost(); // "localhost:9092"
		int port = uri.getPort();
		try {
			Map<String, List<String>> queyParams = URIQueryTools.splitQuery(uri);
			topics = queyParams.get("topics");
			if (topics == null) {
				topics = Arrays.asList("test");
			}
			
			// TODO add other parameter
			props  = new Properties();
	        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, host + ":" + port);
	        props.put(ConsumerConfig.GROUP_ID_CONFIG, "test");
	        props.put("enable.auto.commit", "true");
	        props.put("auto.commit.interval.ms", "1000");
	        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
	        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
	        
	        getExecutorService().execute(new MonitorHandler());
	        
	        for (ConnectionListener connectionListener : connectionListenerList) {
				try {
					connectionListener.onConnect(ConnectionStatusEnum.Connected);
				} catch (Exception e) {
					log.severe(Session.Command, "ConnectionListener raise an error [" + e.getMessage() +"]");
				}
			}
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void disconnect() {
		props = null;
		
		for (ConnectionListener connectionListener : connectionListenerList) {
			try {
				connectionListener.onClose();
			} catch (Exception e) {
				log.severe(Session.Command, "ConnectionListener raise an error [" + e.getMessage() +"]");
			}
		}
		
		log.fine(Session.Command, "End connection: Command session closed...");
	}
	
	public boolean isConnected() {
		return props != null;
	}

	public void addConnectionListener(ConnectionListener connectionListener) {
		if (!connectionListenerList.contains(connectionListener)) {
			connectionListenerList.add(connectionListener);
		}
	}

	public void removeConnectionListener(ConnectionListener connectionListener) {
		connectionListenerList.remove(connectionListener);
	}

	public void addUnknownControllerListener(UnknownControllerListener arg0) {
		// TODO Auto-generated method stub
		
	}

	public void removeUnknownControllerListener(UnknownControllerListener arg0) {
		// TODO Auto-generated method stub
		
	}
	
	private class MonitorHandler implements Runnable {

		private KafkaConsumer<String, String> consumer;
		
		@Override
		public void run() {
        	// connect to server to get message from monitor
            consumer = new KafkaConsumer<>(props);
            consumer.subscribe(topics);
            while (connected) {
                ConsumerRecords<String, String> records = consumer.poll(100);
                for (ConsumerRecord<String, String> record : records) {
                    System.out.printf("offset = %d, key = %s, value = %s%n", record.offset(), record.key(), record.value());
                	onMessageReceipt(record.value());
            	}
            }
		}
		
		private void onMessageReceipt(String event) {
			try {
				Command command = CommandJsonCodec.fromJson(event);
				for (FlowableEmitter<Command> suscriber : suscriberList) {
					suscriber.onNext(command);
				}
			} catch (Exception e) {
				log.severe(Session.Command, "Exception occurs with message ["
						+ String.valueOf(event) + "]. Message dropped. " + e.getMessage());
			}
		}
	}
}
