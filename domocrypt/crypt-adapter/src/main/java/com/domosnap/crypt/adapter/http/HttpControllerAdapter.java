package com.domosnap.crypt.adapter.http;

import java.net.URI;

/*
 * #%L
 * DomoSnap Crypt Adapter project
 * %%
 * Copyright (C) 2018 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.core.AbstractControllerAdapter;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.ConnectionListener;
import com.domosnap.engine.adapter.core.ScanListener;
import com.domosnap.engine.adapter.core.UnknownControllerListener;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.temperature.TemperatureSensor;
import com.domosnap.engine.controller.where.Where;
import com.domosnap.engine.event.EventFactory;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.functions.Consumer;
import io.reactivex.processors.FlowableProcessor;
import io.reactivex.processors.PublishProcessor;

public class HttpControllerAdapter extends AbstractControllerAdapter {

	private Log log = new Log(HttpControllerAdapter.class.getSimpleName());
	
	private HttpMonitorPublisher monitorPublisher;
	private HttpCommanderConsumer commanderConsumer;
	
	public HttpControllerAdapter(String url) {

		this.uri = URI.create(url);
		
		monitor = createMonitorStream();
		monitor.subscribe(new Consumer<Command>() {
			
			@Override
			public void accept(Command t) throws Exception {
				EventFactory.SendEvent(Session.Monitor, t);		
			}
		});
		log.finest(Session.Monitor, "Monitor created.");
		
		commander = createCommandStream();
		
		PublishProcessor<Command> commanderStream = PublishProcessor.create();
		commanderStream.subscribe(commander);

		log.finest(Session.Command, "Commander created.");
	}
		
	
	@Override
	public void connect() {
		connect(this.uri.toString());
	}

	@Override
	public void connect(String url) {
		this.uri = URI.create(url);
		if (!monitorPublisher.isConnected()) {
			monitorPublisher.connect(url);
		}
		if (!commanderConsumer.isConnected()) {
			commanderConsumer.connect(url);
		}
	}
	
	@Override
	public void disconnect() {
		monitorPublisher.disconnect();
		commanderConsumer.disconnect();
	}

	@Override
	public boolean isConnected() {
		return monitorPublisher.isConnected() && commanderConsumer.isConnected();
	}

	@Override
	public void addCommanderConnectionListener(ConnectionListener listener) {
		// TODO
	}

	@Override
	public void removeCommanderConnectionListener(ConnectionListener listener) {
		// TODO
	}
	
	@Override
	public void addMonitorConnectionListener(ConnectionListener listener) {
		monitorPublisher.addConnectionListener(listener);
	}

	@Override
	public void removeMonitorConnectionListener(ConnectionListener listener) {
		monitorPublisher.removeConnectionListener(listener);
	}

	@Override
	public void addUnknowControllerListener(UnknownControllerListener arg0) {
		monitorPublisher.addUnknownControllerListener(arg0);
	}
	
	@Override
	public void removeUnknowControllerListener(UnknownControllerListener arg0) {
		monitorPublisher.removeUnknownControllerListener(arg0);
	}

	@Override
	public void scan(ScanListener listener) {

		// récuprer de la maison????  bo...

	}
	
	// TODO override => put in parent
	public List<Class<? extends Controller>> getSupportedDevice() {
		List<Class<? extends Controller>> result = new ArrayList<Class<? extends Controller>>();
		result.add(TemperatureSensor.class);
		
		return result;
	}
	
	@Override
	protected Flowable<Command> createMonitorStream() {
		monitorPublisher = new HttpMonitorPublisher();
		return FlowableProcessor.create(monitorPublisher, BackpressureStrategy.DROP).share();
	}

	@Override
	protected PublishProcessor<Command> createCommandStream() {
		PublishProcessor<Command> commanderStream = PublishProcessor.create();
		commanderConsumer = new HttpCommanderConsumer();
		commanderStream.subscribe(commanderConsumer);
		return commanderStream;
	}

	@Override
	protected void onCreatedController(Controller c) {
//		monitorPublisher.addKnownDeviceId(c.getWhere().getPath());
	}

	@Override
	protected String formatURI(Where where) {
		String path;
		if (where == null) {
			path = "";
		} else {
			path = where.getPath();
		}
		return path;
	}

	@Override
	public String getSchema() {
		return null;
	}
}
