package com.domosnap.crypt.adapter.http;

import java.io.IOException;

/*
 * #%L
 * DomoSnap Crypt Adapter project
 * %%
 * Copyright (C) 2018 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.core.Command;

import io.reactivex.functions.Consumer;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class HttpCommanderConsumer implements Consumer<Command> {

	protected final Log log = new Log(this.getClass().getSimpleName());
	private int errorCount = 0;
	private int RETRY = 5;
	private String url;
	private OkHttpClient client;

	@Override
	public void accept(Command command) {
		if (command == null) {
			log.severe(Session.Monitor, "Command unsupported (null).");
			return;
		}
		
		// Send to SERVER
		do {
			try {
				
				OkHttpClient client = new OkHttpClient.Builder().build();
				Request r = new Request.Builder().build();
				
				client.newCall(r);
				
				
			} catch (Exception e) {
				log.severe(Session.Command, e.getMessage());
				errorCount++;
			}
		} while (errorCount > 0 && errorCount < RETRY);
		
		if (errorCount == RETRY) {
			log.severe(Session.Command, "Impossible to send command {" + command.toString() + "}.");
		}
	}


	public boolean isConnected() {
		try {
			
			Request r = new Request.Builder().get().url(url).build();
			return client.newCall(r).execute().isSuccessful();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}


	public void connect(String url) {
		this.url = url;
		this.client = new OkHttpClient();
	}


	public void disconnect() {
		this.client = null;
		url = null;
	}
	
	public void setRetry(int retry) {
		this.RETRY = retry;
	}
	

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		disconnect();
	}
}
