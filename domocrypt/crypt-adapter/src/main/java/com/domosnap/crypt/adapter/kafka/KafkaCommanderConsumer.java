package com.domosnap.crypt.adapter.kafka;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

/*
 * #%L
 * DomoSnap Crypt Adapter project
 * %%
 * Copyright (C) 2018 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.CommandJsonCodec;
import com.domosnap.engine.adapter.core.ConnectionListener;
import com.domosnap.engine.adapter.core.ConnectionStatusEnum;
import com.domosnap.engine.adapter.core.URIQueryTools;

import io.reactivex.functions.Consumer;

public class KafkaCommanderConsumer implements Consumer<Command> {

	protected final Log log = new Log(this.getClass().getSimpleName());
	private KafkaProducer<String, String> producer;
	private Collection<String> topics;
	private List<ConnectionListener> connectionListenerList = Collections.synchronizedList(new ArrayList<ConnectionListener>());
       
	@Override
	public void accept(Command command) {
		if (command == null) {
			log.severe(Session.Monitor, "Command unsupported (null).");
			return;
		}
		// Send to SERVER
		for (String topic : topics) {
			ProducerRecord<String, String> record = new ProducerRecord<String, String>(topic, UUID.randomUUID().toString(), CommandJsonCodec.toJSon(command));
			producer.send(record, new Callback() {
				@Override
				public void onCompletion(RecordMetadata metadata, Exception exception) {
					if (exception != null) {
						log.severe(Session.Command, exception.getMessage());
						// disconnect()?
					} else {
						log.finest(Session.Command, "Command send to Kafka with success [" + command + "]");
					}
				}
			});
		}				
	}


	public boolean isConnected() {
		return producer != null;
	}


	public void connect(URI uri) {
		try {
			String host = uri.getHost(); // "localhost:9092"
			int port = uri.getPort();
			Map<String, List<String>> queyParams = URIQueryTools.splitQuery(uri);
			topics = queyParams.get("topics");
			if (topics == null) {
				topics = Arrays.asList("test");
			}
			
			// TODO add parameter
			Map<String, Object> props = new HashMap<String, Object>();
			props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "PLAINTEXT://" + host + ":" + port);
			props.put(ProducerConfig.ACKS_CONFIG, "all");
			props.put(ProducerConfig.RETRIES_CONFIG, "0");
			props.put(ProducerConfig.BATCH_SIZE_CONFIG, "100");
			props.put(ProducerConfig.LINGER_MS_CONFIG, "1");
			props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, "33554432");
			props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
					"org.apache.kafka.common.serialization.StringSerializer");
			props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
					"org.apache.kafka.common.serialization.StringSerializer");
		
			producer = new KafkaProducer<String, String>(props);
			
			for (ConnectionListener connectionListener : connectionListenerList) {
				try {
					connectionListener.onConnect(ConnectionStatusEnum.Connected);
				} catch (Exception e) {
					log.severe(Session.Command, "ConnectionListener raise an error [" + e.getMessage() +"]");
				}
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}


	public void disconnect() {
		producer.close();
		for (ConnectionListener connectionListener : connectionListenerList) {
			try {
				connectionListener.onClose();
			} catch (Exception e) {
				log.severe(Session.Command, "ConnectionListener raise an error [" + e.getMessage() +"]");
			}
		}
		
		log.fine(Session.Command, "End connection: Command session closed...");
	}
	
	public void addConnectionListener(ConnectionListener connectionListener) {
		if (!connectionListenerList.contains(connectionListener)) {
			connectionListenerList.add(connectionListener);
		}
	}

	public void removeConnectionListener(ConnectionListener connectionListener) {
		connectionListenerList.remove(connectionListener);
	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		disconnect();
	}
}
