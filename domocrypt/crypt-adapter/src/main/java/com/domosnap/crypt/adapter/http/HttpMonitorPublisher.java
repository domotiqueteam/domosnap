package com.domosnap.crypt.adapter.http;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

/*
 * #%L
 * DomoSnap Crypt Adapter project
 * %%
 * Copyright (C) 2018 - 2019 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.domosnap.engine.Log;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.CommandJsonCodec;
import com.domosnap.engine.adapter.core.ConnectionListener;
import com.domosnap.engine.adapter.core.UnknownControllerListener;

import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import okhttp3.Request;

public class HttpMonitorPublisher implements FlowableOnSubscribe<Command> {

	private List<FlowableEmitter<Command>> suscriberList = Collections
			.synchronizedList(new ArrayList<FlowableEmitter<Command>>());
	protected final Log log = new Log(this.getClass().getSimpleName());
	private boolean connected = false;
	private String url;
//	private OkHttpClient client;
	com.sun.net.httpserver.HttpServer server;

	private void handleRequest(com.sun.net.httpserver.HttpExchange exchange) throws IOException {

		try {
			
			ByteArrayOutputStream result = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int length;
			while ((length = exchange.getRequestBody().read(buffer)) != -1) {
			    result.write(buffer, 0, length);
			}
			// StandardCharsets.UTF_8.name() > JDK 7
			String body = result.toString("UTF-8");
			
			// deserialied command
			Command command = CommandJsonCodec.fromJson(body);
			
			for (FlowableEmitter<Command> suscriber : suscriberList) {
				suscriber.onNext(command);
			}
			
			exchange.sendResponseHeaders(200, 0);// response code and length
			
		} catch (Exception e) {
//			log.severe(Session.Command, "Exception occurs with message [" + String.valueOf(event)
//					+ "]. Message dropped. " + e.getMessage());
			String response = "Internal error." + e.getMessage();
			exchange.sendResponseHeaders(500, response.getBytes().length);// response code and length
			OutputStream os = exchange.getResponseBody();
			os.write(response.getBytes());
			os.close();
		}
	}

	@Override
	public void subscribe(FlowableEmitter<Command> e) throws Exception {
		suscriberList.add(e);
	}

//	protected ExecutorService getExecutorService() {
//		return Executors.newSingleThreadExecutor();
//	}

	public void connect(String url) {
		try {
			this.server = com.sun.net.httpserver.HttpServer.create(new InetSocketAddress(8500), 0);
			com.sun.net.httpserver.HttpContext context = server.createContext("/");
			context.setHandler(this::handleRequest);
			server.start();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			this.server = null;
		}
	}

	public void disconnect() {
		if (server != null) {
			this.server.stop(0);
			this.server = null;
			url = null;
		}
	}

	public boolean isConnected() {
		return this.server != null;
	}

	public void addConnectionListener(ConnectionListener listener) {
		// TODO Auto-generated method stub

	}

	public void removeConnectionListener(ConnectionListener listener) {
		// TODO Auto-generated method stub

	}

	public void addUnknownControllerListener(UnknownControllerListener arg0) {
		// TODO Auto-generated method stub

	}

	public void removeUnknownControllerListener(UnknownControllerListener arg0) {
		// TODO Auto-generated method stub

	}
}
